<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
date_default_timezone_set('Asia/Jakarta');
Route::get('/', function () {
    return 'We can talk, ... 
    If you are looking for disassembling or test my code, Im afraid it will cost your time because it was very protected code and high security. Need help for your project? We can talk. Just contact me at magicwarms@gmail.com';
});
//ini khusus untuk konfirmasi link
Route::get('confirm_forgot_password/{mail}/{code}', 'API\CustomerController@confirm_forgot_password');
Route::get('confirm_registered_email/{customer_id}', 'API\CustomerController@confirm_registered_email');
Route::post('register', 'API\CustomerController@register');
Route::post('login', 'API\CustomerController@login');
Route::post('forgot_password', 'API\CustomerController@forgot_password');
Route::post('new_password', 'API\CustomerController@new_password');
Route::get('resend_email/{email}', 'API\CustomerController@resend_email');

Route::group(['middleware' => 'check_api_key'], function(){

	Route::prefix('product')->group(function () {
		Route::get('list/{home?}/{new?}/{hot?}/{featured?}/{promo?}/{restock?}/{old?}', 'API\ProductController@list_products');
		Route::get('hot', 'API\ProductController@list_products_hot');
		Route::get('promo', 'API\ProductController@list_products_promo');
		Route::get('restock', 'API\ProductController@list_products_restock');
		Route::get('detail/{id?}', 'API\ProductController@detail_product');
		Route::get('categories/{id?}', 'API\ProductController@get_product_by_category');
		Route::get('/show_comment_product/{product_id}', 'API\CommentController@show_comment_product');
		Route::get('searching', 'API\ProductController@search_product');
	});

	Route::get('categories', 'API\CategoriesController@list_categories');
	Route::get('customer_service', 'API\CustomerServiceController@list_customer_service');
	Route::get('ekspedisi', 'API\EkspedisiController@list_ekspedisi');
	Route::get('province/{id?}', 'API\EkspedisiController@list_province');
	Route::get('get_city/{city_id?}/{province_id?}', 'API\EkspedisiController@get_city_by_province');
	Route::get('cost/{origin}/{destination}/{weight}/{courier}', 'API\EkspedisiController@get_cost_ekspedition');
	Route::get('resi/{resi}/{courier}', 'API\EkspedisiController@check_waybill');
	Route::get('get_subdistrict/{city_id}', 'API\EkspedisiController@get_subdistrict_by_city');
	Route::get('bank_account_list', 'API\BankAccountController@list_bank_account');

	Route::prefix('banner')->group(function () {
		Route::get('list', 'API\BannerController@list_banner');
	});
});

Route::group(['middleware' => 'auth:api'], function(){
	//customer api
	Route::prefix('customer')->group(function () {
		Route::post('refresh_token', 'API\CustomerController@refresh_token');
		Route::get('details', 'API\CustomerController@details');
		Route::post('save_deposit', 'API\CustomerController@save_deposit');
		Route::get('list_deposit', 'API\CustomerController@list_deposit');
		Route::post('upload_deposit', 'API\CustomerController@evidence_deposit');
		Route::get('logout', 'API\CustomerController@logout');
		Route::post('update', 'API\CustomerController@update_profile_customer');
		Route::post('update_profile_photo', 'API\CustomerController@update_profile_photo');
		Route::put('change_password', 'API\CustomerController@change_password');
		Route::put('update_deposit', 'API\CustomerController@update_deposit_customer');
		Route::get('list_upgrade_account', 'API\CustomerController@list_upgrade_account');
		Route::get('upgrade_account', 'API\CustomerController@upgrade_account');
		Route::post('confirm_upgrade_account', 'API\CustomerController@confirm_upgrade_account');
	});
	//wishlist customer api
	Route::prefix('wishlist')->group(function () {
		Route::get('list', 'API\WishlistController@list_wishlist');
		Route::post('save_wishlist', 'API\WishlistController@save_wishlist');
		Route::delete('delete_wishlist', 'API\WishlistController@delete_wishlist');
	});
	//cart product api
	Route::prefix('cart')->group(function () {
		Route::get('list', 'API\CartController@list_cart');
		Route::post('save_cart', 'API\CartController@save_cart');
		Route::delete('delete_cart', 'API\CartController@delete_cart');
		Route::delete('delete_cart_by_customer', 'API\CartController@delete_cart_by_customer');
		Route::put('update_qty_cart', 'API\CartController@update_qty_cart');
	});

	//rating product api
	Route::prefix('rating')->group(function () {
		Route::post('save_rating', 'API\RatingController@save_rating');
		Route::put('update_rating', 'API\RatingController@update_rating');
		Route::get('is_rated/{product_id}/{customer_id}', 'API\RatingController@is_rated');
	});

	//comment api
	Route::prefix('comment')->group(function () {
		Route::post('/save_comment', 'API\CommentController@save_comment');
	});

	//order api
	Route::prefix('order')->group(function () {
		Route::post('/save_order', 'API\OrderController@save_order');
		Route::get('/history_order', 'API\OrderController@history_order');
		Route::post('/confirmation_order', 'API\OrderController@confirmation_order');
		Route::post('/cancel_order', 'API\OrderController@cancel_order_by_customer');
		Route::get('/fetch_order_code', 'API\OrderController@fetch_order_code');
		Route::post('/save_ekspedition', 'API\OrderController@save_ekspedition');
	});

	Route::post('/save_return_product', 'API\ProductController@save_return_product');

	//withdrawal api
	Route::prefix('withdrawal')->group(function () {
		Route::post('/', 'API\WithdrawalDepositController@withdrawal');
		Route::get('/list', 'API\WithdrawalDepositController@list_withdrawal');
	});

	//product api route with optional parameter
	// Route::get('products/{hot?}/{featured?}', 'API\ProductController@list_products');
});
