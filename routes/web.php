<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
date_default_timezone_set('Asia/Jakarta');

Route::get('/', 'LoginController@index')->name('login')->middleware('guest');
Route::post('/login/process_login','LoginController@process_login')->middleware('guest')->name('user.login');

Route::group(['middleware' => ['auth','check_access_menu']], function () {
	//common routes
	Route::get('/beranda', 'Administrator\DashboardController@home')->name('beranda');
	Route::post('/logout', 'LoginController@logout')->name('logout');
	//menu admin routes
	Route::prefix('menu')->group(function () {
		Route::get('/', 'Administrator\MenuController@index_menu')->name('menu');
		Route::post('/show', 'Administrator\MenuController@show_menu')->name('menu.show');
		Route::post('/save', 'Administrator\MenuController@save_menu')->name('menu.store');
		Route::delete('/delete', 'Administrator\MenuController@delete_menu')->name('menu.delete');
		Route::get('/edit/{menu}', 'Administrator\MenuController@fetch_data_menu');
		Route::put('/update', 'Administrator\MenuController@update_menu')->name('menu.update');
	});
	//user admin routes
	Route::prefix('user')->group(function () {
		Route::get('/', 'Administrator\UserController@index_user')->name('user');
		Route::post('/save', 'Administrator\UserController@save_user')->name('user.store');
		Route::delete('/{user}/delete', 'Administrator\UserController@delete_user')->name('user.delete');
		Route::get('/{user}/edit', 'Administrator\UserController@index_user')->name('user.edit');
		Route::patch('/{user}/update', 'Administrator\UserController@update_user')->name('user.update');
		Route::post('/change_password', 'Administrator\UserController@change_password_user')->name('user.change.password');
	});
	//customer routes
	Route::prefix('customer')->group(function () {
		Route::get('/', 'Administrator\CustomerController@index_customer')->name('customer');
		Route::get('/change_status_customer/{customer_id}/{customer_status}', 'Administrator\CustomerController@change_status_customer')->name('customer.change_status_customer');
		Route::get('/change_cs_customer/{customer_id}/{customer_cs}', 'Administrator\CustomerController@change_cs_customer')->name('customer.change_cs_customer');
		Route::get('/detail/{customer_id}/{customer_name}', 'Administrator\CustomerController@detail_customer')->name('customer.detail');
		Route::post('/get_deposit_customer/{customer_id}', 'Administrator\CustomerController@show_deposit_customer')->name('customer.show_deposit_customer');
		Route::get('/searching', 'Administrator\CustomerController@search_customer')->name('customer.searching');
		Route::get('/edit/{customer_id}', 'Administrator\CustomerController@fetch_data_cutomer');
		Route::put('/change_acc_type_customer', 'Administrator\CustomerController@change_acc_type_customer')->name('customer.change_acc_type_customer');
		Route::put('/change_cs_customer', 'Administrator\CustomerController@change_cs_customer')->name('customer.change_cs_customer');
		Route::post('/filter', 'Administrator\CustomerController@filter_customer')->name('customer.filter');
		Route::delete('/delete', 'Administrator\CustomerController@delete_customer')->name('customer.delete');
	});
	//deposit routes
	Route::prefix('deposit')->group(function () {
		Route::get('/', 'Administrator\DepositController@index_deposit')->name('deposit');
		Route::post('/show/{start_date}/{end_date}/{status}', 'Administrator\DepositController@show_deposit')->name('deposit.show');
		Route::get('/detail_deposit/{deposit_id}', 'Administrator\DepositController@fetch_data_deposit');
		Route::put('/verify_deposit', 'Administrator\DepositController@verify_deposit')->name('deposit.verify');
	});
	//product routes
	Route::prefix('product')->group(function () {
		Route::get('/', 'Administrator\ProductController@index_product')->name('product');
		Route::post('/show', 'Administrator\ProductController@show_product')->name('product.show');
		Route::post('/save', 'Administrator\ProductController@save_product')->name('product.store');
		Route::delete('/delete', 'Administrator\ProductController@delete_product')->name('product.delete');
		Route::get('/edit/{product}/{product_name}', 'Administrator\ProductController@fetch_data_product');
		Route::post('/update', 'Administrator\ProductController@update_product')->name('product.update');
		Route::get('/delete_picture_product/{product}/{filename}', 'Administrator\ProductController@delete_picture_product')->name('product.delete_picture_product');
		Route::get('/detail/{product}/{product_name}', 'Administrator\ProductController@detail_product')->name('product.detail');
		Route::get('/delete_color_more/{more_detail_id?}', 'Administrator\ProductController@delete_color_more_detail');
		Route::get('/delete_color_more_sepatu/{more_detail_id?}', 'Administrator\ProductController@delete_color_more_detail_sepatu');
	});
	//return product routes
	Route::prefix('return_product')->group(function () {
		Route::get('/', 'Administrator\ReturnProductController@index_return')->name('return_product');
		Route::post('/show', 'Administrator\ReturnProductController@show_return')->name('return_product.show');
		Route::delete('/delete', 'Administrator\ReturnProductController@delete_return')->name('return_product.delete');
		Route::get('/edit/{return_product_id}', 'Administrator\ReturnProductController@fetch_data_return');
		Route::put('/update', 'Administrator\ReturnProductController@update_return')->name('return_product.update');
	});
	//category product routes
	Route::prefix('category')->group(function () {
		Route::get('/', 'Administrator\CategoriesController@index_categories')->name('category');
		Route::post('/show', 'Administrator\CategoriesController@show_categories')->name('category.show');
		Route::post('/save', 'Administrator\CategoriesController@save_categories')->name('category.store');
		Route::delete('/delete', 'Administrator\CategoriesController@delete_categories')->name('category.delete');
		Route::get('/edit/{category}', 'Administrator\CategoriesController@fetch_data_categories');
		Route::post('/update', 'Administrator\CategoriesController@update_categories')->name('category.update');
		Route::get('/delete_picture_category/{category}', 'Administrator\CategoriesController@delete_picture_category')->name('category.delete_picture_category');
	});

	//ekspedisi routes
	Route::prefix('ekspedisi')->group(function () {
		Route::get('/', 'Administrator\EkspedisiController@index_ekspedisi')->name('ekspedisi');
		Route::post('/show', 'Administrator\EkspedisiController@show_ekspedisi')->name('ekspedisi.show');
		Route::post('/save', 'Administrator\EkspedisiController@save_ekspedisi')->name('ekspedisi.store');
		Route::delete('/delete', 'Administrator\EkspedisiController@delete_ekspedisi')->name('ekspedisi.delete');
		Route::get('/edit/{ekspedisi}', 'Administrator\EkspedisiController@fetch_data_ekspedisi');
		Route::post('/update', 'Administrator\EkspedisiController@update_ekspedisi')->name('ekspedisi.update');
		Route::get('/delete_picture_ekspedisi/{ekspedisi}', 'Administrator\EkspedisiController@delete_picture_ekspedisi')->name('ekspedisi.delete_picture_ekspedisi');
		Route::get('/detail_ekspedition/{name}', 'Administrator\EkspedisiController@detail_ekspedisi')->name('ekspedisi.detail');
		Route::post('/filter/{ekspedition_name}/{start_date}/{end_date}', 'Administrator\EkspedisiController@filter_used_ekspedition');
		Route::get('/calculate/{ekspedition_name}/{start_date}/{end_date}', 'Administrator\EkspedisiController@calculate_used_ekspedition');
	});

	//comment routes
	Route::prefix('comment')->group(function () {
		Route::delete('/delete', 'Administrator\CommentController@delete_comment')->name('comment.delete');
	});

	//Customer service routes
	Route::prefix('cs')->group(function () {
		Route::get('/', 'Administrator\CustomerServiceController@index_cs')->name('cs');
		Route::post('/show', 'Administrator\CustomerServiceController@show_cs')->name('cs.show');
		Route::post('/save', 'Administrator\CustomerServiceController@save_cs')->name('cs.store');
		Route::delete('/delete', 'Administrator\CustomerServiceController@delete_cs')->name('cs.delete');
		Route::get('/edit/{cs_id}', 'Administrator\CustomerServiceController@fetch_data_cs');
		Route::put('/update', 'Administrator\CustomerServiceController@update_cs')->name('cs.update');
		Route::get('/detail/{cs_id}', 'Administrator\CustomerServiceController@detail_customer_service')->name('cs.detail');
		Route::post('/filter/{cs_id}/{start_date}/{end_date}', 'Administrator\CustomerServiceController@filter_profit_cs');
		Route::get('/calculate/{cs_id}/{start_date}/{end_date}', 'Administrator\CustomerServiceController@calculate_profit_cs');
	});

	//order routes
	Route::prefix('order')->group(function () {
		Route::get('/', 'Administrator\OrderController@index_customer_order')->name('order');
		Route::post('/show/{start_date}/{end_date}/{status_id}', 'Administrator\OrderController@show_customer_order')->name('order.show');
		Route::delete('/delete', 'Administrator\OrderController@delete_customer_order')->name('order.delete');
		Route::get('/detail_orders/{order_id}', 'Administrator\OrderController@detail_order')->name('order.detail');
		Route::get('/confirmation_order', 'Administrator\OrderController@index_confirmation_order')->name('confirmation_order');
		Route::post('/show_confirmation_order/{startdate}/{enddate}/{filter}', 'Administrator\OrderController@show_confirmation_order')->name('confirmation_order.show');
		Route::get('/detail_confirmation_order/{confirmation_order_id}', 'Administrator\OrderController@fetch_data_confirmation_order');
		Route::put('/verify_confirmation_order', 'Administrator\OrderController@verify_confirmation_order')->name('confirmation_order.verify');
		Route::get('/change_status/{order_id}/{status}', 'Administrator\OrderController@change_status_order');
		Route::put('/update_resi_order', 'Administrator\OrderController@update_resi_order')->name('order.update_resi_order');
		Route::get('/delete_resi_order/{order_id}', 'Administrator\OrderController@delete_resi_order')->name('order.delete_resi_order');
		Route::put('/cancel_order', 'Administrator\OrderController@cancel_order')->name('order.cancel_order');
		Route::get('/print/{order_id}', 'Administrator\OrderController@print_order')->name('order.print');
		Route::get('/return_stock/{order_id}/{product_more_detail_id}', 'Administrator\OrderController@return_stock_for_refund')->name('order.return_stock');
	});

	//upgrade account routes
	Route::prefix('upgrade')->group(function () {
		Route::get('/', 'Administrator\UpgradeController@index_upgrade')->name('upgrade');
		Route::post('/confirmation_upgrade/{startdate}/{enddate}/{filter}', 'Administrator\UpgradeController@show_upgrade')->name('upgrade.show');
		Route::get('/detail_upgrade/{confirmation_upgrade_id}', 'Administrator\UpgradeController@fetch_data_confirmation_upgrade');
		Route::put('/verify_confirmation_upgrade', 'Administrator\UpgradeController@verify_confirmation_upgrade')->name('confirmation_upgrade.verify');
	});

	//activity account routes
	Route::prefix('activity')->group(function () {
		Route::get('/', 'Administrator\ActivityController@index_activity')->name('activity');
		Route::get('/filter_delete/{start_date}/{end_date}', 'Administrator\ActivityController@filter_delete');
	});

	//Profit routes
	Route::prefix('profit')->group(function () {
		Route::get('/', 'Administrator\ProfitController@index_profit')->name('profit');
		Route::post('/filter/{start_date}/{end_date}/{acc_type}', 'Administrator\ProfitController@profit');
		Route::get('/calculate/{start_date}/{end_date}/{acc_type}', 'Administrator\ProfitController@calculate_profit');
	});

	//banner routes
	Route::prefix('banner')->group(function () {
		Route::get('/', 'Administrator\BannerController@index_banner')->name('banner');
		Route::post('/show', 'Administrator\BannerController@show_banner')->name('banner.show');
		Route::post('/save', 'Administrator\BannerController@save_banner')->name('banner.store');
		Route::delete('/delete', 'Administrator\BannerController@delete_banner')->name('banner.delete');
		Route::get('/edit/{banner}', 'Administrator\BannerController@fetch_data_banner');
		Route::post('/update', 'Administrator\BannerController@update_banner')->name('banner.update');
		Route::get('/delete_picture_banner/{banner_id}', 'Administrator\BannerController@delete_picture_banner')->name('banner.delete_picture_banner');
	});
	
	//bank_account product routes
	Route::prefix('bank_account')->group(function () {
		Route::get('/', 'Administrator\BankAccountController@index_bank_account')->name('bank_account');
		Route::post('/show', 'Administrator\BankAccountController@show_bank_account')->name('bank_account.show');
		Route::post('/save', 'Administrator\BankAccountController@save_bank_account')->name('bank_account.store');
		Route::delete('/delete', 'Administrator\BankAccountController@delete_bank_account')->name('bank_account.delete');
		Route::get('/edit/{bank_account_id}', 'Administrator\BankAccountController@fetch_data_bank_account');
		Route::post('/update', 'Administrator\BankAccountController@update_bank_account')->name('bank_account.update');
		Route::get('/delete_picture_bank_account/{bank_account_id}', 'Administrator\BankAccountController@delete_picture_bank_account')->name('bank_account.delete_picture_bank_account');
	});

	//withdrawal_deposit routes
	Route::prefix('withdrawal')->group(function () {
		Route::get('/', 'Administrator\WithdrawalDepositController@index_withdrawal')->name('withdrawal');
		Route::post('/show', 'Administrator\WithdrawalDepositController@show_withdrawal')->name('withdrawal.show');
		Route::get('/detail_withdrawal/{withdrawal_account_id}', 'Administrator\WithdrawalDepositController@fetch_data_withdrawal');
		Route::put('/verify_withdrawal', 'Administrator\WithdrawalDepositController@verify_withdrawal')->name('withdrawal.verify');
	});

	//Profit routes
	Route::prefix('perday')->group(function () {
		Route::get('/', 'Administrator\DailyReportProductController@index_perday')->name('perday');
		Route::post('/total_product_perday/{start_date}', 'Administrator\DailyReportProductController@total_product_perday');
		Route::get('/calculate_order_product/{start_date}', 'Administrator\DailyReportProductController@calculate_order_product');
	});

	//Profit routes
	Route::prefix('stock')->group(function () {
		Route::get('/', 'Administrator\ExportStockController@index')->name('stock');
		Route::post('/export', 'Administrator\ExportStockController@export')->name('stock.export');
	});

});
