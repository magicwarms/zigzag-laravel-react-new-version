<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 
use Faker\Factory as Faker;

class RatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
    	$faker = Faker::create('id_ID');
    	foreach (range(1,200) as $index) {
	        DB::table('rating')->insert([
	            'customer_id' => $faker->numberBetween(1054, 1453),
	            'product_id' => $faker->numberBetween(1, 202),
	            'value_rating' => $faker->numberBetween(1, 5),
	        ]);
		}
    }
}
