<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 
use Faker\Factory as Faker;

class CategoriesSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker::create('id_ID');
    	foreach (range(1,50) as $index) {
	        DB::table('categories')->insert([
	            'name' => $faker->sentence(2, true),
	            'parent' => $faker->numberBetween(1, 50)
	        ]);
		}
    }
}
