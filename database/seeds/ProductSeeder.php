<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 
use Faker\Factory as Faker;

class ProductSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker::create('id_ID');
    	foreach (range(1,200) as $index) {
	        DB::table('products')->insert([
	            'categories_id' => $faker->numberBetween(1, 14),
	            'hot' => array_random([0, 1]),
	            'featured' => array_random([0, 1]),
	            'promo' => array_random([0, 1]),
	            'name' => $faker->sentence(3, true),
	            'price' => $faker->randomNumber(6),
	            'desc' => $faker->text(200),
	            'material' => $faker->sentence(2, true),
	            'dimension' => '10CM',
	            'weight' => $faker->numberBetween(5, 10),
	            'stock' => $faker->randomNumber(2),
	            'code' => 'ZG '.strtoupper($faker->randomNumber(4)),
	            'color' => $faker->colorName(),
	            'picture' => '["picture-product-zg-12451-3.JPG"]'
	        ]);
		}
    }
}
