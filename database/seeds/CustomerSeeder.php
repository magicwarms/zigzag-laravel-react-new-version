<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 
use Faker\Factory as Faker;

class CustomerSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker::create('id_ID');
    	foreach (range(1,200) as $index) {
	        DB::table('customers')->insert([
	            'name' => $faker->unique()->name, 
	            'email' => $faker->unique()->freeEmail, 
	            'password' => bcrypt('customer1234'),
	            'address' => $faker->address,
	            'province'=> '17',
	            'city' => '48',
	            'zip' => '29432',
	            'tele' => $faker->e164PhoneNumber,
	            'deposit' => $faker->numberBetween(100000, 1000000),
	            'picture' => '',
	            'acc_type' => $faker->numberBetween(1, 2)
	        ]);
		}
    }
}
