<table>
    <thead>
        <tr>
            <th>KODE BRG</th>
            <th>WARNA/SIZE</th>
            <th>STOK</th>
            <th>HPP</th>
            <th>TOTAL</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($products as $product) { ?>
        <tr>
            <td>{{ $product->name }}</td>
        </tr>
        <?php
        if(!$product->colorDetails->isEmpty()){
            foreach ($product->colorDetails as $color) {
                if(!$color->moreDetails->isEmpty()){
                foreach ($color->moreDetails as $more) {
                    $size = $more->size;
                    if($size == 0){
                        $size = '';
                    }
        ?>
        <tr>
            <td></td>
            <td>{{ $color->color }} <b>{{ $size }}</b></td>
            <td>{{ $more->stock }}</td>
            <?php
                foreach ($product->priceDetails as $price) { 
                    if($price->price_type == 'POKOK'){
            ?>
                <td>{{ $price->price }}</td>
                <td>{{ $price->price*$more->stock }}</td>
                    <?php } ?>
                <?php } ?>
        </tr>
            <?php } ?>
            <?php 
                } else { 
                    foreach ($color->moreDetails as $more) {
                    $size = $more->size;
                    if($size == 0){
                        $size = '';
                    }
            ?>
            <tr>
                <td></td>
                <td>{{ $color->color }} <b>{{ $size }}</b>TEST</td>
                <td>0</td>
                <?php 
                foreach ($product->priceDetails as $price) { 
                    if($price->price_type == 'POKOK'){
                ?>
                    <td>{{ $price->price }}</td>
                    <td>{{ $price->price*0 }}</td>
                        <?php } ?>
                    <?php } ?>
            </tr>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <?php } else { ?>
        <tr>
            <td></td>
            <td>WARNA &amp; STOK BELUM DIMASUKKAN</td>
            <td>0</td>
             <?php 
                foreach ($product->priceDetails as $price) { 
                    if($price->price_type == 'POKOK'){
                ?>
                    <td>{{ $price->price }}</td>
                    <td>{{ $price->price*0 }}</td>
                    <?php } ?>
                <?php } ?>
        </tr>
        <?php } ?>
        <?php } ?>
    </tbody>
</table>