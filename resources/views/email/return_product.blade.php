<html>
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body>
<p>Halo {{ $name }} ({{ $to }}),</p>
<p>Berikut adalah pemberitahuan status pengembalian produk kamu.</p>
<p>Kamu telah meminta untuk mengembalikan produk dengan nomor kode <b>{{ $return_code }}</b></p>
<p>Status pengembalian barang kamu telah <b>{{ $remark }}</b></p>
<p>Terima Kasih</p>
<p>Hormat kami,</p>
<p>{{ env('APP_NAME') }}</p>
<p>Surat elektronik ini dikirimkan secara otomatis dan tidak untuk dibalas.</p>
</body>
</html>
