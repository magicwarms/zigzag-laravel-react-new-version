<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
    body {width: 600px;margin: 0 auto;}
    table {border-collapse: collapse;}
    table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
    img {-ms-interpolation-mode: bicubic;}
    </style>
    <![endif]-->
    <style type="text/css">
    body, p, div {
    font-family: arial;
    font-size: 14px;
    }
    body {
    color: #658989;
    }
    body a {
    color: #1188E6;
    text-decoration: none;
    }
    p { margin: 0; padding: 0; }
    table.wrapper {
    width:100% !important;
    table-layout: fixed;
    -webkit-font-smoothing: antialiased;
    -webkit-text-size-adjust: 100%;
    -moz-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    }
    img.max-width {
    max-width: 100% !important;
    }
    .column.of-2 {
    width: 50%;
    }
    .column.of-3 {
    width: 33.333%;
    }
    .column.of-4 {
    width: 25%;
    }
    @media screen and (max-width:480px) {
    .preheader .rightColumnContent,
    .footer .rightColumnContent {
    text-align: left !important;
    }
    .preheader .rightColumnContent div,
    .preheader .rightColumnContent span,
    .footer .rightColumnContent div,
    .footer .rightColumnContent span {
    text-align: left !important;
    }
    .preheader .rightColumnContent,
    .preheader .leftColumnContent {
    font-size: 80% !important;
    padding: 5px 0;
    }
    table.wrapper-mobile {
    width: 100% !important;
    table-layout: fixed;
    }
    img.max-width {
    height: auto !important;
    max-width: 480px !important;
    }
    a.bulletproof-button {
    display: block !important;
    width: auto !important;
    font-size: 80%;
    padding-left: 0 !important;
    padding-right: 0 !important;
    }
    .columns {
    width: 100% !important;
    }
    .column {
    display: block !important;
    width: 100% !important;
    padding-left: 0 !important;
    padding-right: 0 !important;
    margin-left: 0 !important;
    margin-right: 0 !important;
    }
    }
    </style>
    <!--user entered Head Start-->
    
    <!--End Head user entered-->
  </head>
  <body>
    <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size: 14px; font-family: arial; color: #658989; background-color: #f9f9f9;">
    <div class="webkit">
      <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#f9f9f9">
        <tr>
          <td valign="top" bgcolor="#f9f9f9" width="100%">
            <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="100%">
                  <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td>
                        <!--[if mso]>
                        <center>
                        <table><tr><td width="600">
                          <![endif]-->
                          <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                            <tr>
                              <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #658989; text-align: left;" bgcolor="#ffffff" width="100%" align="left">
                                
                                <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%"
                                  style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
                                  <tr>
                                    <td role="module-content">
                                      <p>Hi, ini notifikasi pemeriksaan stok harian Zigzag Batam.</p>
                                    </td>
                                  </tr>
                                </table>
                                
                                <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                  <tr>
                                    <td style="font-size:6px;line-height:10px;padding:20px 0px 20px 0px;background-color:#f9f9f9;" valign="top" align="center">
                                      <img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:20% !important;width:20%;height:auto !important;" src="https://marketing-image-production.s3.amazonaws.com/uploads/0e5663763bc218ebccab8a13d9e50635d3ee0b4bab051904dcfa312332cd1b8a6e167cf6f1f7ab37af6c54aac1f228fc2d034cdf3434bff24cbceea96a78e598.png" alt="Zigzag Logo" width="120">
                                    </td>
                                  </tr>
                                </table>
                                
                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                  <tr>
                                    <td style="padding:18px 20px 18px 20px;line-height:22px;text-align:inherit;"
                                      height="100%"
                                      valign="top"
                                      bgcolor="">
                                      <div>
                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(101, 137, 137);"><span style="font-weight: 600; background: transparent; font-size: inherit; color: inherit;">Hi, BigBoss & Tasya!</span></div>
                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(101, 137, 137);">Ini adalah daftar stok barang yang jumlahnya kurang dari 3 untuk tanggal {{ date('d F Y') }}, Berikut data-nya:</div>
                                      </div>
                                    </td>
                                  </tr>
                                </table>
                                
                                <table class="module"
                                  role="module"
                                  data-type="divider"
                                  border="0"
                                  cellpadding="0"
                                  cellspacing="0"
                                  width="100%"
                                  style="table-layout: fixed;">
                                  <tr>
                                    <td style="padding:0px 20px 0px 20px;"
                                      role="module-content"
                                      height="100%"
                                      valign="top"
                                      bgcolor="">
                                      <table border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        align="center"
                                        width="100%"
                                        height="2px"
                                        style="line-height:2px; font-size:2px;">
                                        <tr>
                                          <td
                                            style="padding: 0px 0px 2px 0px;"
                                          bgcolor="#f0f0f0"></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                
                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                  <tr>
                                    <td style="background-color:#ffffff;padding:18px 20px 18px 20px;line-height:22px;text-align:inherit;"
                                      height="100%"
                                      valign="top"
                                      bgcolor="#ffffff">
                                      <table border="2" cellpadding="0" cellspacing="1" class="product-list" style="width:550px;">
                                        <caption>
                                        <div style="text-align: left;"><span style="font-size:14px;"><strong>Item Barang</strong></span></div>
                                        <div style="text-align: left;">&nbsp;</div>
                                        </caption>
                                        <tbody>
                                          <?php
                                          foreach ($products as $product) {
                                          if($product->size == 0){
                                          $size = '-';
                                          } else {
                                          $size = $product->size;
                                          }
                                          ?>
                                          <tr style="border-bottom: 1px solid #f3f3f3;">
                                            <td>
                                              <div><strong style="font-size:11px;">{{ $product->name }}</strong></div>
                                              <div><span style="font-size:11px;">Warna: {{ $product->color }}</span></div>
                                              <div><span style="font-size:11px;">Size: {{ $size }}</span></div>
                                              <div>&nbsp;</div>
                                            </td>
                                            <td width="100" style="text-align: center; color: red;"><span style="font-size:11px;">{{ $product->stock }} Pcs</span></td>
                                          </tr>
                                          <?php } ?>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                
                                <table class="module"
                                  role="module"
                                  data-type="divider"
                                  border="0"
                                  cellpadding="0"
                                  cellspacing="0"
                                  width="100%"
                                  style="table-layout: fixed;">
                                  <tr>
                                    <td style="padding:0px 20px 0px 20px;"
                                      role="module-content"
                                      height="100%"
                                      valign="top"
                                      bgcolor="">
                                      <table border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        align="center"
                                        width="100%"
                                        height="2px"
                                        style="line-height:2px; font-size:2px;">
                                        <tr>
                                          <td
                                            style="padding: 0px 0px 2px 0px;"
                                          bgcolor="#f0f0f0"></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                
                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                  <tr>
                                    <td style="padding:18px 20px 18px 20px;line-height:22px;text-align:inherit;"
                                      height="100%"
                                      valign="top"
                                      bgcolor="">
                                      <div>
                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(101, 137, 137);">Hayoo udah lihat kan? jangan lupa untuk restock kembali ya!</div>
                                      </div>
                                    </td>
                                  </tr>
                                </table>
                                <table class="module"
                                  role="module"
                                  data-type="spacer"
                                  border="0"
                                  cellpadding="0"
                                  cellspacing="0"
                                  width="100%"
                                  style="table-layout: fixed;">
                                  <tr>
                                    <td style="padding:0px 0px 80px 0px;"
                                      role="module-content"
                                      bgcolor="">
                                    </td>
                                  </tr>
                                </table>
                                
                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                  <tr>
                                    <td style="padding:18px 20px 18px 20px;line-height:22px;text-align:inherit;"
                                      height="100%"
                                      valign="top"
                                      bgcolor="">
                                      <div>Kalo kamu ngerasa&nbsp;ini ada kesalahan, silakan hubungi Abang - abang web ya!.</div>
                                      <div>&nbsp;</div>
                                      <div>&nbsp;</div>
                                      <div>&nbsp;</div>
                                      <div>Thank you,</div>
                                      <div>&nbsp;</div>
                                      <div>Zigzag Online Shop</div>
                                    </td>
                                  </tr>
                                </table>
                                
                                <table class="module"
                                  role="module"
                                  data-type="spacer"
                                  border="0"
                                  cellpadding="0"
                                  cellspacing="0"
                                  width="100%"
                                  style="table-layout: fixed;">
                                  <tr>
                                    <td style="padding:0px 0px 60px 0px;"
                                      role="module-content"
                                      bgcolor="">
                                    </td>
                                  </tr>
                                </table>
                                <div data-role="module-unsubscribe" class="module unsubscribe-css__unsubscribe___2CDlR" role="module" data-type="unsubscribe" style="color:#444444;font-size:12px;line-height:20px;padding:16px 16px 16px 16px;text-align:center"><div class="Unsubscribe--addressLine"><p class="Unsubscribe--senderName" style="font-family:Arial,Helvetica, sans-serif;font-size:12px;line-height:20px">Zigzag Online Shop Batam</p><p style="font-family:Arial,Helvetica, sans-serif;font-size:12px;line-height:20px"><span class="Unsubscribe--senderAddress">{{ env('MAIL_FROM_NAME') }}</span>, <span class="Unsubscribe--senderCity">Batam</span>, <span class="Unsubscribe--senderState">Kepulauan Riau</span></p></div></div>
                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                  <tr>
                                    <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                      height="100%"
                                      valign="top"
                                      bgcolor="">
                                      <div style="text-align: center;"><a href="https://zigzagbatam.com">www.zigzagbatam.com</a></div>
                                    </td>
                                  </tr>
                                </table>
                                
                              </td>
                            </tr>
                          </table>
                          <!--[if mso]>
                        </td></tr></table>
                        </center>
                        <![endif]-->
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
    </center>
  </body>
</html>