<html>
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body>
<p>Halo {{ $name }} ({{ $to }}),</p>
<p>Kamu telah melakukan permintaan pencairan deposit pada akun kamu.</p>
<p>Dengan kode order {{ $code }}.</p>
<p>Jumlah pencairan {{ $amount }}.</p>
<p>Bank Tujuan <b>{{ $bank }}</b></p>
<p>Jika bukan kamu yang meminta ini, ada kemungkinan bahwa orang lain mencoba mengakses Akun kamu.</p>
<p>Segera laporkan kejadian ini ke pihak {{ env('APP_NAME') }}.</p>
<p>Hormat kami,</p>
<p>{{ env('APP_NAME') }}</p>
<p>Surat elektronik ini dikirimkan secara otomatis dan tidak untuk dibalas.</p>
</body>
</html>
