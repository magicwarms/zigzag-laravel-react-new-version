<header id="header_main">
    <div class="header_main_content">
        <nav class="uk-navbar">       
            <!-- main sidebar switch -->
            <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                <span class="sSwitchIcon"></span>
            </a>
            <!-- secondary sidebar switch -->
            <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                <span class="sSwitchIcon"></span>
            </a>
            
            <div class="uk-navbar-flip">
                <ul class="uk-navbar-nav user_actions">
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                        <a href="#" class="user_action_image"><img class="md-user-image" src="{{ asset('templates/img/avatars/avatar_11_tn.png') }}" alt=""></a>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav js-uk-prevent">
                                <li><a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- main header end -->

<!-- main sidebar -->
<aside id="sidebar_main">
    <div class="sidebar_main_header">
        <div class="sidebar_logo">
            <a href="" class="sSidebar_hide sidebar_logo_large">
                <img class="logo_regular" src="#" alt="" width="100%"/>
                <img class="logo_light" src="{{ asset('templates/img/logo_main_white.png') }}" alt="" height="15" width="71"/>
            </a>
            <a href="" class="sSidebar_show sidebar_logo_small">
                <img class="logo_regular" src="{{ asset('templates/img/logo_main_small.png') }}" alt="" height="32" width="32"/>
                <img class="logo_light" src="{{ asset('templates/img/logo_main_small_light.png') }}" alt="" height="32" width="32"/>
            </a>
        </div>
    </div>
    <?php
      $seg1 = strtolower(Request::segment(1));
      $menus = menu_active(1,NULL);
      $menuschild = menu_active(NULL,1);
    ?>
    <div class="menu_section">
        <ul>
            <?php
                foreach ($menus as $val1) {
            ?>
                <li title="{{ $val1->name }}"{!! $seg1 == $val1->function ? ' class="submenu_trigger current_section act_section"' : '' !!}>
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">{{ $val1->icon }}</i></span>
                        <span class="menu_title">{{ $val1->name }}</span>
                    </a>
                    <ul{!! $seg1 == $val1->function ? ' style="display: block;"' : '' !!}>
                        <?php 
                        foreach ($menuschild as $val2) { 
                            if($val1->id == $val2->parent){
                        ?>
                        <li{!! $seg1 == $val2->function ? ' class="act_item"' : '' !!}><a href="{{ route($val2->function) }}">{{ $val2->name }}</a></li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
</aside>