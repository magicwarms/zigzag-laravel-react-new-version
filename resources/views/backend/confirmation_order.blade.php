@extends('layouts.baselayout')

@section('css')
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')

<div class="uk-width-medium-1-1">
	<h4 class="heading_a uk-margin-bottom">Data Konfirmasi Order</h4>
	<div class="md-card">
	  <div class="md-card-content">
      <div class="uk-grid uk-margin-bottom" data-uk-grid-margin>
        <div class="uk-width-medium-1-4">
          <input class="md-input" name="startdate" type="text" id="startdate" data-uk-datepicker="{format:'DD.MM.YYYY'}" value="{{ date('d.m.Y') }}">
        </div>
        <div class="uk-width-medium-1-4">
          <input class="md-input" name="end_date" type="text" id="enddate" data-uk-datepicker="{format:'DD.MM.YYYY'}" value="{{ date('d.m.Y') }}">
        </div>
        <div class="uk-width-medium-1-4">
          <select id="filter_confirm" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Filter Status Upgrade">
              <option selected="true" value="-">Semua...</option>
              <option value="1">Konfirmasi valid & disetujui</option>
              <option value="2">Menunggu konfirmasi</option>
              <option value="3">Cek Mutasi</option>
              <option value="0">Konfirmasi tidak disetujui</option>
          </select>
        </div>
      </div>
	    <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#tabs_4'}">
	      <li class="uk-width-1-1" id="list-tab"><a href="#" class="list_cs">List Data Konfirmasi Order</a></li>
	    </ul>
	    <ul id="tabs_4" class="uk-switcher uk-margin">
	      <li>
	        <table id="data_table" class="uk-table" cellspacing="0" width="100%">
	          <thead>
	            <tr>
	               <th class="number-order">No.</th>
    	            <th>Nama</th>
                    <th>Kode Order</th>
                    <th>Tanggal Konfirmasi</th>
                    <th>Status</th>
                    <th>Created</th>
    	            <th>Updated</th>
	              <th class="action-order">Action</th>
	            </tr>
	          </thead>
	          <tfoot>
	            <tr>
	              <th class="number-order">No.</th>
                    <th>Nama</th>
                    <th>Kode Order</th>
                    <th>Tanggal Konfirmasi</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th>Updated</th>
                  <th class="action-order">Action</th>
	            </tr>
	          </tfoot>
	          <tbody></tbody>
          </table>
        </li>
	 		</ul>
		</div>
	</div>
</div>
@endsection

@section('js')
    <div class="uk-modal" id="evidence_order">
        <div class="uk-modal-dialog">
            <form class="form_evidence_order" class="uk-form-stacked">
                <div class="uk-modal-header"></div>
                <input type="hidden" name="_method" id="method" value="PUT">
                <input type="hidden" id="id" name="id">
                <input type="hidden" id="order_id" name="order_id">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                      <label>Nama</label>
                      <br>
                      <input type="text" id="name" class="md-input label-fixed" readonly="true" />
                    </div>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                      <label>Kode Order</label>
                      <br>
                      <input type="text" id="order_code" name="order_code" class="md-input label-fixed" readonly="true"/>
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                      <label>Bank Pengirim</label>
                      <br>
                      <input type="text" id="bank_sender" class="md-input label-fixed" name="bank_sender" readonly="true" />
                    </div>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                      <label>Total Transfer</label>
                      <br>
                      <input type="text" id="total_transfer" class="md-input label-fixed" name="total_transfer" readonly="true" />
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                      <label>No. Rekening Penerima (Zigzag)</label>
                      <br>
                      <input type="text" id="bank_receiver" class="md-input label-fixed" name="bank_receiver" readonly="true" />
                    </div>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                      <label>Tanggal Transfer</label>
                      <br>
                      <input type="text" id="date" class="md-input label-fixed" name="date" readonly="true" />
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 uk-margin-top">
                      <label>Bukti</label>
                      <hr class="uk-margin-bottom">
                      <div id="evidence_pic"></div>
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                        <label>Verifikasi</label>
                        <br>
                        <span class="icheck-inline uk-margin-top">
                            <input type="radio" name="status" id="status" data-md-icheck value="1"/>
                            <label for="status" class="inline-label">DISETUJUI</label>
                        </span>
                    </div>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                        <br>
                        <span class="icheck-inline uk-margin-top">
                            <input type="radio" name="status" id="status" data-md-icheck value="0"/>
                            <label for="status" class="inline-label">DITOLAK</label>
                        </span>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-wave waves-effect waves-button uk-modal-close uk-modal-close">Tutup</button>
                    <input type="submit" value="SUBMIT" class="md-btn md-btn-danger" id="show_preloader_md">
                </div>
            </form>
        </div>
    </div>
    <!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <!-- datatable -->
    <script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('templates/js/custom/datatables/datatables.uikit.min.js') }}"></script>
    <script src="{{ asset('templates/js/pages/plugins_datatables.min.js') }}"></script>
    <!-- page specific plugins -->
    <script src="{{ asset('templates/js/pages/full_numbers_no_ellipses.js') }}"></script>
    <script src="{{ asset('templates/js/pages/components_preloaders.min.js') }}"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        var APP_URL = {!! json_encode(url('/')) !!}
        var start_date = $('#startdate').val();
        var end_date = $('#enddate').val();
        var filter = $('#filter_confirm').val();
        var dt = $('#data_table').DataTable({
            orderCellsTop: true,
            responsive: true,
            processing: true,
            serverside: true,
            scrollX: true,
            searching: true,
            "autoWidth": true,
            lengthMenu: [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
            'pagingType': 'full_numbers_no_ellipses',
            ajax: {
                url:  APP_URL + "/order/show_confirmation_order/"+start_date+"/"+end_date+"/"+filter,
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
                { data: 'DT_Row_Index', searchable: false, "width": "15px", "className": "text-center"},
                { data: 'customer_name'},
                { data: 'order_code'},
                { data: 'date'},
                { data: 'status'},
                { data: 'created_date'},
                { data: 'updated_date'},
                { data: 'action', name: 'action', orderable: false, searchable: false, "width": "25px", "className": "text-center" },
            ]
        })
        $('#startdate, #enddate, #filter_confirm').change(function($event) {
          altair_helpers.content_preloader_show('md');
          var start_date = $('#startdate').val();
          var end_date = $('#enddate').val();
          var filter = $('#filter_confirm').val();
          dt.ajax.url(APP_URL + "/order/show_confirmation_order/"+start_date+"/"+end_date+"/"+filter).load();
          altair_helpers.content_preloader_hide();
        });
    });
    $(document).on('click', '.show_evidence_order', function (e) {
        var id = $(this).data('id');
        var APP_URL = {!! json_encode(url('/')) !!}
        $.ajax({
          url: APP_URL + "/order/detail_confirmation_order/"+id,
          method:'GET',
          dataType:'json',
          beforeSend: function(){
            altair_helpers.content_preloader_show('md');
          },
          success:function(data) {
            $('input#id').val(id);
            $('input#name').val(data.customer_name);
            $('input#order_code').val(data.order_code);
            $('input#order_id').val(data.order_id);
            $('input#bank_sender').val(data.bank_sender);
            $('input#bank_receiver').val(data.bank_receiver);
            $('input#date').val(data.date);
            $('input#total_transfer').val(data.total_transfer);
            $("#evidence_pic").html('<div class="uk-margin-bottom uk-text-center uk-position-relative"><a href="{{ asset("storage/img/") }}/'+data.evidence+'" data-uk-lightbox="{group:"gallery"}"><img src="{{ asset("storage/img/") }}/'+data.evidence+'" alt=""></a></div>')
            $(".uk-modal-header").html('<h3 class="uk-modal-title">Konfirmasi Order '+data.order_code+'</h3>')
            UIkit.modal("#evidence_order").show();
            altair_helpers.content_preloader_hide();
          }
        })
      e.preventDefault();
    });
    $(".form_evidence_order").on('submit',function(event) {
        $.ajax({
            type: 'PUT',
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: "{{ route('confirmation_order.verify') }}",
            data: $(this).serialize(),
            dataType: 'JSON',
            cache: false,
            beforeSend: function(){
              (function(modal){ modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Mohon tunggu sebentar data sedang diproses...<br/></div>'); setTimeout(function(){ modal.hide() }, 2000) })();
            },
            success: function(result) {
              UIkit.modal("#evidence_order").hide();
              $('#data_table').DataTable().ajax.reload();
              altair_helpers.content_preloader_hide();
              if(result.status=='success'){
                UIkit.notify({
                  message: result.msg,
                  status: 'success',
                  timeout: 8000,
                  pos: 'top-center'
                });
              } else {
                UIkit.notify({
                  message: result.msg,
                  status: 'error',
                  timeout: 10000,
                  pos: 'top-center'
                });
              }
            },
            error: function (result) {
              var response = JSON.parse(result.responseText)
              $.each(response, function (key, value) {
                $('#output').html('<div class="uk-alert uk-alert-danger" data-uk-alert><a href="#" class="uk-alert-close uk-close"></a>'+value+'</div>')
              });
              altair_helpers.content_preloader_hide();
            }
        });
      event.preventDefault();
    });
    </script>
@endsection