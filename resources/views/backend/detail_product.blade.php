@extends('layouts.baselayout')

@section('css')
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')
<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
    <h1>{{ $output['name'] }}</h1>
</div>
<div id="page_content_inner">

    <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
        <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
            <div class="md-card">
                <div class="md-card-toolbar">
                    <h3 class="md-card-toolbar-heading-text">
                        Photos
                    </h3>
                </div>
                <div class="md-card-content">
                	<?php
                		$photo = $output['picture'];
                		if($photo != ''){
                			$pic = asset('storage/img/product/'.$photo[0]['image']);
                		} else {
                			$pic = asset('templates/img/no-image-available.png');
                		}
                	?>
                    <div class="uk-margin-bottom uk-text-center">
                        <img src="{{ $pic }}" alt="{{ $output['name'] }}" class="img_medium" />
                    </div>
                    <ul class="uk-grid uk-grid-width-small-1-3 uk-text-center" data-uk-grid-margin>
                    <?php
                    if(!empty($photo)) {
                    	foreach ($photo as $val) {
                    		if($val != ''){
                    			$pics = asset('storage/img/product/'.$val->image);
                    		} else {
                    			$pics = asset('templates/img/no-image-available.png');
                    		}
                    ?>
                        <li>
                            <img src="{{ $pics }}" alt="{{ $output['name'] }}" class="img_small"/>
                        </li>
                    	<?php } ?>
                    <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="md-card">
                <div class="md-card-toolbar">
                    <h3 class="md-card-toolbar-heading-text">
                        Data
                    </h3>
                </div>
                <div class="md-card-content">
                    <ul class="md-list">
                    <li>
                        <div class="md-list-content">
                            <span class="uk-text-medium uk-text-muted uk-display-block">Rating - {{ round($output['rating_product']) }} {{ str_plural('Star', round($output['rating_product'])) }}</span>
                            <?php
                            $ratings = round($output['rating_product']);
                            for ($i=0; $i < $ratings; $i++) { ?>
                            	<i class="material-icons uk-icon-large">star_rate</i>
	                        <?php } ?>
                        </div>
                    </li>
                    <?php
                    $prices = $output['price'];
                    foreach ($prices as $price) {
                        if(Auth::user()->level != 1) {
                            if($price->price_type == 'PARTNER' OR $price->price_type == 'POKOK'){
                                $prices = 0;
                            } else {
                                $prices = $price->price;
                            }
                        } else {
                            $prices = $price->price;
                        }
                    ?>
                        <li>
                            <div class="md-list-content">
                                <span class="uk-text-small uk-text-muted uk-display-block">Harga {{ strtoupper($price->price_type) }}</span>
                                <span class="md-list-heading uk-text-large uk-text-success">Rp. {{ number_format($prices, 0, ',', '.') }}</span>
                            </div>
                        </li>
                    <?php } ?>
                        <li>
                        	<?php
                    		$promo = $output['promo'];
                    			if($promo == 1){
				                	$promo = '<a href="#" data-uk-tooltip title="Promo Product"><i class="material-icons md-36 uk-text-success">&#xE86C;</i></a>';
					            } else {
					                $promo ='<a href="#" data-uk-tooltip title="Bukan Promo Product"><i class="material-icons md-36 uk-text-danger">&#xE5C9;</i></a>';
					            }
                        	?>
                            <div class="md-list-content">
                                <span class="uk-text-small uk-text-muted uk-display-block">Promo</span>
                                <span class="md-list-heading uk-text-large">{!! $promo !!}</span>
                            </div>
                        </li>
                        <li>
                        	<?php
                    		$hot = $output['hot'];
                    			if($hot == 1){
				                	$hot = '<a href="#" data-uk-tooltip title="Hot Product"><i class="material-icons md-36 uk-text-success">&#xE86C;</i></a>';
					            } else {
					                $hot ='<a href="#" data-uk-tooltip title="Bukan Hot Product"><i class="material-icons md-36 uk-text-danger">&#xE5C9;</i></a>';
					            }
                        	?>
                            <div class="md-list-content">
                                <span class="uk-text-small uk-text-muted uk-display-block">Hot</span>
                                <span class="md-list-heading uk-text-large">{!! $hot !!}</span>
                            </div>
                        </li>
                        <li>
                            <?php
                            $restock = $output['restock'];
                                if($restock == 1){
                                    $restock = '<a href="#" data-uk-tooltip title="Restock Product"><i class="material-icons md-36 uk-text-success">&#xE86C;</i></a>';
                                } else {
                                    $restock ='<a href="#" data-uk-tooltip title="Bukan Restock Product"><i class="material-icons md-36 uk-text-danger">&#xE5C9;</i></a>';
                                }
                            ?>
                            <div class="md-list-content">
                                <span class="uk-text-small uk-text-muted uk-display-block">Restock</span>
                                <span class="md-list-heading uk-text-large">{!! $restock !!}</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
            <div class="md-card">
                <div class="md-card-toolbar">
                    <h3 class="md-card-toolbar-heading-text">
                        Details
                    </h3>
                </div>
                <div class="md-card-content large-padding">
                    <div class="uk-grid uk-grid-divider uk-grid-medium">
                        <div class="uk-width-large-1-1">
                            <div class="uk-grid uk-grid-small">
                                <div class="uk-width-large-1-3">
                                    <span class="uk-text-muted uk-text-small">Nama Produk</span>
                                </div>
                                <div class="uk-width-large-2-3">
                                    <span class="uk-text-large uk-text-middle"><a href="#">{{ $output['name'] }}</a></span>
                                </div>
                            </div>
                            <hr class="uk-grid-divider">
                            <div class="uk-grid uk-grid-small">
                                <div class="uk-width-large-1-3">
                                    <span class="uk-text-muted uk-text-small">Kategori</span>
                                </div>
                                <div class="uk-width-large-2-3">
                                    <span class="uk-text-large uk-text-middle">{{ $output['categories_name'] }}</span>
                                </div>
                            </div>
                            <hr class="uk-grid-divider">
                            <div class="uk-grid uk-grid-small">
                                <div class="uk-width-large-1-3">
                                    <span class="uk-text-muted uk-text-small">Material</span>
                                </div>
                                <div class="uk-width-large-2-3">
                                    {{ $output['material'] }}
                                </div>
                            </div>
                            <hr class="uk-grid-divider">
                            <div class="uk-grid uk-grid-small">
                                <div class="uk-width-large-1-3">
                                    <span class="uk-text-muted uk-text-small">Produk dilihat</span>
                                </div>
                                <div class="uk-width-large-2-3">
                                    <span class="uk-badge uk-badge-danger uk-badge-notification uk-text-large">{{ $product_viewer }}</span>
                                </div>
                            </div>
                            <hr class="uk-grid-divider">
                            <?php if($output['is_shoes'] == 1)$ShoesOrTas='Sepatu'; else $ShoesOrTas='Tas'; ?>
                            <div class="uk-grid uk-grid-small">
                                <div class="uk-width-large-1-3">
                                    <span class="uk-text-medium uk-text-bold uk-margin-remove">{{ $ShoesOrTas }}</span>
                                </div>
                                <div class="uk-width-large-2-3">
                                <?php 
                                    $ShoesOrTasEach = $output['product_more_detail'];
                                    foreach ($ShoesOrTasEach as $shoestas) {
                                ?>
                                <li>
                                    <p class="uk-text-bold uk-margin-remove">Color: {{ $shoestas->color }}</p>
                                    <p class="uk-text-bold uk-margin-remove">Stock: {{ $shoestas->stock }}</p>
                                    <?php if($shoestas->size != ''){ ?>
                                        <p class="uk-text-bold uk-margin-remove">Size: {{ $shoestas->size }}</p>
                                    <?php } ?>
                                </li>
                                <br>
                                <?php } ?>
                                </div>
                            </div>
                            <hr class="uk-grid-divider">
                        </div>
                    </div>
                </div>
            </div>
            <div class="md-card">
                <div class="md-card-toolbar">
                    <h3 class="md-card-toolbar-heading-text">
                        Komentar Produk
                    </h3>
                </div>
                <div class="md-card-content large-padding">
                	<div class="uk-slidenav-position" data-uk-slideshow="{animation:'scroll'}">
                        <ul class="uk-slideshow">
                        <?php
                            if(!empty($comments)){
                                foreach ($comments as $comment) {
                        ?>
                            <li>
                                <h2 class="heading_b">
                                    {{ $comment->customer_name }} - <a onclick="delete_data(<?php echo $comment->id;?>)" href="#"><i class="md-icon material-icons">&#xE16C;</i></a>
                                </h2>
                                <div class="uk-grid" data-uk-grid-margin="">
                                    <div class="uk-width-medium-1-4">
                                        <img src="{{ asset('storage/img/'.$comment->customer_picture) }}" alt="{{ $comment->customer_name }}" class="img_small">
                                    </div>
                                    <div class="uk-width-medium-3-4">
                                        <h5 class="heading_c">
                                            Komentar:
                                        </h5><br>
                                        <p><i>{{ $comment->comment }}</i></p>
                                    </div>
                                </div>
                            </li>
                            <?php } ?>
                        <?php } ?>
                        </ul>
                        <a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                        <a href="" class="uk-slidenav uk-slidenav-next" data-uk-slideshow-item="next"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <script type="text/javascript">
        function delete_data(id){
            UIkit.modal.confirm("Apakah kamu yakin akan menghapus data ini?", function(){  
              $.ajax({
                method: 'DELETE',
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}'
                },
                url: "{{ route('comment.delete') }}",
                dataType: 'JSON',
                cache: false,
                data: {id: id},
                beforeSend: function(){
                    altair_helpers.content_preloader_show('md');
                },
                success: function(result) {
                  altair_helpers.content_preloader_hide();
                  if(result.status=='success'){
                    UIkit.notify({
                        message: result.msg,
                        status: 'success',
                        timeout: 8000,
                        pos: 'top-center'
                    });
                    location.reload();
                  } else  {
                    UIkit.notify({
                        message: result.msg,
                        status: 'danger',
                        timeout: 8000,
                        pos: 'top-center'
                    });
                    location.reload();
                  }
                }
              });
            });
          }
    </script>
@endsection