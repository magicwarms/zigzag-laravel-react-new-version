@extends('layouts.baselayout')

@section('css')
    <!-- additional styles for plugins -->
    <!-- chartist -->
    <link rel="stylesheet" href="{{ asset('bower_components/chartist/dist/chartist.min.css') }}">
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
    <style type="text/css">
        .md-bg-process{background-color:#711b58!important}
        .md-bg-wash{background-color:#ECD80A!important}
    </style>
@endsection

@section('content')
<div class="uk-grid uk-grid-width-large-1-2 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
    <div>
        <div class="md-card">
            <div class="md-card-content md-bg-process">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">{{$count_order_ekspedition_handled}}/1000</span></div>
                <span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Jumlah Total Pemakaian Ekspedisi</div></span>
                <h2 class="uk-margin-remove uk-text-contrast"><span class="countUpMe">0<noscript>{{ $count_order_ekspedition_handled }}</noscript></span></h2>
            </div>
        </div>
    </div>
    <div>
        <div class="md-card">
            <div class="md-card-content md-bg-wash">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">{{ $count_ekspedition_success_order }}/1000</span></div>
                <span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Jumlah Ekspedisi Order Berhasil</div></span>
                <h2 class="uk-margin-remove uk-text-contrast"><span class="countUpMe">0<noscript>{{ $count_ekspedition_success_order }}</noscript></span></h2>
            </div>
        </div>
    </div>
</div>
<div class="uk-width-large-1-1 uk-margin-top">
    <div class="md-card">
        <div class="md-card-content">
            <h4 class="heading_c uk-margin-bottom">Grafik Pendapatan {{ $detail_ekspedisi->name }}</h4>
            <div id="chartist_distributed_series" class="chartist"></div>
        </div>
    </div>
</div>
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large-1-1 uk-margin-top">
        <div class="md-card">
            <div class="md-card-content">
                <h4 class="heading_c uk-margin-bottom">Pemakaian Ekspedisi {{ $detail_ekspedisi->name }} </h4>
                <div class="uk-grid uk-margin-bottom" data-uk-grid-margin>
                    <div class="uk-width-medium-1-4">
                      <input class="md-input" name="start_date" type="text" id="start_date" data-uk-datepicker="{format:'DD.MM.YYYY'}" value="{{ date('d.m.Y') }}">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <input class="md-input" name="end_date" type="text" id="end_date" data-uk-datepicker="{format:'DD.MM.YYYY'}" value="{{ date('d.m.Y') }}">
                    </div>
                    <div class="uk-width-medium-1-2" id="profit_ekspedition"></div>
                </div>
                <div class="uk-overflow-container">
                <table id="dt_tableExport_wrapper" class="uk-table" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                       <th class="number-order">No.</th>
                        <th>Kode Order</th>
                        <th>Telepon</th>
                        <th>Status</th>
                        <th>Grandtotal</th>
                        <th>Created</th>
                        <th>Updated</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th class="number-order">No.</th>
                        <th>Kode Order</th>
                        <th>Telepon</th>
                        <th>Status</th>
                        <th>Grandtotal</th>
                        <th>Created</th>
                        <th>Updated</th>
                    </tr>
                  </tfoot>
                  <tbody></tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
	<!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <!-- datatable -->
    <script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/dataTables.buttons.js') }}"></script>
    <script src="{{ asset('templates/js/custom/datatables/buttons.uikit.js') }}"></script>
    <script src="{{ asset('bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('bower_components/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.colVis.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.html5.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.print.js') }}"></script>
    <script src="{{ asset('templates/js/custom/datatables/datatables.uikit.min.js') }}"></script>
    <script src="{{ asset('templates/js/pages/plugins_datatables.min.js') }}"></script>
    <!-- page specific plugins -->
    <script src="{{ asset('templates/js/pages/full_numbers_no_ellipses.js') }}"></script>
    <!-- chartist -->
    <script src="{{ asset('bower_components/chartist/dist/chartist.min.js') }}"></script>
    <!-- peity (small charts) -->
    <script src="{{ asset('bower_components/peity/jquery.peity.min.js') }}"></script>
    <!-- easy-pie-chart (circular statistics) -->
    <script src="{{ asset('bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
    <!-- countUp -->
    <script src="{{ asset('bower_components/countUp.js/countUp.js') }}"></script>\
    <script src="{{ asset('templates/js/pages/dashboard.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $(function() {
                // chartist
                altair_charts.chartist_charts();
            });
            altair_charts = {
                chartist_charts: function() {
                    // distributed series
                    var ch_distributed_series = new Chartist.Bar('#chartist_distributed_series', {
                        labels: ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"],
                        series: <?php echo $chart_data;?>
                    }, {
                        distributeSeries: true
                    });
                    $window.on('resize',function() {
                        ch_distributed_series.update();
                    });
                }
            };

            var APP_URL = {!! json_encode(url('/')) !!}
            var detail_ekspedisi   = "{{ $detail_ekspedisi->name }}";
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            var dt = $('#dt_tableExport_wrapper').DataTable({
                orderCellsTop: true,
                responsive: true,
                processing: true,
                serverside: true,
                scrollX: true,
                searching: false,
                "autoWidth": true,
                paging: false,
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excel',
                        pageSize: 'A4',
                        exportOptions: {
                          columns: [ 0, 1, 2, 3, 4 ]
                        }
                    },
                    {
                        extend: 'pdf',
                        orientation: 'potrait',
                        pageSize: 'A4',
                        exportOptions: {
                          columns: [ 0, 1, 2, 3, 4 ]
                        }
                    },
                ],
                lengthMenu: [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
                'pagingType': 'full_numbers_no_ellipses',
                ajax: {
                    url:  APP_URL + "/ekspedisi/filter/"+detail_ekspedisi+"/"+start_date+"/"+end_date,
                    data: { '_token' : '{{ csrf_token() }}'},
                    type: 'POST',
                },
                columns: [
                    { data: 'DT_Row_Index', searchable: false, "width": "15px", "className": "text-center"},
                    { data: 'order_code'},
                    { data: 'tele'},
                    { data: 'status_order'},
                    { data: 'ekspedition_total'},
                    { data: 'created_date'},
                    { data: 'updated_date'},
                ],
            });

            $('#start_date, #end_date').change(function(event) {
                (function(modal){ modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Mohon tunggu sebentar data sedang diproses...<br/></div>'); setTimeout(function(){ modal.hide() }, 3000) })();
                var detail_ekspedisi   = "{{ $detail_ekspedisi->name }}"
                var start_date = $('#start_date').val();
                var end_date = $('#end_date').val();
                dt.ajax.url(APP_URL + "/ekspedisi/filter/"+detail_ekspedisi+"/"+start_date+"/"+end_date).load();

                $.ajax({
                  url: APP_URL + "/ekspedisi/calculate/"+detail_ekspedisi+"/"+start_date+"/"+end_date,
                  method:'GET',
                  dataType:'json',
                  success:function(data) {
                    $('#profit_ekspedition').html('<div class="uk-alert uk-alert-success" data-uk-alert><a href="#" class="uk-alert-close uk-close"></a>Total Ekspedisi: '+data.total_ekspedition+' </div>');
                  }
                }) 
            });
        });
    </script>

@endsection