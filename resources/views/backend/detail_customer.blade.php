@extends('layouts.baselayout')

@section('css')
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection
@section('content')
	<div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
	    <div class="uk-width-large-1-1">
	        <div class="md-card">
	            <div class="user_heading user_heading_bg" style="background-image: url('{{ asset('templates/img/gallery/Image10.jpg') }}')">
	                <div class="bg_overlay">
	                    <div class="user_heading_avatar">
	                        <div class="thumbnail">
	                        	<?php
				                	if($customer->picture == ''){
				                		$src = asset('templates/img/no-image-available.png');
				                	} else {
				                		$src = asset('storage/img/'.$customer->picture);
				                	}
				                ?>
	                            <img src="{!! $src !!}" alt="{{ $customer->name }}"/>
	                        </div>
	                    </div>
	                    <div class="user_heading_content">
	                        <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate">{{ ucwords($customer->name) }}</span><span class="sub-heading">{{ $customer->email }}</span></h2>
	                        <ul class="user_stats">
	                            <li>
	                                <h4 class="heading_a">{{ $customer->tele }} <span class="sub-heading">Telepon</span></h4>
	                            </li>
	                            <?php
	                            	$customerCity = array_first($cities, function ($city, $index) use ($customer) {
	                                    return ($city['city_id'] == $customer->city && $city['province_id'] == $customer->province);
	                                });
	                                if ($customerCity) {
	                                    $city = $customerCity['city_name'];
	                                    $province = $customerCity['province'];
	                                }
	                            ?>
	                            <li>
	                                <h4 class="heading_a">{{ $city }} <span class="sub-heading">Kota</span></h4>
	                            </li>
	                            <?php
	                            	$subdistrict = selectall_subdistrict_by_city($customer->city, $customer->subdistrict, 1);
	                            	if($subdistrict == NULL){
	                            		$subdistrict = '-';
	                            	} else {
	                            		$subdistrict = $subdistrict['subdistrict_name'];
	                            	}
	                            ?>
	                            <li>
	                                <h4 class="heading_a">{{ $subdistrict }} <span class="sub-heading">Kecamatan</span></h4>
	                            </li>
	                            <li>
	                                <h4 class="heading_a">{{ $province }} <span class="sub-heading">Provinsi</span></h4>
	                            </li>
	                        </ul>
	                    </div>
	                </div>
	            </div>
	            <div class="user_content">
	                <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
	                    <li class="uk-active"><a href="#">Tentang</a></li>
	                    <li><a href="#">Riwayat Deposit</a></li>
	                </ul>
	                <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
	                    <li>
	                        <div class="uk-grid uk-margin-medium-top uk-margin-large-bottom" data-uk-grid-margin>
	                            <div class="uk-width-large-1-1">
	                                <h4 class="heading_c uk-margin-small-bottom">Informasi Kontak</h4>
	                                <ul class="md-list md-list-addon">
	                                    <li>
	                                        <div class="md-list-addon-element">
	                                            <i class="md-list-addon-icon material-icons">&#xE158;</i>
	                                        </div>
	                                        <div class="md-list-content">
	                                            <span class="md-list-heading">{{ $customer->email }}</span>
	                                            <span class="uk-text-small uk-text-muted">Email</span>
	                                        </div>
	                                    </li>
	                                    <li>
	                                        <div class="md-list-addon-element">
	                                            <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
	                                        </div>
	                                        <div class="md-list-content">
	                                            <span class="md-list-heading">{{ $customer->tele }}</span>
	                                            <span class="uk-text-small uk-text-muted">Telepon</span>
	                                        </div>
	                                    </li>
	                                    <li>
	                                        <div class="md-list-addon-element">
	                                            <i class="material-icons md-36">place</i>
	                                        </div>
	                                        <div class="md-list-content">
	                                            <span class="md-list-heading">{{ $customer->address }}</span>
	                                            <span class="uk-text-small uk-text-muted">Alamat</span>
	                                        </div>
	                                    </li>
	                                    <li>
	                                        <div class="md-list-addon-element">
	                                            <i class="material-icons md-36">local_post_office</i>
	                                        </div>
	                                        <div class="md-list-content">
	                                            <span class="md-list-heading">{{ $customer->zip }}</span>
	                                            <span class="uk-text-small uk-text-muted">Kode Pos</span>
	                                        </div>
	                                    </li>
	                                    <li>
	                                    	<?php
										        if($customer->deposit == 0){
										            $deposit = '-';
										        } else {
										            $deposit = number_format($customer->deposit, 0,',','.');
										        }
										    ?>
										    <div class="md-list-addon-element">
	                                            <i class="material-icons md-36">attach_money</i>
	                                        </div>
	                                        <div class="md-list-content">
	                                            <span class="md-list-heading">Rp. {{ $deposit }}</span>
	                                            <span class="uk-text-small uk-text-muted">Total Deposit</span>
	                                        </div>
	                                    </li>
	                                </ul>
	                            </div>
	                        </div>
	                    </li>
	                    <li>
					        <div class="md-card uk-margin-medium-bottom">
				                <div class="md-card-content">
				                    <table id="datatable_default" class="uk-table" cellspacing="0" width="100%">
				                        <thead>
				                            <tr>
				                            	<th>No.</th>
				                            	<th>Total</th>
				                                <th>Status</th>
				                                <th>Created</th>
				                                <th>Updated</th>
				                            </tr>
				                        </thead>
				                        <tfoot>
					                        <tr>
					                            <th>No.</th>
					                            <th>Total</th>
					                            <th>Status</th>
					                            <th>Created</th>
				                                <th>Updated</th>
					                        </tr>
				                        </tfoot>
				                        <tbody></tbody>
				                    </table>
				                </div>
				            </div>
	                    </li>
	                </ul>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('js')
	<!-- common functions -->
	<script src="{{ asset('templates/js/common.min.js') }}"></script>
	<!-- uikit functions -->
	<script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
	<!-- altair common functions/helpers -->
	<script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>

	<!-- page specific plugins -->
    <!-- datatables -->
    <script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <!-- datatables custom integration -->
    <script src="{{ asset('templates/js/custom/datatables/datatables.uikit.min.js') }}"></script>
    <!--  datatables functions -->
    <script src="{{ asset('templates/js/pages/plugins_datatables.min.js') }}"></script>\
    <script src="{{ asset('templates/js/pages/full_numbers_no_ellipses.js') }}"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
            var dt = $('#datatable_default').DataTable({
                orderCellsTop: true,
                stateSave: true,
                responsive: true,
                processing: true,
                serverside: true,
                searching: true,
                "autoWidth": true,
                lengthMenu: [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
                'pagingType': 'full_numbers_no_ellipses',
                ajax: {
                    url:  "{{ route('customer.show_deposit_customer', $customer->id) }}",
                    data: { '_token' : '{{ csrf_token() }}'},
                    type: 'POST',
                },
                columns: [
                    { data: 'DT_Row_Index', searchable: false, "width": "15px", "className": "text-center"},
                    { data: 'total'},
                    { data: 'status'},
                    { data: 'created_date'},
                    { data: 'updated_date'},
                ],
            });
        });
    </script>
@endsection