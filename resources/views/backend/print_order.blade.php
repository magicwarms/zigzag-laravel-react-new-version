<!DOCTYPE html>
<html>

<head>
    <title>Print Customer - {{ $order->name }} - Zigzag Online Shop</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{{ asset('assets_print/css/main.css') }}" />
    <noscript>
        <link rel="stylesheet" href="{{ asset('assets_print/css/noscript.css') }}" /></noscript>
</head>

<body class="is-preload">
    <!-- Wrapper -->
    <div id="wrapper">
        <!-- Main -->
        <div id="main">
            <!-- Two -->
            <section id="two" class="spotlights">
                <section>
                    <div class="content">
                        <div class="inner">
                            <div class="most-top">
                                <div class="logo">
                                    <div></div>
                                </div>
                                <div class="greeting">
                                    <h3>
                                        Waktu komplain 1x24 jam<br />
                                        <span>Harap foto barang yang rusak + kertas surat ini</span>
                                    </h3>
                                </div>
                            </div>
                            <div class="topping">
                                <div>
                                    <div>
                                        <header class="major">
                                            <h3 style="font-size: 35px !important; color: red;"><strong>{{ $order->name }}</strong></h3>
                                            <strong>{{ $order->tele }}</strong>
                                        </header>
                                        <br />
                                        <?php
                                        if ($order->jne_online_booking != '-') {
                                        ?>
                                            <header class="major order">
                                                <h3>Online Booking</h3>
                                                <strong style="color: red;">{{ strtoupper($order->jne_online_booking) }}</strong>
                                            </header>
                                            <br>
                                        <?php } ?>
                                    </div>
                                    <?php
                                    if ($order->dropshipper_name != '-') {
                                    ?>
                                        <header class="major">
                                            <h3 style="font-size: 25pt !important;">Pengirim</h3>
                                            <strong style="font-size: 35pt !important; color: red;">{{ $order->dropshipper_name }}</strong>
                                        </header>
                                    <?php } ?>
                                    <br>
                                    <header class="major address" style="width: 100%;">
                                        <h3>Alamat</h3>
                                        <strong>
                                            {{ $order->shipping_address }} <br>
                                            {{ $order->subdistrict_name }}, {{ $order->city_name }}, {{ $order->province_name }} <br>
                                            {{ $order->zip }}
                                        </strong>
                                    </header>
                                </div>
                            </div>
                            <ul>
                                <li class="courier">
                                    Kurir yang dipilih<br />
                                    <?php
                                    if ($order->ekspedition_code == 'sicepat') {
                                        $styler = 'style="color: red"';
                                    } else if ($order->ekspedition_code == 'jne') {
                                        $styler = 'style="color: blue"';
                                    } else if ($order->ekspedition_code == 'J&T') {
                                        $styler = 'style="color: purple"';
                                    } else {
                                        $styler = '';
                                    }
                                    ?>
                                    <strong <?php echo $styler; ?>>{{ strtoupper($order->ekspedition_code) }}</strong>
                                </li>
                                <li>
                                    Total ongkos kirim<br />
                                    <strong>Rp. {{ number_format($order->ekspedition_total, 0,',','.') }}</strong>
                                </li>
                                <li>
                                    Total berat<br />
                                    <strong>{{ number_format($order->total_weight, 0,',','.') }} grams</strong>
                                </li>
                                <li>
                                    Customer service<br />
                                    <strong>{{ strtoupper($order->customerService->name) }}</strong>
                                </li>
                            </ul>
                            <div class="items">
                                <header>
                                    <h4>Belanjaan kamu</h4>
                                </header>
                                <ul>
                                    @foreach($order->orderDetails as $detail)
                                    <li>
                                        <span>
                                            <strong style="font-size: 35pt !important; color: red;">{{ $detail->product_qty }}x</strong>
                                            <strong>{{ changeCodeProduct($detail->product_name) }}</strong>, warna <strong>{{ $detail->product_color }}</strong>,
                                            @if($detail->product_size > 0)
                                            size
                                            <strong>{{ $detail->product_size }}</strong>
                                            @endif
                                        </span>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <footer>
                            <header class="major order">
                                Kode order
                                <h3>{{ $order->order_code }} - {{ indonesian_date($order->created_date) }}</h3>
                            </header>
                        </footer>
                    </div>
                </section>
            </section>
        </div>
    </div>
</body>

</html>
