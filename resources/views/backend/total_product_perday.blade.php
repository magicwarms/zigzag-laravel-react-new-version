@extends('layouts.baselayout')

@section('css')
    <!-- additional styles for plugins -->
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')
<div class="uk-grid uk-grid-width-large-1-2 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin id="perday_count"></div>

<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large-1-1 uk-margin-top">
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid uk-margin-bottom" data-uk-grid-margin>
                    <div class="uk-width-medium-1-4">
                      <input class="md-input" name="startdate" type="text" id="startdate" data-uk-datepicker="{format:'DD.MM.YYYY'}" value="{{ date('d.m.Y') }}">
                    </div>
                </div>
                <div class="uk-overflow-container">
                <table id="dt_tableExport_wrapper" class="uk-table" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                        <th class="number-order">No.</th>
                        <th>Nama</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Warna</th>
                        <th>Size</th>
                        <th>Total Harga</th>
                        <th>Return Stok</th>
                        <th>Created</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th class="number-order">No.</th>
                        <th>Nama</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Warna</th>
                        <th>Size</th>
                        <th>Total Harga</th>
                        <th>Return Stok</th>
                        <th>Created</th>
                    </tr>
                  </tfoot>
                  <tbody></tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <!-- datatable -->
    <script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/dataTables.buttons.js') }}"></script>
    <script src="{{ asset('templates/js/custom/datatables/buttons.uikit.js') }}"></script>
    <script src="{{ asset('bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('bower_components/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.colVis.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.html5.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.print.js') }}"></script>
    <script src="{{ asset('templates/js/custom/datatables/datatables.uikit.min.js') }}"></script>
    <script src="{{ asset('templates/js/pages/plugins_datatables.min.js') }}"></script>
    <!-- page specific plugins -->
    <script src="{{ asset('templates/js/pages/full_numbers_no_ellipses.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function(){
            var APP_URL = {!! json_encode(url('/')) !!}
            var startdate = $('#startdate').val();
            var dt = $('#dt_tableExport_wrapper').DataTable({
            orderCellsTop: true,
            responsive: true,
            processing: true,
            serverSide: true,
            scrollX: true,
            searching: true,
            "autoWidth": true,
            lengthMenu: [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
            'pagingType': 'full_numbers_no_ellipses',
            ajax: {
                url:  APP_URL + "/perday/total_product_perday/"+startdate,
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
                { data: 'DT_Row_Index', searchable: false, "width": "15px", "className": "text-center"},
                { data: 'product_name'},
                { data: 'product_qty'},
                { data: 'product_price'},
                { data: 'product_color'},
                { data: 'product_size'},
                { data: 'product_total_price'},
                { data: 'is_return'},
                { data: 'created_date'},
            ],
        });

        $('#startdate').change(function(event) {
            var startdate = $('#startdate').val();
            dt.ajax.url(APP_URL + "/perday/total_product_perday/"+startdate).load();
            $.ajax({
              url: APP_URL + "/perday/calculate_order_product/"+startdate,
              method:'GET',
              dataType:'json',
              beforeSend: function(){
                (function(modal){ modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Mohon tunggu sebentar data sedang diproses...<br/></div>'); setTimeout(function(){ modal.hide() }, 3000) })();
              },
              success:function(data) {
                
                $('#perday_count').html('<div><div class="md-card"><div class="md-card-content" style="background-color:#711b58; !important"><div class="uk-float-right uk-margin-top uk-margin-small-right"></div><span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Jumlah Produk Tanggal '+startdate+'</div></span><h2 class="uk-margin-remove uk-text-contrast">'+data.total_order_perday+'</h2></div></div></div><div><div class="md-card"><div class="md-card-content" style="background-color:#CE3D19; !important"><div class="uk-float-right uk-margin-top uk-margin-small-right"></div><span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Total Profit Tanggal '+startdate+'</div></span><h2 class="uk-margin-remove uk-text-contrast">Rp. '+data.sum_total_perday+'</h2></div></div></div>');
              }
            })
        });
    });
    </script>
@endsection