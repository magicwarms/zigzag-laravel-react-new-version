@extends('layouts.baselayout')

@section('css')
    <!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')
<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }" class="uk-margin-bottom">
	<h1>{{ $detail->name }} , {{ $detail->tele }} </h1>
	<span class="uk-text-muted uk-text-upper"><b>{{ $detail->order_code }}  ({{ date('d F Y H:i', strtotime($detail->created_date)) }} ) - <span id="status_will_change">{{ status_order($detail->status_order) }}</span></span></b>
</div>

<div class="uk-grid uk-grid-medium" data-uk-grid-margin>
	<div class="uk-width-xLarge-2-10 uk-width-large-3-10">
		<div class="md-card">
			<div class="md-card-toolbar">
				<h3 class="md-card-toolbar-heading-text">
					Informasi Order  
				</h3>
				<a class="md-btn md-btn-flat md-btn-flat-primary md-btn-wave waves-effect waves-button" href="{{ route('order.print',['order_id' => $detail->id]) }}" target="_blank" onClick="reload_delay_page()">
                    <i class="material-icons">print</i>
                    Cetak Order
                </a>
			</div>
			<div class="md-card-content">
				<ul class="md-list">
					<li>
						<div class="md-list-content">
							<span class="uk-text-small uk-text-muted uk-display-block">Destinasi Order</span>
							<span class="md-list-heading uk-text-medium uk-text-success">{{ $detail->province_name }} - {{ $detail->city_name }} - {{ $detail->subdistrict_name }} </span>
						</div>
					</li>
					<li>
						<div class="md-list-content">
							<span class="uk-text-small uk-text-muted uk-display-block">Alamat Order</span>
							<span class="md-list-heading uk-text-medium uk-text-success">{{ $detail->shipping_address }} </span>
						</div>
					</li>
					<li>
						<div class="md-list-content">
							<span class="uk-text-small uk-text-muted uk-display-block">Kode Order</span>
							<span class="md-list-heading uk-text-medium"><i>{{ $detail->order_code }} </i></span>
						</div>
					</li>
					<li>
						<div class="md-list-content">
							<span class="uk-text-small uk-text-muted uk-display-block">Kode Unik Order</span>
							<span class="md-list-heading uk-text-medium"><i>{{ $detail->unique_code }} </i></span>
						</div>
					</li>
                <li>
                    <div class="md-list-content">
                        <span class="uk-text-small uk-text-muted uk-display-block">Ekspedisi</span>
                        <span class="md-list-heading uk-text-medium"><i>{{ strtoupper($detail->ekspedition_company) }} - {{ $detail->ekspedition_remark }} - Rp. {{ number_format($detail->ekspedition_total, 0,',','.') }}</i></span>
                    </div>
                </li>
                <li>
                    <div class="md-list-content">
                        <span class="uk-text-small uk-text-muted uk-display-block">JOB (JNE Online Booking)</span>
                        <span class="md-list-heading uk-text-medium"><i>{{  
						strtoupper($detail->jne_online_booking) ? strtoupper($detail->jne_online_booking) : '-' }}</i></span>
                    </div>
                </li>
                <li>
                    <div class="md-list-content">
                        <span class="uk-text-small uk-text-muted uk-display-block">No. Resi
                        <?php
                        	if($detail->resi_order != ''){
                        ?>
                        	<a class="uk-text-danger uk-text-upper uk-text-bold" href="{{ route('order.delete_resi_order',['order_id' => $detail->id]) }}" onClick="reload_delay_page()">
                        		<i class="material-icons">delete</i>
			                     Hapus Resi?
			                </a>
			            <?php } ?>
                        </span>
                        <span class="md-list-heading uk-text-medium"><i>{{ ($detail->resi_order == "") ? "-":$detail->resi_order }}</i></span>
                    </div>
                </li>
                <li>
                    <div class="md-list-content">
                        <span class="uk-text-small uk-text-muted uk-display-block">Keterangan Pembatalan Order</span>
                        <span class="md-list-heading uk-text-medium"><i>{{ ($detail->remark_cancel_order == "") ? "-":$detail->remark_cancel_order }}</i></span>
                    </div>
                </li>
                <li>
					<div class="md-list-content">
						<span class="uk-text-small uk-text-muted uk-display-block">Total Berat</span>
						<span class="md-list-heading uk-text-medium uk-text-primary">{{ number_format($detail->total_weight, 0,',','.') }} Gram</span>
					</div>
				</li>
				<li>
					<div class="md-list-content">
						<span class="uk-text-small uk-text-muted uk-display-block">Diskon</span>
						<span class="md-list-heading uk-text-medium uk-text-primary">{{ number_format($detail->discount, 0,',','.') }}</span>
					</div>
				</li>
                <li>
					<div class="md-list-content">
						<span class="uk-text-small uk-text-muted uk-display-block">Subtotal Order</span>
						<?php if($detail->is_partner == 1 AND Auth::user()->level != 1) { ?>
						<span class="md-list-heading uk-text-medium uk-text-primary">Rp. - </span>
						<?php } else { ?>
						<span class="md-list-heading uk-text-medium uk-text-primary">Rp. {{ number_format($detail->subtotal_order, 0,',','.') }}</span>
						<?php } ?>
					</div>
				</li>
				<li>
					<div class="md-list-content">
						<span class="uk-text-small uk-text-muted uk-display-block">Grandtotal Order</span>
						<?php if($detail->is_partner == 1 AND Auth::user()->level != 1) { ?>
						<span class="md-list-heading uk-text-medium uk-text-primary">Rp. - </span>
						<?php } else { ?>
						<span class="md-list-heading uk-text-medium uk-text-primary">Rp. {{ number_format($detail->grandtotal_order, 0,',','.') }}</span>
						<?php } ?>
					</div>
				</li>
				<li>
                    <div class="md-list-content">
                        <span class="uk-text-small uk-text-muted uk-display-block">Catatan</span>
                        <span class="md-list-heading uk-text-medium"><i>{{ ($detail->notes == "") ? "-": $detail->notes }}</i></span>
                    </div>
                </li>
				</ul>
			</div>
		</div>
	</div>
	<div class="uk-width-xLarge-8-10  uk-width-large-7-10">
		<div class="md-card">
			<div class="md-card-toolbar uk-grid-width-large-1-3">
				<h3 class="md-card-toolbar-heading-text">
					Detail Pelanggan
				</h3>
			</div>
			<div class="md-card-content large-padding">
				<div class="uk-grid uk-grid-divider uk-grid-medium">
					<div class="uk-width-large-1-2">
						<div class="uk-grid uk-grid-small">
							<div class="uk-width-large-1-3">
								<span class="uk-text-muted uk-text-small">Nama</span>
							</div>
							<div class="uk-width-large-2-3">
								<span class="uk-text-large uk-text-middle">{{ $detail->name }}</span>
							</div>
						</div>
						<hr class="uk-grid-divider">
						<div class="uk-grid uk-grid-small">
							<div class="uk-width-large-1-3">
								<span class="uk-text-muted uk-text-small">Email</span>
							</div>
							<div class="uk-width-large-2-3">
								<span class="uk-text-large uk-text-middle"><a href="mailto:{{ $detail->email }} ">{{ $detail->email }} </a></span>
							</div>
						</div>
						<hr class="uk-grid-divider">
						<div class="uk-grid uk-grid-small">
							<div class="uk-width-large-1-3">
								<span class="uk-text-muted uk-text-small">No. Telepon/HP</span>
							</div>
							<div class="uk-width-large-2-3">
								{{ $detail->tele }} 
							</div>
						</div>
						<hr class="uk-grid-divider uk-hidden-large">
					</div>
                <div class="uk-width-large-1-2">
                	<div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Customer Login</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle"><a href="{{ route('customer.detail', [$detail->customer_id, str_slug($detail->customer_login, '-')]) }}"><b>{{ $detail->customer_login }} </b></a></span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Alamat</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle">{{ $detail->shipping_address }} </span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Kota - Provinsi</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle">{{ $detail->province_name }} - {{ $detail->city_name }} </span>
                        </div>
                    </div>
                </div>
				</div>
			</div>
		</div>
		<div class="md-card">
	        <div class="md-card-toolbar">
	            <h3 class="md-card-toolbar-heading-text">
	                Detail Order
	            </h3>
	        </div>
	        <div class="md-card-content large-padding">
	            <div class="uk-grid" data-uk-grid-margin>
	                <div class="uk-width-medium-1-1">
	                    <table class="uk-table">
	                        <thead>
	                            <tr>
	                                <th>Produk</th>
	                                <th>Warna</th>
	                                <th>Size</th>
	                                <th>Qty</th>
	                                <th>Harga</th>
	                                <th>Total Harga</th>
	                                <th>Kembalikan</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        <?php
	                            foreach ($order_details as $order_detail) {
	                        ?>
	                            <tr>
	                                <td><a href="{{ route('product.detail', [$order_detail->product_id, str_slug($order_detail->product_name, '-')]) }}">{{ $order_detail->product_name }}</a></td>
	                                <td>{{ $order_detail->product_color }}</td>
	                                <td>{{ $order_detail->product_size == '0' ? '-' : $order_detail->product_size }}</td>
	                                <td>{{ $order_detail->product_qty }} </td>
	                                <?php if($detail->is_partner == 1 AND Auth::user()->level != 1) { ?>
	                                <td> - </td>
	                                <td> - </td>
	                                <?php } else { ?>
	                                <td>Rp. {{ number_format($order_detail->product_price, 0,',','.') }}</td>
	                                <td>Rp. {{ number_format($order_detail->product_total_price, 0,',','.') }} &nbsp;
	                                <?php } ?>
	                                <td>
	                                <?php 
	                                if($detail->status_order != 9) { 
	                                	if($order_detail->is_return == 0){
	                                ?>
	                                	<a class="md-btn md-btn-danger md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light refund_stock" data-id="{{ $order_detail->order_id }}" data-productmore="{{ $order_detail->product_more_detail_id }}" href="#">
                                	<i class="uk-icon-cart-arrow-down"></i> Kembalikan Stok</a>
                                		<?php } else { ?>
                                		<a class="md-btn md-btn-success md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light" href="#">
                                		<i class="uk-icon-cart-arrow-down"></i>Stok sudah dikembalikan</a>
                                		<?php } ?>
                                	<?php } ?>
                                	</td>
	                            </tr>
	                        <?php } ?>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<?php if($detail->status_order != 9) { ?>
<div class="md-fab-wrapper">
	<div class="md-fab md-fab-accent md-fab-sheet">
	   <div class="md-fab-wrapper md-fab-speed-dial-horizontal">
            <a class="md-fab md-fab-primary" href="javascript:void(0)"><i class="uk-icon-refresh uk-icon-medium uk-icon-spin"></i></a>
            <div class="md-fab-wrapper-small">
                <a data-id="{{ $detail->id }}" data-status="5" data-uk-tooltip="{cls:'long-text'}" title="Proses Gudang" class="update_status_order md-fab md-fab-small md-fab-warning"><i class="material-icons">&#xE863;</i></a>
                
                <a data-id="{{ $detail->id }}" data-status="6" data-uk-tooltip="{cls:'long-text'}"data-uk-tooltip title="Barang Terkirim" class="update_status_order md-fab md-fab-small md-fab-primary"><i class="material-icons">local_shipping</i></a>
			<?php if($detail->status_order != 10) { ?>
                <a data-id="{{ $detail->id }}" data-status="7" data-uk-tooltip="{cls:'long-text'}" title="Batal Pesanan" class="update_status_order md-fab md-fab-small md-fab-danger"><i class="material-icons">remove_shopping_cart</i></a>
			<?php } ?>
                <a data-id="{{ $detail->id }}" data-status="9" data-uk-tooltip="{cls:'long-text'}" title="Pesanan Selesai" class="update_status_order md-fab md-fab-small md-fab-success"><i class="material-icons">tag_faces</i></a>
            </div>
        </div>
	</div>
</div>
<?php } ?>
@endsection

@section('js')
	<div class="uk-modal" id="resi_order">
        <div class="uk-modal-dialog">
            <form class="form_resi_order" class="uk-form-stacked">
                <div class="uk-modal-header">Input Nomor Resi</div>
                <input type="hidden" id="id" name="id" value="{{ $detail->id }}">
                <div id="notif-error"></div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 uk-margin-top">
                      <label>Nomor Resi</label>
                      <br>
                      <input type="text" name="resi_order" id="resi_order" class="md-input label-fixed"/>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <input type="submit" value="SUBMIT" class="md-btn md-btn-danger" id="show_preloader_md">
                </div>
            </form>
        </div>
    </div>
    <div class="uk-modal" id="cancel_order">
        <div class="uk-modal-dialog">
            <form class="form_cancel_order" class="uk-form-stacked">
                <div class="uk-modal-header">Masukan alasan pembatalan order</div>
                <input type="hidden" id="id" name="id" value="{{ $detail->id }}">
                <div id="notif-error-cancel"></div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 uk-margin-top">
                      <label>Alasan</label>
                      <br>
                      <textarea id="remark_cancel_order" cols="30" rows="4" name="remark_cancel_order" class="md-input label-fixed"></textarea>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <input type="submit" value="SUBMIT" class="md-btn md-btn-danger" id="show_preloader_md">
                </div>
            </form>
        </div>
    </div>
    <!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <script src="{{ asset('templates/js/pages/components_preloaders.min.js') }}"></script>
    <script type="text/javascript">
    	function reload_delay_page() {
    		setTimeout(function(){
			    location.reload();
			},3000);
    	}
    	$(document).on('click', '.update_status_order', function (e) {
          var id = $(this).data('id');
          var status = $(this).data('status');
          UIkit.modal.confirm("Apakah kamu yakin akan mengubah status order ini?", function(){
            var APP_URL = {!! json_encode(url('/')) !!}
            $.ajax({
	            url: APP_URL + "/order/change_status/"+id+"/"+status,
	            method:'GET',
	            headers: {
	            	'X-CSRF-Token': '{{ csrf_token() }}'
	          	},
	            dataType:'json',
	            cache: false,
            	success:function(result) {
                altair_helpers.content_preloader_hide();
                $('#status_will_change').html(result.data)
	                if(result.status=='success'){
	                    UIkit.notify({
	                        message: result.msg,
	                        status: 'success',
	                        timeout: 8000,
	                        pos: 'top-center'
	                    });
	                } else  {
	                    UIkit.notify({
	                        message: result.msg,
	                        status: 'danger',
	                        timeout: 8000,
	                        pos: 'top-center'
	                    });
	                }
		            if(status == 6) {
		            	UIkit.modal("#resi_order").show();
		            } else if (status == 7) {
		            	UIkit.modal("#cancel_order").show();
		            }
              	}
            })
          });
          e.preventDefault(); 
        });
        $(".form_resi_order").on('submit',function(event) {
		        $.ajax({
		            type: 'PUT',
		            headers: {
		                'X-CSRF-Token': $('input[name="_token"]').val()
		            },
		            url: "{{ route('order.update_resi_order') }}",
		            data: $(this).serialize(),
		            dataType: 'JSON',
		            cache: false,
		            beforeSend: function(){
		              (function(modal){ modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Mohon tunggu sebentar data sedang diproses...<br/></div>'); setTimeout(function(){ modal.hide() }, 2000) })();
		            },
	            success: function(result) {
		            UIkit.modal("#resi_order").hide();
		            $('.form_resi_order')[0].reset();
		            location.reload();
		            if(result.status=='success'){
	                    UIkit.notify({
	                        message: result.msg,
	                        status: 'success',
	                        timeout: 8000,
	                        pos: 'top-center'
	                    });
	                } else  {
	                    UIkit.notify({
	                        message: result.msg,
	                        status: 'danger',
	                        timeout: 8000,
	                        pos: 'top-center'
	                    });
	                }
	            },
	            error: function (result) {
			        $('#notif-error').html('<p class="uk-text-danger">Resi Order ini sudah anda inputkan sebelumnya.</p>')
		            altair_helpers.content_preloader_hide();
	         	}
	        });
      		event.preventDefault();
    	});
    	$(".form_cancel_order").on('submit',function(event) {
		        $.ajax({
		            type: 'PUT',
		            headers: {
		                'X-CSRF-Token': $('input[name="_token"]').val()
		            },
		            url: "{{ route('order.cancel_order') }}",
		            data: $(this).serialize(),
		            dataType: 'JSON',
		            cache: false,
		            beforeSend: function(){
		              (function(modal){ modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Mohon tunggu sebentar data sedang diproses...<br/></div>'); setTimeout(function(){ modal.hide() }, 2000) })();
		            },
	            success: function(result) {
		            UIkit.modal("#cancel_order").hide();
		            $('.form_cancel_order')[0].reset();
		            location.reload();
		            if(result.status=='success'){
	                    UIkit.notify({
	                        message: result.msg,
	                        status: 'success',
	                        timeout: 8000,
	                        pos: 'top-center'
	                    });
	                } else  {
	                    UIkit.notify({
	                        message: result.msg,
	                        status: 'danger',
	                        timeout: 8000,
	                        pos: 'top-center'
	                    });
	                }
	            },
	            error: function (result) {
			        $('#notif-error-cancel').html('<p class="uk-text-danger">Alasan Order harus terisi.</p>')
		            altair_helpers.content_preloader_hide();
	         	}
	        });
	      event.preventDefault();
	    });
	    $(document).on('click', '.refund_stock', function (e) {
          var id = $(this).data('id');
          var product_more = $(this).data('productmore');
          UIkit.modal.confirm("Apakah kamu yakin akan mengembalikan stok produk ini?", function(){
            var APP_URL = {!! json_encode(url('/')) !!}
            $.ajax({
              url: APP_URL + "/order/return_stock/"+id+"/"+product_more,
              method:'GET',
              dataType:'json',
              beforeSend: function(){
                altair_helpers.content_preloader_show('md');
              },
              success:function(result) {
                if(result.status=='success'){
                    UIkit.notify({
                        message: result.msg,
                        status: 'success',
                        timeout: 8000,
                        pos: 'top-center'
                    });
                    location.reload();
                } else  {
                    UIkit.notify({
                        message: result.msg,
                        status: 'danger',
                        timeout: 10000,
                        pos: 'top-center'
                    });
                }
                altair_helpers.content_preloader_hide();
              }
            }) 
          });
          e.preventDefault(); 
        });
    </script>
@endsection