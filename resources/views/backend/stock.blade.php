@extends('layouts.baselayout')

@section('css')
    <!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')
<div class="uk-grid">
    <div class="uk-width-1-1">
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a">Laporan Stock</h3>
                <br>
                <form method="POST" action="{{ route('stock.export') }}">
                {{ csrf_field() }}
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-1-1 uk-width-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_start">Mulai Tanggal</label>
                            <input name="start_date" class="md-input" type="text" id="uk_dp_start" required="true" value="{{ date('d.m.Y') }}" readonly="true" disabled="true">
                        </div>
                    </div>
                    <div class="uk-width-large-1-1 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon">
                                <input type="submit" value="GENERATE" class="md-btn md-btn-primary">
                            </span>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <!-- jquery ui -->
    <script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- ionrangeslider -->
    <script src="{{ asset('bower_components/ion.rangeslider/js/ion.rangeSlider.min.js') }}"></script>
    <!--  forms advanced functions -->
    <script src="{{ asset('templates/js/pages/forms_advanced.min.js') }}"></script>
@endsection