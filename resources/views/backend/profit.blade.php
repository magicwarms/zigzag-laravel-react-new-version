@extends('layouts.baselayout')

@section('css')
    <!-- additional styles for plugins -->
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')
<div class="uk-grid uk-grid-width-large-1-2 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin id="profit_count"></div>

<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large-1-1 uk-margin-top">
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid uk-margin-bottom" data-uk-grid-margin>
                    <div class="uk-width-medium-1-4">
                      <input class="md-input" name="start_date" type="text" id="start_date" data-uk-datepicker="{format:'DD.MM.YYYY'}" value="{{ date('d.m.Y') }}">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <input class="md-input" name="end_date" type="text" id="end_date" data-uk-datepicker="{format:'DD.MM.YYYY'}" value="{{ date('d.m.Y') }}">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <select id="acc_type" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Filter Tipe Akun">
                        <option disabled="true" selected="true">Filter Tipe Akun...</option>
                        <option value="0">Customer</option>
                        <option value="1">Partner</option>
                    </select>
                    </div>
                    <div class="uk-width-medium-1-2" id="profit_cs"></div>
                </div>
                <div class="uk-overflow-container">
                <table id="dt_tableExport_wrapper" class="uk-table" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                        <th class="number-order">No.</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Kode Order</th>
                        <th>Telepon</th>
                        <th>Status</th>
                        <th>Grandtotal</th>
                        <th>Created</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th class="number-order">No.</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Kode Order</th>
                        <th>Telepon</th>
                        <th>Status</th>
                        <th>Grandtotal</th>
                        <th>Created</th>
                    </tr>
                  </tfoot>
                  <tbody></tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <!-- datatable -->
    <script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/dataTables.buttons.js') }}"></script>
    <script src="{{ asset('templates/js/custom/datatables/buttons.uikit.js') }}"></script>
    <script src="{{ asset('bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('bower_components/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.colVis.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.html5.js') }}"></script>
    <script src="{{ asset('bower_components/datatables-buttons/js/buttons.print.js') }}"></script>
    <script src="{{ asset('templates/js/custom/datatables/datatables.uikit.min.js') }}"></script>
    <script src="{{ asset('templates/js/pages/plugins_datatables.min.js') }}"></script>
    <!-- page specific plugins -->
    <script src="{{ asset('templates/js/pages/full_numbers_no_ellipses.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function(){
            var APP_URL = {!! json_encode(url('/')) !!}
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            var acc_type = $('select#acc_type').val();
            var dt = $('#dt_tableExport_wrapper').DataTable({
            orderCellsTop: true,
            responsive: true,
            "processing": true,
            "serverSide": true,
            scrollX: true,
            searching: false,
            "autoWidth": false,
            paging: false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    pageSize: 'A4',
                    exportOptions: {
                      columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
                    }
                },
                {
                    extend: 'pdf',
                    title: 'Laporan Profit Penjualan',
                    customize: function(dt) {
                        dt.pageMargins = [ 10, 10, 10, 10 ]; 
                    },
                    orientation: 'potrait',
                    pageSize: 'A4',
                    exportOptions: {
                      columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
                    }
                },
            ],
            lengthMenu: [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
            'pagingType': 'full_numbers_no_ellipses',
            ajax: {
                url:  APP_URL + "/profit/filter/"+start_date+"/"+end_date+"/"+acc_type,
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
                { data: 'DT_Row_Index', searchable: false, "width": "15px", "className": "text-center"},
                { data: 'name'},
                { data: 'email'},
                { data: 'order_code'},
                { data: 'tele'},
                { data: 'status_order'},
                { data: 'grandtotal_order'},
                { data: 'created_date'},
            ],
        });

        $('#start_date, #end_date, #acc_type').change(function(event) {
            (function(modal){ modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Mohon tunggu sebentar data sedang diproses...<br/></div>'); setTimeout(function(){ modal.hide() }, 800) })();
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            var acc_type = $('#acc_type').val();
            acc_type = (acc_type=="") ? acc_type = 0 : acc_type;
            dt.ajax.url(APP_URL + "/profit/filter/"+start_date+"/"+end_date+"/"+acc_type).load();
            $.ajax({
              url: APP_URL + "/profit/calculate/"+start_date+"/"+end_date+"/"+acc_type,
              method:'GET',
              dataType:'json',
              success:function(data) {
                $('#profit_count').html('<div><div class="md-card"><div class="md-card-content" style="background-color:#711b58; !important"><div class="uk-float-right uk-margin-top uk-margin-small-right"></div><span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Jumlah Order Berhasil</div></span><h2 class="uk-margin-remove uk-text-contrast">'+data.total_order+'</h2></div></div></div><div><div class="md-card"><div class="md-card-content" style="background-color:#CE3D19; !important"><div class="uk-float-right uk-margin-top uk-margin-small-right"></div><span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Total Profit '+start_date+' - '+end_date+'</div></span><h2 class="uk-margin-remove uk-text-contrast">Rp. '+data.total_profit+'</h2></div></div></div>');
              }
            })
        });
    });
    </script>
@endsection