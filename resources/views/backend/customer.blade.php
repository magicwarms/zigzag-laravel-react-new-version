@extends('layouts.baselayout')

@section('css')
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')
<h3 class="heading_b uk-margin-bottom">Daftar Pelanggan</h3>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-2">
                <form action="{{ route('customer.searching') }}" method="GET">
                    <label for="customer_list_search">Cari... (min. 3 karakter.)</label>
                    <input name="search" class="md-input" type="search" id="customer_list_search" required="true" value="{{ request('search') ? request('search') : '' }}" />
                </form>
            </div>
            <div class="uk-width-medium-1-2">
                <form action="{{ route('customer.filter') }}" method="POST">
                    {{ csrf_field() }}
                    <ul id="contact_list_filter" class="uk-subnav uk-subnav-pill uk-margin-remove">
                        <li>
                            {{ Form::select('customer_service_id', $customer_services, null, array('class' =>'md-input','required')) }}
                        </li>
                        <li>
                            <select class="md-input" required="" name="status">
                                <option selected="selected" value="" disabled="true">Pilih Status</option>
                                <option value="0" {{ $status == 0 ? 'selected' : '' }}>Non-Aktif</option>
                                <option value="1" {{ $status == 1 ? 'selected' : '' }}>Aktif</option>
                            </select>
                        </li>
                        <li>
                            <div class="uk-form-row">
                                <span class="uk-input-group-addon">
                                <input type="submit" value="FILTER" class="md-btn md-btn-primary">
                             </span>
                           </div>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
<?php if($searching != 'empty' AND $searching != 'index' AND $searching != 'filter_empty') { ?>
<h3 class="uk-text-left">Ditemukan {{ $count_search }} data <i class="material-icons md-24">check</i></h3>
<div class="uk-grid uk-grid-medium uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4" id="list_grid">

@foreach ($searching as $search)
@php
    if($search->deposit == 0){
        $deposit = '-';
    } else {
        $deposit = number_format($search->deposit, 0,',','.');
    }
    if($search->status == 1){
        $word_status = 'Active';
        $status = '0';
    } else {
        $word_status = 'Non-Aktif';
        $status = '1';
    }

    if($search->acc_type == 1){
        $acctype = '<span class="uk-badge uk-badge-danger" style="float: left;">Reguler</span>';
    } else if($search->acc_type == 2){
        $acctype = '<span class="uk-badge uk-badge-primary" style="float: left;">VIP</span>';
    } else {
        $acctype = '<span class="uk-badge uk-badge-warning" style="float: left;">Partner</span>';
    }
@endphp
<div class="uk-margin-medium-top">
    <div class="md-card">
        <div class="md-card-head uk-text-center uk-position-relative">
            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">{{ $word_status }}</div>
            @php
                if($search->picture == ''){
                    $src = asset('templates/img/no-image-available.png');
                } else {
                    $src = asset('storage/img/'.$search->picture);
                }
            @endphp
            <img class="md-card-head-img" src="{!! $src !!}" alt="{{ $search->name }}"/>
        </div>
        <div class="md-card-content">
            <h4 class="heading_c uk-margin-bottom">{{ ucwords($search->name) }}</h4>
                <ul class="md-list">
                    <li>
                        <div class="md-list-content">
                            <span class="md-list-heading">Deposit</span>
                            <span class="uk-text-small uk-text-muted uk-text-truncate">
                            Rp. {{ $deposit }}</span>
                        </div>
                    </li>
                    <li>
                        <div class="md-list-content">
                            <span class="md-list-heading">Nama</span>
                            <span class="uk-text-small uk-text-muted uk-text-truncate">{{ $search->name }}</span>
                        </div>
                    </li>
                    <li>
                        <div class="md-list-content">
                            <span class="md-list-heading">Email</span>
                            <span class="uk-text-small uk-text-muted uk-text-truncate">{{ strtolower($search->email) }} </span>
                        </div>
                    </li>
                    <li>
                        <div class="md-list-content">
                            <span class="md-list-heading">Alamat</span>
                            <span class="uk-text-small uk-text-muted uk-text-truncate">{{ $search->address }}</span>
                        </div>
                    </li>
                    <li>
                        <div class="md-list-content">
                            <span class="md-list-heading">Kota/Provinsi</span>
                            <span class="uk-text-small uk-text-muted uk-text-truncate">
                            @php
                                $searchCity = array_first($cities, function ($city, $index) use ($search) {
                                    return ($city['city_id'] == $search->city && $city['province_id'] == $search->province);
                                });
                                if ($searchCity) {
                                    echo $searchCity['city_name'] .' - '. $searchCity['province'];
                                }
                            @endphp
                            </span>
                        </div>
                    </li>
                    <li>
                        <div class="md-list-content">
                            <span class="md-list-heading">Tipe Akun</span>
                            {!! $acctype !!}
                        </div>
                    </li>
                    <li>
                        <div class="md-list-content">
                            <span class="md-list-heading">Terdaftar pada</span>
                            {{ date('d F Y H:i:s', strtotime($search->created_date)) }}
                        </div>
                    </li>
                    <li>
                        <div class="md-list-content">
                            <span class="md-list-heading">Customer Service</span>
                            <span class="uk-text-upper uk-text-muted uk-text-truncate uk-text-bold">{{ $search->customer_service_name }}</span>
                        </div>
                    </li>
                </ul>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <a class="md-btn md-btn-small md-btn-success change_status_customer" data-id="{{ $search->id }}" data-status="{{ $status }}" href="#">Ganti Status</a>
                </div>
                <div class="uk-width-medium-1-1">
                    <a class="md-btn md-btn-small md-btn-primary" href="{{ route('customer.detail', [$search->id, str_slug($search->name, '-')]) }}">Detail</a>
                </div>
                <div class="uk-width-medium-1-1">
                    <a href="#change_acc_type" id="change_acc_type" class="md-btn md-btn-small md-btn-warning" data-id="{{ $search->id }}" data-uk-modal="{target:"#change_acc_type"}">Ganti Tipe Akun</a>
                </div>
                <div class="uk-width-medium-1-1">
                    <a href="#change_cs" id="change_cs" class="md-btn md-btn-small md-btn-danger" data-id="{{ $search->id }}" data-uk-modal="{target:"#change_cs"}">Ganti Customer Service</a>
                </div>
                <?php if(Auth::user()->id == 1 OR Auth::user()->id == 11 OR Auth::user()->id == 13) { ?>
                <div class="uk-width-medium-1-1">
                    <a onclick="delete_customer({{ $search->id }})" href="#" class="md-btn md-btn-small md-btn-danger">Delete</a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
@endforeach
</div>
<?php } else if ($searching == 'empty') { ?>
<h3 class="heading_b uk-text-center grid_no_results">Tidak ada data</h3>
<?php } else if ($searching == 'index') { ?>
<div class="uk-grid uk-grid-medium uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4" id="list_grid">
@foreach ($customers as $customer)
    @php
        if($customer->deposit == 0){
            $deposit = '-';
        } else {
            $deposit = number_format($customer->deposit, 0,',','.');
        }
        if($customer->status == 1){
            $word_status = 'Active';
            $status_span = '<span class="uk-badge uk-badge-success" style="float: left;">Aktif</span>';
            $status = '0';
        } else {
            $word_status = 'Non-Aktif';
            $status_span = '<span class="uk-badge uk-badge-danger" style="float: left;">Non-Aktif</span>';
            $status = '1';
        }
        if($customer->acc_type == 1){
            $acctype = '<span class="uk-badge uk-badge-danger" style="float: left;">Reguler</span>';
        } else if($customer->acc_type == 2){
            $acctype = '<span class="uk-badge uk-badge-primary" style="float: left;">VIP</span>';
        } else {
            $acctype = '<span class="uk-badge uk-badge-warning" style="float: left;">Partner</span>';
        }
    @endphp
    <div class="uk-margin-medium-top">
        <div class="md-card">
            <div class="md-card-head uk-text-center uk-position-relative">
                <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">{{ $word_status }}</div>
                @php
                    if($customer->picture == ''){
                        $src = asset('templates/img/no-image-available.png');
                    } else {
                        $src = asset('storage/img/'.$customer->picture);
                    }
                @endphp
                <img class="md-card-head-img" src="{!! $src !!}" alt="{{ $customer->name }}"/>
            </div>
            <div class="md-card-content">
                <h4 class="heading_c uk-margin-bottom">{{ ucwords($customer->name) }}</h4>
                    <ul class="md-list">
                        <li>
                            <div class="md-list-content">
                                <span class="md-list-heading">Deposit</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">
                                Rp. {{ $deposit }}</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <span class="md-list-heading">Nama</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">{{ $customer->name }}</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <span class="md-list-heading">Email</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">{{ strtolower($customer->email) }} </span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <span class="md-list-heading">Alamat</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">{{ $customer->address }}</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <span class="md-list-heading">Kota/Provinsi</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">
                                @php
                                    $customerCity = array_first($cities, function ($city, $index) use ($customer) {
                                        return ($city['city_id'] == $customer->city && $city['province_id'] == $customer->province);
                                    });
                                    if ($customerCity) {
                                        echo $customerCity['city_name'] .' - '. $customerCity['province'];
                                    }
                                @endphp
                                </span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <span class="md-list-heading">Tipe Akun</span>
                                {!! $acctype !!}
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <span class="md-list-heading">Terdaftar pada</span>
                                {{ indonesian_date($customer->created_date) }}
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <span class="md-list-heading">Customer Service</span>
                                <span class="uk-text-upper uk-text-muted uk-text-truncate uk-text-bold">{{ $customer->customer_service_name }}</span>
                            </div>
                        </li>
                    </ul>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1">
                        <a class="md-btn md-btn-small md-btn-success change_status_customer" data-id="{{ $customer->id }}" data-status="{{ $status }}" href="#">Ganti Status</a>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <a class="md-btn md-btn-small md-btn-primary" href="{{ route('customer.detail', [$customer->id, str_slug($customer->name, '-')]) }}">Detail</a>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <a href="#change_acc_type" id="change_acc_type" class="md-btn md-btn-small md-btn-warning" data-id="{{ $customer->id }}" data-uk-modal="{target:"#change_acc_type"}">Ganti Tipe Akun</a>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <a href="#change_cs" id="change_cs" class="md-btn md-btn-small md-btn-danger" data-id="{{ $customer->id }}" data-uk-modal="{target:"#change_cs"}">Ganti Customer Service</a>
                    </div>
                    <?php if(Auth::user()->id == 1 OR Auth::user()->id == 11 OR Auth::user()->id == 13) { ?>
                    <div class="uk-width-medium-1-1">
                        <a onclick="delete_customer({{ $customer->id }})" href="#" class="md-btn md-btn-small md-btn-danger">Delete</a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
@endforeach
</div>
{{ $customers->links() }}
<?php } else if ($searching == 'filter_empty') { ?>
<h3 class="heading_b uk-text-center grid_no_results">Tidak ada data</h3>
<?php } ?>
@endsection

@section('js')
	<!-- common functions -->
	<script src="{{ asset('templates/js/common.min.js') }}"></script>
	<!-- uikit functions -->
	<script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
	<!-- altair common functions/helpers -->
	<script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
	<!--  contact list functions -->
	<script src="{{ asset('templates/js/pages/page_contact_list.js') }}"></script>
    <div class="uk-modal" id="account_type">
        <div class="uk-modal-dialog" style="width: 800px">
            <form method="POST" class="form_input_acc_type" class="uk-form-stacked">
            <div class="uk-modal-header" id="header_acc_type"></div>
            {{ csrf_field() }}
            <input type="hidden" name="_method" id="method" value="POST">
            <input type="hidden" name="customer_id" id="customer_id">
            <div class="uk-grid uk-margin-bottom" data-uk-grid-margin id="profile_picture"></div>
            <hr>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1 uk-margin-bottom">
                  <label>Nama</label>
                  <br>
                  <input type="text" id="name" class="md-input label-fixed" name="name" required readonly="true" />
                </div>
                <div class="uk-width-medium-1-1">
                  <label>Email</label>
                  <br>
                  <input type="email" id="email" class="md-input label-fixed" name="email" required readonly="true" />
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-3 uk-margin-top">
                    <label>Akun Tipe</label>
                    <br>
                    <span class="icheck-inline uk-margin-top">
                        <input type="radio" name="acc_type" id="acc_type" value="1"/>
                        <label for="acc_type" class="inline-label">Member</label>
                    </span>
                </div>
                <div class="uk-width-medium-1-3 uk-margin-top">
                    <br>
                    <span class="icheck-inline uk-margin-top">
                        <input type="radio" name="acc_type" id="acc_type" value="2"/>
                        <label for="acc_type" class="inline-label">VIP Member</label>
                    </span>
                </div>
                <?php if(Auth::user()->level == 1){ ?>
                <div class="uk-width-medium-1-3 uk-margin-top">
                    <br>
                    <span class="icheck-inline uk-margin-top">
                        <input type="radio" name="acc_type" id="acc_type" value="3"/>
                        <label for="acc_type" class="inline-label">Partner</label>
                    </span>
                </div>
                <?php } ?>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-wave waves-effect waves-button uk-modal-close">Tutup</button>
                <input type="submit" value="UPDATE" class="md-btn md-btn-danger" id="show_preloader_md">
            </div>
            </form>
        </div>
    </div>
    <div class="uk-modal" id="account_cs">
        <div class="uk-modal-dialog" style="width: 800px">
            <form class="form_input_cs" class="uk-form-stacked">
            {{ csrf_field() }}
            <input type="hidden" name="_method" id="method" value="POST">
            <input type="hidden" name="customer_id" id="customer_id">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1 uk-margin-bottom">
                  <label>Nama</label>
                  <br>
                  <input type="text" id="name" class="md-input label-fixed" name="name" required readonly="true" />
                </div>
                <div class="uk-width-medium-1-1">
                  <label>Email</label>
                  <br>
                  <input type="email" id="email" class="md-input label-fixed" name="email" required readonly="true" />
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1 uk-margin-top">
                   <label>Customer Service Customer</label>
                  <br>
                  {{ Form::select('customer_service_id', $customer_services, null, array('class' =>'md-input', 'placeholder' => 'Pilih Customer Service', 'id' => 'customer_service_id','required')) }}
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-wave waves-effect waves-button uk-modal-close">Tutup</button>
                <input type="submit" value="UPDATE" class="md-btn md-btn-danger" id="show_preloader_md">
            </div>
            </form>
        </div>
    </div>
	<script type="text/javascript">
	$(document).on('click', '.change_status_customer', function (e) {
      var id = $(this).data('id');
      var status = $(this).data('status');

      if(status == 0){
        var word = 'menon-aktifkan';
      } else {
        var word = 'aktifkan';
      }
      UIkit.modal.confirm("Apakah kamu yakin akan "+word+" customer ini?", function(){
        var APP_URL = {!! json_encode(url('/')) !!}
        $.ajax({
          url: APP_URL + "/customer/change_status_customer/"+id+"/"+status,
          method:'GET',
          dataType:'json',
          beforeSend: function(){
            altair_helpers.content_preloader_show('md');
          },
          success:function(result) {
            if(result.status=='success'){
                UIkit.notify({
                    message: result.msg,
                    status: 'success',
                    timeout: 8000,
                    pos: 'top-center'
                });
                location.reload();
            } else  {
                UIkit.notify({
                    message: result.msg,
                    status: 'danger',
                    timeout: 8000,
                    pos: 'top-center'
                });
                location.reload();
            }
            altair_helpers.content_preloader_hide();
          }
        }) 
      });
      e.preventDefault(); 
    });
    $(document).on('click', '#change_acc_type', function (e) {
        var id = $(this).data('id');
        var APP_URL = {!! json_encode(url('/')) !!}
        $.ajax({
          url: APP_URL + "/customer/edit/"+id,
          method:'GET',
          dataType:'json',
          beforeSend: function(){
            altair_helpers.content_preloader_show('md');
          },
          success:function(data) {
            $('input#customer_id').val(id);
            $('input#name').val(data.name);
            $('input#email').val(data.email);
            if(data.picture == ''){
                $("#profile_picture").html('<div class="uk-margin-bottom uk-text-center uk-position-relative"><img src="{{ asset("templates/img/no-image-available.png") }}" class="img_medium"/></div>')
            } else {
                $("#profile_picture").html('<div class="uk-margin-bottom uk-text-center uk-position-relative"><img src="{{ asset("storage/img/") }}/'+data.picture+'" class="img_medium"/></div>')
            }
            $('#header_acc_type').html('<h3 class="uk-modal-title">Ganti Akun Type '+data.name+'</h3>');
            $('input:radio[name=acc_type]').attr('checked',false);
            if(data.acc_type == 1){
                $('input:radio[name=acc_type]')[0].checked = true;
            } else if(data.acc_type == 2){
                $('input:radio[name=acc_type]')[1].checked = true;
            } else {
                $('input:radio[name=acc_type]')[2].checked = true;
            }
            show_modal_account();
            altair_helpers.content_preloader_hide();
          }
        })
      e.preventDefault();
    });

    $(document).on('click', '#change_cs', function (e) {
        var id = $(this).data('id');
        var APP_URL = {!! json_encode(url('/')) !!}
        $.ajax({
          url: APP_URL + "/customer/edit/"+id,
          method:'GET',
          dataType:'json',
          beforeSend: function(){
            altair_helpers.content_preloader_show('md');
          },
          success:function(data) {
            $('input#customer_id').val(id);
            $('input#name').val(data.name);
            $('input#email').val(data.email);
            $('select#customer_service_id').val(data.customer_service_id);
            show_cs();
            altair_helpers.content_preloader_hide();
          }
        })
        e.preventDefault();
    });
    function delete_customer(id){
      UIkit.modal.confirm("Apakah kamu yakin akan menghapus data customer ini? semua data yang berhubungan dengan customer ini akan ikut terhapus, seperti rating produk.", function(){
        $.ajax({
          method: 'DELETE',
          headers: {
              'X-CSRF-Token': '{{ csrf_token() }}'
          },
          url: "{{ route('customer.delete') }}",
          dataType: 'JSON',
          cache: false,
          data: {id: id},
          beforeSend: function(){
              altair_helpers.content_preloader_show('md');
          },
          success: function(result) {
            altair_helpers.content_preloader_hide();
            if(result.status=='success'){
              UIkit.notify({
                  message: result.msg,
                  status: 'success',
                  timeout: 8000,
                  pos: 'top-center'
              });
              location.reload();
            } else  {
              UIkit.notify({
                  message: result.msg,
                  status: 'danger',
                  timeout: 8000,
                  pos: 'top-center'
              });
            }
          }
        });
      });
    }

    function show_cs() {
        UIkit.modal("#account_cs").show();
    }

    function hide_cs() {
        UIkit.modal("#account_cs").hide();
    }

    function show_modal_account() {
        UIkit.modal("#account_type").show();
    }

    function hide_modal_account() {
        UIkit.modal("#account_type").hide();
    }
    $(".form_input_acc_type").on('submit',function() {
      $.ajax({
        type: 'PUT',
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: "{{ route('customer.change_acc_type_customer') }}",
        cache: false,
        data: $(this).serialize(),
        dataType: 'JSON',
        beforeSend: function(){
            altair_helpers.content_preloader_show('md');
        },
        success: function(result) {
          hide_modal_account()
          altair_helpers.content_preloader_hide();
          if(result.status=='success'){
            UIkit.notify({
              message: result.msg,
              status: 'success',
              timeout: 8000,
              pos: 'top-center'
            });
          }
        },
        error: function (result) {
          var response = JSON.parse(result.responseText)
          $.each(response.errors, function (key, value) {
              UIkit.notify({
              message: value,
              status: 'warning',
              timeout: 10000,
              pos: 'top-center'
            });
          });
          altair_helpers.content_preloader_hide();
        }
      });
      event.preventDefault(); 
    });
    $(".form_input_cs").on('submit',function() {
      $.ajax({
        type: 'PUT',
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: "{{ route('customer.change_cs_customer') }}",
        cache: false,
        data: $(this).serialize(),
        dataType: 'JSON',
        beforeSend: function(){
            altair_helpers.content_preloader_show('md');
        },
        success: function(result) {
          hide_cs()
          altair_helpers.content_preloader_hide();
          if(result.status=='success'){
            UIkit.notify({
              message: result.msg,
              status: 'success',
              timeout: 8000,
              pos: 'top-center'
            });
          }
        },
        error: function (result) {
          var response = JSON.parse(result.responseText)
          $.each(response.errors, function (key, value) {
              UIkit.notify({
              message: value,
              status: 'warning',
              timeout: 10000,
              pos: 'top-center'
            });
          });
          altair_helpers.content_preloader_hide();
        }
      });
      event.preventDefault(); 
    });
	</script>
@endsection