@extends('layouts.baselayout')

@section('css')
<!-- common css / default css -->
<link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
<link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
<link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
<link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')

<div class="uk-width-medium-1-1">
    <h4 class="heading_a uk-margin-bottom">Data Order</h4>
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-grid uk-margin-bottom" data-uk-grid-margin>
                <div class="uk-width-medium-1-4">
                    <input class="md-input" name="start_date" type="text" id="start_date" data-uk-datepicker="{format:'DD.MM.YYYY'}" value="{{ date('d.m.Y') }}">
                </div>
                <div class="uk-width-medium-1-4">
                    <input class="md-input" name="end_date" type="text" id="end_date" data-uk-datepicker="{format:'DD.MM.YYYY'}" value="{{ date('d.m.Y') }}">
                </div>
                <div class="uk-width-medium-1-4">
                    <select id="filter_order" class="md-input" data-uk-tooltip="{pos:'top'}" title="Filter Status Order">
                        <option selected="true" value="0">Semua...</option>
                        <option value="1">Belum Pembayaran</option>
                        <option value="3">Proses pembayaran</option>
                        <option value="4">Pembayaran disetujui</option>
                        <option value="5">Proses digudang</option>
                        <option value="6">Barang terkirim</option>
                        <option value="7">Pesanan dibatalkan otomatis</option>
                        <option value="8">Pembayaran dari deposit</option>
                        <option value="9">Pesanan Selesai</option>
                        <option value="10">Pesanan dibatalkan oleh customer</option>
                        <option value="11">Pembayaran ditolak</option>
                    </select>
                </div>
            </div>
            <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#tabs_4'}">
                <li class="uk-width-1-1" id="list-tab"><a href="#" class="list_cs">List Data Order</a></li>
            </ul>
            <ul id="tabs_4" class="uk-switcher uk-margin">
                <li>
                    <table id="data_table" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="number-order">No.</th>
                                <th>Customer Order Login</th>
                                <th>Nama</th>
                                <th>Kode Order</th>
                                <th>Kode Unik Order</th>
                                <th>Telepon</th>
                                <th>CS</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th class="action-order">Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th class="number-order">No.</th>
                                <th>Customer Order Login</th>
                                <th>Nama</th>
                                <th>Kode Order</th>
                                <th>Kode Unik Order</th>
                                <th>Telepon</th>
                                <th>CS</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th class="action-order">Action</th>
                            </tr>
                        </tfoot>
                        <tbody></tbody>
                    </table>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- common functions -->
<script src="{{ asset('templates/js/common.min.js') }}"></script>
<!-- uikit functions -->
<script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
<!-- altair common functions/helpers -->
<script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
<!-- datatable -->
<script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('templates/js/custom/datatables/datatables.uikit.min.js') }}"></script>
<script src="{{ asset('templates/js/pages/plugins_datatables.min.js') }}"></script>
<!-- page specific plugins -->
<script src="{{ asset('templates/js/pages/full_numbers_no_ellipses.js') }}"></script>
<script src="{{ asset('templates/js/pages/components_preloaders.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        let filter;
        let start_date;
        let end_date;
        if(localStorage.getItem('filterId') == null) {
            filter = $('select#filter_order').val();
        } else {
            filter = localStorage.getItem('filterId');
        }
        if(localStorage.getItem('startDateLocal') == null) {
            start_date = $('#start_date').val();
        } else {
            start_date = localStorage.getItem('startDateLocal');
        }
        if(localStorage.getItem('endDateLocal') == null){
            end_date = $('#end_date').val();
        } else {
            end_date = localStorage.getItem('endDateLocal');
        }
        $('select#filter_order').val(filter);
        $('#start_date').val(start_date);
        $('#end_date').val(end_date);

        var APP_URL = {!! json_encode(url('/')) !!}
        var dt = $('#data_table').DataTable({
            orderCellsTop: true,
            responsive: true,
            processing: true,
            serverside: true,
            scrollX: true,
            searching: true,
            "autoWidth": true,
            lengthMenu: [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            'pagingType': 'full_numbers_no_ellipses',
            ajax: {
                url: APP_URL + "/order/show/" + start_date + "/" + end_date + "/" + filter,
                data: {
                    '_token': '{{ csrf_token() }}'
                },
                type: 'POST',
            },
            "deferRender": true,
            stateSave: true,
            columns: [{
                    data: 'DT_Row_Index',
                    searchable: false,
                    "width": "15px",
                    "className": "text-center"
                },
                {
                    data: 'customer_login'
                },
                {
                    data: 'customer_name'
                },
                {
                    data: 'order_code'
                },
                {
                    data: 'unique_code'
                },
                {
                    data: 'customer_tele'
                },
                {
                    data: 'customer_service_name'
                },
                {
                    data: 'status_order'
                },
                {
                    data: 'created_date'
                },
                {
                    data: 'updated_date'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                    "width": "25px",
                    "className": "text-center"
                },
            ]
        })
        dt.draw(false);
        $('#start_date, #end_date, #filter_order').change(function($event) {
            altair_helpers.content_preloader_show('md');
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            var filter = $('select#filter_order').val();
            localStorage.setItem('filterId', filter);
            localStorage.setItem('startDateLocal', start_date);
            localStorage.setItem('endDateLocal', end_date);
            dt.ajax.url(APP_URL + "/order/show/" + start_date + "/" + end_date + "/" + filter).load();
            altair_helpers.content_preloader_hide();
        });
    });
</script>
@endsection
