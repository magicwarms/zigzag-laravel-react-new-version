@extends('layouts.baselayout')

@section('css')
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')
<div class="uk-grid">
    <div class="uk-width-1-1">
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a">Hapus aktifitas</h3>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-large-1-3 uk-width-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_start">Mulai Tanggal</label>
                            <input class="md-input" type="text" id="uk_dp_start" required="true">
                        </div>
                    </div>
                    <div class="uk-width-large-1-3 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_end">Sampai Tanggal</label>
                            <input class="md-input" type="text" id="uk_dp_end" required="true">
                        </div>
                    </div>
                    <div class="uk-width-large-1-3 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <a id="submit_delete" class="md-btn md-btn-danger md-btn-block md-btn-wave-light" href="#">HAPUS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-large-1-1">
        <h3 class="heading_a uk-margin-bottom">Daftar Activity</h3>
        <div class="timeline timeline-center">
        <?php if(!empty($list_activity)){
            foreach ($list_activity as $key => $act) {
                if(strpos($act->activity, 'Mengunjungi') == true){
                    $icon = 'visibility';
                    $style = 'timeline_icon_primary';
                } elseif (strpos($act->activity, 'Menyimpan') == true) {
                    $icon = 'save';
                    $style = 'timeline_icon_success';
                } elseif (strpos($act->activity, 'Memperbaharui') == true) {
                    $icon = 'save';
                    $style = 'timeline_icon_success';
                } elseif (strpos($act->activity, 'logout') == true) {
                    $icon = 'exit_to_app';
                    $style = 'timeline_icon_danger';
                } elseif (strpos($act->activity, 'tidak berhasil') == true) {
                    $icon = 'error';
                    $style = 'timeline_icon_danger';
                } elseif (strpos($act->activity, 'berhasil') == true) {
                    $icon = 'success';
                    $style = 'timeline_icon_success';
                } else {
                    $icon = 'error';
                    $style = 'timeline_icon_warning';
                }
                if($key % 2 == 0){
                    $cls = 'uk-animation-slide-right';
                } else {
                    $cls = 'uk-animation-slide-left';
                }
        ?>
            <div class="timeline_item">
                <div class="timeline_icon <?php echo $style;?> uk-invisible" data-uk-scrollspy="{cls:'uk-animation-scale-up uk-invisible', delay:300, repeat: true}"><i class="material-icons"><?php echo $icon;?></i></div>
                <div class="timeline_content uk-invisible" data-uk-scrollspy="{cls:'<?php echo $cls;?> uk-invisible', delay:300}">
                    <strong>{{ $act->name }} - Melakukan {{ $act->activity }} menggunakan {{ $act->user_agent }} - Pada waktu {{ indonesian_date($act->created_date, 'l, j F Y H:i','') }}</strong>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        </div>
        {{ $list_activity->links() }}
    </div>
</div>
@endsection

@section('js')
	<!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <!-- jquery ui -->
    <script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- ionrangeslider -->
    <script src="{{ asset('bower_components/ion.rangeslider/js/ion.rangeSlider.min.js') }}"></script>
    <!--  forms advanced functions -->
    <script src="{{ asset('templates/js/pages/forms_advanced.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('click', '#submit_delete', function (e) {
                var APP_URL = {!! json_encode(url('/')) !!}
                var start_date = $('#uk_dp_start').val();
                var end_date = $('#uk_dp_end').val();
                if(start_date == '' || end_date == ''){
                    UIkit.notify({
                      message: 'Tanggal tidak boleh kosong',
                      status: 'error',
                      timeout: 10000,
                      pos: 'top-center'
                    });
                }
                $.ajax({
                    url: APP_URL + "/activity/filter_delete/"+start_date+"/"+end_date,
                    method: 'GET',
                    cache: false,
                    dataType: 'JSON',
                    beforeSend: function(){
                        altair_helpers.content_preloader_show('md');
                    },
                    success: function(result) {
                      if(result.status=='success'){
                        UIkit.notify({
                          message: result.msg,
                          status: 'success',
                          timeout: 8000,
                          pos: 'top-center'
                        });
                      }
                      location.reload();
                      altair_helpers.content_preloader_hide();
                    },
                    error: function (result) {
                      var response = JSON.parse(result.responseText)
                      $.each(response.errors, function (key, value) {
                          UIkit.notify({
                          message: value,
                          status: 'warning',
                          timeout: 10000,
                          pos: 'top-center'
                        });
                      });
                      altair_helpers.content_preloader_hide();
                    }
                  });
                e.preventDefault(); 
            });
        });
    </script>
@endsection