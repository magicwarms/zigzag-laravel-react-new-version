@extends('layouts.baselayout')

@section('css')
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')

<div class="uk-width-medium-1-1">
	<h4 class="heading_a uk-margin-bottom">Data Pencairan Deposit Customer</h4>
	<div class="md-card">
	  <div class="md-card-content">
	    <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#tabs_4'}">
	      <li class="uk-width-1-1" id="list-tab"><a href="#" class="list_cs">List Data Pencairan Deposit Customer</a></li>
	    </ul>
	    <ul id="tabs_4" class="uk-switcher uk-margin">
	      <li>
	        <table id="data_table" class="uk-table" cellspacing="0" width="100%">
	          <thead>
	            <tr>
	               <th class="number-order">No.</th>
    	            <th>Nama</th>
                  <th>Kode</th>
                  <th>Status</th>
                  <th>Created</th>
    	            <th>Updated</th>
	              <th class="action-order">Action</th>
	            </tr>
	          </thead>
	          <tfoot>
	            <tr>
	              <th class="number-order">No.</th>
                  <th>Nama</th>
                  <th>Kode</th>
                  <th>Status</th>
                  <th>Created</th>
                  <th>Updated</th>
                <th class="action-order">Action</th>
	            </tr>
	          </tfoot>
	          <tbody></tbody>
          </table>
        </li>
	 		</ul>
		</div>
	</div>
</div>
@endsection

@section('js')
    <div class="uk-modal" id="withdrawal_account">
        <div class="uk-modal-dialog">
            <form class="form_withdrawal_account" class="uk-form-stacked">
                <div class="uk-modal-header"></div>
                <input type="hidden" name="_method" id="method" value="PUT">
                <input type="hidden" id="id" name="id">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 uk-margin-top" id="header-modal"></div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2 uk-margin-top">
                    <label>Nama</label>
                    <br>
                    <input type="text" id="name" class="md-input label-fixed" readonly="true" />
                  </div>
                  <div class="uk-width-medium-1-2 uk-margin-top">
                    <label>Kode</label>
                    <br>
                    <input type="text" id="code" name="code" class="md-input label-fixed" readonly="true"/>
                  </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2 uk-margin-top">
                    <label>Pencairan</label>
                    <br>
                    <input type="text" id="amount" class="md-input label-fixed" readonly="true" />
                  </div>
                  <div class="uk-width-medium-1-2 uk-margin-top">
                    <label>Rekening Tujuan</label>
                    <br>
                    <input type="text" id="bank" class="md-input label-fixed" readonly="true" />
                  </div>
                </div>
                <div class="uk-grid option_approve" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                        <label>Verifikasi</label>
                        <br>
                        <span class="icheck-inline uk-margin-top">
                            <input type="radio" name="status" id="status" value="2"/>
                            <label for="status" class="inline-label">DISETUJUI</label>
                        </span>
                    </div>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                        <br>
                        <span class="icheck-inline uk-margin-top">
                            <input type="radio" name="status" id="status" value="3"/>
                            <label for="status" class="inline-label">DITOLAK</label>
                        </span>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-wave waves-effect waves-button uk-modal-close uk-modal-close">Tutup</button>
                    <input type="submit" value="SUBMIT" class="md-btn md-btn-danger" id="show_preloader_md">
                </div>
            </form>
        </div>
    </div>
    <!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <!-- datatable -->
    <script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('templates/js/custom/datatables/datatables.uikit.min.js') }}"></script>
    <script src="{{ asset('templates/js/pages/plugins_datatables.min.js') }}"></script>
    <!-- page specific plugins -->
    <script src="{{ asset('templates/js/pages/full_numbers_no_ellipses.js') }}"></script>
    <script src="{{ asset('templates/js/pages/components_preloaders.min.js') }}"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        var dt = $('#data_table').DataTable({
            orderCellsTop: true,
            responsive: true,
            processing: true,
            serverside: true,
            scrollX: true,
            searching: true,
            "autoWidth": true,
            lengthMenu: [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
            'pagingType': 'full_numbers_no_ellipses',
            ajax: {
                url:  "{{ route('withdrawal.show') }}",
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
                { data: 'DT_Row_Index', searchable: false, "width": "15px", "className": "text-center"},
                { data: 'customer_name'},
                { data: 'code'},
                { data: 'status'},
                { data: 'created_date'},
                { data: 'updated_date'},
                { data: 'action', name: 'action', orderable: false, searchable: false, "width": "25px", "className": "text-center" },
            ]
        })
    });
    $(document).on('click', '.show_withdrawal_account', function (e) {
        var id = $(this).data('id');
        var APP_URL = {!! json_encode(url('/')) !!}
        $.ajax({
          url: APP_URL + "/withdrawal/detail_withdrawal/"+id,
          method:'GET',
          dataType:'json',
          beforeSend: function(){
            altair_helpers.content_preloader_show('md');
          },
          success:function(data) {
            $('input#id').val(id);
            $('input#name').val(data.customer_name);
            $('input#code').val(data.code);
            $('input#amount').val(data.amount);
            $('input:radio[name=status]').attr('checked',false);
            if(data.status == 2 || data.status == 3){
              $('.option_approve').empty();
            }
            if(data.status == 1){
              if(data.status == 2){
                  $('input:radio[name=status]')[0].checked = true;
              } else if(data.status == 3){
                  $('input:radio[name=status]')[1].checked = true;
              }
            }
            $(".uk-modal-header").html('<h3 class="uk-modal-title">Pencairan Akun '+data.code+'</h3>')
            $("#header-modal").html('<label>Status: </label>'+data.status_detail)
            UIkit.modal("#withdrawal_account").show();
            altair_helpers.content_preloader_hide();
          }
        })
      e.preventDefault();
    });
    $(".form_withdrawal_account").on('submit',function(event) {
        $.ajax({
            type: 'PUT',
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: "{{ route('withdrawal.verify') }}",
            data: $(this).serialize(),
            dataType: 'JSON',
            cache: false,
            beforeSend: function(){
              (function(modal){ modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Mohon tunggu sebentar data sedang diproses...<br/></div>'); setTimeout(function(){ modal.hide() }, 2000) })();
            },
            success: function(result) {
              UIkit.modal("#withdrawal_account").hide();
              $('#data_table').DataTable().ajax.reload();
              altair_helpers.content_preloader_hide();
              if(result.status=='success'){
                UIkit.notify({
                  message: result.msg,
                  status: 'success',
                  timeout: 8000,
                  pos: 'top-center'
                });
              } else {
                UIkit.notify({
                  message: result.msg,
                  status: 'error',
                  timeout: 10000,
                  pos: 'top-center'
                });
              }
            },
            error: function (result) {
              var response = JSON.parse(result.responseText)
              $.each(response, function (key, value) {
                $('#output').html('<div class="uk-alert uk-alert-danger" data-uk-alert><a href="#" class="uk-alert-close uk-close"></a>'+value+'</div>')
              });
              altair_helpers.content_preloader_hide();
            }
        });
      event.preventDefault();
    });
    </script>
@endsection