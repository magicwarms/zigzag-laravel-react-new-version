@extends('layouts.baselayout')

@section('css')
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')
<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }" class="uk-margin-bottom">
  <h1>Daftar Deposit Customer</h1>
  <span class="uk-text-upper uk-text-small"><a href="#">Deposit</a></span>
</div>

<div class="uk-grid uk-grid-width-large-1-3 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show uk-margin-bottom" data-uk-sortable data-uk-grid-margin>
    <div>
        <div class="md-card">
            <div class="md-card-content md-bg-orange-500">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"></div>
                <span style="color: white" class="uk-text-small">Total Deposit Customer</span>
                <h2 style="color: white" class="uk-margin-remove uk-text-italic">Rp. <?php echo number_format($total_deposit, 0,',','.'); ?></h2>
            </div>
        </div>
    </div>
    <div>
        <div class="md-card">
            <div class="md-card-content md-bg-deep-purple-700">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"></div>
                <span style="color: white" class=" uk-text-small ">Total Customer Deposit</span>
                <h2 style="color: white" class="uk-margin-remove "><?php echo $customer_deposit;?></h2>
            </div>
        </div>
    </div>
    <div>
        <div class="md-card">
            <div class="md-card-content md-bg-red-700">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"></div>
                <span style="color: white" class=" uk-text-small ">Total Deposit Terpakai</span>
                <h2 style="color: white" class="uk-margin-remove uk-text-italic">Rp. <?php echo number_format($customer_deposit_used, 0,',','.'); ?></h2>
            </div>
        </div>
    </div>
</div>

<div class="uk-width-medium-1-1">
	<div class="md-card">
	  <div class="md-card-content">
      <div class="uk-grid uk-margin-bottom" data-uk-grid-margin>
        <div class="uk-width-medium-1-4">
          <input class="md-input" name="start_date" type="text" id="start_date" data-uk-datepicker="{format:'DD.MM.YYYY'}" value="{{ date('d.m.Y') }}">
        </div>
        <div class="uk-width-medium-1-4">
          <input class="md-input" name="end_date" type="text" id="end_date" data-uk-datepicker="{format:'DD.MM.YYYY'}" value="{{ date('d.m.Y') }}">
        </div>
        <div class="uk-width-medium-1-4">
            <select id="filter_deposit" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Filter Status Deposit">
                <option disabled="true" selected="true">Semua...</option>
                <option value="1">Belum Pembayaran / Menunggu Konfirmasi</option>
                <option value="2">Transaksi Deposit Ditolak</option>
                <option value="3">Transaksi Deposit Ditambah</option>
                <option value="4">Transaksi Dikurang</option>
                <option value="5">Proses Cek Mutasi</option>
                <option value="6">Deposit Dikembalikan</option>
            </select>
        </div>
      </div>
	    <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#tabs_4'}">
        <li class="uk-width-1-1" id="list-tab"><a href="#" class="list_category">Daftar Deposit</a></li>
	    </ul>
	    <ul id="tabs_4" class="uk-switcher uk-margin">
	      <li>
	        <table id="data_table" class="uk-table" cellspacing="0" width="100%">
	          <thead>
	            <tr>
	            <th class="number-order">No.</th>
                <th>Nama</th>
                <th>Total</th>
                <th>Balance</th>
                <th>Status</th>
	              <th>Created</th>
                <th>Updated</th>
                <th class="action-order">Action</th>
	            </tr>
	          </thead>
	          <tfoot>
	            <tr>
	            <th class="number-order">No.</th>
                <th>Nama</th>
                <th>Total</th>
                <th>Balance</th>
                <th>Status</th>
                <th>Created</th>
                <th>Updated</th>
                <th class="action-order">Action</th>
	            </tr>
	          </tfoot>
	          <tbody></tbody>
          </table>
        </li>
	 		</ul>
		</div>
	</div>
</div>
@endsection

@section('js')
    <div class="uk-modal" id="evidence_deposit">
        <div class="uk-modal-dialog">
            <form class="form_evidence" class="uk-form-stacked">
                <div class="uk-modal-header">
                    <h3 class="uk-modal-title">Verifikasi Deposit</h3>
                </div>
                <input type="hidden" name="_method" id="method" value="PUT">
                <input type="hidden" id="id" name="id">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                      <label>Nama</label>
                      <br>
                      <input type="text" id="name" class="md-input label-fixed" readonly="true" />
                    </div>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                      <label>Kode Deposit</label>
                      <br>
                      <input type="text" id="kode_order" class="md-input label-fixed" readonly="true"/>
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-3 uk-margin-top">
                      <label>Total Transfer</label>
                      <br>
                      <input type="text" id="total" class="md-input label-fixed" name="total" readonly="true" />
                    </div>
                    <div class="uk-width-medium-1-3 uk-margin-top">
                      <label>Bank Pengirim</label>
                      <br>
                      <input type="text" id="bank" class="md-input label-fixed" name="bank" readonly="true" />
                    </div>
                    <div class="uk-width-medium-1-3 uk-margin-top">
                      <label>No. Rekening Pengirim</label>
                      <br>
                      <input type="text" id="sender_bank" class="md-input label-fixed" name="sender_bank" readonly="true" />
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                      <label>No. Rekening Penerima (Zigzag)</label>
                      <br>
                      <input type="text" id="received_bank" class="md-input label-fixed" name="received_bank" readonly="true" />
                    </div>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                      <label>Tanggal Transfer</label>
                      <br>
                      <input type="text" id="transfer_date" class="md-input label-fixed" name="transfer_date" readonly="true" />
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1 uk-margin-top">
                      <label>Bukti</label>
                      <hr class="uk-margin-bottom">
                      <div id="evidence_pic"></div>
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                        <label>Verifikasi</label>
                        <br>
                        <span class="icheck-inline uk-margin-top">
                            <input type="radio" name="status" id="status" value="3"/>
                            <label for="status" class="inline-label">ACCEPT</label>
                        </span>
                    </div>
                    <div class="uk-width-medium-1-2 uk-margin-top">
                        <br>
                        <span class="icheck-inline uk-margin-top">
                            <input type="radio" name="status" id="status" value="2"/>
                            <label for="status" class="inline-label">REJECT</label>
                        </span>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-wave waves-effect waves-button uk-modal-close uk-modal-close">Tutup</button>
                    <input type="submit" value="SUBMIT" class="md-btn md-btn-danger" id="show_preloader_md">
                </div>
            </form>
        </div>
    </div>
    <!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <!-- datatable -->
    <script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('templates/js/custom/datatables/datatables.uikit.min.js') }}"></script>
    <script src="{{ asset('templates/js/pages/plugins_datatables.min.js') }}"></script>
    <!-- page specific plugins -->
    <script src="{{ asset('templates/js/pages/full_numbers_no_ellipses.js') }}"></script>
    <script src="{{ asset('templates/js/pages/components_preloaders.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            var APP_URL = {!! json_encode(url('/')) !!}
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            var filter = $('#filter_deposit').val();
            var dt = $('#data_table').DataTable({
                orderCellsTop: true,
                stateSave: true,
                responsive: true,
                processing: true,
                serverside: true,
                searching: true,
                "autoWidth": true,
                lengthMenu: [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
                'pagingType': 'full_numbers_no_ellipses',
                ajax: {
                    url:  APP_URL + "/deposit/show/"+start_date+"/"+end_date+"/"+filter,
                    data: { '_token' : '{{ csrf_token() }}'},
                    type: 'POST',
                },
                "deferRender": true,
                columns: [
                    { data: 'DT_Row_Index', searchable: false, "width": "15px", "className": "text-center"},
                    { data: 'customer_name'},
                    { data: 'total'},
                    { data: 'balance'},
                    { data: 'status'},
                    { data: 'created_date'},
                    { data: 'updated_date'},
                    { data: 'action', name: 'action', orderable: false, searchable: false, "width": "25px", "className": "text-center" },
                ],
            });

          $('#start_date, #end_date, #filter_deposit').change(function($event) {
              altair_helpers.content_preloader_show('md');
              var start_date = $('#start_date').val();
              var end_date = $('#end_date').val();
              var filter = $('#filter_deposit').val();
              filter = (filter=="") ? filter = 0 : filter;
              dt.ajax.url(APP_URL + "/deposit/show/"+start_date+"/"+end_date+"/"+filter).load();
              altair_helpers.content_preloader_hide();
          });

        });
        $(document).on('click', '.show_evidence', function (e) {
            var id = $(this).data('id');
            var APP_URL = {!! json_encode(url('/')) !!}
            $.ajax({
              url: APP_URL + "/deposit/detail_deposit/"+id,
              method:'GET',
              dataType:'json',
              beforeSend: function(){
                altair_helpers.content_preloader_show('md');
              },
              success:function(data) {
                $('input#id').val(id);
                $('input#name').val(data.customer_name);
                $('input#kode_order').val(data.kode_order);
                $('input#total').val(data.total);
                $("#evidence_pic").html('<div class="uk-margin-bottom uk-text-center uk-position-relative"><a href="{{ asset("storage/img/") }}/'+data.evidence+'" data-uk-lightbox="{group:"gallery"}"><img src="{{ asset("storage/img/") }}/'+data.evidence+'" alt=""></a></div>')
                $('input#bank').val(data.bank);
                $('input#sender_bank').val(data.sender_bank);
                $('input#received_bank').val(data.received_bank);
                $('input#transfer_date').val(data.transfer_date);
                $('input:radio[name=status]').attr('checked',false);
                if(data.status == 3){
                    $('input:radio[name=status]')[0].checked = true;
                } else if(data.status == 2){
                    $('input:radio[name=status]')[1].checked = true;
                } 
                UIkit.modal("#evidence_deposit").show();
                altair_helpers.content_preloader_hide();
              }
            })
          e.preventDefault();
        });
        $(".form_evidence").on('submit',function(event) {
            $.ajax({
                type: 'PUT',
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url: "{{ route('deposit.verify') }}",
                data: $(this).serialize(),
                dataType: 'JSON',
                cache: false,
                beforeSend: function(){
                  (function(modal){ modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Mohon tunggu sebentar data sedang diproses...<br/></div>'); setTimeout(function(){ modal.hide() }, 2000) })();
                },
                success: function(result) {
                  UIkit.modal("#evidence_deposit").hide();
                  $('#data_table').DataTable().ajax.reload();
                  altair_helpers.content_preloader_hide();
                  if(result.status=='success'){
                    UIkit.notify({
                      message: result.msg,
                      status: 'success',
                      timeout: 8000,
                      pos: 'top-center'
                    });
                  } else {
                    UIkit.notify({
                      message: result.msg,
                      status: 'error',
                      timeout: 10000,
                      pos: 'top-center'
                    });
                  }
                },
                error: function (result) {
                  var response = JSON.parse(result.responseText)
                  $.each(response, function (key, value) {
                    $('#output').html('<div class="uk-alert uk-alert-danger" data-uk-alert><a href="#" class="uk-alert-close uk-close"></a>'+value+'</div>')
                  });
                  altair_helpers.content_preloader_hide();
                }
            });
          event.preventDefault();
        });

    </script>
@endsection