@extends('layouts.baselayout')

@section('css')
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
    <style type="text/css">
      /* Hide HTML5 Up and Down arrows. */
      input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button {
          -webkit-appearance: none;
          margin: 0;
      }
      input[type="number"] {
          -moz-appearance: textfield;
      }
    </style>
@endsection

@section('content')
<div class="uk-width-medium-1-1">
  <h4 class="heading_a uk-margin-bottom">Buat Data Produk Baru</h4>
  <div class="md-card">
    <div class="md-card-content">
      <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#tabs_4'}">
        <li class="uk-width-1-2" id="list-tab"><a href="#" class="list_product"><i class="material-icons md-24 uk-text-primary">playlist_add_check</i> List Produk</a></li>
        <li class="uk-width-1-2" id="form-tab"><a href="#" class="form_product"><i class="material-icons md-24 uk-text-primary">add_box</i> Tambah Produk</a></li>
      </ul>
      <ul id="tabs_4" class="uk-switcher uk-margin">
        <li>
          <table id="data_table" class="uk-table" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="number-order">No.</th>
                <th>Gambar</th>
                <th>Nama</th>
                <th>Kategori</th>
                <th>Hot</th>
                <th>Promo</th>
                <th>Restock</th>
                <th>Created</th>
                <th class="action-order">Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th class="number-order">No.</th>
                <th>Gambar</th>
                <th>Nama</th>
                <th>Kategori</th>
                <th>Hot</th>
                <th>Promo</th>
                <th>Restock</th>
                <th>Created</th>
                <th class="action-order">Action</th>
              </tr>
            </tfoot>
            <tbody></tbody>
          </table>
        </li>
        <li>
        <h3 class="heading_a uk-margin-bottom">Buat data baru atau Perbaharui data</h3>
        <form class="form_input_produk" name="formproduk" id="form_validation" enctype="multipart/form-data">
          <input type="hidden" name="id" id="id">
          <input type="hidden" name="method" id="method" value="POST">
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-1">
              <div class="md-card">
                <div class="md-card-content" id="picture_products"></div>
                <div class="md-card-content" id="preview_picture_products"></div>
                <div class="md-card-content" id="picture_product">
                  <input type="file" id="picture" class="md-input" name="picture[]" accept="image/png, image/jpg, image/jpeg" multiple="multiple">
                </div>
              </div>
            </div>
          </div>
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-3 uk-margin-top">
              <label>Nama Produk</label>
              <br>
              <input type="text" id="name" class="md-input label-fixed" name="name" required/>
            </div>
            <div class="uk-width-medium-1-3 uk-margin-top">
               <label>Kategori</label>
              <br>
              {{ Form::select('categories_id', $categories, null, array('class' =>'md-input', 'placeholder' => 'Pilih Kategori', 'id' => 'categories_id','required')) }}
            </div>
            <div class="uk-width-medium-1-3 uk-margin-top">
              <label>Berat (Gram)</label>
              <br>
              <input type="number" id="weight" class="md-input label-fixed" name="weight" required placeholder="1000 gram" />
            </div>
          </div>
          <h2 class="heading_a">
           Harga Barang
           <span class="sub-heading">Silakan masukkan harga barang sesuai form inputan dibawah.</span>
         </h2>
         <hr class="md-hr"/>
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-2 uk-margin-top">
              <label>Tipe Harga</label>
              <br>
              <input type="text" id="price_type" class="md-input label-fixed" name="price_type[]" required value="REGULER" readonly="true" required="true" />
            </div>
            <input type="hidden" name="product_price_detail_id[]" id="product_price_detail_id">
            <div class="uk-width-medium-1-2 uk-margin-top">
               <label>Harga</label>
              <br>
              <input type="number" id="price" class="md-input label-fixed" name="price[]" required/>
            </div>
          </div>
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-2 uk-margin-top">
              <label>Tipe Harga</label>
              <br>
              <input type="text" id="price_type" class="md-input label-fixed" name="price_type[]" required value="VIP" readonly="true" required="true" />
            </div>
            <input type="hidden" name="product_price_detail_id[]" id="product_price_detail_id">
            <div class="uk-width-medium-1-2 uk-margin-top">
               <label>Harga</label>
              <br>
              <input type="number" id="price" class="md-input label-fixed" name="price[]" required/>
            </div>
          </div>
          <?php 
          //hanya admin dan superadmin yang dapat isi ini
          if(Auth::user()->level != 1) { 
            $input_type = 'hidden';
          } else {
            $input_type = 'number';
          }
           ?>
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-2 uk-margin-top">
              <label>Tipe Harga (<b><i>Hanya admin yang dapat mengisi ini</i></b>)</label>
              <br>
              <input type="text" id="price_type" class="md-input label-fixed" name="price_type[]" required value="PARTNER" readonly="true" required="true" />
            </div>
            <input type="hidden" name="product_price_detail_id[]" id="product_price_detail_id">
            <div class="uk-width-medium-1-2 uk-margin-top">
               <label>Harga</label>
              <br>
              <input type="{{ $input_type }}" id="price" class="md-input label-fixed" name="price[]"/>
            </div>
          </div>
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-2 uk-margin-top">
              <label>Tipe Harga Pokok (<b><i>Hanya admin yang dapat mengisi ini</i></b>)</label>
              <br>
              <input type="text" id="price_type" class="md-input label-fixed" name="price_type[]" required value="POKOK" readonly="true" required="true" />
            </div>
            <input type="hidden" name="product_price_detail_id[]" id="product_price_detail_id">
            <div class="uk-width-medium-1-2 uk-margin-top">
               <label>Harga</label>
              <br>
              <input type="{{ $input_type }}" id="price" class="md-input label-fixed" name="price[]" value="0" />
            </div>
          </div>
          
          <h2 class="heading_a">
           Jenis Barang
           <span class="sub-heading">Silakan masukkan jenis barang sesuai inputan ceklist dibawah.</span>
          </h2>
          <hr class="md-hr">
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-5 uk-margin-top">
              <div class="parsley-row">
                <input class="hot" id="hot" type="checkbox" name="hot">
                <label class="inline-label"><b>Hot Barang</b></label>
              </div>
            </div>
            <div class="uk-width-medium-1-5 uk-margin-top">
              <div class="parsley-row">
                <input class="promo" id="promo" type="checkbox" name="promo">
                <label class="inline-label"><b>Promo Barang</b></label>
              </div>
            </div>
            <div class="uk-width-medium-1-5 uk-margin-top">
              <div class="parsley-row">
                <input class="restock" id="restock" type="checkbox" name="restock">
                <label class="inline-label"><b>Restock Barang</b></label>
              </div>
            </div>
          </div>
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-1 uk-margin-top">
              <label>Material</label>
              <br>
              <textarea id="material" cols="30" rows="4" name="material" class="md-input label-fixed"></textarea>
            </div>
          </div>

          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-2 uk-margin-top">
              <div class="parsley-row">
                <input type="checkbox" name="product_category_tas" id="tas" class="md-input" />
                <label class="inline-label"><b>Produk Tas</b></label>
              </div>
            </div>
            <div class="uk-width-medium-1-2 uk-margin-top">
              <div class="parsley-row">
                <input type="checkbox" name="product_category_sepatu" id="sepatu" class="md-input" />
                <label class="inline-label"><b>Produk Sepatu</b></label>
              </div>
            </div>
          </div>

          <div id="product_more_detail">
            <h2 class="heading_a uk-margin-top">
             Produk Tas
             <span class="sub-heading">Silakan masukkan data produk tas sesuai form inputan dibawah.</span>
            </h2>
            <hr class="md-hr">

            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1 multi-field-wrapper">
                <button style="min-width: 49px !important; margin-left: 17px !important; padding-bottom: 10px;" type="button" class="md-btn md-btn-primary add-field"><i class="material-icons md-24">&#xE146;</i></button>

                <div class="multi-fields">
                  <div class="uk-grid multi-field" data-uk-grid-margin>
                    <div style="width: 10%" class="uk-width-medium-1-3 uk-margin-top">
                      <div class="uk-input-group mt-m">
                        <span class="uk-input-group-addon remove-field">
                          <button type="button" style="min-width:1px; padding-bottom: 10px;" class="md-btn md-btn-danger removed-button" id="delete_button_tas" onclick="delete_color_more(this.value)"><i class="material-icons md-24">&#xE872;</i></button>
                        </span>
                      </div>
                    </div>
                    <div class="uk-width-medium-1-3 uk-margin-top">
                      <?php if(Auth::user()->id == 1){ ?>
                      <input type="text" name="product_color_detail_id[]" id="product_color_detail_id">
                      <?php } else { ?>
                      <input type="hidden" name="product_color_detail_id[]" id="product_color_detail_id">
                      <?php } ?>
                      <div class="uk-input-group">
                        <label class="uk-form-label"><b>Variasi Barang</b></label>
                        <input type="text" class="md-input label-fixed" name="color_tas[]" id="color_tas">
                      </div>
                    </div>
                    <div class="uk-width-medium-1-3 uk-margin-top">
                      <?php if(Auth::user()->id == 1){ ?>
                      <input type="text" name="product_more_detail_id[]" id="product_more_detail_id">
                      <?php } else { ?>
                      <input type="hidden" name="product_more_detail_id[]" id="product_more_detail_id">
                      <?php } ?>
                      <div class="uk-input-group">
                        <label class="uk-form-label"><b>Stok Barang</b></label>
                        <input type="number" class="md-input label-fixed" name="stock_tas[]" id="stock_tas">
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div id="product_sepatu">
            <h2 class="heading_a uk-margin-top">
             Produk Sepatu
             <span class="sub-heading"><b>Perhatian, harap hati-hati bisa saja produk ini sedang berada didalam salah satu orderan aktif customer, harap lakukan segala bentuk perubahan dengan hati-hati.</b></span>
            </h2>
            <hr class="md-hr">
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1 multi-field-wrapper-sepatu">
                <button style="min-width: 49px !important; margin-left: 17px !important; padding-bottom: 10px;" type="button" class="md-btn md-btn-primary add-field-sepatu"><i class="material-icons md-24">&#xE146;</i></button>
                <div class="multi-fields-sepatu">

                  <div class="uk-grid multi-field-sepatu" data-uk-grid-margin>
                    <div style="width: 10%" class="uk-width-medium-1-6 uk-margin-top">
                      <div class="uk-input-group mt-m">
                        <span class="uk-input-group-addon remove-field-sepatu">
                          <button type="button" style="min-width:1px; padding-bottom: 10px;" class="md-btn md-btn-danger removed-button" id="delete_button_sepatu" onclick="delete_color_more_sepatu(this.value)"><i class="material-icons md-24">&#xE872;</i></button>
                        </span>
                      </div>
                    </div>
                    <div class="uk-width-medium-1-6 uk-margin-top">
                      <?php if(Auth::user()->id == 1){ ?>
                      <input type="text" name="product_color_detail_id_sepatu[]" id="product_color_detail_id_sepatu">
                      <?php } else { ?>
                      <input type="hidden" name="product_color_detail_id_sepatu[]" id="product_color_detail_id_sepatu"> 
                      <?php } ?>
                      <div class="uk-input-group">
                        <label class="uk-form-label"><b>Variasi Barang</b></label>
                        <input type="text" class="md-input label-fixed" name="color_sepatu[]" id="color_sepatu">
                      </div>
                    </div>
                    <?php if(Auth::user()->id == 1){ ?>
                    <input type="text" name="product_more_detail_id_sepatu[]" id="product_more_detail_id_sepatu">
                    <?php } else { ?>
                    <input type="hidden" name="product_more_detail_id_sepatu[]" id="product_more_detail_id_sepatu">
                    <?php } ?>
                    <div class="uk-width-medium-1-6 uk-margin-top">
                      <div class="uk-input-group">
                        <label class="uk-form-label"><b>Size Barang</b></label>
                        <input type="number" class="md-input label-fixed" name="size_sepatu[]" id="size_sepatu">
                      </div>
                    </div>
                    <div class="uk-width-medium-1-6 uk-margin-top">
                      <div class="uk-input-group">
                        <label class="uk-form-label"><b>Stock Barang</b></label>
                        <input type="number" class="md-input label-fixed" name="stock_sepatu[]" id="stock_sepatu">
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div class="uk-width-medium-1-1 uk-margin-top">
           <div class="uk-form-row">
             <span class="uk-input-group-addon" id="input_submit_type">
              <input id="save_item" type="submit" value="SAVE" class="md-btn md-btn-primary">
             </span>
           </div>
          </div>
        </form>

        </li>
      </ul>
    </div>
  </div>
</div>
@endsection

@section('js')
    <!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <!-- datatable -->
    <script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('templates/js/custom/datatables/datatables.uikit.min.js') }}"></script>
    <script src="{{ asset('templates/js/pages/plugins_datatables.min.js') }}"></script>
    <!-- page specific plugins -->
    <script src="{{ asset('templates/js/pages/full_numbers_no_ellipses.js') }}"></script>
    <!-- parsley (validation) -->
    <script>
    // load parsley config (altair_admin_common.js)
    altair_forms.parsley_validation_config();
    </script>
    <script src="{{ asset('bower_components/parsleyjs/dist/parsley.min.js') }}"></script>
    <!--  forms validation functions -->
    <script src="{{ asset('templates/js/pages/forms_validation.min.js') }}"></script>
    <script src="{{ asset('templates/js/pages/components_preloaders.min.js') }}"></script>
    
    <script type="text/javascript">
      $(document).ready(function() {
        // Disable scroll when focused on a number input.
        $('.form_input_produk').on('focus', 'input[type=number]', function(e) {
            $(this).on('wheel', function(e) {
                e.preventDefault();
            });
        });
        // Restore scroll on number inputs.
        $('.form_input_produk').on('blur', 'input[type=number]', function(e) {
            $(this).off('wheel');
        });
        // Disable up and down keys.
        $('.form_input_produk').on('keydown', 'input[type=number]', function(e) {
            if ( e.which == 38 || e.which == 40 )
                e.preventDefault();
        });
        $("#product_more_detail").hide();
        $("#product_sepatu").hide();
        $("#tas").click(function () {
          if ($(this).is(":checked")) {
            $("#sepatu").attr("checked",false);
            $("#product_more_detail").show();
            $("#product_sepatu").hide();
            $("input#color_sepatu").val("");
            $("input#size_sepatu").val("");
            $("input#stock_sepatu").val("");
          }
        });
        $("#sepatu").click(function () {
          if ($(this).is(":checked")) {
            $("#tas").attr("checked",false);
            $("#product_sepatu").show();
            $("#product_more_detail").hide();
            $("input#color_tas").val("");
            $("input#stock_tas").val("");
          }
        });
        var dt = $('#data_table').DataTable({
          orderCellsTop: true,
          responsive: true,
          processing: true,
          serverside: true,
          scrollX: true,
          searching: true,
          "autoWidth": true,
          lengthMenu: [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
          'pagingType': 'full_numbers_no_ellipses',
          ajax: {
              url:  "{{ route('product.show') }}",
              data: { '_token' : '{{ csrf_token() }}'},
              type: 'POST',
          },
          "deferRender": true,
          stateSave: true,
          columns: [
              { data: 'DT_Row_Index', searchable: false, "width": "15px", "className": "text-center"},
              { data: 'picture'},
              { data: 'name'},
              { data: 'categories_name'},
              { data: 'hot'},
              { data: 'promo'},
              { data: 'restock'},
              { data: 'created_date'},
              { data: 'action', name: 'action', orderable: false, searchable: false, "width": "25px", "className": "text-center" },
          ],
      });

      $(".form_input_produk").on('submit',function() {
        var method = $('input#method').val();
        var url_action;
        if(method == 'POST'){
          url_action = "{{ route('product.store') }}";
        } else {
          url_action = "{{ route('product.update') }}";
        }
        $.ajax({
          type: 'POST',
          headers: {
              'X-CSRF-Token': $('input[name="_token"]').val()
          },
          url: url_action,
          processData: false,
          contentType: false,
          cache: false,
          data: new FormData($(this)[0]),
          dataType: 'JSON',
          beforeSend: function(){
              altair_helpers.content_preloader_show('md');
          },
          success: function(result) {
            altair_helpers.content_preloader_hide();
            if(result.status=='success'){
              UIkit.notify({
                message: result.msg,
                status: 'success',
                timeout: 8000,
                pos: 'top-center'
              });
              location.reload();
            } else {
              UIkit.notify({
                  message: result.msg,
                  status: 'danger',
                  timeout: 8000,
                  pos: 'top-center'
              });
              altair_helpers.content_preloader_hide();
            }
          }
        });
        event.preventDefault(); 
      });

      $(document).on('click', '.edit_data', function (e) {
        var id = $(this).data('id');
        var name = $(this).data('name');
        UIkit.modal.confirm("Apakah kamu yakin akan mengubah data ini?", function(){
          var APP_URL = {!! json_encode(url('/')) !!}
          $.ajax({
            url: APP_URL + "/product/edit/"+id+"/"+name,
            method:'GET',
            dataType:'json',
            beforeSend: function(){
              altair_helpers.content_preloader_show('md');
            },
            success:function(data) {
              $('.form_input_produk')[0].reset();
              $('input#method').val('PUT');
              $("#form-tab").addClass("uk-active");
              $('.form_product')[0].click();
              $("#list-tab").removeClass("uk-active");
              $('#input_submit_type').html('<input id="update_item" type="submit" value="UPDATE" class="md-btn md-btn-danger">');
              if(data.product_image_detail == ''){
                $("#picture_products").empty();
                $('#preview_picture_products').empty();
              } else {
                $("#picture_products").empty();
                $('#preview_picture_products').empty();
                $.each(data.product_image_detail, function( index, value ) {
                  $("#picture_products").append('<div class="uk-margin-bottom uk-text-center uk-position-relative"><a href="#" class="uk-modal-close uk-close uk-close-alt uk-position-absolute delete_picture" data-id="'+id+'" data-product_image_id="'+data.product_image_detail[index].id+'" data-picture="'+data.product_image_detail[index].image+'"></a><img src="storage/img/product/'+data.product_image_detail[index].image+'" class="img_medium"/><br><input type="text" name="caption[]" class="md-input label-fixed" placeholder="Caption" value="'+data.product_image_detail[index].caption+'"></div>')
                });  
              }
              $('input#id').val(id);
              $('input#name').val(data.name);
              $('select#categories_id').val(data.categories_id);
              $('textarea#desc').val(data.desc);
              $('textarea#material').val(data.material);
              $('input#dimension').val(data.dimension);
              $('input#weight').val(data.weight);
              $('input#stock').val(data.stock);
              $('input#code').val(data.code);
              $('.hot')[0].checked = data.hot;
              $('.promo')[0].checked = data.promo;
              $('.restock')[0].checked = data.restock;
              if(data.is_shoes != 1){
                reset_field_tas();
                $('#tas')[0].click();
                $("#tas").attr("checked",true);
                $("#sepatu").attr("checked",false);
                $.each(data.product_more_detail, function( index_tas, value_tas ) {
                    if(index_tas>0) {
                      add_field_tas();
                    }
                    $('button#delete_button_tas:eq('+index_tas+')').val(value_tas.id);
                    $('input#product_color_detail_id:eq('+index_tas+')').val(value_tas.color_id);
                    $('input#product_more_detail_id:eq('+index_tas+')').val(value_tas.id);
                    $('input#color_tas:eq('+index_tas+')').val(value_tas.color);
                    $('input#stock_tas:eq('+index_tas+')').val(value_tas.stock);
                });
              } else {
                reset_field_sepatu();
                $('#sepatu')[0].click();
                $("#sepatu").attr("checked",true);
                $("#tas").attr("checked",false);
                $.each(data.product_more_detail, function( index_sepatu, value_sepatu ) {
                    if(index_sepatu>0) {
                      add_field_sepatu();
                    }
                    $('button#delete_button_sepatu:eq('+index_sepatu+')').val(value_sepatu.id);
                    $('input#product_color_detail_id_sepatu:eq('+index_sepatu+')').val(value_sepatu.color_id);
                    $('input#product_more_detail_id_sepatu:eq('+index_sepatu+')').val(value_sepatu.id);
                    $('input#color_sepatu:eq('+index_sepatu+')').val(value_sepatu.color);
                    $('input#stock_sepatu:eq('+index_sepatu+')').val(value_sepatu.stock);
                    $('input#size_sepatu:eq('+index_sepatu+')').val(value_sepatu.size);
                });
              }
              $.each(data.product_price_detail, function( index_sepatu, value_sepatu ) {
                  $('input#product_price_detail_id:eq('+index_sepatu+')').val(value_sepatu.id);
                  $('input#price_type:eq('+index_sepatu+')').val(value_sepatu.price_type.toUpperCase());
                  $('input#price:eq('+index_sepatu+')').val(value_sepatu.price);
              });
              altair_helpers.content_preloader_hide();
            }
          }) 
        });
        e.preventDefault(); 
      });
    });
    function delete_color_more(color_more_id) {
      UIkit.modal.confirm("Apakah kamu yakin akan menghapus data ini?", function(){
        var APP_URL = {!! json_encode(url('/')) !!}
        $.ajax({
          method: 'GET',
          headers: {
              'X-CSRF-Token': '{{ csrf_token() }}'
          },
          url: APP_URL + "/product/delete_color_more/"+color_more_id,
          dataType: 'JSON',
          cache: false,
          beforeSend: function(){
              altair_helpers.content_preloader_show('md');
          },
          success: function(result) {
            altair_helpers.content_preloader_hide();
            if(result.status=='success'){
              UIkit.notify({
                  message: result.msg,
                  status: 'success',
                  timeout: 8000,
                  pos: 'top-center'
              });
            } else  {
              UIkit.notify({
                  message: result.msg,
                  status: 'danger',
                  timeout: 8000,
                  pos: 'top-center'
              });
            }
          }
        })
      });
    }
    function delete_color_more_sepatu(more_detail_id) {
      UIkit.modal.confirm("Apakah kamu yakin akan menghapus data ini?", function(){
        var APP_URL = {!! json_encode(url('/')) !!}
        $.ajax({
          method: 'GET',
          headers: {
              'X-CSRF-Token': '{{ csrf_token() }}'
          },
          url: APP_URL + "/product/delete_color_more_sepatu/"+more_detail_id,
          dataType: 'JSON',
          cache: false,
          beforeSend: function(){
              altair_helpers.content_preloader_show('md');
          },
          success: function(result) {
            altair_helpers.content_preloader_hide();
            if(result.status=='success'){
              UIkit.notify({
                  message: result.msg,
                  status: 'success',
                  timeout: 8000,
                  pos: 'top-center'
              });
            } else  {
              UIkit.notify({
                  message: result.msg,
                  status: 'danger',
                  timeout: 10000,
                  pos: 'top-center'
              });
            }
          }
        })
      });
    }
    function previewImages() {
      var $preview = $('#preview_picture_products').empty();
      if (this.files) $.each(this.files, readAndPreview);
      function readAndPreview(i, file) {
        if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
          return alert(file.name +" is not an image");
        } // else...
        var reader = new FileReader();
        $(reader).on("load", function() {
          $preview.append('<div class="uk-margin-bottom uk-text-center uk-position-relative"><img src="'+this.result+'" class="img_medium"><br><input type="text" name="caption[]" class="md-input label-fixed" placeholder="Caption"></div>')
        });
        reader.readAsDataURL(file);
      }
    }
    $('#picture').on("change", previewImages);

    function reset_field_tas(){
      $(".multi-field").each(function(e,i){
        var idx = $("div.multi-field").index(this);
        if(idx > 0) {
          $(this).remove();
        }
      });
    }
    function reset_field_sepatu(){
      $(".multi-field-sepatu").each(function(e,i){
        var idx_sepatu = $("div.multi-field-sepatu").index(this);
        if(idx_sepatu > 0) {
          $(this).remove();
        }
      });
    }
    function add_field_tas() {
      var wrapper = $('.multi-fields');
      $('.multi-field:first-child').clone(true).appendTo(wrapper);
    }
    function add_field_sepatu() {
      var wrapper = $('.multi-fields-sepatu');
      $('.multi-field-sepatu:first-child').clone(true).appendTo(wrapper);
    }
    function delete_data(id){
      UIkit.modal.confirm("Apakah kamu yakin akan menghapus data produk ini? <b>semua data yang berhubungan dengan produk ini akan dihapus (size, warna, harga), bahkan di keranjang customer kamu.</b>", function(){  
        $.ajax({
          method: 'DELETE',
          headers: {
              'X-CSRF-Token': '{{ csrf_token() }}'
          },
          url: "{{ route('product.delete') }}",
          dataType: 'JSON',
          cache: false,
          data: {id: id},
          beforeSend: function(){
              altair_helpers.content_preloader_show('md');
          },
          success: function(result) {
            $('#data_table').DataTable().ajax.reload();
            altair_helpers.content_preloader_hide();
            if(result.status=='success'){
              UIkit.notify({
                  message: result.msg,
                  status: 'success',
                  timeout: 8000,
                  pos: 'top-center'
              });
            } else  {
              UIkit.notify({
                  message: result.msg,
                  status: 'danger',
                  timeout: 8000,
                  pos: 'top-center'
              });
            }
          }
        });
      });
    }

    $('.multi-field-wrapper').each(function() {
        var $wrapper = $('.multi-fields', this);
        $(".add-field", $(this)).click(function(e) {
            $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input, button').val('');
        });
        $('.multi-field .remove-field ', $wrapper).click(function() {
            if ($('.multi-field', $wrapper).length > 1){
              $(this).parents('.multi-field').remove();
            }
        });
    });

    $('.multi-field-wrapper-sepatu').each(function() {
        var $wrapper = $('.multi-fields-sepatu', this);
        $(".add-field-sepatu", $(this)).click(function(e) {
            $('.multi-field-sepatu:first-child', $wrapper).clone(true).appendTo($wrapper).find('input, button').val('');
        });
        $('.multi-field-sepatu .remove-field-sepatu ', $wrapper).click(function() {
            if ($('.multi-field-sepatu', $wrapper).length > 1)
                $(this).parents('.multi-field-sepatu').remove();
        });
    });

    $(document).on('click', '.delete_picture', function (e) {
      var product_image_id = $(this).data('product_image_id');
      var product_id = $(this).data('id');
      var filename = $(this).data('picture');
      //UIkit.modal.confirm("Apakah kamu yakin akan menghapus data ini?", function() {
      var APP_URL = {!! json_encode(url('/')) !!}
        $.ajax({
          method: 'GET',
          headers: {
              'X-CSRF-Token': '{{ csrf_token() }}'
          },
          url: APP_URL + "/product/delete_picture_product/"+product_image_id+"/"+filename,
          dataType: 'JSON',
          cache: false,
          beforeSend: function(){
              altair_helpers.content_preloader_show('md');
          },
          success: function(result) {
            altair_helpers.content_preloader_hide();
            if(result.status=='success'){
                UIkit.notify({
                    message: result.msg,
                    status: 'success',
                    timeout: 8000,
                    pos: 'top-center'
                });
                $.ajax({
                  url: APP_URL + "/product/edit/"+product_id+"/"+filename,
                  method:'GET',
                  dataType:'json',
                  success:function(pic_data) {
                    $('#picture_products').empty();
                    if(pic_data.product_image_detail != ''){
                      $.each(pic_data.product_image_detail, function( index_pic, value_pic ) {
                        $("#picture_products").append('<div class="uk-margin-bottom uk-text-center uk-position-relative"><a href="#" class="uk-modal-close uk-close uk-close-alt uk-position-absolute delete_picture" data-product_image_id="'+pic_data.product_image_detail[index_pic].id+'" data-id="'+product_id+'" data-picture="'+pic_data.product_image_detail[index_pic].image+'"></a><img src="storage/img/product/'+pic_data.product_image_detail[index_pic].image+'" class="img_medium"/><br><input type="text" name="caption[]" class="md-input label-fixed" placeholder="Caption" value="'+pic_data.product_image_detail[index_pic].caption+'" readonly></div>')
                      });
                    } else {
                      $('#picture_products').empty();
                    }
                  }
                });
            } else  {
              UIkit.notify({
                  message: result.msg,
                  status: 'danger',
                  timeout: 8000,
                  pos: 'top-center'
              });
            }
          }
        });
      //});
    });
    </script>
@endsection