@extends('layouts.baselayout')

@section('css')
	<!-- common css / default css -->
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
@endsection

@section('content')
<div class="uk-width-medium-1-1">
	<h4 class="heading_a uk-margin-bottom">Buat Data Return Baru</h4>
	<div class="md-card">
	  <div class="md-card-content">
	    <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#tabs_4'}">
        <li class="uk-width-1-1" id="list-tab"><a href="#" class="list_return">List Return</a></li>
	    </ul>
	    <ul id="tabs_4" class="uk-switcher uk-margin">
	      <li>
	        <table id="data_table" class="uk-table" cellspacing="0" width="100%">
	          <thead>
	            <tr>
	              <th class="number-order">No.</th>
                <th>Gambar</th>
                <th>Customer</th>
                <th>Alasan</th>
                <th>Status</th>
                <th>Created</th>
	              <th>Updated</th>
                <th class="action-order">Action</th>
	            </tr>
	          </thead>
	          <tfoot>
	            <tr>
	              <th class="number-order">No.</th>
                <th>Gambar</th>
                <th>Customer</th>
                <th>Alasan</th>
                <th>Status</th>
                <th>Created</th>
                <th>Updated</th>
                <th class="action-order">Action</th>
	            </tr>
	          </tfoot>
	          <tbody></tbody>
          </table>
        </li>
	 		</ul>
		</div>
	</div>
</div>
@endsection

@section('js')
    <!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <!-- datatable -->
    <script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('templates/js/custom/datatables/datatables.uikit.min.js') }}"></script>
    <script src="{{ asset('templates/js/pages/plugins_datatables.min.js') }}"></script>
    <!-- page specific plugins -->
    <script src="{{ asset('templates/js/pages/full_numbers_no_ellipses.js') }}"></script>
    <!-- parsley (validation) -->
    <script>
    // load parsley config (altair_admin_common.js)
    altair_forms.parsley_validation_config();
    </script>
    <script src="{{ asset('bower_components/parsleyjs/dist/parsley.min.js') }}"></script>
    <!--  forms validation functions -->
    <script src="{{ asset('templates/js/pages/forms_validation.min.js') }}"></script>
    <script src="{{ asset('templates/js/pages/components_preloaders.min.js') }}"></script>

    <div class="uk-modal" id="return_modal">
      <div class="uk-modal-dialog" style="width: 800px">
          <form class="form_input_return" class="uk-form-stacked" id="form_validation">
          <div class="uk-modal-header">Form Return Product</div>
          {{ csrf_field() }}
          <input type="hidden" name="_method" id="method" value="POST">
          <input type="hidden" name="id" id="id">
          <div class="uk-grid uk-margin-bottom" data-uk-grid-margin id="picture_return"></div>
          <hr>
          <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1 uk-margin-bottom">
                <label>Kode Return Produk</label>
                <br>
                <input type="text" id="return_code" class="md-input label-fixed" readonly="true" />
              </div>
          </div>
          <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1 uk-margin-bottom">
                <label>Alasan</label>
                <br>
                <textarea id="reason" cols="30" rows="4" name="reason" class="md-input label-fixed"  readonly="true"></textarea>
              </div>
          </div>
          <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-2 uk-margin-top">
                  <label>Akun Tipe</label>
                  <br>
                  <span class="icheck-inline uk-margin-top">
                      <input type="radio" name="status" id="status" value="1"/>
                      <label for="status" class="inline-label">Setuju</label>
                  </span>
              </div>
              <div class="uk-width-medium-1-2 uk-margin-top">
                  <br>
                  <span class="icheck-inline uk-margin-top">
                      <input type="radio" name="status" id="status" value="0"/>
                      <label for="status" class="inline-label">Tolak</label>
                  </span>
              </div>
          </div>
          <div class="uk-modal-footer uk-text-right">
              <button type="button" class="md-btn md-btn-wave waves-effect waves-button uk-modal-close">Tutup</button>
              <input type="submit" id="save_item" value="UPDATE" class="md-btn md-btn-danger" id="show_preloader_md">
          </div>
          </form>
      </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
        var dt = $('#data_table').DataTable({
            orderCellsTop: true,
            stateSave: true,
            responsive: true,
            processing: true,
            serverside: true,
            searching: true,
            "autoWidth": true,
            lengthMenu: [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
            'pagingType': 'full_numbers_no_ellipses',
            ajax: {
                url:  "{{ route('return_product.show') }}",
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
                { data: 'DT_Row_Index', searchable: false, "width": "15px", "className": "text-center"},
                { data: 'picture'},
                { data: 'customer'},
                { data: 'reason'},
                { data: 'status'},
                { data: 'created_date'},
                { data: 'updated_date'},
                { data: 'action', name: 'action', orderable: false, searchable: false, "width": "25px", "className": "text-center" },
            ],
        });
      });

      $(document).on('click', '#change_approve', function (e) {
          var id = $(this).data('id');
          var APP_URL = {!! json_encode(url('/')) !!}
          $.ajax({
            url: APP_URL + "/return_product/edit/"+id,
            method:'GET',
            dataType:'json',
            beforeSend: function(){
              altair_helpers.content_preloader_show('md');
            },
            success:function(data) {
              $('input#id').val(id);
              $('textarea#reason').val(data.reason);
              $('input#return_code').val(data.return_code);
              if(data.picture == ''){
                  $("#picture_return").html('<div class="uk-margin-bottom uk-text-center uk-position-relative"><img src="{{ asset("templates/img/no-image-available.png") }}" class="img_medium"/></div>')
              } else {
                  $.each(data.picture, function( index, value ) {
                    $("#picture_return").append('<div class="uk-margin-bottom uk-text-center uk-position-relative"><img src="storage/img/return_product/'+value.picture+'" class="img_medium"/></div>')
                  });
              }
              $('input:radio[name=status]').attr('checked',false);
              if(data.status == 1){
                  $('input:radio[name=status]')[0].checked = true;
              } else if(data.status == 0){
                  $('input:radio[name=status]')[1].checked = true;
              }
              show_modal_return();
              altair_helpers.content_preloader_hide();
            }
          })
        e.preventDefault();
      });

      function show_modal_return() {
        UIkit.modal("#return_modal").show();
      }

      function hide_modal_return() {
        UIkit.modal("#return_modal").hide();
      }

      $(".form_input_return").on('submit',function() {
        $.ajax({
          type: 'PUT',
          headers: {
              'X-CSRF-Token': $('input[name="_token"]').val()
          },
          url: "{{ route('return_product.update') }}",
          cache: false,
          data: $(this).serialize(),
          dataType: 'JSON',
          beforeSend: function(){
              altair_helpers.content_preloader_show('md');
          },
          success: function(result) {
            hide_modal_return()
            altair_helpers.content_preloader_hide();
            if(result.status=='success'){
              UIkit.notify({
                message: result.msg,
                status: 'success',
                timeout: 8000,
                pos: 'top-center'
              });
              $('#data_table').DataTable().ajax.reload();
            }
          },
          error: function (result) {
            var response = JSON.parse(result.responseText)
            $.each(response.errors, function (key, value) {
                UIkit.notify({
                message: value,
                status: 'warning',
                timeout: 10000,
                pos: 'top-center'
              });
            });
            altair_helpers.content_preloader_hide();
          }
        });
        event.preventDefault(); 
      });

      function delete_data(id){
        UIkit.modal.confirm("Apakah kamu yakin akan menghapus data ini?", function(){  
          $.ajax({
            method: 'DELETE',
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}'
            },
            url: "{{ route('return_product.delete') }}",
            dataType: 'JSON',
            cache: false,
            data: {id: id},
            beforeSend: function(){
                altair_helpers.content_preloader_show('md');
            },
            success: function(result) {
              $('#data_table').DataTable().ajax.reload();
              altair_helpers.content_preloader_hide();
              if(result.status=='success'){
                UIkit.notify({
                    message: result.msg,
                    status: 'success',
                    timeout: 8000,
                    pos: 'top-center'
                });
              } else  {
                UIkit.notify({
                    message: result.msg,
                    status: 'danger',
                    timeout: 8000,
                    pos: 'top-center'
                });
              }
            }
          });
        });
      }

    </script>
@endsection