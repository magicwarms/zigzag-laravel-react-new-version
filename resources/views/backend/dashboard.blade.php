@extends('layouts.baselayout')

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/main.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/custom-admin.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('templates/css/themes/themes_combined.min.css') }}" media="all">
    <!-- metrics graphics (charts) -->
    <link rel="stylesheet" href="{{ asset('bower_components/metrics-graphics/dist/metricsgraphics.css') }}">
    <!-- chartist -->
    <link rel="stylesheet" href="{{ asset('bower_components/chartist/dist/chartist.min.css') }}">
    <style type="text/css">
        .md-bg-process{background-color:#711b58!important}
        .md-bg-wash{background-color:#ECD80A!important}
        .md-bg-waitingpayment{background-color:#CE3D19!important}
        .md-bg-done{background-color:#98DE05!important}
    </style>

@endsection

@section('content')

<div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
    <div>
        <div class="md-card">
            <div class="md-card-content md-bg-process">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">{{ $count_customer }}/1000</span></div>
                <span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Jumlah Customer Aktif</div></span>
                <h2 class="uk-margin-remove uk-text-contrast"><span class="countUpMe">0<noscript>{{ $count_customer }}</noscript></span></h2>
            </div>
        </div>
    </div>
    <div>
        <div class="md-card">
            <div class="md-card-content md-bg-wash">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">{{ $count_order }}/1000</span></div>
                <span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Jumlah Pesanan Berhasil</div></span>
                <h2 class="uk-margin-remove uk-text-contrast"><span class="countUpMe">0<noscript>{{ $count_order }}</noscript></span></h2>
            </div>
        </div>
    </div>
    <div>
        <div class="md-card">
            <div class="md-card-content md-bg-waitingpayment">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">{{ $count_fail_order_by_customer }}/1000</span></div>
                <span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Order dibatalkan oleh customer</div></span>
                <h2 class="uk-margin-remove uk-text-contrast"><span class="countUpMe">0<noscript>{{ $count_fail_order_by_customer }}</noscript></span></h2>
            </div>
        </div>
    </div>
    <div>
        <div class="md-card">
            <div class="md-card-content md-bg-waitingpayment">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">{{ $count_fail_order_by_admin }}/1000</span></div>
                <span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Order dibatalkan oleh admin</div></span>
                <h2 class="uk-margin-remove uk-text-contrast"><span class="countUpMe">0<noscript>{{ $count_fail_order_by_admin }}</noscript></span></h2>
            </div>
        </div>
    </div>
</div>

<div class="uk-grid uk-grid-width-large-1-2 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
    <div>
        <div class="md-card">
            <div class="md-card-content md-bg-process">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">{{ $get_visitor_per_day }}/1000</span></div>
                <span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Jumlah Pengunjung Hari ini - {{ date('d F Y') }}</div></span>
                <h2 class="uk-margin-remove uk-text-contrast"><span class="countUpMe">0<noscript>{{ $get_visitor_per_day }}</noscript></span></h2>
            </div>
        </div>
    </div>
    <div>
        <div class="md-card">
            <div class="md-card-content md-bg-waitingpayment">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">{{ $get_total_visitor }}/1000</span></div>
                <span class="uk-text-muted uk-text-small"><div class="uk-text-contrast">Jumlah Semua Pengunjung</div></span>
                <h2 class="uk-margin-remove uk-text-contrast"><span class="countUpMe">0<noscript>{{ $get_total_visitor }}</noscript></span></h2>
            </div>
        </div>
    </div>
</div>

<h3 class="heading_b uk-margin-bottom">Grafik Total Penjualan Tahun {{ date('Y') }}</h3>
<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                {!! $chart->container() !!}
            </div>
        </div>
    </div>
</div>
<div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
    <div class="uk-width-medium-1-2">
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a uk-margin-bottom">5 Produk yang sering dilihat</h3>
                <div class="uk-overflow-container">
                    <table class="uk-table">
                        <thead>
                            <tr>
                                <th class="uk-text-nowrap">Barang</th>
                                <th class="uk-text-nowrap">Dilihat</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($top_5_most_view_product as $key => $most_view) {
                        ?>
                            <tr class="uk-table-middle">
                                <td class="uk-width-5-10 uk-text-nowrap"><a href="{{ route('product.detail', [$most_view->id, str_slug($most_view->name, '-')]) }}">{{ $most_view->name }}</a></td>
                                <td class="uk-width-3-10 uk-text-nowrap">{{ $most_view->ViewCountProduct }} kali</td>
                            </tr>
                        <?php } ?>  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-width-medium-1-2">
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a uk-margin-bottom">Coming soon</h3>

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <!-- common functions -->
    <script src="{{ asset('templates/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('templates/js/uikit_custom.min.js') }}"></script>
    <!-- altair common functions/helpers -->
    <script src="{{ asset('templates/js/altair_admin_common.min.js') }}"></script>
    <!-- chartist (charts) -->
    <script src="{{ asset('bower_components/chartist/dist/chartist.min.js') }}"></script>
    <!-- peity (small charts) -->
    <script src="{{ asset('bower_components/peity/jquery.peity.min.js') }}"></script>
    <!-- easy-pie-chart (circular statistics) -->
    <script src="{{ asset('bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
    <!-- countUp -->
    <script src="{{ asset('bower_components/countUp.js/countUp.js') }}"></script>
    <script src="{{ asset('templates/js/pages/dashboard.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    {!! $chart->script() !!}
@endsection