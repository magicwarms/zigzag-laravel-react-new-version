<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Model\VisitorModel;

class SavingVisitor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $saving_visitor_data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($saving_visitor_data) {
        $this->saving_visitor_data = $saving_visitor_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        VisitorModel::create($this->saving_visitor_data);
    }
}
