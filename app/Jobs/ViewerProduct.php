<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Model\ViewProductModel;

class ViewerProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $saving_viewer_data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($saving_viewer_data) {
        $this->saving_viewer_data = $saving_viewer_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        ViewProductModel::create($this->saving_viewer_data);
    }
}
