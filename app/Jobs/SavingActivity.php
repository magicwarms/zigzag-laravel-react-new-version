<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Model\ActivityModel;

class SavingActivity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $saving_activity_data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($saving_activity_data) {
        $this->saving_activity_data = $saving_activity_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        ActivityModel::create($this->saving_activity_data);
    }
}
