<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\ActivityModel;
use DB;

class ActivityController extends Controller {
    
	public function index_activity(Request $request) {
        record_activity("Aktifitas mengunjungi halaman aktifitas");
    	$list_activity = ActivityModel::paginate(10);
    	return view('backend.activity', compact('list_activity'));
	}

    public function filter_delete($start_date, $end_date) {
        DB::beginTransaction();
        try {
        	$start_date = str_replace('.', '/', date("Y-m-d", strtotime($start_date)));
        	$end_date = str_replace('.', '/', date("Y-m-d", strtotime($end_date)));

	        $activity_delete = DB::table('activity_record')
	        ->whereDate('created_date','>=', $start_date)
	        ->whereDate('created_date','<=', $end_date)
	        ->delete();
	        DB::commit();
	        record_activity("Aktifitas menghapus data aktifitas dari ".$start_date." sampai ".$end_date);
	        return response()->json(['status' => 'success','msg' => 'Data Aktifitas Berhasil Dihapus']);
        } catch (\Exception $e) {
        	DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas menghapus data Aktifitas tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Aktifitas tidak berhasil Dihapus']);
        }
    }

}
