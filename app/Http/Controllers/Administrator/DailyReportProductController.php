<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\OrderModel;
use App\Model\OrderDetailModel;

use DB;

class DailyReportProductController extends Controller {
    
	public function index_perday() {
        record_activity("Aktifitas mengunjungi halaman pehitungan total order perhari");
        return view('backend.total_product_perday');
    }

	public function total_product_perday($startdate) {
        $replace_date = str_replace('.', '-', $startdate);
        $per_day = DB::table('orders')
        ->join('orders_detail', 'orders_detail.order_id', '=', 'orders.id')
        ->join('customer_service', 'customer_service.id', '=', 'orders.customer_service_id')
        ->join('customers', 'customers.id', '=', 'orders.customer_id')
        ->orderBy('id','DESC')
        ->whereDate('orders.created_date', date('Y-m-d', strtotime($replace_date)))
        ->select([ // [ ]<-- biar lebih rapi aja
            'orders.id',
            'orders.name AS customer_name',
            'customers.name AS customer_login',
            'orders.tele AS customer_tele',
            'orders.order_code',
            'orders.status_order',
            'orders.unique_code',
            'customer_service.name AS customer_service_name',
            'orders_detail.is_return AS is_return',
            'orders_detail.product_price AS product_price',
            'orders_detail.product_total_price AS product_total_price',
            'orders_detail.product_name AS product_name',
            'orders_detail.product_qty AS product_qty',
            'orders_detail.product_color AS product_color',
            'orders_detail.product_size AS product_size',
            'orders.created_date',
            'orders.updated_date'
        ]);

        return Datatables::of($per_day)
        ->editColumn('is_return', function ($model) {
            if($model->is_return == 0){
            	$status_return = '-';
            } else {
            	$status_return = 'Stok dikembalikan';
            }
            return $status_return;
        })
        ->editColumn('product_price', function ($model) {
            $product_price = number_format($model->product_price, 0, ',', '.');
            return $product_price;
        })
        ->editColumn('product_total_price', function ($model) {
            $product_total_price = number_format($model->product_total_price, 0, ',', '.');
            return $product_total_price;
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->addIndexColumn()->make(true);
    }

	public function calculate_order_product($startdate) {
        $total_order_perday = OrderDetailModel::whereDate('created_date', date("Y-m-d", strtotime($startdate)))->get();

        $output_total = array(
            'total_order_perday'    =>  $total_order_perday->count(),
            'sum_total_perday'		=> $total_order_perday->sum('product_total_price')
        );
        echo json_encode($output_total);
    }

}
