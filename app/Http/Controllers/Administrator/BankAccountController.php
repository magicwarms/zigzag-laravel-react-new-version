<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\BankAccountModel;

use DB;
use File;

class BankAccountController extends Controller {
    
	public function index_bank_account() {
		return view('backend.bank_account');
	}

    public function show_bank_account() {
        record_activity("Aktifitas mengunjungi halaman akun bank");
        $bank_accounts = BankAccountModel::all();
        return Datatables::of($bank_accounts)
        ->editColumn('picture', function ($model) {
            $pic = $model->picture;
            if(!empty($pic)) {
              $picture = asset('storage/img/bank_accounts/'.$pic);
            } else {
              $picture = asset('templates/img/no-image-available.png');
            }

            return '<img src="'.$picture.'" alt="'.$model->name.'" class="img_medium"/>';
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = $model->updated_date;
            if($updated != null){
              $updated = date('d F Y H:i:s', strtotime($updated));
            } else {
              $updated = '-';
            }
            return $updated;
        })
        ->addColumn('action', function ($model) {
            $action = '
                <a href="#" class="edit_data" data-id="'.$model->id.'"><i class="md-icon material-icons">&#xE254;</i></a>
                <a onclick="delete_data('.$model->id.')" href="#"><i class="md-icon material-icons">&#xE16C;</i>
                </a>
            ';
            return $action;
        })
        ->rawColumns(['parent','action','picture'])
        ->addIndexColumn()->make(true);
    }

    function fetch_data_bank_account($bank_id) {
        $bank = BankAccountModel::findOrFail($bank_id);
        $output = array(
            'bank_account'    =>  $bank->bank_account,
            'bank_name'    =>  $bank->bank_name,
            'under_name'    =>  $bank->under_name,
            'picture'     =>  $bank->picture
        );
        record_activity("Aktifitas edit data akun bank ".$bank->bank_account);
        echo json_encode($output);
    }

	public function save_bank_account() {
		DB::beginTransaction();
		$this->validate(request(), [
            'bank_account' => 'required|max:25|min:7|unique:bank_accounts,bank_account',
            'bank_name' => 'required|max:25|min:3',
            'under_name' => 'required|max:150|min:4',
            'picture' => 'required|image|mimes:jpeg,png,jpg'
        ]);

        $picture = request('picture');
        if($picture){
            $name_file = strtolower(str_random(3)).'-'.$picture->getClientOriginalName();
            $picture->move(public_path('storage/img/bank_accounts'), $name_file);
        }

    	BankAccountModel::create([
            'bank_account' => request('bank_account'),
            'bank_name' => request('bank_name'),
    		'under_name' => request('under_name'),
            'picture' => $name_file
    	]);

    	DB::commit();
        record_activity("Aktifitas menyimpan data akun bank ".request('bank_account'));
        return response()->json(['status' => 'success','msg' => 'Data akun bank Berhasil Ditambahkan']);
	}

	public function delete_bank_account(){
		DB::beginTransaction();
		try {
            $bank = BankAccountModel::findOrFail(request('id'));
            record_activity("Aktifitas menghapus data akun bank ".$bank->bank_account);
            $file_to_delete = public_path('storage/img/bank_accounts/').$bank->picture;
            File::delete($file_to_delete); // For delete from folder
	        $bank->delete();

	        DB::commit();
            return response()->json(['status' => 'success','msg' => 'Data akun bank Berhasil Dihapus']);
        } catch (\Exception $e) {
        	DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas menghapus data akun bank tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data akun bank tidak berhasil Dihapus']);
        }
    }

    public function update_bank_account() {
    	DB::beginTransaction();
        $this->validate(request(), [
            'bank_account' => 'required|max:25|min:7',
            'bank_name' => 'required|max:25|min:3',
            'under_name' => 'required|max:150|min:4',
            'picture' => 'image|mimes:jpeg,png,jpg'
        ]);

        if(request('picture') == NULL){

            $bank = BankAccountModel::findOrFail(request('id'));
            $bank->bank_account = request('bank_account');
            $bank->bank_name = request('bank_name');
            $bank->under_name = request('under_name');
            $bank->save();

        } else {

            $picture = request('picture');
            $name_file = strtolower(str_random(3)).'-'.$picture->getClientOriginalName();
            $picture->move(public_path('storage/img/bank_accounts'), $name_file);

            $bank = BankAccountModel::findOrFail(request('id'));
            $bank->bank_account = request('bank_account');
            $bank->bank_name = request('bank_name');
            $bank->under_name = request('under_name');
            $bank->picture = $name_file;
            $bank->save();
        }
    	DB::commit();
        record_activity("Aktifitas memperbaharui data akun bank ".$bank->bank_account);
        return response()->json(['status' => 'success','msg' => 'Data akun bank Berhasil Diperbaharui']);
    }

    public function delete_picture_bank_account($id){
        try {
            $bank = BankAccountModel::findOrFail($id);
            //hapus file nya dulu
            $file_to_delete = public_path('storage/img/bank_accounts/').$bank->picture;
            File::delete($file_to_delete); // For delete from folder
            //lalu update table
            DB::table('bank')->where('id', $bank->id)->update(['picture' => '']);

            record_activity("Aktifitas menghapus data gambar akun bank ".$bank->name);
            return response()->json(['status' => 'success','msg' => 'Data Gambar Akun Bank Berhasil Dihapus']);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas tidak berhasil menghapus data gambar akun bank");
            return response()->json(['status' => 'error','msg' => 'success','msg' => 'Data Gambar Akun Bank Tidak Berhasil Dihapus']);
        }
    }

}
