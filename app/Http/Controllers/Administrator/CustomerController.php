<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\CustomerModel;
use App\Model\CustomerServiceModel;

use DB;
use File;

class CustomerController extends Controller {
   
	public function index_customer() {
        record_activity("Aktifitas mengunjungi halaman customer");
		$customers = DB::table('customers')
        ->join('customer_service','customer_service.id','customers.customer_service_id')
        ->select([
            'customers.*',
            'customer_service.name as customer_service_name'
        ])
        ->paginate(8);
        $customer_services = CustomerServiceModel::pluck('name','id');
        $cities = selectall_city_by_province();
        $searching = 'index';
		return view('backend.customer')->with([
            'cities' => $cities,
            'customers' => $customers,
            'searching' => $searching,
            'customer_services' => $customer_services,
            'status' => ''
        ]);
	}

    public function detail_customer($customer_id) {

        $customer = CustomerModel::findOrFail($customer_id);
        record_activity("Aktifitas mengunjungi halaman detail customer ".$customer->name);
        $cities = selectall_city_by_province();
        return view('backend.detail_customer', compact('customer','cities','deposit'));
    }

    public function show_deposit_customer($customer_id) {
        $deposit = DB::table('deposit')
        ->select([ // [ ]<-- biar lebih rapi aja
            'deposit.total',
            'deposit.status',
            'deposit.created_date',
            'deposit.updated_date'
        ])
        ->where('customer_id', $customer_id);
        return Datatables::of($deposit)
        ->editColumn('total', function ($model) {
            $total="";
            if($model->status == 2){
                $total = "<p class='uk-text-danger uk-text-italic uk-text-bold'> - Rp. ".number_format($model->total, 0,',','.')."</p>";
            } else if ($model->status == 4) {
                $total = "<p class='uk-text-warning uk-text-italic uk-text-bold'> - Rp. ".number_format($model->total, 0,',','.')."</p>";
            } else if ($model->status == 3) {
                $total = "<p class='uk-text-success uk-text-italic uk-text-bold'> + Rp. ".number_format($model->total, 0,',','.')."</p>";
            } else if ($model->status == 1) {
                $total = "<p class='uk-text-primary uk-text-italic uk-text-bold'>Rp. ".number_format($model->total, 0,',','.')."</p>";
            } else if($model->status == 6) {
                $total = "<p class='uk-text-success uk-text-italic uk-text-bold'> + Rp. ".number_format($model->total, 0,',','.')."</p>";
            }
            return $total;
        })
        ->editColumn('status', function ($model) {
            if($model->status == 1){
                $status='<span class="uk-badge uk-badge-primary"> Belum Pembayaran / Menunggu Konfirmasi</span>';
            } elseif($model->status == 2) {
                $status='<span class="uk-badge uk-badge-danger"> Transaksi Deposit Ditolak</span>';
            } elseif ($model->status == 3) {
                $status='<span class="uk-badge uk-badge-success"> Transaksi Deposit Ditambah</span>';
            } else if ($model->status == 4) {
                $status='<span class="uk-badge uk-badge-warning"> Transaksi Dikurang</span>';
            } else if ($model->status == 5) {
                $status='<span class="uk-badge uk-badge-primary"> Proses Cek Mutasi</span>';
            } else if ($model->status == 6) {
                $status='<span class="uk-badge uk-badge-success"> Deposit Dikembalikan</span>';
            }

            return $status;
        })
        ->editColumn('created_date', function ($model) {
            $created = $model->created_date;
            if($created != null){
              $created = date('d F Y H:i:s', strtotime($created));
            } else {
              $created = '-';
            }
            return $created;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = $model->updated_date;
            if($updated != null){
              $updated = date('d F Y H:i:s', strtotime($updated));
            } else {
              $updated = '-';
            }
            return $updated;
        })
        ->rawColumns(['status','total'])
        ->addIndexColumn()->make(true);
    }

	public function change_status_customer($customer_id, $customer_status){
		DB::beginTransaction();
		try {
            if($customer_status == 0){
                $status = 0;
            } else {
                $status = 1;
            }
			$customer = CustomerModel::findOrFail($customer_id);
            $customer->status = $status;
	        $customer->save();

	        DB::commit();
            record_activity("Aktifitas merubah status customer ".$customer->name);
            return response()->json(['status' => 'success','msg' => 'Customer Berhasil Di nonaktifkan']);
        } catch (\Exception $e) {
        	DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas merubah status customer tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Customer Tidak Berhasil Di nonaktifkan']);
        }
    }

    public function change_cs_customer(){
        DB::beginTransaction();
        try {

            $customer = CustomerModel::findOrFail(request('customer_id'));
            $customer->customer_service_id = request('customer_service_id');
            $customer->save();

            DB::commit();
            record_activity("Aktifitas merubah customer service customer ".$customer->name);
            return response()->json(['status' => 'success','msg' => 'Customer Service customer Berhasil Dirubah']);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas merubah status customer tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Customer Service customer Tidak Berhasil Dirubah']);
        }
    }

    public function search_customer() {
        $search = request('search');
        if($search == '') {
            return redirect()->route('customer')->with('danger','Kata pencarian tidak boleh kosong');
        }
        if(strlen($search) < 3) {
           return redirect()->route('customer')->with('warning','Kata pencarian harus lebih dari 3 huruf'); 
        }
        $searching = DB::table('customers')
        ->join('customer_service','customer_service.id','customers.customer_service_id')  
        ->where('customers.name','LIKE','%'.$search.'%')
        ->orWhere('customers.email','LIKE','%'.$search.'%')
        ->select([
            'customers.*',
            'customer_service.name AS customer_service_name'
        ])
        ->get();

        $count_search = count($searching);
        if($searching->isEmpty()){
            $searching = 'empty';
            $count_search = 0;
        }
        $status = '';
        $cities = selectall_city_by_province();
        $customer_services = CustomerServiceModel::pluck('name','id');
        record_activity("Aktifitas mencari data customer ".$search);
        return view('backend.customer', compact('searching','cities','count_search','customer_services','status'));
    }

    function fetch_data_cutomer($customer_id) {
        $customer = CustomerModel::findOrFail($customer_id);
        $output = array(
            'name'    =>  $customer->name,
            'email'     =>  $customer->email,
            'picture'     =>  $customer->picture,
            'acc_type'     =>  $customer->acc_type,
            'customer_service_id'     =>  $customer->customer_service_id,
        );
        record_activity("Aktifitas edit data customer ".$customer->name);
        echo json_encode($output);
    }

    public function change_acc_type_customer(){
        DB::beginTransaction();
        try {
            $customer = CustomerModel::findOrFail(request('customer_id'));
            $customer->acc_type = request('acc_type'); //not active
            $customer->save();

            DB::commit();
            record_activity("Aktifitas merubah akun tipe customer ".$customer->name);
            return response()->json(['status' => 'success','msg' => 'Data Akun Tipe Customer berhasil dirubah']);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas merubah akun tipe customer tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Akun Tipe Customer Tidak berhasil dirubah']);
        }
    }

    public function filter_customer() {

        $cs_filter = request('customer_service_id');
        $status = request('status');

        $filter = DB::table('customers')
        ->join('customer_service','customer_service.id','customers.customer_service_id');
        if($cs_filter > 0)$filter = $filter->where('customer_service_id', $cs_filter);
        $filter = $filter->select([
            'customers.*',
            'customer_service.name as customer_service_name'
        ])
        ->where('customers.status', $status)
        ->get();
        if($filter->isEmpty()){
            $searching = 'filter_empty';
            $count_search = 0;
        } else {
            $searching = $filter;
            $count_search = count($filter);
        }

        $cities = selectall_city_by_province();
        $customer_services = CustomerServiceModel::pluck('name','id');
        record_activity("Aktifitas filter data customer");
        return view('backend.customer', compact('searching','cities','customer_services','searching','count_search','status','cs_filter'));
    }

    public function delete_customer(){
        DB::beginTransaction();
        try {
            $customer_id = request('id');
            //delete file picture first
            $customer_image = CustomerModel::find($customer_id);
            $file_to_delete = public_path('storage/img/').$customer_image->picture;
            File::delete($file_to_delete); // For delete from folder
            //hapus rating juga
            DB::table('rating')->where('customer_id', $customer_id)->delete();

            $customer_image->delete();

            DB::commit();
            record_activity("Aktifitas menghapus data customer berhasil ".$customer_image->name);
            return response()->json(['status' => 'success','msg' => 'Data Customer Berhasil Dihapus']);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas menghapus data customer tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Customer Tidak Berhasil Dihapus']);
        }
    }
}
