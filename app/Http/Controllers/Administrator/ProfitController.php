<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\OrderModel;

use DB;

class ProfitController extends Controller {
    
    public function index_profit() {
        record_activity("Aktifitas mengunjungi halaman profit");
        return view('backend.profit');
    }

	public function profit($start_date, $end_date, $acc_type) {
        $order_filter = OrderModel::whereDate('created_date','>=', date("Y-m-d", strtotime($start_date)))
        ->whereDate('created_date','<=', date("Y-m-d", strtotime($end_date)))
        ->where('is_partner', $acc_type)
        ->where('status_order', 9)
        ->select([ // [ ]<-- biar lebih rapi aja
            'name',
            'email',
            'order_code',
            'tele',
            'status_order',
            'grandtotal_order',
            'is_partner',
            'created_date',
        ])
        ->get();

        return Datatables::of($order_filter)
        ->editColumn('status_order', function ($model) {
            $status_order = '<span class="uk-badge">'.status_order($model->status_order).'</span>';
            return $status_order;
        })
        ->editColumn('grandtotal_order', function ($model) {
            $grandtotal_order = number_format($model->grandtotal_order, 0, ',', '.');
            return $grandtotal_order;
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->rawColumns(['status_order'])
        ->addIndexColumn()->make(true);
    }

    public function calculate_profit($start_date, $end_date, $acc_type) {
        $total_profit = OrderModel::whereDate('created_date','>=', date("Y-m-d", strtotime($start_date)))
        ->whereDate('created_date','<=', date("Y-m-d", strtotime($end_date)))
        ->where('is_partner', $acc_type)
        ->where('status_order', 9)
        ->sum('grandtotal_order');

        $total_order = OrderModel::whereDate('created_date','>=', date("Y-m-d", strtotime($start_date)))
        ->whereDate('created_date','<=', date("Y-m-d", strtotime($end_date)))
        ->where('is_partner', $acc_type)
        ->where('status_order', 9)
        ->count();

        $output_total = array(
            'total_order'    =>  $total_order,
            'total_profit'    =>  number_format($total_profit, 0, ',', '.')
        );
        echo json_encode($output_total);
    }
}
