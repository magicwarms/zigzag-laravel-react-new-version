<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\EkspedisiModel;
use App\Model\OrderModel;

use DB;
use File;

class EkspedisiController extends Controller {
    
	public function index_ekspedisi() {
		return view('backend.ekspedisi');
	}

    public function show_ekspedisi() {
        record_activity("Aktifitas mengunjungi halaman ekspedisi");
        $students = EkspedisiModel::all();
        return Datatables::of($students)
        ->editColumn('status', function ($model) {
            $status = $model->status;
            if($status == 1){
                $status = '<a href="#" data-uk-tooltip title="Aktif"><i class="material-icons md-36 uk-text-success">&#xE86C;</i></a>';
            } else {
                $status='<a href="#" data-uk-tooltip title="Tak Aktif"><i class="material-icons  md-36 uk-text-danger">&#xE5C9;</i></a>';
            }
            return $status;
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = $model->updated_date;
            if($updated != null){
              $updated = date('d F Y H:i:s', strtotime($updated));
            } else {
              $updated = '-';
            }
            return $updated;
        })
        ->editColumn('img_file', function ($model) {
            if(!empty($model->img_file)) {
              $img_file = asset('storage/img/ekspedisi/'.$model->img_file);
            } else {
              $img_file = asset('storage/no-image-available.png');
            }

            return '<img src="'.$img_file.'" alt="'.$model->name.'" class="img_medium"/>';
        })
        ->addColumn('action', function ($model) {
            $action = '
                <a href="#" class="edit_data" data-id="'.$model->id.'"><i class="md-icon material-icons">&#xE254;</i></a>
                <a onclick="delete_data('.$model->id.')" href="#"><i class="md-icon material-icons">&#xE16C;</i>
                </a>
                <a href="'.route('ekspedisi.detail', $model->name).'"><i class="md-icon material-icons">subdirectory_arrow_right</i></a>
            ';
            return $action;
        })
        ->rawColumns(['status','action','img_file'])
        ->addIndexColumn()->make(true);
    }

    function fetch_data_ekspedisi($ekspedisi_id) {
        $ekspedisi = EkspedisiModel::findOrFail($ekspedisi_id);
        if($ekspedisi->status == 1)$status='true'; else $status='';
        $output = array(
            'name'     =>  $ekspedisi->name,
            'img_file'     =>  $ekspedisi->img_file,
            'status'     =>  $status
        );
        record_activity("Aktifitas edit data ekspedisi ".$ekspedisi->name);
        echo json_encode($output);
    }

	public function save_ekspedisi() {
		DB::beginTransaction();
		$this->validate(request(), [
            'name' => 'required|min:3|max:80',
            'img_file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);
	
		if(request('status') == 'on')$status=1; else $status=0;
		$img_file = request('img_file');
		$img_file = strtolower(str_random(3)).'-'.$img_file->getClientOriginalName();
		request()->img_file->move(public_path('storage/img/ekspedisi'), $img_file);

    	EkspedisiModel::create([
            'name' => request('name'),
            'status' => $status,
            'img_file' => $img_file,
    	]);

    	DB::commit();
        record_activity("Aktifitas menyimpan data ekspedisi ".request('name'));
        return response()->json(['status' => 'success','msg' => 'Data Ekspedisi Berhasil Ditambahkan']);
	}

	public function delete_ekspedisi(){
		DB::beginTransaction();
		try {
            $ekspedisi = EkspedisiModel::findOrFail(request('id'));
            $file_to_delete = public_path('storage/img/ekspedisi/').$ekspedisi->img_file;
	        File::delete($file_to_delete); // For delete from folder  
	        $ekspedisi->delete();

	        DB::commit();
            record_activity("Aktifitas menghapus data ekspedisi ".$ekspedisi->name);
            return response()->json(['status' => 'success','msg' => 'Data Ekspedisi Berhasil Dihapus']);
        } catch (\Exception $e) {
        	DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas menghapus data ekspedisi tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Ekspedisi Tidak Berhasil Dihapus']);
        }
    }

    public function delete_picture_ekspedisi($id){
		try {
            $ekspedisi = EkspedisiModel::findOrFail($id);
            //hapus file nya dulu
            $files_to_delete = public_path('storage/img/ekspedisi').$ekspedisi->img_file;
	        File::delete($files_to_delete); // For delete from folder
	        //lalu update table
	        DB::table('ekspedisi')->where('id', $ekspedisi->id)->update(['img_file' => '']);
            record_activity("Aktifitas menghapus data gambar ekspedisi berhasil");
            return response()->json(['status' => 'success','msg' => 'Data Gambar Ekspedisi Berhasil Dihapus']);
        } catch (\Exception $e) {
        	DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas menghapus data gambar ekspedisi tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Gambar Ekspedisi Tidak Berhasil Dihapus']);
        }
    }

    public function update_ekspedisi() {
    	DB::beginTransaction();
        $this->validate(request(), [
            'name' => 'required|min:3|max:80',
            'img_file' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);
        if(request('status') == 'on')$status=1; else $status=0;
    	if(request('img_file') == NULL){
            
            $ekspedisi = EkspedisiModel::findOrFail(request('id'));
            $ekspedisi->name = request('name');
            $ekspedisi->status = $status;
            $ekspedisi->save();
            
        } else {
            
        	if(request('status') == 'on')$status=1; else $status=0;
            $img_file = request('img_file');
			$img_file = strtolower(str_random(3)).'-'.$img_file->getClientOriginalName();
			request()->img_file->move(public_path('storage/img/ekspedisi'), $img_file);

            $student = EkspedisiModel::findOrFail(request('id'));
            $student->name = request('name');
            $student->status = $status;
            $student->img_file = $img_file;
            $student->save();

        }
        \Artisan::call('cache:clear');
        DB::commit();
        record_activity("Aktifitas memperbaharui data ekspedisi berhasil");
        return response()->json(['status' => 'success','msg' => 'Data Ekspedisi Berhasil Diperbaharui']);
    }

    public function detail_ekspedisi($ekspedition_company) {
        //dd(strtolower($ekspedition_company));
        $detail_ekspedisi = EkspedisiModel::where('name', $ekspedition_company)->first();
        if($detail_ekspedisi == ''){
            return redirect()->route('ekspedisi');
        }
        //chart
        $revenue = [];
        for($index=1; $index<=12; $index++){
            $ekspedisi_order = OrderModel::where('ekspedition_code', strtolower($ekspedition_company))
            ->whereMonth('created_date', $index)
            ->where('status_order', 9)
            ->sum('ekspedition_total');
            
            if($ekspedisi_order) {
                array_push($revenue, number_format($ekspedisi_order, 0, ',', '.'));
            } else {
                array_push($revenue, 0);
            }
        }
        foreach ($revenue as $key => $chart) {
            $outputs[$key] = $chart;
        }
        
        $chart_data = json_encode($outputs);

        $count_order_ekspedition_handled = OrderModel::where('ekspedition_code', strtolower($ekspedition_company))->count();
        $count_ekspedition_success_order = OrderModel::where('ekspedition_code', strtolower($ekspedition_company))
        ->where('status_order', 9)
        ->count();

        record_activity("Aktifitas memperbaharui data detail ekspedisi");
        return view('backend.detail_ekspedisi', compact('detail_ekspedisi','count_order_ekspedition_handled','count_ekspedition_success_order','chart_data'));
    }

    public function filter_used_ekspedition($ekspedition_company, $start_date, $end_date) {

        $ekspedition_filter = OrderModel::where('ekspedition_code', strtolower($ekspedition_company))
        ->whereDate('created_date','>=', date("Y-m-d", strtotime($start_date)))
        ->whereDate('created_date','<=', date("Y-m-d", strtotime($end_date)))
        ->where('status_order', 9)
        ->select([
            'order_code',
            'tele',
            'status_order',
            'ekspedition_total',
            'created_date',
            'updated_date',
        ])
        ->get();

        return Datatables::of($ekspedition_filter)
        ->editColumn('status_order', function ($model) {
            $status_order = '<span class="uk-badge">'.status_order($model->status_order).'</span>';
            return $status_order;
        })
        ->editColumn('ekspedition_total', function ($model) {
            $ekspedition_total = number_format($model->ekspedition_total, 0, ',', '.');
            return $ekspedition_total;
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = $model->updated_date;
            if($updated != null){
              $updated = date('d F Y H:i:s', strtotime($updated));
            } else {
              $updated = '-';
            }
            return $updated;
        })
        ->rawColumns(['status_order'])
        ->addIndexColumn()->make(true);
    }

    public function calculate_used_ekspedition($ekspedition_company,$start_date, $end_date) {
        $total_ekspedition = OrderModel::where('ekspedition_code', strtolower($ekspedition_company))
        ->whereRaw('created_date BETWEEN '."'".date("Y-m-d", strtotime($start_date))."'".' AND '."'".date("Y-m-d", strtotime($end_date))."'".'')
        ->where('status_order', 9)
        ->sum('ekspedition_total');
        $output_total = array(
            'total_ekspedition'    =>  number_format($total_ekspedition, 0, ',', '.')
        );
        echo json_encode($output_total);
    }
}
