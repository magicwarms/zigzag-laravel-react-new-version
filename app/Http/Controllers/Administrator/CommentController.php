<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\CommentModel;

use DB;

class CommentController extends Controller {
    
    public function delete_comment(){
        DB::beginTransaction();
        try {
            $comment = CommentModel::findOrFail(request('id'));
            record_activity("Aktifitas menghapus data komentar ".$comment->comment);
            $comment->delete();

            DB::commit();
            
            return response()->json(['status' => 'success','msg' => 'Data Comment Berhasil Dihapus']);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas tidak berhasil menghapus data komentar");
            return response()->json(['status' => 'error','msg' => 'Data Comment Tidak Berhasil Dihapus']);
        }
    }
}
