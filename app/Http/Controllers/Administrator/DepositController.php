<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\DepositModel;
use App\Model\CustomerModel;

use App\Mail\DepositAccepted;
use App\Mail\DepositRejected;

use DB;

class DepositController extends Controller {

    public function index_deposit() {
    	$total_deposit = DepositModel::where('status', 3)->sum('total');
        //hitung jumlah customer deposit, pakai count eloquent tak nak
        $customer_deposit = DepositModel::select('customer_id')->groupBy('customer_id')->get();
        $customer_deposit = count($customer_deposit);
        $customer_deposit_used = DepositModel::where('status', 4)->sum('total');
		return view('backend.deposit', compact('total_deposit','customer_deposit','customer_deposit_used'));
	}

	public function show_deposit($start_date, $end_date, $filter) {
        record_activity("Aktifitas mengunjungi halaman deposit");
        $deposit = DB::table('deposit')
        ->join('customers', 'customers.id', '=', 'deposit.customer_id')
        ->whereDate('deposit.created_date','>=', date("Y-m-d", strtotime($start_date)))
        ->whereDate('deposit.created_date','<=', date("Y-m-d", strtotime($end_date)))
        ->where('deposit.deleted_at', null)
        ->orderBy('deposit.id','DESC')
        ->select([ // [ ]<-- biar lebih rapi aja
            'deposit.id',
            'deposit.total',
            'deposit.balance',
            'deposit.status',
            'customers.name AS customer_name',
            'customers.acc_type AS customer_acc_type',
            'deposit.created_date',
            'deposit.updated_date'
        ]);

        if($filter > 0){
            $deposit = $deposit->where('deposit.status', $filter);
        }

        return Datatables::of($deposit)
        ->editColumn('total', function ($model) {
            $total = "-";
            if($model->customer_acc_type != 3 OR \Auth::user()->level == 1) {
    	        if($model->status == 2){
    	        	$total = "<p class='uk-text-danger uk-text-italic uk-text-bold'> - Rp. ".number_format($model->total, 0,',','.')."</p>";
    	        } else if ($model->status == 4) {
    	        	$total = "<p class='uk-text-warning uk-text-italic uk-text-bold'> - Rp. ".number_format($model->total, 0,',','.')."</p>";
    	        } else if ($model->status == 3) {
    	        	$total = "<p class='uk-text-success uk-text-italic uk-text-bold'> + Rp. ".number_format($model->total, 0,',','.')."</p>";
    	        } else if ($model->status == 1) {
                    $total = "<p class='uk-text-primary uk-text-italic uk-text-bold'>Rp. ".number_format($model->total, 0,',','.')."</p>";
                } else if ($model->status == 6) {
                    $total = "<p class='uk-text-success uk-text-italic uk-text-bold'> + Rp. ".number_format($model->total, 0,',','.')."</p>";
                }
            }
            return $total;
        })
        ->editColumn('balance', function ($model) {
            $balance = "-";
            if($model->customer_acc_type != 3 OR \Auth::user()->level == 1) {
                $balance = "Rp. ".number_format($model->balance, 0,',','.');
            }
            return $balance;
        })
        ->editColumn('status', function ($model) {
            if($model->status == 1){
                $status='<span class="uk-badge uk-badge-primary"> Belum Pembayaran / Menunggu Konfirmasi</span>';
            } elseif($model->status == 2) {
                $status='<span class="uk-badge uk-badge-danger"> Transaksi Deposit Ditolak</span>';
            } elseif ($model->status == 3) {
                $status='<span class="uk-badge uk-badge-success"> Transaksi Deposit Ditambah</span>';
            } else if ($model->status == 4) {
                $status='<span class="uk-badge uk-badge-warning"> Transaksi Dikurang</span>';
            } else if ($model->status == 5) {
                $status='<span class="uk-badge uk-badge-primary"> Proses Cek Mutasi</span>';
            } else {
                $status='<span class="uk-badge uk-badge-primary"> Deposit Dikembalikan</span>';
            }

            return $status;
        })
        ->editColumn('created_date', function ($model) {
            $created = $model->created_date;
            if($created != null){
              $created = date('d F Y H:i:s', strtotime($created));
            } else {
              $created = '-';
            }
            return $created;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = $model->updated_date;
            if($updated != null){
              $updated = date('d F Y H:i:s', strtotime($updated));
            } else {
              $updated = '-';
            }
            return $updated;
        })
        ->addColumn('action', function ($model) {
            $action = '';
            if($model->customer_acc_type != 3 OR \Auth::user()->level == 1 OR \Auth::user()->level == 2) {
                $action = '
                    <a href="#" class="show_evidence" data-id="'.$model->id.'"><i class="md-icon material-icons">pageview</i></a>
                ';
            }
            return $action;
        })
        ->rawColumns(['action','status','total'])
        ->addIndexColumn()->make(true);
    }

    function fetch_data_deposit($deposit_id) {

        $isread = DepositModel::findOrFail($deposit_id);
        $isread->is_read = 1;
        $isread->save();

        $deposit = DB::table('deposit')
        ->join('customers', 'customers.id', '=', 'deposit.customer_id')
        ->where('deposit.deleted_at', null)
        ->where('deposit.id', $deposit_id)
        ->select([ // [ ]<-- biar lebih rapi aja
            'customers.name AS customer_name',
            'deposit.kode_order',
            'deposit.total',
            'deposit.evidence',
            'deposit.bank',
            'deposit.sender_bank',
            'deposit.received_bank',
            'deposit.status',
            'deposit.transfer_date'
        ])->first();

        $output = array(
            'customer_name'     =>  $deposit->customer_name,
            'kode_order'     =>  $deposit->kode_order,
            'total'     =>  $deposit->total,
            'evidence'     =>  $deposit->evidence,
            'bank'     =>  $deposit->bank,
            'sender_bank'     =>  $deposit->sender_bank,
            'received_bank'     =>  $deposit->received_bank,
            'status'     =>  $deposit->status,
            'transfer_date'     =>  $deposit->transfer_date
        );
        record_activity("Aktifitas edit data deposit ".$deposit->customer_name);
        echo json_encode($output);
    }

    public function verify_deposit() {
    	DB::beginTransaction();
        $this->validate(request(), [
            'status' => 'required',
        ]);

        $deposit = DepositModel::findOrFail(request('id'));

        $deposit->status = request('status');
        
        $customer_data = CustomerModel::findOrFail($deposit->customer_id);
        $old_customer_depo = $customer_data->deposit;
        $data = [
            'to' => $customer_data->email,
            'name' => $customer_data->name,
            'kode_order' => $deposit->kode_order,
            'total' => $deposit->total,
            'grandtotal_deposit' => $customer_data->deposit+$deposit->total
        ];
        $when = now()->addSeconds(3);
        if(request('status') == 3){
            $deposit->balance = $customer_data->deposit+$deposit->total;
            $customer_data->deposit = $customer_data->deposit+$deposit->total;
            $customer_data->save();

            $sending_mail = Mail::to($customer_data->email)->later($when, new DepositAccepted($data));
        } else {
            $deposit->balance = $old_customer_depo;
            $sending_mail = Mail::to($customer_data->email)->later($when, new DepositRejected($data));
        }

        $save_deposit = $deposit->save();

        DB::commit();

        if($save_deposit){
            record_activity("Aktifitas memperbaharui data deposit berhasil");
            return response()->json(['status' => 'success','msg' => 'Status deposit berhasil diperbaharui dan notifikasi telah dikirimkan']);
        } else {
            DB::rollBack();
            record_activity("Aktifitas memperbaharui data deposit tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Status deposit tidak berhasil diperbaharui']);
        }
        
    }
}
