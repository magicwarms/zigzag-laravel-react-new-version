<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;

use App\Model\ReturnProductModel;
use App\Model\ReturnProductImageModel;
use App\Model\CustomerModel;

use App\Mail\ReturnProduct;

use DB;
use File;

class ReturnProductController extends Controller {
    
	public function index_return() {
		return view('backend.return_product');
	}

    public function show_return() {
        record_activity("Aktifitas mengunjungi halaman return product");
        $returns = ReturnProductModel::with([
        	'images:id,return_product_id,picture',
        	'customers:id,name'
        ]);

        return Datatables::of($returns)
        ->editColumn('customer', function ($model) {
            $customer = $model->customers->name;
            return $customer;
        })
        ->editColumn('reason', function ($model) {
            $desc = $model->reason;
            return $desc;
        })
        ->editColumn('picture', function ($model) {
            $img_pic = $model->images[0]->picture;
            if($img_pic != '') {
              $picture = asset('storage/img/return_product/'.$img_pic);
            } else {
              $picture = asset('templates/img/no-image-available.png');
            }

            return '<img src="'.$picture.'" alt="'.$model->reason.'" class="img_medium"/>';
        })
        ->editColumn('status', function ($model) {
            $status = $model->status;
            if($status == 1){
                $status = '<a href="#" data-uk-tooltip title="Terima"><i class="material-icons md-36 uk-text-success">&#xE86C;</i></a>';
            } else if ($status == 0) {
                $status ='<a href="#" data-uk-tooltip title="Ditolak"><i class="material-icons md-36 uk-text-danger">&#xE5C9;</i></a>';
            } else if ($status == 2) {
            	$status = '<a href="#" data-uk-tooltip title="Menunggu Konfirmasi"><i class="material-icons md-36 uk-text-warning">gavel</i></a>';
            } 
            return $status;
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = $model->updated_date;
            if($updated != null){
              $updated = date('d F Y H:i:s', strtotime($updated));
            } else {
              $updated = '-';
            }
            return $updated;
        })
        ->addColumn('action', function ($model) {
            $action = '
                <a href="#change_approve" id="change_approve" data-id="'.$model->id.'" data-uk-modal="{target:"#change_approve"}"><i class="md-icon material-icons">check_circle_outline</i>
                </a>
                <a onclick="delete_data('.$model->id.')" href="#"><i class="md-icon material-icons">&#xE16C;</i>
                </a>
            ';
            return $action;
        })
        ->rawColumns(['action','status','picture'])
        ->addIndexColumn()->make(true);
    }

    function fetch_data_return($return_id) {
        $return = ReturnProductModel::with([
            'images:id,return_product_id,picture',
            'customers:id,name'
        ])
        ->where('return_product.id', $return_id)
        ->first();
        $output = array(
            'reason'    =>  $return->reason,
            'status' =>  $return->status,
            'customer'    =>  $return->customers->name,
            'picture' => $return->images,
            'return_code' => $return->return_code
        );
        record_activity("Aktifitas edit data return");
        echo json_encode($output);
    }

    public function delete_return(){
		DB::beginTransaction();
		try {

			$return_product_picture = ReturnProductImageModel::where('return_product_id', request('id'))->get();
            foreach ($return_product_picture as $picture) {
                $file_to_delete = public_path('storage/img/return_product/').$picture->picture;
                File::delete($file_to_delete); // For delete from folder  
            }
            //delete all return product image by return product id
            DB::table('return_product_image')->where('return_product_id', request('id'))->delete();

            $return_product = ReturnProductModel::findOrFail(request('id'));
            record_activity("Aktifitas menghapus data return product ".$return_product->reason);
	        $return_product->delete();

	        DB::commit();            
            return response()->json(['status' => 'success','msg' => 'Data Return Produk Berhasil Dihapus']);
        } catch (\Exception $e) {
        	DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas menghapus data return product tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Return Product tidak berhasil Dihapus']);
        }
    }

    public function update_return() {
		DB::beginTransaction();
		$this->validate(request(), [
            'status' => 'required',
        ]);

    	$return = ReturnProductModel::findOrFail(request('id'));
        $return->status = request('status');
        $return->save();

        if(request('status') ==  1){
            $remark = 'Diterima';
        } else {
            $remark = 'Ditolak';
        }
        $customer_data = CustomerModel::findOrFail($return->customer_id);
        $when = now()->addSeconds(3);
        $data = [
            'to' => $customer_data->email,
            'name' => $customer_data->name,
            'remark' => $remark,
            'return_code' => $return->return_code
        ];

        $sending_mail = Mail::to($customer_data->email)->later($when, new ReturnProduct($data));

    	DB::commit();
        record_activity("Aktifitas mengupdate data return produk");
        return response()->json(['status' => 'success','msg' => 'Data Return Produk Berhasil Diupdate']);
	}
}
