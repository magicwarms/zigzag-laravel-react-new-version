<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\CustomerUpgradeModel;
use App\Model\CustomerModel;

use App\Mail\UpgradeAccountAccepted;
use App\Mail\UpgradeAccountRejected;

use DB;

class UpgradeController extends Controller {

	public function index_upgrade() {
        return view('backend.upgrade');
    }

    public function show_upgrade($start_date, $end_date, $filter) {
        record_activity("Aktifitas mengunjungi halaman upgrade akun customer");
        $upgrade = DB::table('confirmation_upgrade_account')
        ->join('customers', 'customers.id', '=', 'confirmation_upgrade_account.customer_id')
        ->whereDate('confirmation_upgrade_account.created_date','>=', date("Y-m-d", strtotime($start_date)))
        ->whereDate('confirmation_upgrade_account.created_date','<=', date("Y-m-d", strtotime($end_date)))
        ->where('confirmation_upgrade_account.deleted_at', null)
        ->select([ // [ ]<-- biar lebih rapi aja
            'customers.name AS customer_name',
            'confirmation_upgrade_account.order_code',
            'confirmation_upgrade_account.id',
            'confirmation_upgrade_account.date',
            'confirmation_upgrade_account.status',
            'confirmation_upgrade_account.unique_code',
            'confirmation_upgrade_account.created_date',
            'confirmation_upgrade_account.updated_date'
        ]);

        if($filter != '-'){
            $upgrade = $upgrade->where('confirmation_upgrade_account.status', $filter);
        }

        return Datatables::of($upgrade)
        ->editColumn('status', function ($model) {
            if($model->status == 1){
                $status = '<span class="uk-badge uk-badge-notification uk-badge-primary">Konfirmasi valid & disetujui</span>';
            } elseif ($model->status == 2) {
                $status = '<span class="uk-badge uk-badge-notification uk-badge-warning">Upgrade diajukan dan menunggu konfirmasi upgrade</span>';
            } elseif ($model->status == 3) {
                $status = '<span class="uk-badge uk-badge-notification uk-badge-primary">Cek Mutasi</span>';
            } else {
                $status = '<span class="uk-badge uk-badge-notification uk-badge-danger">Konfirmasi tidak disetujui</span>';
            }
            return $status;
        })
        ->editColumn('date', function ($model) {
            if($model->date == ''){
                $transfer_date = '-';
            } else {
                $transfer_date = date('d F Y', strtotime($model->date));
            }
            return $transfer_date;
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = date('d F Y H:i:s', strtotime($model->updated_date));
            if($model->updated_date == ''){
                $updated = '-';
            }
            return $updated;
        })
        ->addColumn('action', function ($model) {
            $action = '
                <a href="#" class="show_evidence_upgrade" data-id="'.$model->id.'"><i class="md-icon material-icons">pageview</i></a>
            ';
            return $action;
        })
        ->rawColumns(['action','status'])
        ->addIndexColumn()->make(true);
    }

    function fetch_data_confirmation_upgrade($confirmation_upgrade_id) {

        $isread = CustomerUpgradeModel::findOrFail($confirmation_upgrade_id);
        $isread->is_read = 1;
        $isread->save();
        
        $confirm = DB::table('confirmation_upgrade_account')
        ->join('customers', 'customers.id', '=', 'confirmation_upgrade_account.customer_id')
        ->where('confirmation_upgrade_account.deleted_at', null)
        ->where('confirmation_upgrade_account.id', $confirmation_upgrade_id)
        ->select([ // [ ]<-- biar lebih rapi aja
            'customers.name AS customer_name',
            'confirmation_upgrade_account.order_code',
            'confirmation_upgrade_account.bank_sender',
            'confirmation_upgrade_account.bank_number_sender',
            'confirmation_upgrade_account.bank_receiver',
            'confirmation_upgrade_account.date',
            'confirmation_upgrade_account.evidence',
            'confirmation_upgrade_account.total_transfer',
            'confirmation_upgrade_account.status',
            'confirmation_upgrade_account.unique_code'
        ])
        ->first();

        if($confirm->status == 1){
            $status_detail = '<span class="uk-badge uk-badge-notification uk-badge-primary">Konfirmasi valid & disetujui</span>';
        } elseif ($confirm->status == 2) {
            $status_detail = '<span class="uk-badge uk-badge-notification uk-badge-warning">Upgrade diajukan dan menunggu konfirmasi upgrade</span>';
        } elseif ($confirm->status == 3) {
            $status_detail = '<span class="uk-badge uk-badge-notification uk-badge-primary">Cek Mutasi</span>';
        } else {
            $status_detail = '<span class="uk-badge uk-badge-notification uk-badge-danger">Konfirmasi tidak disetujui</span>';
        }

        $output = array(
            'customer_name'     =>  $confirm->customer_name,
            'order_code'     =>  $confirm->order_code,
            'bank_sender'     =>  $confirm->bank_sender,
            'bank_number_sender'     =>  $confirm->bank_number_sender,
            'bank_receiver'     =>  $confirm->bank_receiver,
            'unique_code'     =>  $confirm->unique_code,
            'date'     =>  date('d F Y', strtotime($confirm->date)),
            'evidence'     =>  $confirm->evidence,
            'total_transfer'     =>  number_format($confirm->total_transfer, 0,',','.'),
            'status'     =>  $confirm->status,
            'status_detail'     =>  $status_detail,
        );
        record_activity("Aktifitas edit upgrade akun customer ".$confirm->customer_name);
        echo json_encode($output);
    }

    public function verify_confirmation_upgrade() {
        DB::beginTransaction();
        $this->validate(request(), [
            'status' => 'required',
            'order_code' => 'required|min:5',
        ]);

        //update account type nya sih customer
        $confirm = CustomerUpgradeModel::findOrFail(request('id'));
        $confirm->status = request('status');
        $save_confirm = $confirm->save();

        $customer_data = CustomerModel::findOrFail($confirm->customer_id);
        $data = [
            'to' => $customer_data->email,
            'name' => $customer_data->name,
            'order_code' => request('order_code')
        ];
        $when = now()->addSeconds(3);
        if(request('status') == 1){
            $update_status_order = DB::table('confirmation_upgrade_account')->where('order_code', request('order_code'))->update(['status' => 1]); //pembayaran disetujui
            $customer_data->acc_type = 2;
            $customer_data->save();
            $sending_mail = Mail::to($customer_data->email)->later($when, new UpgradeAccountAccepted($data));
        } else {
            $update_status_order = DB::table('confirmation_upgrade_account')->where('order_code', request('order_code'))->update(['status' => 0]); //pembayaran ditolak
            $customer_data->acc_type = 1;
            $customer_data->save();
            $sending_mail = Mail::to($customer_data->email)->later($when, new UpgradeAccountRejected($data));
        }

        DB::commit();
        if($save_confirm){
            record_activity("Aktifitas memperbaharui konfirmasi upgrade akun customer berhasil ".$customer_data->name);
            return response()->json(['status' => 'success','msg' => 'Status konfirmasi upgrade akun berhasil diperbaharui dan notifikasi telah dikirimkan']);
        } else {
            DB::rollBack();
            record_activity("Aktifitas memperbaharui konfirmasi upgrade akun customer tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Status konfirmasi upgrade akun tidak berhasil diperbaharui']);
        }
    }
}
