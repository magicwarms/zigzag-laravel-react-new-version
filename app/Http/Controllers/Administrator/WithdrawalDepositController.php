<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\WithdrawalDepositModel;
use App\Model\CustomerModel;

use App\Mail\WithdrawalDepositAccepted;
use App\Mail\WithdrawalDepositRejected;

use DB;

class WithdrawalDepositController extends Controller {
    
	public function index_withdrawal() {
        return view('backend.withdrawal');
    }

    public function show_withdrawal() {
        record_activity("Aktifitas mengunjungi halaman withdrawal deposit customer");

        $withdrawal = WithdrawalDepositModel::with('customer:id,name');

        return Datatables::of($withdrawal)
        ->editColumn('customer_name', function ($model) {
            $customer_name = $model->customer->name;
            return $customer_name;
        })
        ->editColumn('status', function ($model) {
            if($model->status == 1){
                $status = '<span class="uk-badge uk-badge-notification uk-badge-primary">Pencairan Diajukan</span>';
            } elseif ($model->status == 2) {
                $status = '<span class="uk-badge uk-badge-notification uk-badge-warning">Pencairan disetujui</span>';
            } elseif ($model->status == 3) {
                $status = '<span class="uk-badge uk-badge-notification uk-badge-primary">Pencairan ditolak</span>';
            }

            return $status;
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = date('d F Y H:i:s', strtotime($model->updated_date));
            if($model->updated_date == ''){
                $updated = '-';
            }
            return $updated;
        })
        ->addColumn('action', function ($model) {
            $action = '
                <a href="#" class="show_withdrawal_account" data-id="'.$model->id.'"><i class="md-icon material-icons">pageview</i></a>
            ';
            return $action;
        })
        ->rawColumns(['action','status'])
        ->addIndexColumn()->make(true);
    }

    function fetch_data_withdrawal($withdrawal_deposit_id) {

        $withdrawal = WithdrawalDepositModel::with('customer:id,name')
        ->where('withdrawal_deposit.id', $withdrawal_deposit_id)
        ->first();
        
        if($withdrawal->status == 1){
            $status_detail = '<span class="uk-badge uk-badge-notification uk-badge-primary">Pencairan Diajukan</span>';
        } elseif ($withdrawal->status == 2) {
            $status_detail = '<span class="uk-badge uk-badge-notification uk-badge-warning">Pencairan disetujui</span>';
        } elseif ($withdrawal->status == 3) {
            $status_detail = '<span class="uk-badge uk-badge-notification uk-badge-primary">Pencairan ditolak</span>';
        }

        $output = array(
            'customer_name'     =>  $withdrawal->customer->name,
            'code'     =>  $withdrawal->code,
            'amount'     =>  number_format($withdrawal->amount, 0,',','.'),
            'status'     =>  $withdrawal->status,
            'bank'     =>  $withdrawal->bank,
            'status_detail'     =>  $status_detail
        );
        record_activity("Aktifitas edit withdrawal deposit customer ".$withdrawal->customer->name);
        echo json_encode($output);
    }

    public function verify_withdrawal() {
        DB::beginTransaction();
        $this->validate(request(), [
            'status' => 'required',
        ]);
        
        //update status withdrawal
        $wd = WithdrawalDepositModel::findOrFail(request('id'));
        $wd->status = request('status');
        $update_wd = $wd->save();

        $customer_data = CustomerModel::findOrFail($wd->customer_id);
        $data = [
            'to' => $customer_data->email,
            'name' => $customer_data->name,
            'code' => $wd->code,
            'amount' => number_format($wd->amount, 0,',','.'),
            'bank' => $wd->bank
        ];
        $when = now()->addSeconds(3);
        if(request('status') == 2){ 
        	//disetujui
            $sending_mail = Mail::to($customer_data->email)->later($when, new WithdrawalDepositAccepted($data));
        } else {
        	//ditolak,kembalikan lagi deposit sebelum nya ke customer
            $customer_data->deposit = $customer_data->deposit+$wd->amount;
            $sending_mail = Mail::to($customer_data->email)->later($when, new WithdrawalDepositRejected($data));
        }

        $customer_data->save();

        DB::commit();
        if($update_wd){
            record_activity("Aktifitas memperbaharui konfirmasi withdrawal deposit customer berhasil dengan kode ".$wd->code);
            return response()->json(['status' => 'success','msg' => 'Status konfirmasi withdrawal deposit customer berhasil diperbaharui dan notifikasi telah dikirimkan']);
        } else {
            DB::rollBack();
            record_activity("Aktifitas memperbaharui konfirmasi withdrawal deposit customer tidak berhasil dengan kode ".$wd->code);
            return response()->json(['status' => 'error','msg' => 'Status konfirmasi withdrawal deposit customer tidak berhasil diperbaharui']);
        }
    }
}
