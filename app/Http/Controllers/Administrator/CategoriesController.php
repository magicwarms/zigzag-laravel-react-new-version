<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\CategoriesModel;

use DB;
use File;

class CategoriesController extends Controller {
    
	public function index_categories() {
		$categories = CategoriesModel::pluck('name','id');
		return view('backend.categories', compact('categories'));
	}

    public function show_categories() {
        record_activity("Aktifitas mengunjungi halaman kategori");
        $categories = CategoriesModel::all();
        return Datatables::of($categories)
        ->editColumn('parent', function ($model) {
            if($model->parent == 0){
            	$parent = 'Parent';
            } else {
            	$parents = CategoriesModel::where('id', $model->parent)->first();
            	$parent = '<span class="uk-badge uk-badge-primary">'.$parents->name.'</span>';
            }
            return $parent;
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = $model->updated_date;
            if($updated != null){
              $updated = date('d F Y H:i:s', strtotime($updated));
            } else {
              $updated = '-';
            }
            return $updated;
        })
        ->editColumn('picture', function ($model) {
            $pic = $model->picture;
            if(!empty($pic)) {
              $picture = asset('storage/img/category/'.$pic);
            } else {
              $picture = asset('templates/img/no-image-available.png');
            }

            return '<img src="'.$picture.'" alt="'.$model->name.'" class="img_medium"/>';
        })
        ->addColumn('action', function ($model) {
            $action = '
                <a href="#" class="edit_data" data-id="'.$model->id.'"><i class="md-icon material-icons">&#xE254;</i></a>
                <a onclick="delete_data('.$model->id.')" href="#"><i class="md-icon material-icons">&#xE16C;</i>
                </a>
            ';
            return $action;
        })
        ->rawColumns(['parent','action','picture'])
        ->addIndexColumn()->make(true);
    }

    function fetch_data_categories($category_id) {
        $cat = CategoriesModel::findOrFail($category_id);
        $output = array(
            'name'    =>  $cat->name,
            'parent'     =>  $cat->parent,
            'picture'     =>  $cat->picture
        );
        record_activity("Aktifitas edit data kategori ".$cat->name);
        echo json_encode($output);
    }

	public function save_categories() {
		DB::beginTransaction();
		$this->validate(request(), [
            'name' => 'required|max:60|min:3',
            'picture' => 'required|image|mimes:jpeg,png,jpg'
        ]);

        $picture = request('picture');
        if($picture){
            $name_file = strtolower(str_random(3)).'-'.$picture->getClientOriginalName();
            $picture->move(public_path('storage/img/category'), $name_file);
        }

		if(request('parent') == '')$parent = 0; else $parent = request('parent');
    	CategoriesModel::create([
    		'name' => request('name'),
    		'parent' => $parent,
            'picture' => $name_file
    	]);

    	DB::commit();
        record_activity("Aktifitas menyimpan data kategori ".request('name'));
        return response()->json(['status' => 'success','msg' => 'Data Kategori Berhasil Ditambahkan']);
	}

	public function delete_categories(){
		DB::beginTransaction();
		try {
            $category = CategoriesModel::findOrFail(request('id'));
            record_activity("Aktifitas menghapus data kategori ".$category->name);
            $file_to_delete = public_path('storage/img/category/').$category->picture;
            File::delete($file_to_delete); // For delete from folder

	        $category->delete();

	        DB::commit();
            return response()->json(['status' => 'success','msg' => 'Data Kategori Berhasil Dihapus']);
        } catch (\Exception $e) {
        	DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas menghapus data kategori tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Kategori tidak berhasil Dihapus']);
        }
    }

    public function update_categories() {
    	DB::beginTransaction();
        $this->validate(request(), [
            'name' => 'required|max:60|min:3',
            'picture' => 'image|mimes:jpeg,png,jpg'
        ]);

        if(request('parent') == '')$parent = 0; else $parent = request('parent');

        if(request('picture') == NULL){
            
            $category = CategoriesModel::findOrFail(request('id'));
            $category->name = request('name');
            $category->parent = $parent;
            $category->save();
        } else {

            $picture = request('picture');
            $name_file = strtolower(str_random(3)).'-'.$picture->getClientOriginalName();
            $picture->move(public_path('storage/img/category'), $name_file);

            $category = CategoriesModel::findOrFail(request('id'));
            $category->name = request('name');
            $category->parent = $parent;
            $category->picture = $name_file;
            $category->save();
        }
    	DB::commit();
        record_activity("Aktifitas memperbaharui data kategori ".$category->name);
        return response()->json(['status' => 'success','msg' => 'Data Kategori Berhasil Diperbaharui']);
    }

    public function delete_picture_category($id){
        try {
            $categories = CategoriesModel::findOrFail($id);
            //hapus file nya dulu
            $file_to_delete = public_path('storage/img/category/').$categories->picture;
            File::delete($file_to_delete); // For delete from folder
            //lalu update table
            DB::table('categories')->where('id', $categories->id)->update(['picture' => '']);

            record_activity("Aktifitas menghapus data gambar kategori ".$categories->name);
            return response()->json(['status' => 'success','msg' => 'Data Gambar Category Berhasil Dihapus']);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas tidak berhasil menghapus data gambar kategori");
            return response()->json(['status' => 'error','msg' => 'success','msg' => 'Data Gambar Category Tidak Berhasil Dihapus']);
        }
    }
}
