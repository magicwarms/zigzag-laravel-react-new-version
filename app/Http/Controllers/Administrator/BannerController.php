<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\BannerModel;
use App\Model\BannerDetailModel;
use App\Model\ProductModel;

use DB;
use File;

class BannerController extends Controller {
    
	public function index_banner() {
		$products = ProductModel::select('name','id','material')->get();
		return view('backend.banner', compact('products'));
	}

    public function show_banner() {
        record_activity("Aktifitas mengunjungi halaman banner");
        $banners = BannerModel::all();
        return Datatables::of($banners)
        ->editColumn('picture', function ($model) {
            $pic = $model->picture;
            if(!empty($pic)) {
              $picture = asset('storage/img/banner/'.$pic);
            } else {
              $picture = asset('templates/img/no-image-available.png');
            }

            return '<img src="'.$picture.'" alt="'.$model->name.'" class="img_medium"/>';
        })
        ->editColumn('description', function ($model) {
            $desc = str_limit($model->description, 20);
            return $desc;
        })
        ->addColumn('count_products', function ($model) {
            $count_products = BannerDetailModel::where('banner_id', $model->id)->get();
            return '<span class="uk-badge uk-badge-danger uk-badge-notification">'.$count_products->count().'</span>';
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->editColumn('start_date', function ($model) {
            $start_date = date('d F Y', strtotime($model->start_date));
            return $start_date;
        })
        ->editColumn('end_date', function ($model) {
            $end_date = date('d F Y', strtotime($model->end_date));
            return $end_date;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = $model->updated_date;
            if($updated != null){
              $updated = date('d F Y H:i:s', strtotime($updated));
            } else {
              $updated = '-';
            }
            return $updated;
        })
        ->addColumn('status', function ($model) {
            $today = strtotime(date("Y-m-d"));
            $startads = strtotime($model->start_date);
            $endads = strtotime($model->end_date);

            if($today > $endads){
              $status = '<span class="uk-badge uk-badge-danger">Expired</span>';
            } elseif($startads > $today) {
              $status = '<span class="uk-badge uk-badge-warning">Not Active</span>';
            } else {
              $status = '<span class="uk-badge uk-badge-primary">Active</span>'; 
            }

            return $status;
        })
        ->addColumn('action', function ($model) {
            $action = '
                <a href="#" class="edit_data" data-id="'.$model->id.'"><i class="md-icon material-icons">&#xE254;</i></a>
                <a onclick="delete_data('.$model->id.')" href="#"><i class="md-icon material-icons">&#xE16C;</i>
                </a>
            ';
            return $action;
        })
        ->rawColumns(['action','count_products','status','picture'])
        ->addIndexColumn()->make(true);
    }

    function fetch_data_banner($banner_id) {
        $banner = BannerModel::findOrFail($banner_id);

        $banner_detail = DB::table('banner_detail')
        ->join('products', 'products.id', '=', 'banner_detail.product_id')
        ->where('banner_id', $banner_id)
        ->select([
        	'products.id',
        	'products.name',
        	'products.material',
        ])
        ->get();

        $output = array(
            'title'    =>  $banner->title,
            'description' =>  $banner->description,
            'picture' =>  $banner->picture,
            'start_date' =>  date("d.m.Y",strtotime($banner->start_date)),
            'end_date' => date("d.m.Y",strtotime($banner->end_date)),
            'banner_detail' => $banner_detail,
        );
        record_activity("Aktifitas edit data banner ".$banner->title);
        echo json_encode($output);
    }

	public function save_banner() {
		DB::beginTransaction();
		$this->validate(request(), [
            'title' => 'required|max:120|min:3',
            'picture' => 'required|image|mimes:jpeg,png,jpg',
            'description' => 'required|min:5',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ]);

        //START upload banner picture
        $picture = request('picture');
        if($picture){
            $name_file = strtolower(str_random(3)).'-'.$picture->getClientOriginalName();
            $picture->move(public_path('storage/img/banner'), $name_file);
        }
        // END upload banner picture

    	$save_banner = BannerModel::create([
    		'title' => request('title'),
    		'description' => request('description'),
            'picture' => $name_file,
            'start_date' => date("Y-m-d",strtotime(request('start_date'))),
            'end_date' => date("Y-m-d",strtotime(request('end_date')))
    	]);
    	
    	foreach (request('product_id') as $value) {
    		BannerDetailModel::create([
	          'banner_id' => $save_banner->id, //last id save
	          'product_id' => $value
          	]);
    	}

    	DB::commit();
        record_activity("Aktifitas menyimpan data banner ".request('title'));
        return response()->json(['status' => 'success','msg' => 'Data Banner Berhasil Ditambahkan']);
	}

	public function delete_banner(){
		DB::beginTransaction();
		try {

			$banner_detail = BannerDetailModel::where('banner_id', request('id'))->delete();
			
            $banner = BannerModel::findOrFail(request('id'));
            record_activity("Aktifitas menghapus data banner ".$banner->title);
            $file_to_delete = public_path('storage/img/banner/').$banner->picture;
            File::delete($file_to_delete); // For delete from folder
	        $banner->delete();

	        DB::commit();            
            return response()->json(['status' => 'success','msg' => 'Data Banner Berhasil Dihapus']);
        } catch (\Exception $e) {
        	DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas menghapus data banner tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Banner tidak berhasil Dihapus']);
        }
    }

    public function update_banner() {
    	DB::beginTransaction();
        $this->validate(request(), [
            'title' => 'required|max:120|min:3',
            'picture' => 'image|mimes:jpeg,png,jpg',
            'description' => 'required|min:5',
            'start_date' => 'required|date',
            'end_date' => 'required|date'
        ]);

        $banner = BannerModel::findOrFail(request('id'));
        $banner->title = request('title');
        $banner->description = request('description');
        $banner->start_date = date("Y-m-d",strtotime(request('start_date')));
        $banner->end_date = date("Y-m-d",strtotime(request('end_date')));

        //START upload banner picture
        $picture = request('picture');
        if($picture){
            $name_file = strtolower(str_random(3)).'-'.$picture->getClientOriginalName();
            $picture->move(public_path('storage/img/banner'), $name_file);
            $banner->picture = $name_file;
        }
        // END upload banner picture

        $banner->save();

        //start delete first
        $delete_banner_detail = BannerDetailModel::where('banner_id', request('id'));
        $delete_banner_detail->delete();
        //end delete first
        if(request('product_id') != ''){
        	//START save data di table banner_detail
	        foreach (request('product_id') as $value) {
	            BannerDetailModel::create([
	                'banner_id' => request('id'), //last id save
	                'product_id' => $value,
	            ]);
	        }
	        // END save data di table banner_detail
        }

    	DB::commit();
        record_activity("Aktifitas memperbaharui data banner ".$banner->name);
        return response()->json(['status' => 'success','msg' => 'Data Banner Berhasil Diperbaharui']);
    }

    public function delete_picture_banner($id){
        try {
            $banner = BannerModel::findOrFail($id);
            //hapus file nya dulu
            $file_to_delete = public_path('storage/img/banner/').$banner->picture;
            File::delete($file_to_delete); // For delete from folder
            //lalu update table
            DB::table('banner')->where('id', $banner->id)->update(['picture' => '']);

            record_activity("Aktifitas menghapus data gambar banner ".$banner->title);
            return response()->json(['status' => 'success','msg' => 'Data Gambar Banner Berhasil Dihapus']);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas tidak berhasil menghapus data gambar banner");
            return response()->json(['status' => 'error','msg' => 'success','msg' => 'Data Gambar Banner Tidak Berhasil Dihapus']);
        }
    }
}
