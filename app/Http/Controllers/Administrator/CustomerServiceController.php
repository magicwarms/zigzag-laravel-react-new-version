<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\CustomerServiceModel;
use App\Model\OrderModel;
use App\Model\OrderDetailModel;
use App\Model\CustomerModel;
use App\Model\ViewOrderModel;

use App\Charts\OrderSuccessfullCustomerServiceChart;

use DB;

class CustomerServiceController extends Controller {
    
	public function index_cs() {
		return view('backend.customer_service');
	}

    public function show_cs() {
        record_activity("Aktifitas mengunjungi halaman customer service");
        $customer_service = CustomerServiceModel::all();
        return Datatables::of($customer_service)
        ->editColumn('status', function ($model) {
            $status = $model->status;
            if($status == 1){
                $status = '<a href="#" data-uk-tooltip title="Aktif"><i class="material-icons md-36 uk-text-success">&#xE86C;</i></a>';
            } else {
                $status='<a href="#" data-uk-tooltip title="Tak Aktif"><i class="material-icons  md-36 uk-text-danger">&#xE5C9;</i></a>';
            }
            return $status;
        })
        ->editColumn('is_partner', function ($model) {
            $is_partner = $model->is_partner;
            if($is_partner == 1){
                $is_partner = '<a href="#" data-uk-tooltip title="CS Khusus Partner"><i class="material-icons md-36 uk-text-primary">&#xE86C;</i></a>';
            } else {
                $is_partner='<a href="#" data-uk-tooltip title="CS Reguler"><i class="material-icons  md-36 uk-text-warning">&#xE5C9;</i></a>';
            }
            return $is_partner;
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = $model->updated_date;
            if($updated != null){
              $updated = date('d F Y H:i:s', strtotime($updated));
            } else {
              $updated = '-';
            }
            return $updated;
        })
        ->addColumn('action', function ($model) {
            $action = '
                <a href="#" class="edit_data" data-id="'.$model->id.'"><i class="md-icon material-icons">&#xE254;</i></a>
                <a onclick="delete_data('.$model->id.')" href="#"><i class="md-icon material-icons">&#xE16C;</i>
                </a>
                <a href="'.route('cs.detail', $model->id).'"><i class="md-icon material-icons">subdirectory_arrow_right</i></a>
            ';
            return $action;
        })
        ->rawColumns(['status','action','is_partner'])
        ->addIndexColumn()->make(true);
    }

    function fetch_data_cs($cs_id) {
        $cs = CustomerServiceModel::findOrFail($cs_id);
        if($cs->status == 1)$status='true'; else $status='';
        if($cs->is_partner == 1)$is_partner='true'; else $is_partner='';
        $output = array(
            'name'    =>  $cs->name,
            'whatsapp'     =>  $cs->whatsapp,
            'address'     =>  $cs->address,
            'line'     =>  $cs->line,
            'commission'  =>  $cs->commission,
            'status'     =>  $status,
            'is_partner'     =>  $is_partner,
        );
        record_activity("Aktifitas edit data customer service ".$cs->name);
        echo json_encode($output);
    }

	public function save_cs() {
		DB::beginTransaction();
		$this->validate(request(), [
            'name' => 'required|max:80|min:3',
            'address' => 'required|min:3',
            'commission' => 'required|integer',
        ]);

        if(request('status') == 'on')$status=1; else $status=0;
		if(request('is_partner') == 'on')$is_partner=1; else $is_partner=0;
    	CustomerServiceModel::create([
    		'name' => request('name'),
    		'whatsapp' => request('whatsapp'),
    		'address' => request('address'),
            'line' => request('line'),
    		'commission' => request('commission'),
            'status' => $status,
            'is_partner' => $is_partner,
    	]);

    	DB::commit();
        record_activity("Aktifitas menyimpan data customer service berhasil");
        return response()->json(['status' => 'success','msg' => 'Data Customer Service Berhasil Ditambahkan']);
	}

	public function delete_cs(){
		DB::beginTransaction();
		try {

            $check_cs = CustomerModel::where('customer_service_id', request('id'))->get();
            $count_check_cs = count($check_cs);
            if(!$check_cs->isEmpty()){
                return response()->json(['status' => 'error','msg' => 'Data Customer Service Masih Terkait Pada '.$count_check_cs.' Customer.']);
            }

            $cs = CustomerServiceModel::findOrFail(request('id'));
	        $cs->delete();

	        DB::commit();
            record_activity("Aktifitas menghapus data customer service berhasil");
            return response()->json(['status' => 'success','msg' => 'Data Customer Service Berhasil Dihapus']);
        } catch (\Exception $e) {
        	DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas menyimpan data customer service tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Customer Service tidak Berhasil Dihapus']);
        }
    }

    public function update_cs() {
    	DB::beginTransaction();
        $this->validate(request(), [
            'name' => 'required|max:80|min:3',
            'address' => 'required|min:3',
            'commission' => 'required|integer',
        ]);

        if(request('status') == 'on')$status=1; else $status=0;
        if(request('is_partner') == 'on')$is_partner=1; else $is_partner=0;
        $cs = CustomerServiceModel::findOrFail(request('id'));
        $cs->name = request('name');
        $cs->whatsapp = request('whatsapp');
        $cs->address = request('address');
        $cs->line = request('line');
        $cs->commission = request('commission');
        $cs->status = $status;
        $cs->is_partner = $is_partner;
        $cs->save();

    	DB::commit();
        record_activity("Aktifitas memperbaharui data customer service berhasil");
        return response()->json(['status' => 'success','msg' => 'Data Customer Service Berhasil Diperbaharui']);
    }

    public function detail_customer_service($customer_service_id) {

        $detail_cs = CustomerServiceModel::where('id', $customer_service_id)->first();
        if($detail_cs == ''){
            return redirect()->route('cs');
        }
        //chart
        $chart = new OrderSuccessfullCustomerServiceChart;
        $chart->labels(["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"]);
        $revenue = [];
        for($index=1; $index<=12; $index++){
            $order_success_by_cs = OrderModel::where('customer_service_id', $customer_service_id)
            ->whereMonth('created_date','=', $index)
            ->whereYear('created_date','=', date('Y'))
            ->where('status_order', 9)
            ->sum('grandtotal_order');
            
            if($order_success_by_cs != '') {
                array_push($revenue, $order_success_by_cs);
            } else {
                array_push($revenue, 0);
            }
        }

        $chart->dataset('Order Successful By '.$detail_cs->name, 'bar', $revenue)->options([
            'hoverBackgroundColor' => random_color(),
            'backgroundColor' => random_color()
        ]);
        
        $count_customer = OrderModel::where('customer_service_id', $customer_service_id)->count();
        $count_customer_success_order = OrderModel::where('customer_service_id', $customer_service_id)
        ->where('status_order', 9)
        ->count();
        record_activity("Aktifitas mengunjungi halaman detail customer service");
        return view('backend.detail_customer_service', compact('detail_cs','count_customer','count_customer_success_order','chart'));
    }

    public function filter_profit_cs($customer_service_id, $start_date, $end_date) {

        $order_filter = OrderModel::where('customer_service_id', $customer_service_id)
        ->whereDate('created_date','>=', date("Y-m-d", strtotime($start_date)))
        ->whereDate('created_date','<=', date("Y-m-d", strtotime($end_date)))
        ->select([ // [ ]<-- biar lebih rapi aja
            'name',
            'order_code',
            'tele',
            'status_order',
            'grandtotal_order',
            'created_date',
            'updated_date',
        ])
        ->get();

        return Datatables::of($order_filter)
        ->editColumn('status_order', function ($model) {
            $status_order = '<span class="uk-badge">'.status_order($model->status_order).'</span>';
            return $status_order;
        })
        ->editColumn('grandtotal_order', function ($model) {
            $grandtotal_order = number_format($model->grandtotal_order, 0, ',', '.');
            return $grandtotal_order;
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->editColumn('updated_date', function ($model) {
            $updated = $model->updated_date;
            if($updated != null){
              $updated = date('d F Y H:i:s', strtotime($updated));
            } else {
              $updated = '-';
            }
            return $updated;
        })
        ->rawColumns(['status_order'])
        ->addIndexColumn()->make(true);
    }

    public function calculate_profit_cs($customer_service_id,$start_date, $end_date) {
        $total_order_qty = OrderModel::where('customer_service_id', $customer_service_id)
        ->whereDate('created_date','>=', date("Y-m-d", strtotime($start_date)))
        ->whereDate('created_date','<=', date("Y-m-d", strtotime($end_date)))
        ->where('status_order', 9)
        ->select('id')
        ->get();

        $total_qty = 0;
        foreach ($total_order_qty as $val) {
            $order_detail = OrderDetailModel::where('order_id', $val->id)
            ->select('product_qty')
            ->get();
            foreach ($order_detail as $detail) {
                $total_qty += $detail->product_qty;
            }
        }

        $get_cs = CustomerServiceModel::where('id', $customer_service_id)
        ->select('commission')
        ->first();

        $total_profit_cs = $total_qty*$get_cs->commission;
        $output_total = array(
            'total_qty'    =>  $total_qty,
            'total_profit_cs'    =>  number_format($total_profit_cs, 0, ',', '.')
        );
        echo json_encode($output_total);
    }
}
