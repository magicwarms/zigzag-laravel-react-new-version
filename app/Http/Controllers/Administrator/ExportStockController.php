<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;

use App\Exports\StockExport;

class ExportStockController extends Controller {

	public function index(){
		record_activity("Aktifitas mengunjungi halaman laporan stock");
        return view('backend.stock');
	}

    public function export()  {
    	$start_date = date('d-F-Y');
    	record_activity("Aktifitas mendownload laporan stock - ".$start_date);
    	return Excel::download(new StockExport, 'Stock-Report-'.$start_date.'.xlsx', \Maatwebsite\Excel\Excel::XLSX);
	}
}
