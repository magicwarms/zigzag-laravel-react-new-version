<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\ProductModel;
use App\Model\CategoriesModel;
use App\Model\ViewProductModel;
use App\Model\RatingModel;

use App\Model\ProductPriceDetailModel;
use App\Model\ProductMoreDetailModel;
use App\Model\ProductImageDetailModel;
use App\Model\ProductColorDetailModel;


use DB;
use File;

class ProductController extends Controller {
    
	public function index_product() {
		$products = ProductModel::all();
		$categories = CategoriesModel::pluck('name','id');
        $price_type = ['reguler'=>'Reguler', 'vip'=>'VIP', 'promo'=>'Promo', 'partner' => 'Partner'];
		return view('backend.product', compact('products','categories','price_type'));
	}

	public function show_product() {
        record_activity("Aktifitas mengunjungi halaman atau refresh produk");
        $products = DB::table('products')
        ->join('categories', 'categories.id', '=', 'products.categories_id')
        ->orderBy('created_date', 'DESC')
        ->select([ // [ ]<-- biar lebih rapi aja
            'products.id',
            'products.name',
            'categories.name AS categories_name',
            'products.hot',
            'products.promo',
            'products.restock',
            'products.created_date'
        ]);
        return Datatables::of($products)
        ->editColumn('hot', function ($model) {
            $hot = $model->hot;
            if($hot == 1){
                $hot = '<a href="#" data-uk-tooltip title="Hot"><i class="material-icons md-36 uk-text-success">&#xE86C;</i></a>';
            } else {
                $hot='<a href="#" data-uk-tooltip title="Bukan Hot"><i class="material-icons md-36 uk-text-danger">&#xE5C9;</i></a>';
            }
            return $hot;
        })
        ->editColumn('promo', function ($model) {
            $promo = $model->promo;
            if($promo == 1){
                $promo = '<a href="#" data-uk-tooltip title="Promo"><i class="material-icons md-36 uk-text-success">&#xE86C;</i></a>';
            } else {
                $promo ='<a href="#" data-uk-tooltip title="Bukan Promo"><i class="material-icons md-36 uk-text-danger">&#xE5C9;</i></a>';
            }
            return $promo;
        })
        ->editColumn('restock', function ($model) {
            $restock = $model->restock;
            if($restock == 1){
                $restock = '<a href="#" data-uk-tooltip title="Restock"><i class="material-icons md-36 uk-text-success">&#xE86C;</i></a>';
            } else {
                $restock ='<a href="#" data-uk-tooltip title="Bukan Restock"><i class="material-icons md-36 uk-text-danger">&#xE5C9;</i></a>';
            }
            return $restock;
        })
        ->editColumn('created_date', function ($model) {
            $created = date('d F Y H:i:s', strtotime($model->created_date));
            return $created;
        })
        ->editColumn('picture', function ($model) {
            $img_pic = ProductImageDetailModel::where('product_id', $model->id)->first();
            if($img_pic != '') {
              $picture = asset('storage/img/product/'.$img_pic->image);
            } else {
              $picture = asset('templates/img/no-image-available.png');
            }

            return '<img src="'.$picture.'" alt="'.$model->name.'" class="img_medium"/>';
        })
        ->addColumn('action', function ($model) {
            $action = '
                <a href="#" class="edit_data" data-id="'.$model->id.'" data-name="'.$model->name.'"><i class="md-icon material-icons">&#xE254;</i></a>
                <a onclick="delete_data('.$model->id.')" href="#"><i class="md-icon material-icons">&#xE16C;</i>
                </a>
                <a href="'.route('product.detail', [$model->id, str_slug($model->name, '-')]).'"><i class="md-icon material-icons">subdirectory_arrow_right</i></a>
            ';
            return $action;
        })
        ->rawColumns(['hot','action','picture','promo','restock'])
        ->addIndexColumn()->make(true);
    }

    function fetch_data_product($product_id) {
        $product = ProductModel::findOrFail($product_id);
        $product_price_detail = ProductPriceDetailModel::where('product_id', $product_id)->get();
        $product_image_detail = ProductImageDetailModel::where('product_id', $product_id)->get();

        $product_more_detail = DB::table('product_more_detail')
        ->join('product_color_detail', 'product_color_detail.id', '=', 'product_more_detail.product_color_detail_id')
        ->where('product_color_detail.product_id', $product_id)
        ->select([
            'product_color_detail.id AS color_id',
            'product_color_detail.color',
            'product_color_detail.is_shoes',
            'product_more_detail.id',
            'product_more_detail.size',
            'product_more_detail.stock'
        ])
        ->get();

        $is_shoes = '';
        foreach ($product_more_detail as $is_shoe) {
            $is_shoes = $is_shoe->is_shoes;
        }
        
        if($product->hot == 1)$hot='true'; else $hot='';
        if($product->promo == 1)$promo='true'; else $promo='';
        if($product->restock == 1)$restock='true'; else $restock='';
        $output = array(
            'categories_id'    =>  $product->categories_id,
            'hot'     =>  $hot,
            'promo'     =>  $promo,
            'restock'     =>  $restock,
            'name'     =>  $product->name,
            'material'     =>  $product->material,
            'dimension'     =>  $product->dimension,
            'weight'     =>  $product->weight,
            'stock'     =>  $product->stock,
            'code'     =>  $product->code,
            'product_price_detail'  =>  $product_price_detail,
            'is_shoes'  =>  $is_shoes, //penanda di detail product dia tas atau sepatu
            'product_more_detail'  =>  $product_more_detail,
            'product_image_detail'  =>  $product_image_detail,
        );
        record_activity("Aktifitas edit data produk ".$product->name);
        echo json_encode($output);
    }

	public function save_product() {
		DB::beginTransaction();
		$this->validate(request(), [
            'categories_id' => 'required',
            'name' => 'required|min:3|max:100',
            'material' => 'required|min:1',
            'weight' => 'required|numeric',
            'picture' => 'required',
            'picture.*' => 'image|mimes:jpeg,png,jpg',
            'caption.*' => 'required|max:190|string',
        ]);
	
        if(request('hot') == 'on')$hot=1; else $hot=0;
        if(request('promo') == 'on')$promo=1; else $promo=0;
        if(request('restock') == 'on')$restock=1; else $restock=0;
        
    	$product_save = ProductModel::create([
    		'categories_id' => request('categories_id'),
            'hot' => $hot,
            'promo' => $promo,
            'restock' => $restock,
            'name' => request('name'),
            'material' => request('material'),
            'weight' => request('weight'),
    	]);

        //START save data di table product_image_detail
        $picture = request('picture');
        if($picture){
            foreach ($picture as $key => $pic) {
                $name_file = strtolower(str_random(3)).'-'.$pic->getClientOriginalName();
                $pic->move(public_path('storage/img/product'), $name_file);

                $caption = '-';
                if(request('caption')[$key])$caption = request('caption')[$key];
                ProductImageDetailModel::create([
                    'product_id' => $product_save->id, //last id save
                    'image' => $name_file,
                    'caption' => $caption
                ]);
            }
        }
        // END save data di table product_image_detail

        //START save data di table product_price_detail
        foreach (request('price_type') as $key => $value) {
            $price = 0;
            if(request('price')[$key]>0)$price = request('price')[$key];
            ProductPriceDetailModel::create([
                'product_id' => $product_save->id, //last id save
                'price_type' => $value,
                'price' => $price
            ]);
        }
        // END save data di table product_price_detail

        if(request('product_category_tas')[0] != ''){
            //START save data di table product_more_detail but for only tas
            foreach(request('color_tas') as $index => $value){
                $productColor = ProductColorDetailModel::where('product_id', $product_save->id)
                ->where('color', $value)->first() ?? ProductColorDetailModel::create([
                    'product_id' => $product_save->id, //last id save
                    'color' => $value,
                    'is_shoes' => 0,
                ]);
                ProductMoreDetailModel::create([
                    'product_color_detail_id' => $productColor->id, //last id save
                    'stock' => request('stock_tas')[$index],
                ]);
            }
            // END save data di table product_more_detail but for only tas
        }

        if(request('product_category_sepatu')[0] != ''){
            //START save data di table product_more_detail but for only sepatu
            foreach(request('color_sepatu') as $index => $value){
                $productColor = ProductColorDetailModel::where('product_id', $product_save->id)
                ->where('color', $value)->first() ?? ProductColorDetailModel::create([
                    'product_id' => $product_save->id, //last id save
                    'color' => $value,
                    'is_shoes' => 1,
                ]);
                ProductMoreDetailModel::create([
                    'product_color_detail_id' => $productColor->id, //last id save
                    'size' => request('size_sepatu')[$index],
                    'stock' => request('stock_sepatu')[$index],
                ]);
            }
            // END save data di table product_more_detail but for only sepatu
        }

        \Artisan::call('cache:clear');
    	DB::commit();
        record_activity("Aktifitas menyimpan data produk berhasil");
        return response()->json(['status' => 'success','msg' => 'Data Produk Berhasil Ditambahkan']);
	}

	public function delete_product(){
		DB::beginTransaction();
		try {
            //delete file picture first
            $product_image = ProductImageDetailModel::where('product_id', request('id'))->get();
            foreach ($product_image as $image) {
                $file_to_delete = public_path('storage/img/product/').$image->image;
                File::delete($file_to_delete); // For delete from folder  
            }
            //delete all product image by product id
            DB::table('product_image_detail')->where('product_id', request('id'))->delete();

            //fetching all product color detail by product id first
            $fetch_color_detail = DB::table('product_color_detail')->where('product_id', request('id'))->get();
            foreach ($fetch_color_detail as $key => $val_col) {
                //delete all product more detail by color id
                DB::table('product_more_detail')->where('product_color_detail_id', $val_col->id)->delete();
            }
            //now delete all product color detail by product id
            DB::table('product_color_detail')->where('product_id', request('id'))->delete();
            //delete all product price by product id
            DB::table('product_price_detail')->where('product_id', request('id'))->delete();
            //delete all cart customer
            DB::table('cart')->where('product_id', request('id'))->delete();
            //delete all view produk
            DB::table('views_product')->where('product_id', request('id'))->delete();
            //delete all wishlist customer
            DB::table('wishlist')->where('product_id', request('id'))->delete();
            //delete product data
            $product = ProductModel::findOrFail(request('id'));
	        $product->delete();

	        DB::commit();
            record_activity("Aktifitas menghapus data produk berhasil ".$product->name);
            return response()->json(['status' => 'success','msg' => 'Data Produk Berhasil Dihapus']);
        } catch (\Exception $e) {
        	DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas menghapus data produk tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Produk Tidak Berhasil Dihapus']);
        }
    }

    public function delete_picture_product($id, $filename){
		try {
            //hapus file nya dulu
            $files_to_delete = public_path('storage/img/product/').$filename;
	        File::delete($files_to_delete); // For delete from folder

	        $product_image = ProductImageDetailModel::findOrFail($id);
            record_activity("Aktifitas menghapus data gambar produk berhasil ".$product_image->caption);
            $product_image->delete();
            return response()->json(['status' => 'success','msg' => 'Data Gambar Produk Berhasil Dihapus']);
        } catch (\Exception $e) {
        	DB::rollBack();
            \Log::error($e);
            record_activity("Aktifitas menghapus data gambar produk tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Gambar Produk Tidak Berhasil Dihapus']);
        }
    }

    public function update_product() {
        $this->validate(request(), [
            'categories_id' => 'required',
            'name' => 'required|min:1',
            'material' => 'required',
            'weight' => 'required|numeric',
        ]);

        if(request('hot') == 'on')$hot=1; else $hot=0;
        if(request('promo') == 'on')$promo=1; else $promo=0;
        if(request('restock') == 'on')$restock=1; else $restock=0;

    	if(request('picture') == NULL) {
            
            $product = ProductModel::findOrFail(request('id'));
            $product->categories_id = request('categories_id');
            $product->hot = $hot;
            $product->promo = $promo;
            $product->restock = $restock;
            $product->name = request('name');
            $product->material = request('material');
            $product->weight = request('weight');
            $product->save();

            //START save data di table product_price_detail
            foreach (request('price_type') as $key => $value) {
                $update_price_detail = ProductPriceDetailModel::where('id', request('product_price_detail_id')[$key])->first();
                if($update_price_detail == ''){
                    ProductPriceDetailModel::create([
                        'product_id' => request('id'),
                        'price_type' => $value,
                        'price' => request('price')[$key]
                    ]);
                } else {
                    $update_price_detail->product_id = request('id');
                    $update_price_detail->price_type = $value;
                    $update_price_detail->price = request('price')[$key];
                    $update_price_detail->save();
                }
            }
            // END save data di table product_price_detail
            if(request('product_category_tas')[0] != '') {
                DB::beginTransaction();
                foreach(request('color_tas') as $index => $value){
                    if($value == ''){
                        return response()->json(['status' => 'error','msg' => 'Isi dulu dong di salah satu inputan warna kamu!']);
                    } else if (request('stock_tas')[$index] == '') {
                        return response()->json(['status' => 'error','msg' => 'Isi dulu dong di salah satu inputan stok tas kamu!']);
                    }
                    $productColor = ProductColorDetailModel::where('id', request('product_color_detail_id')[$index])
                    ->where('is_shoes', 0)
                    ->where('product_id', request('id'))
                    ->first();
                    if($productColor == '') {
                        $validate = ProductColorDetailModel::where('color', $value)
                        ->where('product_id', request('id'))
                        ->first();
                        if($validate != ''){
                            return response()->json(['status' => 'error','msg' => 'Warna '.$validate->color.' yang kamu masukkan sudah ada tuh!']);
                        }
                        $productColor = ProductColorDetailModel::create([
                            'product_id' => request('id'),
                            'color' => $value,
                        ]);
                    } else {
                        $productColor->product_id = request('id');
                        $productColor->color = $value;
                        $productColor->save();
                    }
                    $update_more_detail = ProductMoreDetailModel::where('id', request('product_more_detail_id')[$index])->first();
                    if($update_more_detail == ''){
                        ProductMoreDetailModel::create([
                            'product_color_detail_id' => $productColor->id, //last id save
                            'stock' => request('stock_tas')[$index],
                        ]);
                    } else {
                        $update_more_detail->product_color_detail_id = $productColor->id;
                        $update_more_detail->stock = request('stock_tas')[$index];
                        $update_more_detail->save();
                    }
                }
                DB::commit();
            }

            if(request('product_category_sepatu')[0] != null) {
                DB::beginTransaction();
                //START save data di table product_more_detail but for only sepatu
                foreach(request('color_sepatu') as $index => $value) {
                    if($value == ''){
                        return response()->json(['status' => 'error','msg' => 'Isi dulu dong di salah satu inputan warna kamu!']);
                    } else if (request('stock_sepatu')[$index] == '') {
                        return response()->json(['status' => 'error','msg' => 'Isi dulu dong di salah satu inputan stok sepatu kamu!']);
                    } else if (request('size_sepatu')[$index] == ''){
                        return response()->json(['status' => 'error','msg' => 'Isi dulu dong di salah satu inputan size sepatu kamu!']);
                    }
                    $productColor = ProductColorDetailModel::where('id', request('product_color_detail_id_sepatu')[$index])
                    ->where('product_id', request('id'))
                    ->where('is_shoes', 1)
                    ->first();

                    if($productColor == ''){
                        $productColor = ProductColorDetailModel::where('color', $value)
                        ->where('product_id', request('id'))
                        ->where('is_shoes', 1)
                        ->first();
                        if($productColor != ''){
                            $productColor->product_id = request('id');
                            $productColor->color = $value;
                            $productColor->is_shoes = 1;
                            $productColor->save();
                        } else {
                            $productColor = ProductColorDetailModel::create([
                                'product_id' => request('id'),
                                'color' => $value,
                                'is_shoes' => 1,
                            ]);
                        }  
                    } else {
                        $productColor->product_id = request('id');
                        $productColor->color = $value;
                        $productColor->is_shoes = 1;
                        $productColor->save();
                    }
                    $update_more_detail = ProductMoreDetailModel::where('id', request('product_more_detail_id_sepatu')[$index])
                    ->first();

                    if($update_more_detail == ''){
                        $validate = ProductMoreDetailModel::where('product_color_detail_id', $productColor->id)
                        ->where('size', request('size_sepatu')[$index])
                        ->first();
                        if($validate != ''){
                            return response()->json(['status' => 'error','msg' => 'Warna '.$productColor->color.' size '.$validate->size.' yang kamu masukkan sudah ada tuh!']);
                        }
                        ProductMoreDetailModel::create([
                            'product_color_detail_id' => $productColor->id, //last id save
                            'size' => request('size_sepatu')[$index],
                            'stock' => request('stock_sepatu')[$index],
                        ]);
                    } else {
                        $update_more_detail->product_color_detail_id = $productColor->id;
                        $update_more_detail->stock = request('stock_sepatu')[$index];
                        $update_more_detail->size = request('size_sepatu')[$index];
                        $update_more_detail->save();
                    }
                }
                // END save data di table product_more_detail but for only sepatu
                DB::commit();
            }
            //simpan caption aja dia nih bukan picture yaa
            if(request('caption')){
                $data_img = DB::table('product_image_detail')->where('product_id', request('id'))->get();
                foreach ($data_img as $key => $val) {
                    if(request('caption')[$key])$caption = request('caption')[$key];
                    DB::table('product_image_detail')->where('id', $val->id)->update(['caption' => $caption]);
                }
            }

            $product_price = ProductPriceDetailModel::where('product_id', request('id'))->select('price_type','price')->get();
            if($product_price != ''){
                $update_all_product_incart_customer = DB::table('cart')->where('product_id', request('id'))->update(['price' => json_encode($product_price), 'weight' => request('weight')]);  
            }
        } else {

            $product = ProductModel::findOrFail(request('id'));
            $product->categories_id = request('categories_id');
            $product->hot = $hot;
            $product->promo = $promo;
            $product->restock = $restock;
            $product->name = request('name');
            $product->material = request('material');
            $product->weight = request('weight');
            $product->save();

            //START save data di table product_image_detail
            $picture = request('picture');
            if($picture){
                foreach ($picture as $key => $pic) {
                    $name_file = strtolower(str_random(3)).'-'.$pic->getClientOriginalName();
                    $pic->move(public_path('storage/img/product'), $name_file);
                    $caption = '-';
                    if(request('caption')[$key])$caption = request('caption')[$key];
                    $save_prod_img[$key] = ProductImageDetailModel::create([
                        'product_id' => request('id'), //last id save
                        'image' => $name_file,
                        'caption' => $caption
                    ]);
                }
            }
            
            // END save data di table product_image_detail
            //START save data di table product_price_detail
            foreach (request('price_type') as $key => $value) {
                $update_price_detail = ProductPriceDetailModel::where('id', request('product_price_detail_id')[$key])->first();
                if($update_price_detail == ''){
                    ProductPriceDetailModel::create([
                        'product_id' => request('id'),
                        'price_type' => $value,
                        'price' => request('price')[$key]
                    ]);
                } else {
                    $update_price_detail->product_id = request('id');
                    $update_price_detail->price_type = $value;
                    $update_price_detail->price = request('price')[$key];
                    $update_price_detail->save();
                }
            }
            // END save data di table product_price_detail
            if(request('product_category_tas')[0] != '') {
                DB::beginTransaction();
                foreach(request('color_tas') as $index => $value){
                    if($value == ''){
                        return response()->json(['status' => 'error','msg' => 'Isi dulu dong di salah satu inputan warna kamu!']);
                    } else if (request('stock_tas')[$index] == '') {
                        return response()->json(['status' => 'error','msg' => 'Isi dulu dong di salah satu inputan stok tas kamu!']);
                    }
                    $productColor = ProductColorDetailModel::where('id', request('product_color_detail_id')[$index])
                    ->where('is_shoes', 0)
                    ->where('product_id', request('id'))
                    ->first();
                    if($productColor == '') {
                        $validate = ProductColorDetailModel::where('color', $value)
                        ->where('is_shoes', 0)
                        ->where('product_id', request('id'))
                        ->first();
                        if($validate != ''){
                            return response()->json(['status' => 'error','msg' => 'Warna '.$validate->color.' yang kamu masukkan sudah ada tuh!']);
                        }
                        $productColor = ProductColorDetailModel::create([
                            'product_id' => request('id'),
                            'color' => $value,
                        ]);
                    } else {
                        $productColor->product_id = request('id');
                        $productColor->color = $value;
                        $productColor->save();
                    }
                    $update_more_detail = ProductMoreDetailModel::where('id', request('product_more_detail_id')[$index])->first();
                    if($update_more_detail == ''){
                        ProductMoreDetailModel::create([
                            'product_color_detail_id' => $productColor->id, //last id save
                            'stock' => request('stock_tas')[$index],
                        ]);
                    } else {
                        $update_more_detail->product_color_detail_id = $productColor->id;
                        $update_more_detail->stock = request('stock_tas')[$index];
                        $update_more_detail->save();
                    }
                }
                DB::commit();
            }

            if(request('product_category_sepatu')[0] != null) {
                DB::beginTransaction();
                //START save data di table product_more_detail but for only sepatu
                foreach(request('color_sepatu') as $index => $value) {
                    if($value == ''){
                        return response()->json(['status' => 'error','msg' => 'Isi dulu dong di salah satu inputan warna kamu!']);
                    } else if (request('stock_sepatu')[$index] == '') {
                        return response()->json(['status' => 'error','msg' => 'Isi dulu dong di salah satu inputan stok sepatu kamu!']);
                    } else if (request('size_sepatu')[$index] == ''){
                        return response()->json(['status' => 'error','msg' => 'Isi dulu dong di salah satu inputan size sepatu kamu!']);
                    }
                    $productColor = ProductColorDetailModel::where('id', request('product_color_detail_id_sepatu')[$index])
                    ->where('product_id', request('id'))
                    ->where('is_shoes', 1)
                    ->first();
                    if($productColor == ''){
                        $productColor = ProductColorDetailModel::where('color', $value)
                        ->where('product_id', request('id'))
                        ->where('is_shoes', 1)
                        ->first();
                        if($productColor != ''){
                            $productColor->product_id = request('id');
                            $productColor->color = $value;
                            $productColor->is_shoes = 1;
                            $productColor->save();
                        } else {
                            $productColor = ProductColorDetailModel::create([
                                'product_id' => request('id'),
                                'color' => $value,
                                'is_shoes' => 1,
                            ]);
                        }
                    } else {
                        $productColor->product_id = request('id');
                        $productColor->color = $value;
                        $productColor->is_shoes = 1;
                        $productColor->save();
                    }
                    $update_more_detail = ProductMoreDetailModel::where('id', request('product_more_detail_id_sepatu')[$index])
                    ->first();
                    if($update_more_detail == ''){
                        $validate = ProductMoreDetailModel::where('product_color_detail_id', $productColor->id)
                        ->where('size', request('size_sepatu')[$index])
                        ->first();
                        if($validate != ''){
                            return response()->json(['status' => 'error','msg' => 'Warna '.$productColor->color.' size '.$validate->size.' yang kamu masukkan sudah ada tuh!']);
                        }
                        ProductMoreDetailModel::create([
                            'product_color_detail_id' => $productColor->id, //last id save
                            'size' => request('size_sepatu')[$index],
                            'stock' => request('stock_sepatu')[$index],
                        ]);
                    } else {
                        $update_more_detail->product_color_detail_id = $productColor->id;
                        $update_more_detail->stock = request('stock_sepatu')[$index];
                        $update_more_detail->size = request('size_sepatu')[$index];
                        $update_more_detail->save();
                    }
                }
                // END save data di table product_more_detail but for only sepatu
                DB::commit();
            }
            //update product data in cart customer
            $product_price = ProductPriceDetailModel::where('product_id', request('id'))->select('price_type','price')->get();
            if($product_price != ''){
                $update_all_product_incart_customer = DB::table('cart')->where('product_id', request('id'))->update(['price' => json_encode($product_price), 'weight' => request('weight')]);  
            }
        }
        record_activity("Aktifitas memperbaharui data produk berhasil");
        \Artisan::call('cache:clear');
        return response()->json(['status' => 'success','msg' => 'Data Produk Berhasil Diperbaharui']);
    }

    public function detail_product($product_id, $product_name=NULL) {
        if($product_id == ''){
            return redirect()->route('product')->with('info','Terjadi kesalahan, mohon ulangi atau refresh halaman ini.');
        }
        $product = DB::table('products')
        ->join('categories', 'categories.id', '=', 'products.categories_id')
        ->where('products.id', $product_id)
        ->select([ // [ ]<-- biar lebih rapi aja
            'products.*',
            'categories.id AS categories_id',
            'categories.name AS categories_name',
        ])
        ->first();

        if($product == ''){
            return redirect()->route('product')->with('info','Terjadi kesalahan, mohon ulangi beberapa saat kembali.');
        }

        $product_price_detail = ProductPriceDetailModel::where('product_id', $product->id)
        ->select('price_type','price')
        ->get();

        $product_more_detail = DB::table('product_more_detail')
        ->join('product_color_detail', 'product_color_detail.id', '=', 'product_more_detail.product_color_detail_id')
        ->where('product_color_detail.product_id', $product->id)
        ->select([
            'product_color_detail.color',
            'product_color_detail.is_shoes',
            'product_more_detail.size',
            'product_more_detail.stock'
        ])
        ->get();
        
        foreach ($product_more_detail as $is_shoe) {
            $is_shoes = $is_shoe->is_shoes;
        }

        $product_image_detail = ProductImageDetailModel::where('product_id', $product->id)
        ->select('image','caption')
        ->get();

        $product_viewer = ViewProductModel::where('product_id', $product->id)->count();

        //perhitungan rating product wak
        $ratings = RatingModel::where('product_id', $product->id)
        ->select('value_rating')
        ->get();

        if(count($ratings) > 0){
            $count_total_rating_product = count($ratings);
            $sum_rat = 0;
            foreach ($ratings as $rat) {
                $sum_rat += $rat->value_rating;
            }
            $rating_product = $sum_rat/$count_total_rating_product;
        } else { 
            $rating_product = 0;
        }

        $output = array(
            'product_id' => $product->id,
            'categories_id' => $product->categories_id,
            'categories_name' => $product->categories_name,
            'hot' => $product->hot,
            'promo' => $product->promo,
            'restock' => $product->restock,
            'name' => $product->name,
            'price' => $product_price_detail,
            'material' => $product->material,
            'weight' => $product->weight,
            'product_more_detail' => $product_more_detail,
            'is_shoes' => $is_shoes,
            'picture' => $product_image_detail,
            'rating_product' => $rating_product,
        );

        $comments = DB::table('comments')
        ->join('customers', 'customers.id', '=', 'comments.customer_id')
        ->where('comments.deleted_at', null)
        ->where('comments.product_id', $product_id)
        ->select([ // [ ]<-- biar lebih rapi aja
            'comments.id',
            'comments.comment',
            'customers.name AS customer_name',
            'customers.picture AS customer_picture',
        ])
        ->get();

        record_activity("Aktifitas mengunjungi detail data produk ".$product->name);
        return view('backend.detail_product', compact('output','comments','product_viewer'));
    }

    public function delete_color_more_detail($more_detail_id=NULL) {
        try {
            if($more_detail_id == NULL){
                return response()->json(['status' => 'success','msg' => 'Data field kosong kamu berhasil dihapus']);
            }
            $order_detail = DB::table('orders')
            ->join('orders_detail', 'orders_detail.order_id', '=', 'orders.id')
            ->where('orders.status_order', 1)
            ->where('orders_detail.product_more_detail_id', $more_detail_id)
            ->first();
            if($order_detail != ''){
                return response()->json(['status' => 'error','msg' => 'Maaf, data yang ingin kamu hapus masih ada di data order aktif customer. Silakan refresh kembali halaman ini.']);
            }

            $more = ProductMoreDetailModel::where('id', $more_detail_id)->first();
            $color = ProductColorDetailModel::where('id', $more->product_color_detail_id)->delete();
            $more->delete();

            record_activity("Aktifitas menghapus data size dan stok");
            return response()->json(['status' => 'success','msg' => 'Data Size beserta stok Berhasil Dihapus']);
        } catch (\Exception $e) {
            \Log::error($e);
            record_activity("Aktifitas menghapus data warna dan stok tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Warna beserta stok tidak berhasil Dihapus']);
        }
    }

    public function delete_color_more_detail_sepatu($more_detail_id=NULL) {
        try {
            if($more_detail_id == NULL){
                return response()->json(['status' => 'success','msg' => 'Data field kosong kamu berhasil dihapus']);
            }
            $order_detail = DB::table('orders')
            ->join('orders_detail', 'orders_detail.order_id', '=', 'orders.id')
            ->where('orders.status_order', 1)
            ->where('orders_detail.product_more_detail_id', $more_detail_id)
            ->first();
            if($order_detail != ''){
                return response()->json(['status' => 'error','msg' => 'Maaf, data yang ingin kamu hapus masih ada di data order aktif customer. Silakan refresh kembali halaman ini.']);
            }
            $more_sepatu = ProductMoreDetailModel::where('id', $more_detail_id)->first();
            if($more_sepatu == ''){
                return response()->json(['status' => 'error','msg' => 'Data kamu sudah terhapus kok, kamu bisa menghapus data field tersebut. Silakan refresh kembali halaman ini.']);
            }
            $count_sepatu = ProductMoreDetailModel::where('product_color_detail_id', $more_sepatu->product_color_detail_id)->count();
            if($count_sepatu == 1){
                $more_sepatu_del = ProductMoreDetailModel::where('id', $more_detail_id)->delete();
                $color = ProductColorDetailModel::where('is_shoes', 1)->where('id', $more_sepatu->product_color_detail_id)->delete();
            } else {
                $more_sepatu_del = ProductMoreDetailModel::where('id', $more_detail_id)->delete();
            }
            record_activity("Aktifitas menghapus data size dan stok sepatu");
            return response()->json(['status' => 'success','msg' => 'Data Size beserta stok Sepatu Berhasil Dihapus']);
        } catch (\Exception $e) {
            \Log::error($e);
            record_activity("Aktifitas menghapus data warna dan stok tidak berhasil");
            return response()->json(['status' => 'error','msg' => 'Data Warna beserta stok tidak berhasil Dihapus']);
        }
    }

}
