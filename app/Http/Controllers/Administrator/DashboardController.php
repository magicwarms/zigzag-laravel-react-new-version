<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\OrderModel;
use App\Model\CustomerModel;
use App\Model\ViewOrderModel;
use App\Model\VisitorModel;

use App\Charts\OrderSuccessfullChart;

use DB;
use Cache;

use Illuminate\Support\Facades\Mail;
use App\Mail\CheckDailyStock;

class DashboardController extends Controller {

	public function home(){

		record_activity("Aktifitas mengunjungi halaman dashboard");
		//count customer active
		//cache query 5 minutes
		$count_customer = Cache::remember('count_customer', 5, function () {
		    return CustomerModel::where('status', 1)->count();
		});
		//count success order
		//cache query 5 minutes
		$count_order = Cache::remember('count_order', 5, function () {
		    return OrderModel::where('status_order', 9)->count();
		});
		//count fail order by customer
		//cache query 5 minutes
		$count_fail_order_by_customer = Cache::remember('count_fail_order_by_customer', 5, function () {
		    return OrderModel::where('status_order', 10)->count();
		});
		//count fail order by admin
		//cache query 5 minutes
		$count_fail_order_by_admin = Cache::remember('count_fail_order_by_admin', 5, function () {
		    return OrderModel::where('status_order', 7)->count();
		});
		//chart profit penjualan by transfer
		$chart = new OrderSuccessfullChart;
		$chart->labels(["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"]);
		$revenue = [];
		for($index= 1; $index <= 12; $index++){
            $order_success = ViewOrderModel::where('MONTH', $index)->where('YEAR', date('Y'))->first();
            if($order_success != '') {
                array_push($revenue, $order_success->GRANDTOTAL_ORDER);
            } else {
                array_push($revenue, 0);
            }
        }
		$chart->dataset('Order Successful', 'bar', $revenue)->options([
		    'hoverBackgroundColor' => random_color(),
		    'backgroundColor' => random_color()
		]);
		$top_5_most_view_product = DB::table('views_product')
		->join('products','products.id','=','views_product.product_id')
		->select([
			'products.id',
			'products.name',
		])
		->selectRaw('count(*) AS ViewCountProduct')
		->groupBy('views_product.product_id')
		->orderBy('ViewCountProduct', 'DESC')
		->limit(5)
		->get();

		$get_visitor_per_day = VisitorModel::whereDate('dates_visit', date('Y-m-d'))->groupBy('ip')->get()->count();
		$get_total_visitor = VisitorModel::groupBy('ip')->get()->count();

		return view('backend.dashboard', compact('chart','count_customer','count_order','count_fail_order_by_customer','count_fail_order_by_admin','top_5_most_view_product','get_visitor_per_day','get_total_visitor'));
	}
}
