<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Model\OrderModel;
use App\Model\OrderDetailModel;
use App\Model\ConfirmationOrderModel;
use App\Model\CustomerModel;
use App\Model\ProductModel;
use App\Model\ProductMoreDetailModel;
use App\Model\DepositModel;

use App\Mail\ConfirmationOrderMailAccepted;
use App\Mail\ConfirmationOrderMailRejected;
use App\Mail\NotificationChangeStatusOrder;
use App\Mail\CancelOrder;

use DB;

class OrderController extends Controller
{

    public function index_customer_order()
    {
        return view('backend.orders');
    }

    public function show_customer_order($start_date, $end_date, $filter)
    {
        record_activity("Aktifitas mengunjungi halaman customer order");
        $orders = DB::table('orders')
            ->join('customer_service', 'customer_service.id', '=', 'orders.customer_service_id')
            ->join('customers', 'customers.id', '=', 'orders.customer_id')
            ->whereDate('orders.created_date', '>=', date("Y-m-d", strtotime($start_date)))
            ->whereDate('orders.created_date', '<=', date("Y-m-d", strtotime($end_date)))
            ->select([ // [ ]<-- biar lebih rapi aja
                'orders.id',
                'orders.name AS customer_name',
                'customers.name AS customer_login',
                'orders.tele AS customer_tele',
                'orders.order_code',
                'orders.status_order',
                'orders.unique_code',
                'customer_service.name AS customer_service_name',
                'orders.created_date',
                'orders.updated_date'
            ]);
        if ($filter > 0) {
            $orders = $orders->where('status_order', $filter);
        }
        $orders = $orders->orderBy('id', 'DESC');

        return Datatables::of($orders)
            ->editColumn('customer_login', function ($model) {
                $customer_login = '<b>' . $model->customer_login . '</b>';
                return $customer_login;
            })
            ->editColumn('status_order', function ($model) {
                $status_order = $model->status_order;
                if ($status_order == 1) {
                    $status = '<span class="uk-badge uk-badge-primary"> Belum Pembayaran</span>';
                } elseif ($status_order == 3) {
                    $status = '<span class="uk-badge uk-badge-warning"> Proses pembayaran</span>';
                } else if ($status_order == 4) {
                    $status = '<span class="uk-badge uk-badge-primary"> Pembayaran disetujui</span>';
                } else if ($status_order == 5) {
                    $status = '<span class="uk-badge uk-badge-warning"> Proses digudang</span>';
                } elseif ($status_order == 6) {
                    $status = '<span class="uk-badge uk-badge-primary"> Barang terkirim</span>';
                } elseif ($status_order == 7) {
                    $status = '<span class="uk-badge uk-badge-danger"> Pesanan dibatalkan otomatis</span>';
                } elseif ($status_order == 8) {
                    $status = '<span class="uk-badge uk-badge-danger"> Pembayaran dari deposit</span>';
                } elseif ($status_order == 9) {
                    $status = '<span class="uk-badge uk-badge-success"> Pesanan Selesai</span>';
                } elseif ($status_order == 10) {
                    $status = '<span class="uk-badge uk-badge-warning"> Pesanan dibatalkan oleh customer</span>';
                } else {
                    $status = '<span class="uk-badge uk-badge-danger">Pembayaran ditolak</span>';
                }
                return $status;
            })
            ->editColumn('created_date', function ($model) {
                $created = date('d F Y H:i:s', strtotime($model->created_date));
                return $created;
            })
            ->editColumn('updated_date', function ($model) {
                $updated = date('d F Y H:i:s', strtotime($model->updated_date));
                if ($model->updated_date == '') {
                    $updated = '-';
                }
                return $updated;
            })
            ->addColumn('action', function ($model) {
                $action = '
                <a onclick="delete_data(' . $model->id . ')" href="#"><i class="md-icon material-icons uk-icon-large">&#xE16C;</i>
                </a>
                <a href="' . route('order.detail', $model->id) . '"><i class="md-icon material-icons">subdirectory_arrow_right</i></a>
            ';
                return $action;
            })
            ->rawColumns(['action', 'status_order', 'customer_login'])
            ->addIndexColumn()->make(true);
    }

    public function detail_order($order_id)
    {

        $isread = OrderModel::findOrFail($order_id);
        $isread->is_read = 1;
        $isread->save();

        if ($order_id == '') {
            return redirect()->route('order');
        }

        $detail = DB::table('orders')
            ->join('customer_service', 'customer_service.id', '=', 'orders.customer_service_id')
            ->join('customers', 'customers.id', '=', 'orders.customer_id')
            ->where('orders.deleted_at', null)
            ->where('orders.id', $order_id)
            ->select([ // [ ]<-- biar lebih rapi aja
                'orders.*',
                'customer_service.name AS customer_service_name',
                'customers.name AS customer_login',
            ])
            ->first();

        if ($detail == '') {
            return redirect()->route('order');
        }

        $order_details = OrderDetailModel::where('order_id', $order_id)->get();
        record_activity("Aktifitas mengunjungi detail halaman customer order " . $detail->order_code);
        return view('backend.detail_order', compact('detail', 'order_details'));
    }

    public function index_confirmation_order()
    {
        record_activity("Aktifitas mengunjungi halaman konfirmasi customer order");
        return view('backend.confirmation_order');
    }

    public function show_confirmation_order($start_date, $end_date, $filter)
    {
        $confirmation_order = DB::table('confirmation_order')
            ->join('customers', 'customers.id', '=', 'confirmation_order.customer_id')
            ->join('orders', 'orders.id', '=', 'confirmation_order.order_id')
            ->whereDate('confirmation_order.created_date', '>=', date("Y-m-d", strtotime($start_date)))
            ->whereDate('confirmation_order.created_date', '<=', date("Y-m-d", strtotime($end_date)))
            ->where('confirmation_order.deleted_at', null)
            ->select([ // [ ]<-- biar lebih rapi aja
                'customers.name AS customer_name',
                'orders.order_code',
                'orders.unique_code',
                'confirmation_order.id',
                'confirmation_order.date',
                'confirmation_order.status',
                'confirmation_order.created_date',
                'confirmation_order.updated_date'
            ]);
        if ($filter != '-') {
            $confirmation_order = $confirmation_order->where('confirmation_order.status', $filter);
        }
        return Datatables::of($confirmation_order)
            ->editColumn('status', function ($model) {
                if ($model->status == 1) {
                    $status = '<span class="uk-badge uk-badge-notification uk-badge-primary">Konfirmasi valid & disetujui</span>';
                } elseif ($model->status == 2) {
                    $status = '<span class="uk-badge uk-badge-notification uk-badge-warning">Menunggu konfirmasi</span>';
                } elseif ($model->status == 3) {
                    $status = '<span class="uk-badge uk-badge-notification uk-badge-danger">Cek Mutasi</span>';
                } elseif ($model->status == 4) {
                    $status = '<span class="uk-badge uk-badge-notification uk-badge-warning">Pesanan Dibatalkan</span>';
                } else {
                    $status = '<span class="uk-badge uk-badge-notification uk-badge-warning">Konfirmasi tidak disetujui</span>';
                }
                return $status;
            })
            ->editColumn('date', function ($model) {
                $transfer_date = date('d F Y', strtotime($model->date));
                return $transfer_date;
            })
            ->editColumn('created_date', function ($model) {
                $created = date('d F Y H:i:s', strtotime($model->created_date));
                return $created;
            })
            ->editColumn('updated_date', function ($model) {
                $updated = date('d F Y H:i:s', strtotime($model->updated_date));
                if ($model->updated_date == '') {
                    $updated = '-';
                }
                return $updated;
            })
            ->addColumn('action', function ($model) {
                $action = '
                <a href="#" class="show_evidence_order" data-id="' . $model->id . '"><i class="md-icon material-icons">pageview</i></a>
            ';
                return $action;
            })
            ->rawColumns(['action', 'status'])
            ->addIndexColumn()->make(true);
    }

    function fetch_data_confirmation_order($confirmation_order_id)
    {
        $confirm = DB::table('confirmation_order')
            ->join('customers', 'customers.id', '=', 'confirmation_order.customer_id')
            ->join('orders', 'orders.id', '=', 'confirmation_order.order_id')
            ->where('confirmation_order.deleted_at', null)
            ->where('confirmation_order.id', $confirmation_order_id)
            ->select([ // [ ]<-- biar lebih rapi aja
                'customers.name AS customer_name',
                'orders.id AS order_id',
                'orders.order_code',
                'orders.unique_code',
                'confirmation_order.bank_sender',
                'confirmation_order.bank_receiver',
                'confirmation_order.date',
                'confirmation_order.evidence',
                'confirmation_order.total_transfer',
                'confirmation_order.status'
            ])
            ->first();
        if ($confirm->status == 1) {
            $status = '<span class="uk-badge uk-badge-notification uk-badge-primary">Konfirmasi valid & disetujui</span>';
        } elseif ($confirm->status == 2) {
            $status = '<span class="uk-badge uk-badge-notification uk-badge-warning">Menunggu konfirmasi</span>';
        } elseif ($confirm->status == 3) {
            $status = '<span class="uk-badge uk-badge-notification uk-badge-danger">Cek Mutasi</span>';
        } elseif ($confirm->status == 4) {
            $status = '<span class="uk-badge uk-badge-notification uk-badge-warning">Pesanan Dibatalkan</span>';
        } else {
            $status = '<span class="uk-badge uk-badge-notification uk-badge-danger">Konfirmasi tidak disetujui</span>';
        }
        $output = array(
            'customer_name'     =>  $confirm->customer_name,
            'order_code'     =>  $confirm->order_code,
            'order_id'     =>  $confirm->order_id,
            'unique_code'     =>  $confirm->unique_code,
            'bank_sender'     =>  $confirm->bank_sender,
            'bank_receiver'     =>  $confirm->bank_receiver,
            'date'     =>  date('d F Y', strtotime($confirm->date)),
            'evidence'     =>  $confirm->evidence,
            'total_transfer'     =>  number_format($confirm->total_transfer, 0, ',', '.'),
            'status'     =>  $status,
        );
        record_activity("Aktifitas edit data konfirmasi customer order " . $confirm->order_code);
        echo json_encode($output);
    }

    public function verify_confirmation_order()
    {
        DB::beginTransaction();
        $this->validate(request(), [
            'status' => 'required',
        ]);

        $confirm = ConfirmationOrderModel::findOrFail(request('id'));
        $confirm->status = request('status');
        $save_confirm = $confirm->save();

        $customer_data = CustomerModel::findOrFail($confirm->customer_id);
        $data = [
            'to' => $customer_data->email,
            'name' => $customer_data->name,
            'kode_order' => request('order_code')
        ];
        $when = now()->addSeconds(3);
        if (request('status') == 1) {
            //Jika dia VIP Member
            if ($customer_data->acc_type == 2) {
                //cek product qty di order detail dulu
                $total_qty_order = OrderDetailModel::where('order_id', request('order_id'))->sum('product_qty');
                //simpan total order berapa kali dia order per product didalam order itu
                $customer_data->total_order = $customer_data->total_order + $total_qty_order;
                $customer_data->save();
            }

            $update_status_order = DB::table('orders')->where('order_code', request('order_code'))->update(['status_order' => 4]); //pembayaran disetujui
            $sending_mail = Mail::to($customer_data->email)->later($when, new ConfirmationOrderMailAccepted($data));
        } else {
            $update_status_order = DB::table('orders')->where('order_code', request('order_code'))->update(['status_order' => 0]); //pembayaran ditolak
            $sending_mail = Mail::to($customer_data->email)->later($when, new ConfirmationOrderMailRejected($data));

            $order = OrderModel::where('order_code', request('order_code'))->first();
            //kembalikan stock jika dia pembayaran di tolak
            $return_stock = OrderDetailModel::where('order_id', $order->id)->get();
            foreach ($return_stock as $stock) {
                $product_qty = $stock->product_qty;

                $product_more = ProductMoreDetailModel::findOrFail($stock->product_more_detail_id); //get data produk more detail
                if ($product_more == '') {
                    return response()->json(['status' => 'error', 'msg' => 'Stok tidak dapat dikembalikan, karena stok produk berkaitan sudah terhapus.']);
                    die();
                }
                $add_product_stock = $product_qty + $product_more->stock; //tambahkan stock nya
                $product_more->stock = $add_product_stock;
                $product_more->save(); //baru update ke product_detail_model table
            }
        }

        DB::commit();
        if ($save_confirm) {
            record_activity("Aktifitas verifikasi konfirmasi customer order berhasil");
            return response()->json(['status' => 'success', 'msg' => 'Konfirmasi order berhasil diperbaharui dan notifikasi telah dikirimkan']);
        } else {
            DB::rollBack();
            record_activity("Aktifitas verifikasi konfirmasi customer order tidak berhasil");
            return response()->json(['status' => 'error', 'msg' => 'Konfirmasi order tidak berhasil diperbaharui']);
        }
    }

    public function change_status_order($order_id, $status)
    {
        DB::beginTransaction();
        //kalo dia batal order otomatis
        $order = OrderModel::findOrFail($order_id);
        if ($status == 7) {
            //jika dia payment method nya deposit kembalikan lagi dana nya ke depo wak
            if ($order->payment_method == 'deposit') {
                $customer_depo = CustomerModel::findOrFail($order->customer_id);
                $return_depo = $customer_depo->deposit + $order->grandtotal_order;
                $customer_depo->deposit = $return_depo;
                $customer_depo->save();
                //buat history depo jika dia cancel order
                DepositModel::create([
                    'customer_id' => $order->customer_id,
                    'total' => $order->grandtotal_order,
                    'balance' => $return_depo,
                    'status' => 6, //Deposit di kembalikan
                ]);
            }
            //kembalikan stock jika dia cancel order
            $return_stock = OrderDetailModel::where('order_id', $order_id)->get();
            foreach ($return_stock as $stock) {
                $product_qty = $stock->product_qty;

                $product_more = ProductMoreDetailModel::find($stock->product_more_detail_id); //get data produk more detail
                if ($product_more == '') {
                    return response()->json(['status' => 'error', 'msg' => 'Stok tidak dapat dikembalikan, karena stok produk berkaitan sudah terhapus.']);
                    $return_stock = 0;
                } else {
                    $return_stock = $product_more->stock;
                }

                $add_product_stock = $product_qty + $return_stock; //tambahkan stock nya
                $product_more->stock = $add_product_stock;
                $product_more->save(); //baru update ke product_detail_model table
            }
            if ($order->payment_method == 'transfer') {
                //update status konfirmasi order juga jika pesanan gagal
                $update_status_confirm = ConfirmationOrderModel::where('order_id', $order_id)->first();
                $update_status_confirm->status = 4; //pesanan dibatalkan
                $update_status_confirm->save();
            }
        }

        $order->status_order = $status;
        $change_status_order = $order->save();

        DB::commit();
        $data = [
            'to' => $order->email,
            'name' => $order->name,
            'status' => status_order($status),
            'order_code' => $order->order_code,
            'resi_order' => ''
        ];
        if ($status != 6 and $status != 7) {
            $when = now()->addSeconds(5);
            Mail::to($order->email)->later($when, new NotificationChangeStatusOrder($data));
        }

        if ($change_status_order) {
            record_activity("Aktifitas memperbaharui status customer order berhasil");
            return response()->json(['status' => 'success', 'msg' => 'Status order berhasil diperbaharui dan notifikasi telah dikirimkan', 'data' => $data['status']]);
        } else {
            DB::rollBack();
            record_activity("Aktifitas memperbaharui status customer order tidak berhasil");
            return response()->json(['status' => 'error', 'msg' => 'Status order tidak berhasil diperbaharui', 'data' => '']);
        }
    }

    public function update_resi_order()
    {
        DB::beginTransaction();
        $this->validate(request(), [
            'id' => 'required',
            'resi_order' => 'required|min:5|unique:orders,resi_order',
        ]);

        $update_resi = OrderModel::findOrFail(request('id'));
        $update_resi->resi_order = strtoupper(request('resi_order'));
        $update_resi_order = $update_resi->save();

        DB::commit();
        $data = [
            'to' => $update_resi->email,
            'name' => $update_resi->name,
            'status' => status_order($update_resi->status_order),
            'order_code' => $update_resi->order_code,
            'resi_order' => request('resi_order')
        ];
        $when = now()->addSeconds(5);
        $sending_mail = Mail::to($update_resi->email)->later($when, new NotificationChangeStatusOrder($data));
        if ($update_resi_order) {
            record_activity("Aktifitas menyimpan nomor resi customer order berhasil");
            return response()->json(['status' => 'success', 'msg' => 'Nomor resi berhasil ditambahkan']);
        } else {
            DB::rollBack();
            record_activity("Aktifitas menyimpan nomor resi customer order tidak berhasil");
            return response()->json(['status' => 'error', 'msg' => 'Nomor resi tidak berhasil ditambahkan']);
        }
    }

    public function delete_resi_order($order_id)
    {
        DB::beginTransaction();

        $delete_resi = OrderModel::findOrFail($order_id);
        $delete_resi->resi_order = '';
        $delete_resi_order = $delete_resi->save();

        DB::commit();

        if ($delete_resi_order) {
            record_activity("Aktifitas menghapus nomor resi customer order berhasil");
            return redirect()->back()->with('success', 'Nomor resi berhasil dihapus');
        } else {
            DB::rollBack();
            record_activity("Aktifitas menghapus nomor resi customer order tidak berhasil");
            return redirect()->back()->with('warning', 'Nomor resi tidak berhasil dihapus');
        }
    }

    public function cancel_order()
    {

        $this->validate(request(), [
            'id' => 'required',
            'remark_cancel_order' => 'required|min:5',
        ]);

        $cancel = OrderModel::findOrFail(request('id'));
        $cancel->status_order = 7;
        $cancel->remark_cancel_order = request('remark_cancel_order');
        $cancel_order = $cancel->save();

        $ekspedition_total = $cancel->ekspedition_total;
        if ($ekspedition_total == '') {
            $ekspedition_total = '-';
        } else {
            $ekspedition_total = $cancel->ekspedition_total;
        }

        $return_stock = OrderDetailModel::where('order_id', request('id'))->get();
        if ($return_stock->isEmpty()) {
            $return_stock = [];
        }
        $data = [
            'to' => $cancel->email,
            'name' => $cancel->name,
            'canceled_order' => $return_stock,
            'status' => status_order(7), //dibatalkan admin
            'remark_cancel_order' => request('remark_cancel_order'),
            'order_code' => $cancel->order_code,
            'subtotal_order' => $cancel->subtotal_order,
            'grandtotal_order' => $cancel->grandtotal_order,
            'ekspedition_total' => $ekspedition_total,
            'discount' => $cancel->discount,
        ];

        $when = now()->addSeconds(5);
        $sending_mail = Mail::to($cancel->email)->later($when, new CancelOrder($data));
        if ($cancel_order) {
            record_activity("Aktifitas menyimpan Pembatalan customer order berhasil");
            return response()->json(['status' => 'success', 'msg' => 'Keterangan Pembatalan order berhasil ditambahkan']);
        } else {
            DB::rollBack();
            record_activity("Aktifitas menyimpan Pembatalan customer order tidak berhasil");
            return response()->json(['status' => 'error', 'msg' => 'Keterangan Pembatalan order tidak berhasil ditambahkan']);
        }
    }

    public function print_order($order_id)
    {

        $order = OrderModel::findOrFail($order_id);
        if ($order->status_order == 4 or $order->status_order == 8) {
            $order->status_order = 5;
        }
        //kalo dia partner langsung pesanan selesai wak!
        if ($order->is_partner == 1) {
            $order->status_order = 9;
        }
        $order->save();

        $order = OrderModel::with([
            'customerService:id,name',
            'orderDetails:id,order_id,product_qty,product_name,product_color,product_size',
        ])
            ->where('id', $order_id)
            ->first();
        record_activity("Aktifitas mencetak order " . $order->order_code);
        return view('backend.print_order', compact('order'));
    }

    public function return_stock_for_refund($id, $product_more_detail_id)
    {
        //kembalikan stock jika dia cancel order
        $return_stock = OrderDetailModel::where('order_id', $id)
            ->where('product_more_detail_id', $product_more_detail_id)
            ->select([
                'id',
                'product_more_detail_id',
                'product_qty',
                'is_return'
            ])
            ->first();

        if ($return_stock->is_return == 1) {
            return response()->json(['status' => 'warning', 'msg' => 'Stok produk telah kamu kembalikan sebelumnya.']);
        }

        $product_qty = $return_stock->product_qty;
        //get data produk more detail
        $product_more = ProductMoreDetailModel::where('id', $return_stock->product_more_detail_id)->first();
        if ($product_more == '') {
            return response()->json(['status' => 'warning', 'msg' => 'Stok produk ini tidak dapat kamu kembalikan, disebabkan kamu telah menghapus data produk ini di form update produk.']);
        }
        //tambahkan stock nya
        $add_product_stock = $product_qty + $product_more->stock;
        $product_more->stock = $add_product_stock;
        //baru update ke product_detail_model table
        $product_more->save();

        //updated status dia pernah refund/kembalikan stok gak produk ini
        $return_status = OrderDetailModel::findOrFail($return_stock->id);
        $return_status->is_return = 1;
        $return_status->save();

        record_activity("Aktifitas refund/kembalikan stok produk " . $return_stock->product_name . " dengan jumlah " . $return_stock->product_qty . " pcs berhasil");
        return response()->json(['status' => 'success', 'msg' => 'Stok barang telah berhasil dikembalikan']);
    }
}
