<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\BankAccountModel;

use DB;

class BankAccountController extends Controller {
    
	public function list_bank_account() {

		$bank_accounts = BankAccountModel::all('id','bank_name as bank_account', 'bank_account as number','under_name','picture'); 

		if($bank_accounts->isEmpty()){
			return response()->json([
	            'result' => [],
	            'status' => 'EMPTY'
	        ]);
		}

		return response()->json([
            'result' => $bank_accounts,
            'status' => 'OK'
        ]);
	}

}
