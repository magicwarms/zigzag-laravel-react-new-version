<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\ProductModel;
use App\Model\ProductPriceDetailModel;
use App\Model\RatingModel;
use App\Model\ProductColorDetailModel;
use App\Model\ProductMoreDetailModel;
use App\Model\ProductImageDetailModel;
use App\Model\ViewProductModel;
use App\Model\ReturnProductModel;
use App\Model\ReturnProductImageModel;

use DB;
use File;
use Carbon\Carbon;
use Cache;
use Validator;

class ProductController extends Controller {
    
	public function list_products($home=NULL,$new=NULL,$hot=NULL,$featured=NULL,$promo=NULL,$restock=NULL,$old=NULL) {
		$products = ProductModel::with([
			'category:id,name',
			'priceDetails:id,product_id,price_type,price',
			'colorDetails.moreDetails:id,product_color_detail_id,stock,size',
			'imageDetails:id,product_id,image,caption',
			'ratings:id,customer_id,product_id,value_rating'
		]);
		if($home > 0)$products = $products->limit(10);
		if($new > 0)$products = $products->orderBy('created_date','desc');
		if($hot > 0)$products = $products->where('hot', 1);
		if($featured > 0)$products = $products->where('featured', 1);
		if($promo > 0)$products = $products->where('promo', 1);
		if($restock > 0)$products = $products->where('restock', 1);
		if($old > 0)$products = $products->orderBy('created_date','asc');
		$products = $products->select([ // [ ]<-- biar lebih rapi aja
        	'id',
        	'categories_id',
        	'hot',
        	'promo',
        	'restock',
        	'material',
        	'name',
        	'weight',
        	'created_date',
        ])->get();
		if($products->isEmpty()){
			return response()->json([
	            'result' => $products,
	            'status' => 'EMPTY'
	        ]);
		}

		foreach ($products as $key => $value) {
			$productMoreDetails = $value->colorDetails->reduce(function ($result, $colorDetails) {
				$result[$colorDetails->color] = $colorDetails->moreDetails;
				return $result;
			});
			if($productMoreDetails == ''){
				$productMoreDetails = (object) array();
			} 
			//perhitungan rating product wak
			$productRating = $value->ratings;
			$productRatingItem = $productRating->count();
			$productRating = ($productRatingItem > 0) ? $productRating->sum('value_rating') / $productRatingItem : 0;
			$out_of_stock = true;
			foreach ($value->colorDetails as $more) {
				$out_of_stock = true;
				if(!$more->moreDetails->isEmpty()){
					$out_of_stock = false;
					break;
				}
			}
			
			$outputs[$key] = array(
				'product_id' => $value->id,
				'categories' => $value->category,
				'hot' => $value->hot,
				'promo' => $value->promo,
				'restock' => $value->restock,
				'name' => $value->name,
				'material' => $value->material,
				'weight' => $value->weight,
				'price' => $value->priceDetails,
				'product_more_detail' => $productMoreDetails,
				'picture' => $value->imageDetails,
				'rating_product' => $productRating,
				'total_customer_rating' => $productRatingItem,
				'out_of_stock' => $out_of_stock,
				'created_date' => $value->created_date
			);
		}
		//record visitor
        record_visitor();
		return response()->json([
            'result' => $outputs,
            'status' => 'OK'
        ]);
	}

	public function detail_product($product_id=NULL) {
		if($product_id == ''){
			return response()->json([
                'result' => 'Parameter required',
                'status' => 'ERROR'
            ], 401);
		}
		$product = ProductModel::with([
			'category:id,name',
			'priceDetails:id,product_id,price_type,price',
			'colorDetails.moreDetails:id,product_color_detail_id,stock,size',
			'imageDetails:id,product_id,image,caption',
			'ratings:id,customer_id,product_id,value_rating'
		])
		->select([ // [ ]<-- biar lebih rapi aja
        	'id',
        	'categories_id',
        	'hot',
        	'promo',
        	'restock',
        	'material',
        	'name',
        	'weight',
        	'created_date',
        ])
		->where('id', $product_id)
        ->first();
		if($product == ''){
			return response()->json([
	            'result' => $product,
	            'status' => 'EMPTY'
	        ]);
		}
		$productMoreDetails = $product->colorDetails->reduce(function ($result, $colorDetails) {
			$result[$colorDetails->color] = $colorDetails->moreDetails;
			return $result;
		});
		if($productMoreDetails == ''){
			$productMoreDetails = (object) array();
		}
		//perhitungan rating product wak
		$productRating = $product->ratings;
		$productRatingItem = $productRating->count();
		$productRating = ($productRatingItem > 0) ? $productRating->sum('value_rating') / $productRatingItem : 0;
		$out_of_stock = true;
		foreach ($product->colorDetails as $more) {
			$out_of_stock = true;
			if(!$more->moreDetails->isEmpty()){
				$out_of_stock = false;
				break;
			}
		}
		//simpan produk yang telah berapa kali di lihat
		viewed_product($product->id);
		$product_viewer = ViewProductModel::where('product_id', $product->id)->count();
		//perhitungan rating si customer beserta value nya
		$customer = auth('api')->user();
		if($customer == ''){
			$customer_id = null;
		} else {
			$customer_id = $customer->id;
		}
		$cek_rating = RatingModel::where('customer_id', $customer_id)
		->where('product_id', $product->id)
		->select('value_rating')
		->first();
		if($cek_rating != ''){
			$is_rated = 1; //kalau 1 dia rated
			$value_rating_customer  = $cek_rating->value_rating;
		} else {
			$is_rated = 0; //kalau 0 dia tak rated
			$value_rating_customer  = 0;
		}
		$product = array(
			'product_id' => $product->id,
			'categories' => $product->category,
			'hot' => $product->hot,
			'promo' => $product->promo,
			'restock' => $product->restock,
			'name' => $product->name,
			'material' => $product->material,
			'weight' => $product->weight,
			'price' => $product->priceDetails,
			'product_more_detail' => $productMoreDetails,
			'picture' => $product->imageDetails,
			'rating_product' => $productRating,
			'total_customer_rating' => $productRatingItem,
			'out_of_stock' => $out_of_stock,
			'is_rated' => $is_rated,
			'rating_per_customer' => $value_rating_customer,
			'viewer_product' => $product_viewer, 
		);

		return response()->json([
            'result' => $product,
            'status' => 'OK'
        ]);
	}

	public function get_product_by_category($category_id=NULL) {
		if($category_id == ''){
			return response()->json([
                'result' => 'Parameter required',
                'status' => 'ERROR'
            ], 401);
		}
		$products = ProductModel::with([
			'category:id,name',
			'priceDetails:id,product_id,price_type,price',
			'colorDetails.moreDetails:id,product_color_detail_id,stock,size',
			'imageDetails:id,product_id,image,caption',
			'ratings:id,customer_id,product_id,value_rating'
		]);
		$products = $products->select([ // [ ]<-- biar lebih rapi aja
        	'id',
        	'categories_id',
        	'hot',
        	'promo',
        	'restock',
        	'material',
        	'name',
        	'weight',
        	'created_date',
        ])
		->where('categories_id', $category_id)
		->orderBy('created_date','desc')
        ->get();
		if($products->isEmpty()){
			return response()->json([
	            'result' => $products,
	            'status' => 'EMPTY'
	        ]);
		}
		foreach ($products as $key => $value) {
			$productMoreDetails = $value->colorDetails->reduce(function ($result, $colorDetails) {
				$result[$colorDetails->color] = $colorDetails->moreDetails;
				return $result;
			});
			if($productMoreDetails == ''){
				$productMoreDetails = (object) array();
			}
			//perhitungan rating product wak
			$productRating = $value->ratings;
			$productRatingItem = $productRating->count();
			$productRating = ($productRatingItem > 0) ? $productRating->sum('value_rating') / $productRatingItem : 0;
			$out_of_stock = true;
			foreach ($value->colorDetails as $more) {
				$out_of_stock = true;
				if(!$more->moreDetails->isEmpty()){
					$out_of_stock = false;
					break;
				}
			}
			$outputs[$key] = array(
				'product_id' => $value->id,
				'categories' => $value->category,
				'hot' => $value->hot,
				'promo' => $value->promo,
				'restock' => $value->restock,
				'name' => $value->name,
				'material' => $value->material,
				'weight' => $value->weight,
				'price' => $value->priceDetails,
				'product_more_detail' => $productMoreDetails,
				'picture' => $value->imageDetails,
				'rating_product' => $productRating,
				'total_customer_rating' => $productRatingItem,
				'out_of_stock' => $out_of_stock,
				'created_date' => $value->created_date
			);
		}
		return response()->json([
            'result' => $outputs,
            'status' => 'OK'
        ]);
	}

	public function save_return_product(Request $request) {
		DB::beginTransaction();
		$validator = Validator::make($request->all(), [ 
            'reason' => 'required|max:255|min:3',
            'picture' => 'required',
            'picture.*' => 'image|mimes:jpeg,png,jpg',
        ]);

        if ($validator->fails()) { 
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        $user = auth('api')->user();
        //START GENERATE RETURN PRODUK //
        $chars = "0123456789";
        $res = "";
        for ($i = 0; $i < 3; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        $returncode = "RETURN-" . strtoupper(indonesian_date(date('F'), 'F','')) . "-" . $res.$user->id;
        //END GENERATE RETURN PRODUK //

    	$save_return = ReturnProductModel::create([
    		'reason' => request('reason'),
    		'customer_id' => $user->id,
    		'status' => 2, //menunggu konfirmasi
    		'return_code' => $returncode
    	]);
    	
    	//START save data di table return_product_image
        $picture = request('picture');
        if($picture != ''){
            foreach ($picture as $key => $pic) {
                $name_file = strtolower(str_random(3)).'-'.$pic->getClientOriginalName();
                $pic->move(public_path('storage/img/return_product'), $name_file);
                ReturnProductImageModel::create([
                    'return_product_id' => $save_return->id, //last id save
                    'picture' => $name_file,
                ]);
            }
        }
        // END save data di table return_product_image
    	DB::commit();
        if(!$save_return){
            return response()->json([
                'result' => 'Data Cannot Saved',
                'status' => 'ERROR'
            ], 400);
        } else {
            return response()->json([
                'result' => 'Return Product successfully saved',
                'status' => 'OK'
            ], 200);
        }
	}

	public function search_product() {
		$query = request('query');
        if($query == '') {
            return response()->json([
                "result" => [
                    "query"=> [
                        "Pencarian tidak boleh kosong"
                    ]
                ], 
                "status" => "ERROR"
            ], 400);
        }
        if(strlen($query) < 2) {
           	return response()->json([
                "result" => [
                    "query"=> [
                        "Pencarian tidak boleh kurang dari 2 huruf"
                    ]
                ], 
                "status" => "ERROR"
            ], 400);
        }
		$searching = ProductModel::with([
			'category:id,name',
			'priceDetails:id,product_id,price_type,price',
			'colorDetails.moreDetails:id,product_color_detail_id,stock,size',
			'imageDetails:id,product_id,caption,image',
		])
		->where('name','LIKE','%'.$query.'%')
		->select([
		   'id',
		   'categories_id',
		   'name'
		])
		->get();

        if($searching->isEmpty()){
            return response()->json([
                "result" => [
                    "search"=> [
                        "Pencarian anda tidak ditemukan"
                    ]
                ], 
                "status" => "ERROR"
            ], 404);
        }

        foreach ($searching as $key => $search) {
			$productMoreDetails = $search->colorDetails->reduce(function ($result, $colorDetails) {
				$result[$colorDetails->color] = $colorDetails->moreDetails;
				return $result;
			});
			if($productMoreDetails == ''){
				$productMoreDetails = (object) array();
			}
			//perhitungan rating product wak
			$productRating = $search->ratings;
			$productRatingItem = $productRating->count();
			$productRating = ($productRatingItem > 0) ? $productRating->sum('value_rating') / $productRatingItem : 0;
			$out_of_stock = true;
			foreach ($search->colorDetails as $more) {
				$out_of_stock = true;
				if(!$more->moreDetails->isEmpty()){
					$out_of_stock = false;
					break;
				}
			}
			$outputs[$key] = array(
				'id' => $search->id,
				'categories' => $search->category,
				'name' => $search->name,
				'price' => $search->priceDetails,
				'product_more_detail' => $productMoreDetails,
				'picture' => $search->imageDetails->shift(),
				'out_of_stock' => $out_of_stock,
			);
		}

        return response()->json([
            'result' => $outputs,
            'status' => 'OK'
        ], 200);
	}
}
