<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\CategoriesModel;

class CategoriesController extends Controller {
    
	public function list_categories() {
		$categories = CategoriesModel::orderBy('created_date', 'DESC')->get();
		if($categories->isEmpty()){
			return response()->json([
	            'result' => [],
	            'status' => 'EMPTY'
	        ]);
		}
		return response()->json([
            'result' => $categories,
            'status' => 'OK'
        ]);
	}

}
