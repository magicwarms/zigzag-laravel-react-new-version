<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\WishlistModel;
use App\Model\ProductModel;
use App\Model\ProductPriceDetailModel;
use App\Model\RatingModel;
use App\Model\ProductColorDetailModel;
use App\Model\ProductMoreDetailModel;
use App\Model\ProductImageDetailModel;

use DB;
use Validator;

class WishlistController extends Controller {
    
	public function list_wishlist() {
		$customer = auth('api')->user();
		$wishlists = DB::table('wishlist')
        ->join('products', 'products.id', '=', 'wishlist.product_id')
        // ->join('product_more_detail', 'product_more_detail.id', '=', 'wishlist.product_more_detail_id')
        ->join('product_image_detail', 'product_image_detail.product_id', '=', 'products.id')
        // ->where('product_more_detail.stock', '>', 0)
        ->where('wishlist.customer_id', $customer->id)
        ->groupBy('wishlist.id')
        ->select([
            'products.id AS product_id',
            'product_image_detail.image AS picture',
            'products.name',
            'products.promo',
            'products.hot',
            'products.restock',
            'products.weight',
            'wishlist.id',
            'wishlist.product_more_detail_id',
            'wishlist.price',
            'wishlist.color',
            'wishlist.size',
        ])
        ->get();

        if($wishlists->isEmpty()) {
            return response()->json([
                'result' => [],
                'status' => 'EMPTY'
            ]);
        }
		
		return response()->json([
            'result' => $wishlists,
            'status' => 'OK'
        ]);
	}

	public function save_wishlist(Request $request) {
		$validator = Validator::make($request->all(), [
            'product_id' => 'required|min:1',
            'product_more_detail_id' => 'required',
            'price' => 'required',
            'color' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        DB::beginTransaction();
        $customer = auth('api')->user();

        $check_wishlist = WishlistModel::where('product_id', request('product_id'))
        ->where('product_more_detail_id', request('product_more_detail_id'))
        ->where('customer_id', $customer->id)->first();

        if ($check_wishlist != '') {
            return response()->json([
                "result" => [
                    "wishlist"=> [
                        "Produk ini sudah ada didaftar wishlist anda."
                    ]
                ], 
                "status" => "ERROR"
            ], 401);       
        }

        if(request('size') == ''){
            $size = 0;
        } else {
            $size = request('size');
        }

        $save_wishlist = WishlistModel::create([
            'customer_id' => $customer->id,
            'product_id' => request('product_id'),
            'product_more_detail_id' => request('product_more_detail_id'),
            'price' => request('price'),
            'color' => request('color'),
            'size' => $size
        ]);

        DB::commit();
        if($save_wishlist){
            return response()->json([
                'result' => 'Wishlist successfully saved',
                'status' => 'OK'
            ], 200);
        } else {
            return response()->json([
                'result' => 'Data Cannot Saved',
                'status' => 'ERROR'
            ], 400);
            DB::rollback();
        }
	}

	public function delete_wishlist(Request $request){
		$validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

		DB::beginTransaction();

        $wishlist = WishlistModel::where('id', request('id'))->first();
        $delete_wishlist = $wishlist->delete();

        DB::commit();
        if($delete_wishlist){
            return response()->json([
                'result' => 'Wishlist successfully deleted',
                'status' => 'OK'
            ], 200);
        } else {
            return response()->json([
                'result' => 'Data Cannot Deleted',
                'status' => 'ERROR'
            ], 400);
            DB::rollback();
        }
    }
}
