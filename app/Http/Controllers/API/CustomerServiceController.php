<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\CustomerServiceModel;

class CustomerServiceController extends Controller {
    
	public function list_customer_service() {
		$customer_services = CustomerServiceModel::where('status', 1)
		->where('is_partner', 0)
		->select([
			'id',
			'name',
			'whatsapp',
			'line'
		])
		->get();
		if($customer_services->isEmpty()){
			return response()->json([
	            'result' => [],
	            'status' => 'EMPTY'
	        ]);
		}
		return response()->json([
            'result' => $customer_services,
            'status' => 'OK'
        ]);
	}

}
