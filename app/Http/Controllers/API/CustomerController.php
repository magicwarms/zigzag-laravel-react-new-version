<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use Laravel\Passport\Client;

use App\Model\CustomerModel;
use App\Model\OauthRefreshTokenModel;

use App\Model\DepositModel;
use App\Model\CustomerUpgradeModel;
use App\Model\BankAccountModel;

use App\Mail\ForgotPassword;
use App\Mail\RegisterEmailActivation;
use App\Mail\ChangedPasswordCustomer;
use App\Mail\DepositPropose;
use App\Mail\ProposeUpgradeAccount;
use App\Mail\ConfirmationUpgradeAccount;
use App\Mail\AccountActivated;

use Route;
use Validator;
use DB;

class CustomerController extends Controller {
    
    public function login(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        $customer = CustomerModel::where('email', strtolower(request('email')))->first();

        if($customer){
            if($customer->status == 0){
                return response()->json([
                    "result" => [
                        "status"=> [
                            "Akun kamu belum aktif, coba cek sekali lagi inbox email kamu dan klik link aktivasi akun nya"
                        ]
                    ], 
                    "status" => "ERROR"
                ], 401);

            }

            if (!Hash::check(request('password'), $customer->password)) {
                return response()->json([
                    "result" => [
                        "password"=> [
                            "kata sandi yang kamu masukkan salah"
                        ]
                    ], 
                    "status" => "ERROR"
                ], 401);
            }
        } else {
            return response()->json([
                "result" => [
                    "not found"=> [
                        "Kami tidak menemukan akun anda"
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }
 
        $client_auth = Client::where('id', 2)->first();
        $params = [
            'grant_type' => 'password',
            'client_id' => $client_auth->id,
            'client_secret' => $client_auth->secret,
            'username' => $customer->email,
            'scope' => '*',
        ];

        $request->request->add($params);
        $request->request->remove('email');

        $response = Request::create('oauth/token', 'POST');
        return response()->json([
            'result' => json_decode(Route::dispatch($response)->content(), true),
            'status' => 'OK'
        ]);
    } 

    public function register(Request $request) {
        DB::beginTransaction();

        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email|unique:customers,email', 
            'password' => 'required|min:8',
            'address' => 'required',
            'province'=> 'required',
            'city' => 'required',
            'subdistrict' => 'required',
            'zip' => 'required',
            'tele' => 'required|unique:customers,tele',
            'acc_type' => 'required', //1 -> Member 2 -> VIP Member 3 -> Partner
            'customer_service_id' => 'required|min:1'
        ]);
        if ($validator->fails()) { 
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        if(request('login_with') == ''){
            $login_with = 0; //register biasa
        } else {
            $login_with = request('login_with'); //register with social media
        }

        $input = $request->all();
        $input['email'] = strtolower($input['email']);
        if(request('acc_type') == 3){ //kalo dia register by partner
            $input['acc_type'] = 3;
        } else {
            $input['acc_type'] = 1;
        }
        if(request('acc_type') == 2){
            $input['acc_type'] = 1;
        }
        $input['password'] = bcrypt($input['password']);
        $input['login_with'] = $login_with;
        $user = CustomerModel::create($input);
        //kalo dia propose vip member kirimin email dan kode order
        // if(request('acc_type') == 2) {
        //     //START GENERATE KODE UPGRADE ACCOUNT //
        //     $chars = "0123456789";
        //     $res = "";
        //     for ($i = 0; $i < 3; $i++) {
        //         $res .= $chars[mt_rand(0, strlen($chars)-1)];
        //     }
        //     $kodeorder = "UPGRADE-" . strtoupper(indonesian_date(date('F'), 'F','')) . "-" . $res.$user->id;
        //     //END GENERATE KODE UPGRADE ACCOUNT //
        //     $bank_accounts =  BankAccountModel::all('bank_name','under_name','bank_account');
        //     $when = now()->addSeconds(3);
        //     $unique_code = mt_rand(10, 99);
        //     $data = [
        //         'to' => request('email'),
        //         'name' => request('name'),
        //         'order_code' => $kodeorder,
        //         'status' => 'Menunggu Konfirmasi',
        //         'bank_accounts' => $bank_accounts,
        //         'unique_code' => $unique_code
        //     ];
        //     $sending_mail = Mail::to($user->email)->later($when, new ProposeUpgradeAccount($data));
        //     CustomerUpgradeModel::create([
        //         'unique_code' => $unique_code,
        //         'customer_id' => $user->id,
        //         'order_code' => $kodeorder,
        //         'status' => 2
        //     ]);
        // }

        DB::commit();

        if(!$user){
            return response()->json([
                "result" => [
                    "cannot save"=> [
                        "Data Cannot Saved"
                    ]
                ], 
                "status" => "ERROR"
            ], 400);
        }

        $encrypt_id_customer = Crypt::encryptString($user->id);
        $data = [
            'to' => $user->email,
            'name' => $user->name,
            'link' => 'https://zigzagbatam.com:8080/api/confirm_registered_email/'.$encrypt_id_customer
        ];
        $when = now()->addSeconds(1);
        $sending_mail = Mail::to($user->email)->later($when, new RegisterEmailActivation($data));

        return response()->json([
            'result' => $user,
            'status' => 'OK'
        ], 200);
    }

    public function resend_email($email) {
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        $user = CustomerModel::where('email', $email)->where('status', 0)->first();

        if($user == ''){
            return response()->json([
                "result" => [
                    "not found"=> [
                        "Kami tidak menemukan akun anda dan bisa saja akun kamu sudah aktif."
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }

        $encrypt_id_customer = Crypt::encryptString($user->id);
        $data = [
            'to' => $user->email,
            'name' => $user->name,
            'link' => 'https://zigzagbatam.com:8080/api/confirm_registered_email/'.$encrypt_id_customer
        ];
        $when = now()->addSeconds(1);
        $sending_mail = Mail::to($user->email)->later($when, new RegisterEmailActivation($data));

        return response()->json([
            'result' => 'Success',
            'status' => 'OK'
        ], 200);
    }

    public function details() {
        $user = auth('api')->user();
        return response()->json([
            'result' => $user,
            'status' => 'OK'
        ], 200); 
    }

    public function refresh_token(Request $request) {
        $validator = Validator::make($request->all(), [
            'refresh_token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        $client_auth = Client::where('id', 2)->first();

        $params = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $request->refresh_token,
            'client_id' => $client_auth->id,
            'client_secret' => $client_auth->secret,
            'scope' => '*',
        ];

        $request->request->add($params);
        $response = Request::create('oauth/token', 'POST');

        return response()->json([
            'result' => json_decode(Route::dispatch($response)->content(), true),
            'status' => 'OK'
        ]);
    }

    public function forgot_password(Request $request) {
        DB::beginTransaction();
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        $email = request('email');
        $customer_mail = CustomerModel::where('email', $email)->select('name','email')->first();
        if($customer_mail == ''){
            return response()->json([
                'result' =>'Data Not Found',
                'status' => 'ERROR'
            ], 404); 
        }
        //simpan kode forgot password
        $code = mt_rand(1000,9999);
        $update_code = CustomerModel::where('email', $email)->first();
        $update_code->code_forgot = $code;
        $update_code->save();

        $encrypt_email_customer = Crypt::encryptString($customer_mail->email);
        $encrypt_code_customer = Crypt::encryptString($code);

        $data = [
            'to' => $customer_mail->email,
            'name' => $customer_mail->name,
            'link' =>'https://zigzagbatam.com:8080/api/confirm_forgot_password/'.$encrypt_email_customer.'/'.$encrypt_code_customer
        ];
        $when = now()->addSeconds(3);
        $sending_mail = Mail::to($customer_mail->email)->later($when, new ForgotPassword($data));

        DB::commit();
        return response()->json([
            'result' => 'Email Sent',
            'status' => 'OK'
        ], 200);

    }

    public function confirm_forgot_password($encrypt_mail, $encrypt_code){
        if($encrypt_mail == '' || $encrypt_code == ''){
            return response()->json([
                'result' => 'Parameter required',
                'status' => 'ERROR'
            ], 401); 
        }
        $decrypted_mail = Crypt::decryptString($encrypt_mail);
        $decrypted_code = Crypt::decryptString($encrypt_code);
        return redirect('https://www.zigzagbatam.com/new-password?email='.$decrypted_mail.'&code='.$decrypted_code); 
    }
    //input new password after get verified email customer from confirm_forgot_password function
    public function new_password(Request $request) {
        $validator = Validator::make($request->all(), [
            'code' => 'required|numeric',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        $check_code = CustomerModel::where('email', request('email'))
        ->where('code_forgot', request('code'))
        ->first();

        if(empty($check_code)){
            return response()->json([
                "result" => [
                    "code"=> [
                        "Maaf, ada kesalahan, anda tidak dapat merubah kata sandi anda untuk saat ini, Kode verifikasi tidak ditemukan."
                    ],
                ], 
                "status" => "ERROR"
            ], 401);
        }
        
        if($check_code->status == 0){
            return response()->json([
                "result" => [
                    "status"=> [
                        "Maaf, status akun anda tidak aktif."
                    ],
                ], 
                "status" => "ERROR"
            ], 401);
        }

        if (Hash::check(request('password'), $check_code->password)) {
            return response()->json([
                "result" => [
                    "password"=> [
                        "Kata sandi baru kamu tidak boleh sama dengan kata sandi yang lama."
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }

        $crypted_password = bcrypt(request('password'));
        $update_password = DB::table('customers')
        ->where('email', $check_code->email)
        ->update(['password' => $crypted_password, 'code_forgot' => '']);

        $when = now()->addSeconds(1);
        $data = [
            'to' => $check_code->email,
            'name' => $check_code->name
        ];
        $sending_mail = Mail::to($check_code->email)->later($when, new ChangedPasswordCustomer($data));

        if($update_password) {
            return response()->json([
                'result' => 'New Password successfully changed',
                'status' => 'OK'
            ], 200); 
        } else {
            return response()->json([
                'result' => 'New Password not successfully changed',
                'status' => 'ERROR'
            ], 400); 
        }
    }

    public function confirm_registered_email($encrypt_id){
        if($encrypt_id == ''){
            return response()->json([
                'result' => 'Parameter required',
                'status' => 'ERROR'
            ], 401); 
        }
        $decrypted_customer_id = Crypt::decryptString($encrypt_id);
        $customer_status = CustomerModel::findOrFail($decrypted_customer_id);
        $customer_status->status = 1;
        $update_status = $customer_status->save();

        if($update_status){

            $data = [
                'to' => $customer_status->email,
                'name' => $customer_status->name,
            ];
            $when = now()->addSeconds(3);
            $sending_mail = Mail::to($customer_status->email)->later($when, new AccountActivated($data));

            return redirect('https://www.zigzagbatam.com/account/active'); 
        } else {
            return response()->json([
                "result" => [
                    "status"=> [
                        "Akun anda tidak berhasil aktif"
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }
    }

    public function save_deposit(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'total' => 'required|numeric'
        ]);
        if ($validator->fails()) { 
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        if(request('total') < 50000){
            return response()->json([
                "result" => [
                    "warning"=> [
                        "Maaf, minimal yang dapat kamu lakukan hanya sebesar Rp. 50.000"
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }

        DB::beginTransaction();
        $user = auth('api')->user();
        //START GENERATE KODE ORDER DEPOSIT //
        $chars = "0123456789";
        $res = "";
        for ($i = 0; $i < 4; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        $kodeorder = "DEPOSIT-" . strtoupper(indonesian_date(date('F'), 'F','')) . "-" . $res.$user->id;
        //END GENERATE KODE ORDER DEPOSIT //
        $save_deposit = DepositModel::create([
            'customer_id' => $user->id,
            'kode_order' => $kodeorder,
            'total' => request('total'),
            'status' => 1,
        ]);
        DB::commit();
        $bank_accounts =  BankAccountModel::all('bank_name','under_name','bank_account');
        $data = [
            'to' => $user->email,
            'name' => $user->name,
            'kode_order' => $kodeorder,
            'bank_accounts' => $bank_accounts,
            'total' => request('total')
        ];
        $when = now()->addSeconds(3);
        $sending_mail = Mail::to($user->email)->later($when, new DepositPropose($data));

        if($save_deposit){
            return response()->json([
                'result' => 'Deposit successfully saved',
                'status' => 'OK'
            ], 200);
        } else {
            return response()->json([
                'result' => 'Data Cannot Saved',
                'status' => 'ERROR'
            ], 400);
            DB::rollback();
        }
    }

    public function list_deposit() {
        $user = auth('api')->user();
        $customer_deposit = DepositModel::where('customer_id', $user->id)
        ->select([
            'id',
            'kode_order',
            'total',
            'balance',
            'evidence',
            'status',
            'created_date',
            'updated_date'
        ])
        ->get();

        if($customer_deposit->isEmpty()){
            return response()->json([
                'result' => [],
                'status' => 'EMPTY'
            ]);
        }

        foreach ($customer_deposit as $key => $val) {
            if($val->status == 1){
                $status = array('status_id' => 1, 'desc' => 'Belum Pembayaran / Menunggu Konfirmasi');
            } else if ($val->status == 2) {
                $status = array('status_id' => 2, 'desc' => 'Transaksi Deposit Ditolak');
            } else if ($val->status == 3) {
                $status = array('status_id' => 3, 'desc' => 'Transaksi Deposit Ditambah');
            } else if ($val->status == 4) {
                $status = array('status_id' => 4, 'desc' => 'Transaksi Dikurang');
            } else if ($val->status == 5) {
                $status = array('status_id' => 5, 'desc' => 'Proses Cek Mutasi');
            } else {
                $status = array('status_id' => 6, 'desc' => 'Deposit Dikembalikan');
            }
            $outputs[$key] = array(
                'id' => $val->id,
                'kode_order' => $val->kode_order,
                'total' => $val->total,
                'balance' => $val->balance,
                'evidence' => $val->evidence,
                'status' => $status,
                'created_date' => $val->created_date,
                'updated_date' => $val->updated_date,
            );
        }

        return response()->json([
            'result' => $outputs,
            'status' => 'OK'
        ]);
    }

    public function evidence_deposit(Request $request) {
        
        $validator = Validator::make($request->all(), [ 
            'kode_order' => 'required|min:5',
            'bank' => 'required',
            'total' => 'required|numeric',
            'sender_bank' => 'required',
            'received_bank' => 'required',
            'transfer_date' => 'required|date',
            'evidence' => 'image|mimes:jpeg,png,jpg|required'
        ]);

        if ($validator->fails()) { 
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }
        
        $deposit = DepositModel::where('kode_order', request('kode_order'))->first();
        if(empty($deposit)) {
            return response()->json([
                "result" => [
                    "kode_order"=> [
                        "Kode order deposit tidak ditemukan"
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }

        if(request('total') != $deposit->total){
            return response()->json([
                "result" => [
                    "total"=> [
                        "Nominal konfirmasi anda tidak sesuai dengan nominal yang diajukan, mohon ulangi."
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }

        //kalo status depo dia udah disetujui tak usah konfirmasi lagi
        if ($deposit->status == 3) {
            return response()->json([
                "result" => [
                    "status"=> [
                        "Status deposit anda sudah disetujui"
                    ]
                ], 
                "status" => "OK"
            ]);
        }

        DB::beginTransaction();
        $user = auth('api')->user();
        $evidence = request('evidence');
        if($evidence){
            $evidence = $evidence->storeAs('evidence_deposit', 'evidence-deposit-'.$user->id.'-'.request('kode_order').'.'.$evidence->getClientOriginalExtension());
            request()->evidence->move(public_path('storage/img/evidence_deposit'), $evidence); 
        }
        
        $transfer_date = date_create(request('transfer_date'));
        $deposit->bank = request('bank');
        $deposit->sender_bank = request('sender_bank');
        $deposit->received_bank = request('received_bank');
        $deposit->transfer_date = date_format($transfer_date,"Y-m-d");
        $deposit->evidence = $evidence;
        $deposit->status = 5; //menunggu verifikasi pembayaran
        $updated_deposit = $deposit->save();
        DB::commit();

        if(!$updated_deposit){
            return response()->json([
                'result' => 'Data Cannot Uploaded',
                'status' => 'ERROR'
            ], 400);
        } else {
            return response()->json([
                'result' => 'Evidence Uploaded',
                'status' => 'OK'
            ], 200);
        }
    }

    public function update_profile_photo(Request $request) {
        DB::beginTransaction();
        $validator = Validator::make($request->all(), [
            'photo_file' => 'image|mimes:jpeg,png,jpg',
        ]);

        if ($validator->fails()) { 
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        $user = auth('api')->user();
        $photo_file = request('photo_file');
        if($photo_file){
            $photo_file = $photo_file->storeAs('photo_profile_customer', 'profile-customer-'.$user->id.'-'.$user->name.'.'.$photo_file->getClientOriginalExtension());
            request()->photo_file->move(public_path('storage/img/photo_profile_customer'), $photo_file);
        } else {
            return response()->json([
                "result" => [
                    "photo_file"=> [
                        "Gambar profil kamu kosong"
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }
        
        $profile = CustomerModel::findOrFail($user->id);
        $profile->picture = $photo_file;
        $updated_profile = $profile->save();
        DB::commit();

        if(!$updated_profile){
            return response()->json([
                'result' => 'Data Profile Picture Cannot Uploaded',
                'status' => 'ERROR'
            ], 400);
        } else {
            return response()->json([
                'result' => 'Data Profile Picture Uploaded',
                'status' => 'OK'
            ], 200);
        }
    }

    public function update_profile_customer(Request $request) {
        DB::beginTransaction();
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
            'address' => 'required',
            'province'=> 'required',
            'city' => 'required',
            'subdistrict' => 'required',
            'zip' => 'required',
            'tele' => 'required',
        ]);

        if ($validator->fails()) { 
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        $user = auth('api')->user();
        
        $profile = CustomerModel::findOrFail($user->id);
        $profile->name = request('name');
        $profile->address = request('address');
        $profile->province = request('province');
        $profile->city = request('city');
        $profile->subdistrict = request('subdistrict');
        $profile->zip = request('zip');
        $profile->bank_account = request('bank_account');
        $profile->tele = request('tele');
        $updated_profile = $profile->save();
        DB::commit();

        if(!$updated_profile){
            return response()->json([
                'result' => 'Data Cannot Updated',
                'status' => 'ERROR'
            ], 400);
        } else {
            return response()->json([
                'result' => 'Data Updated',
                'status' => 'OK'
            ], 200);
        }
    }    
    
    public function change_password(Request $request) {
        DB::beginTransaction();
        $validator = Validator::make($request->all(), [ 
            'old_password' => 'required|min:8',
            'new_password' => 'required|min:8'
        ]);

        if ($validator->fails()) { 
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }
        $user = auth('api')->user();
        $customer = CustomerModel::findOrFail($user->id);

        if (!Hash::check(request('old_password'), $customer->password)) {
            return response()->json([
                "result" => [
                    "old_password"=> [
                        "Kata sandi lama kamu tidak sama dengan kata sandi yang sekarang"
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }

        if (Hash::check(request('new_password'), $customer->password)) {
            return response()->json([
                "result" => [
                    "new_password"=> [
                        "Kata sandi baru kamu tidak boleh sama dengan kata sandi yang sebelumnya"
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        } 

        $data = [
            'to' => $customer->email,
            'name' => $customer->name
        ];
        //harus jalanin php artisan queue:work --tries=3 lalu setting juga class mail nya dan install supervisor di server
        $when = now()->addSeconds(3);
        $sending_mail = Mail::to($customer->email)->later($when, new ChangedPasswordCustomer($data));

        $customer->password = bcrypt(request('new_password'));
        $changed = $customer->save();
        DB::commit();

        if(!$changed){
            return response()->json([
                'result' => 'New Password Cannot Updated',
                'status' => 'ERROR'
            ], 400);
        } else {
            return response()->json([
                'result' => 'New Password Updated',
                'status' => 'OK'
            ], 200);
        }
    }

    public function logout() {
        $accessToken = auth('api')->user()->token();
        $refreshToken = OauthRefreshTokenModel::where('access_token_id', $accessToken->id)->first();
        if ($refreshToken->delete() && $accessToken->delete()) {
            return response()->json([
                'result' => 'Successfully logout',
                'status' => 'OK'
            ], 200);
        }
    }

    public function list_upgrade_account() {
        $user = auth('api')->user();
        $customer_upgrade_acc = CustomerUpgradeModel::where('customer_id', $user->id)
        ->select([
            'order_code',
            'bank_sender',
            'bank_number_sender',
            'bank_receiver',
            'date AS date_confirmation',
            'evidence AS picture_upgrade',
            'total_transfer',
            'status',
            'unique_code',
            'created_date'
        ])
        ->orderBy('created_date', 'DESC')
        ->first();

        if($customer_upgrade_acc == ''){
            return response()->json([
                'result' => [],
                'status' => 'EMPTY'
            ]);
        }
        
        if($customer_upgrade_acc->status == 1){
            $status = array('status_id' => 1, 'desc' => 'Konfirmasi valid & disetujui','created_date' => $customer_upgrade_acc->created_date, 'order_code' => $customer_upgrade_acc->order_code);
        } elseif ($customer_upgrade_acc->status == 2) {
            $status = array('status_id' => 2, 'desc' => 'Upgrade diajukan dan menunggu konfirmasi upgrade','created_date' => $customer_upgrade_acc->created_date, 'order_code' => $customer_upgrade_acc->order_code);
        } elseif ($customer_upgrade_acc->status == 3) {
            $status = array('status_id' => 3, 'desc' => 'Cek Mutasi', 'created_date' => $customer_upgrade_acc->created_date, 'order_code' => $customer_upgrade_acc->order_code);
        } else {
            $status = array('status_id' => 0, 'desc' => 'Konfirmasi tidak disetujui', 'created_date' => $customer_upgrade_acc->created_date, 'order_code' => $customer_upgrade_acc->order_code);
        }

        return response()->json([
            'result' => $status, 
            'status' => 'OK'
        ]);
    }

    public function upgrade_account() {
        return false;
        // $user = auth('api')->user();
        // //START GENERATE KODE UPGRADE ACCOUNT //
        // $chars = "0123456789";
        // $res = "";
        // for ($i = 0; $i < 3; $i++) {
        //     $res .= $chars[mt_rand(0, strlen($chars)-1)];
        // }
        // $kodeorder = "UPGRADE-" . strtoupper(indonesian_date(date('F'), 'F','')) . "-" . $res.$user->id;
        // //END GENERATE KODE UPGRADE ACCOUNT //
        // $unique_code = mt_rand(10, 99);
        // DB::beginTransaction();
        // $save_propose = CustomerUpgradeModel::create([
        //     'customer_id' => $user->id,
        //     'unique_code' => $unique_code,
        //     'order_code' => $kodeorder,
        //     'status' => 2 //upgrade akun diajukan
        // ]);
        // DB::commit();
        // if($save_propose) {
        //     $bank_accounts =  BankAccountModel::all('bank_name','under_name','bank_account');
        //     $when = now()->addSeconds(3);
        //     $data = [
        //         'to' => $user->email,
        //         'name' => $user->name,
        //         'bank_accounts' => $bank_accounts,
        //         'order_code' => $kodeorder,
        //         'unique_code' => $unique_code,
        //         'status' => 'Menunggu Konfirmasi',
        //     ];
        //     $sending_mail = Mail::to($user->email)->later($when, new ProposeUpgradeAccount($data));
        //     return response()->json([
        //         'result' => 'Propose Account successfully saved',
        //         'status' => 'OK'
        //     ], 200);
        // } else {
        //     return response()->json([
        //         'result' => 'Propose Account Not successfully saved',
        //         'status' => 'ERROR'
        //     ], 400);
        //     DB::rollback();
        // }
    }

    public function confirm_upgrade_account(Request $request) {
        return false;
        // $validator = Validator::make($request->all(), [
        //     'order_code' => 'required|min:5',
        //     'bank_sender' => 'required|min:3',
        //     'bank_number_sender' => 'required|min:5',
        //     'bank_receiver' => 'required|min:5',
        //     'date' => 'required|date',
        //     'evidence' => 'image|mimes:jpeg,png,jpg|required',
        //     'total_transfer' => 'required|min:5'
        // ]);

        // if ($validator->fails()) { 
        //     return response()->json([
        //         'result' => $validator->errors(),
        //         'status' => 'ERROR'
        //     ], 401);            
        // }
        // DB::beginTransaction();

        // $user = auth('api')->user();
        // $evidence = request('evidence');
        // $get_id = CustomerUpgradeModel::where('order_code', request('order_code'))->first();
        // if($get_id == ''){
        //     return response()->json([
        //         "result" => [
        //             "order_code"=> [
        //                 "Kode order upgrade akun kamu salah."
        //             ]
        //         ], 
        //         "status" => "ERROR"
        //     ], 401);
        // }

        // if($evidence){
        //     $evidence = $evidence->storeAs('evidence_upgrade_account', 'evidence-upgrade-account-'.$user->id.'-'.$get_id->order_code.'.'.$evidence->getClientOriginalExtension());
        //     request()->evidence->move(public_path('storage/img/evidence_upgrade_account'), $evidence); 
        // }
        
        // if($get_id->unique_code+50000 != request('total_transfer')){
        //     return response()->json([
        //         "result" => [
        //             "total_transfer"=> [
        //                 "Jumlah transfer yang kamu masukkan tidak sesuai"
        //             ]
        //         ], 
        //         "status" => "ERROR"
        //     ], 401);
        // }

        // $confirmation = CustomerUpgradeModel::findOrFail($get_id->id);
        // $confirmation->bank_sender = request('bank_sender');
        // $confirmation->bank_number_sender = request('bank_number_sender');
        // $confirmation->bank_receiver = request('bank_receiver');
        // $confirmation->date = date('Y-m-d', strtotime(request('date')));
        // $confirmation->evidence = $evidence;
        // $confirmation->total_transfer = request('total_transfer');
        // $confirmation->status = 3; //Cek mutasi
        // $save_confirmation = $confirmation->save();

        // if($save_confirmation){
        //     $when = now()->addSeconds(3);
        //     $data = [
        //         'to' => $user->email,
        //         'name' => $user->name,
        //         'order_code' => request('order_code'),
        //         'bank_sender' => request('bank_sender'),
        //         'bank_number_sender' => request('bank_number_sender'),
        //         'bank_receiver' => request('bank_receiver'),
        //         'date' => date('d F Y', strtotime(request('date'))),
        //         'total_transfer' => request('total_transfer'),
        //     ];
        //     $sending_mail = Mail::to($user->email)->later($when, new ConfirmationUpgradeAccount($data));

        //     DB::commit();
        //     return response()->json([
        //         'result' => 'Confirmation Upgrade Account successfully saved',
        //         'status' => 'OK'
        //     ], 200);
        // } else {
        //     return response()->json([
        //         'result' => 'Confirmation Upgrade Account Not successfully saved',
        //         'status' => 'ERROR'
        //     ], 400);
        //     DB::rollback();
        // }
    }
}
