<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\EkspedisiModel;

use DB;
use File;

class EkspedisiController extends Controller {
    
	public function list_ekspedisi() {
		$ekspedisi = EkspedisiModel::all();
		if($ekspedisi->isEmpty()){
			return response()->json([
	            'result' => [],
	            'status' => 'EMPTY'
	        ]);
		}
		return response()->json([
            'result' => $ekspedisi,
            'status' => 'OK'
        ]);
	}

	public function list_province($id=NULL) {
		//list all province
        $provinces = select_all_province($id);
        if(empty($provinces)) {
			return response()->json([
	            'result' => [],
	            'status' => 'EMPTY'
	        ]);
		}

		return response()->json([
            'result' => $provinces,
            'status' => 'OK'
        ]);
	}

	public function get_city_by_province($id=NULL, $id2=NULL) {
		//$id-->city_id, $id2-->province_id-->optional
        $get_cities = selectall_city_by_province($id, $id2);
        if(empty($get_cities)) {
			return response()->json([
	            'result' => [],
	            'status' => 'EMPTY'
	        ]);
		}

		return response()->json([
            'result' => $get_cities,
            'status' => 'OK'
        ]);
	}

	public function get_subdistrict_by_city($id) {
		//$id-->city_id
        $get_subdistrict = selectall_subdistrict_by_city($id);
        if(empty($get_subdistrict)) {
			return response()->json([
	            'result' => [],
	            'status' => 'EMPTY'
	        ]);
		}

		return response()->json([
            'result' => $get_subdistrict,
            'status' => 'OK'
        ]);
	}

	public function get_cost_ekspedition($origin, $destination, $weight, $courier) {
		//$origin,--> asal 
		//$destination, --> tujuan
		//$weight,  --> berat (gram)
		//$courier --> kurir (tiki,jne,pos)
		$get_cost = cost_ekspedisi($origin, $destination, $weight, $courier);
        if(empty($get_cost)) {
			return response()->json([
	            'result' => [],
	            'status' => 'EMPTY'
	        ]);
		}
		return response()->json([
            'result' => $get_cost,
            'status' => 'OK'
        ]);
	}

	public function check_waybill($resi, $courier) {
		//$resi,--> nomor resi
		//$courier --> kurir
		$check_waybill = cek_resi($resi, $courier);
        if(empty($check_waybill)) {
			return response()->json([
	            'result' => [],
	            'status' => 'EMPTY'
	        ]);
		}
		return response()->json([
            'result' => $check_waybill,
            'status' => 'OK'
        ]);
	}

}
