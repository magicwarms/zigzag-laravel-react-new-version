<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\CommentModel;

use DB;
use Validator;

class CommentController extends Controller {
    
	public function save_comment(Request $request) {
		$validator = Validator::make($request->all(), [
            'product_id' => 'required|min:1',
            'comment' => 'required|min:1|max:200'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        DB::beginTransaction();

        $user = auth('api')->user();
        $save_comment = CommentModel::create([
                        'product_id' => request('product_id'),
                        'customer_id' => $user->id,
                        'comment' => request('comment'),
                    ]);
        DB::commit();
        if($save_comment){
            return response()->json([
                'result' => 'Comment successfully saved',
                'status' => 'OK'
            ], 200);
        } else {
            return response()->json([
                'result' => 'Data Comment Cannot Saved',
                'status' => 'ERROR'
            ], 400);
            DB::rollback();
        }
	}

	public function show_comment_product($product_id=NULL) {
		if($product_id == ''){
			return response()->json([
                'result' => 'Parameter required',
                'status' => 'ERROR'
            ], 401);
		}
		$comments = DB::table('comments')
        ->join('customers', 'customers.id', '=', 'comments.customer_id')
        ->where('comments.product_id', $product_id)
        ->where('comments.deleted_at', NULL)
        ->select([ // [ ]<-- biar lebih rapi aja
            'comments.comment',
            'customers.name AS customer_name',
            'customers.picture AS customer_picture',
            'comments.created_date',
        ])
        ->get();

		if($comments == ''){
			return response()->json([
	            'result' => [],
	            'status' => 'EMPTY'
	        ]);
		}

		return response()->json([
            'result' => $comments,
            'status' => 'OK'
        ]);
	}
}
