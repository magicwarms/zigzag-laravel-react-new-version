<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\RatingModel;

use DB;
use Validator;

class RatingController extends Controller {
    
    public function is_rated($product_id, $customer_id) {
        $check_rating = RatingModel::where('product_id', $product_id)
        ->where('customer_id', $customer_id)->first();

        if($check_rating != ''){
            $data = [
                'is_rated' => 1,
                'rating_id' => $check_rating->id,
                'value_rating' => $check_rating->value_rating
            ];
        } else {
           $data = [
                'is_rated' => 0,
                'rating_id' => '',
                'value_rating' => 0
            ]; 
        }

        return response()->json([
            'result' => $data,
            'status' => 'OK'
        ], 200);

    }

	public function save_rating(Request $request) {
		$validator = Validator::make($request->all(), [
            'product_id' => 'required|min:1',
            'value_rating' => 'required|min:1|numeric|between:1,5'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        DB::beginTransaction();

        $user = auth('api')->user();
        $check_rating = RatingModel::where('product_id', request('product_id'))
        ->where('customer_id', $user->id)->first();
        if($check_rating != ''){
            return response()->json([
                "result" => [
                    "rating"=> [
                        "Anda sudah memberikan penilaian pada produk ini sebelumnya"
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }
        $save_rating = RatingModel::create([
                        'product_id' => request('product_id'),
                        'customer_id' => $user->id,
                        'value_rating' => request('value_rating'),
                    ]);
        DB::commit();
        if($save_rating){
            return response()->json([
                'result' => 'Rating successfully saved',
                'status' => 'OK'
            ], 200);
        } else {
            return response()->json([
                'result' => 'Data Rating Cannot Saved',
                'status' => 'ERROR'
            ], 401);
            DB::rollback();
        }
	}

	public function update_rating(Request $request) {
		$validator = Validator::make($request->all(), [
            'id' => 'required|min:1',
            'value_rating' => 'required|min:1|numeric|between:1,5'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);
        }

        DB::beginTransaction();

        $update_rating = DB::table('rating')
        ->where('id', request('id'))
        ->update(['value_rating' => request('value_rating')]);

        DB::commit();
        if($update_rating){
            return response()->json([
                'result' => 'Rating successfully Updated',
                'status' => 'OK'
            ], 200);
        } else {
            return response()->json([
                'result' => 'Data Rating Cannot Updated',
                'status' => 'ERROR'
            ], 400);
            DB::rollback();
        }
	}
}
