<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

use App\Model\OrderModel;
use App\Model\OrderDetailModel;
use App\Model\CartModel;
use App\Model\ProductModel;
use App\Model\CustomerModel;
use App\Model\DepositModel;
use App\Model\ConfirmationOrderModel;
use App\Model\ProductColorDetailModel;
use App\Model\ProductMoreDetailModel;
use App\Model\ProductPriceDetailModel;
use App\Model\BankAccountModel;

use App\Mail\OrderReceived;
use App\Mail\ConfirmationOrderMail;
use App\Mail\CancelOrder;

use DB;
use Validator;

class OrderController extends Controller {
    
    public function save_ekspedition(Request $request) {
        $validator = Validator::make($request->all(), [
            'ekspedition_code' => 'string',
            'ekspedition_company' => 'string',
            'ekspedition_remark' => 'string',
            'ekspedition_total' => 'numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }
        DB::beginTransaction();
        $ekspedition_code = request('ekspedition_code');
        if($ekspedition_code == ''){
            $ekspedition_code = '-';
        }
        $ekspedition_company = request('ekspedition_company');
        if($ekspedition_company == ''){
            $ekspedition_company = '-';
        }
        $ekspedition_remark = request('ekspedition_remark');
        if($ekspedition_remark == ''){
            $ekspedition_remark = '-';
        }
        $ekspedition_total = request('ekspedition_total');
        if($ekspedition_total == ''){
            $ekspedition_total = 0;
        }

        $save_ekspedition = OrderModel::create([
            'ekspedition_code' => $ekspedition_code,
            'ekspedition_company' => $ekspedition_company,
            'ekspedition_remark' => $ekspedition_remark,
            'ekspedition_total' => $ekspedition_total,
        ]);
        DB::commit();
        return response()->json([
            'data' => $save_ekspedition,
            'result' => 'Ekspedition Data successfully saved',
            'status' => 'OK'
        ], 200);
    }

    public function save_order(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3',
            'email' => 'required|email|min:1',
            'tele' => 'required|numeric|min:10',
            'province_name' => 'string|min:1',
            'city_name' => 'string|min:1',
            'subdistrict_name' => 'string|min:1',
            'zip' => 'required|min:1',
            'unique_code' => 'required|max:3',
            'shipping_address' => 'required|string',
            'payment_method' => 'string',
            'dropshipper_name' => 'string',
            'dropshipper_tele' => 'numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        $user = auth('api')->user();
        $payment_depo = strtolower(request('payment_method'));
        if($payment_depo == 'deposit') {
            if($user->deposit == 0){
                return response()->json([
                    "result" => [
                        "deposit"=> [
                            "Anda harus mengisi deposit anda terlebih dahulu"
                        ]
                    ], 
                    "status" => "ERROR"
                ], 401);
            }  
        }
        
        $is_partner = 0;
        if($user->acc_type == 3){
            $is_partner = 1;
            $order_code_type = 'PARTNER-';
        } else if($user->acc_type == 2) {
            $order_code_type = 'VIP-';
        } else if ($user->acc_type == 1) {
            $order_code_type = 'ORDER-';
        }

        //START GENERATE KODE ORDER //
        $chars = "0123456789";
        $res = "";
        for ($i = 0; $i < 4; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        $kodeorder = $order_code_type . strtoupper(indonesian_date(date('F'), 'F','')) . "-" . $res.$user->id;
        //check kode order masih sudah tersedia apa belum!
        $check_kode_order = OrderModel::where('order_code', $kodeorder)->first();
        if($check_kode_order != ''){
            $chars = "0123456789";
            $res = "";
            for ($i = 0; $i < 5; $i++) {
                $res .= $chars[mt_rand(0, strlen($chars)-1)];
            }
            $kodeorder = $order_code_type . strtoupper(indonesian_date(date('F'), 'F','')) . "-" . $res.$user->id;
        }
        //END GENERATE KODE ORDER //
        
        DB::beginTransaction();
        if(request('order_id') != NULL) {
            $save_order = OrderModel::findOrFail(request('order_id'));
            $save_order->customer_id = $user->id;
            $save_order->customer_service_id = $user->customer_service_id;
            $save_order->name = request('name');
            $save_order->email = request('email');
            $save_order->tele = request('tele');
            $save_order->province_name = request('province_name');
            $save_order->city_name = request('city_name');
            $save_order->subdistrict_name = request('subdistrict_name');
            $save_order->zip = request('zip');
            $save_order->unique_code = request('unique_code');
            $save_order->order_code = $kodeorder;
            $save_order->jne_online_booking = request('jne_online_booking');
            $save_order->discount = request('discount');
            $save_order->shipping_address = request('shipping_address');
            $save_order->status_order = 1; // Order Diterima
            $save_order->payment_method = strtolower(request('payment_method'));
            $save_order->dropshipper_name = request('dropshipper_name');
            $save_order->dropshipper_tele = request('dropshipper_tele');
            $save_order->total_weight = request('total_weight');
            $save_order->notes = request('notes');
            $save_order->is_partner = $is_partner; //kalau 1 dia order sbg partner, kalo 0 berarti customer biasa
            $save_order->save();
        } else {
            //kalo kosong order id nya save kayak biasa wak
            $save_order =   OrderModel::create([
                'customer_id' => $user->id,
                'customer_service_id' => $user->customer_service_id,
                'name' => request('name'),
                'email' => request('email'),
                'tele' => request('tele'),
                'province_name' => request('province_name'),
                'city_name' => request('city_name'),
                'subdistrict_name' => request('subdistrict_name'),
                'zip' => request('zip'),
                'unique_code' => request('unique_code'),
                'order_code' => $kodeorder,
                'jne_online_booking' => request('jne_online_booking'),
                'discount' => request('discount'),
                'shipping_address' => request('shipping_address'),
                'status_order' => 1, // Order Diterima
                'payment_method' => strtolower(request('payment_method')),
                'ekspedition_code' => '-',
                'ekspedition_company' => '-',
                'ekspedition_remark' => '-',
                'ekspedition_total' => 0,
                'dropshipper_name' => request('dropshipper_name'),
                'dropshipper_tele' => request('dropshipper_tele'),
                'total_weight' => request('total_weight'),
                'notes' => request('notes'),
                'is_partner' => $is_partner, //kalau 1 dia order sbg partner, kalo 0 berarti customer biasa
            ]);
        }

        $order_detail = request('order_detail');
        $decode = json_decode($order_detail);
        if($decode == null){
            return response()->json([
                "result" => [
                    "order_detail"=> [
                        "Format Order Detail Data Invalid"
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }
        $subtotal_order_add = 0;
        foreach ($decode as $detail) {
            if($detail->product_price == 0){
                return response()->json([
                    "result" => [
                        "product_price"=> [
                            "Harga tidak boleh Nol (0)"
                        ]
                    ], 
                    "status" => "ERROR"
                ], 401);
            }
            
            OrderDetailModel::create([
                'order_id' => $save_order->id,
                'product_id' => $detail->product_id,
                'product_more_detail_id' => $detail->product_more_detail_id,
                'product_name' => $detail->product_name,
                'product_qty' => $detail->product_qty,
                'product_price' => $detail->product_price,
                'product_color' => $detail->product_color,
                'product_size' => $detail->product_size,
                'product_total_price' => $detail->product_total_price,
            ]);
            $subtotal_order_add += $detail->product_price*$detail->product_qty;;

            //kurangin qty product di data product more detail
            $product_more_detail_stock = ProductMoreDetailModel::findOrFail($detail->product_more_detail_id);
            $product_more_detail_stock->stock = $product_more_detail_stock->stock - $detail->product_qty;
            $product_more_detail_stock->save();

            //delete data cart customer
            $cart = CartModel::where('customer_id', $user->id)
            ->where('product_id', $detail->product_id)
            ->where('product_more_detail_id', $detail->product_more_detail_id)
            ->delete();
        }

        $subtotal_order = $subtotal_order_add;
        $grandtotal_order = $subtotal_order+$save_order->ekspedition_total+request('unique_code')-request('discount');
        
        //update subtotal and grandtotal order
        $total_order = OrderModel::findOrFail($save_order->id);
        $total_order->subtotal_order = $subtotal_order;
        $total_order->grandtotal_order = $grandtotal_order;
        $total_order->save();

        if(strtolower(request('payment_method')) ==  'deposit'){
            //cek kalo dia tak cukup depositnya
            if($grandtotal_order > $user->deposit){
                return response()->json([
                    "result" => [
                        "deposit"=> [
                            "Deposit anda tidak cukup"
                        ]
                    ], 
                    "status" => "ERROR"
                ], 401);
            }
            $used_deposit = $user->deposit - $grandtotal_order;
            $customer_deposit = CustomerModel::findOrFail($user->id);
            $customer_deposit->deposit = $used_deposit;
            $customer_deposit->save(); 

            //simpan transaksi deposit terkait customer
            $saved_transaction_depo = DepositModel::create([
                'customer_id' => $user->id,
                'total' => $grandtotal_order,
                'balance' => $used_deposit,
                'status' => 4
            ]);
            //update status jika dia pakai deposit, status pesanan langsung dikirim berarti.
            $update_status_order = OrderModel::findOrFail($save_order->id);
            $update_status_order->status_order = 8;
            $update_status_order->save();
        }

        if($save_order){
            $bank_accounts =  BankAccountModel::all('bank_name','under_name','bank_account');
            //pas checkout sukses baru kirim email
            $data = [
                'to' => $user->email,
                'name' => $total_order->name,
                'order_code' => $kodeorder,
                'payment_method' => strtoupper($total_order->payment_method),
                'tele' => $total_order->tele,
                'ekspedition_code' => $save_order->ekspedition_code,
                'ekspedition_company' => $save_order->ekspedition_company,
                'shipping_address' => $total_order->shipping_address,
                'city_name' => $total_order->city_name,
                'subdistrict_name' => $total_order->subdistrict_name,
                'province_name' => $total_order->province_name,
                'zip' => $total_order->zip,
                'dropshipper_name' => $total_order->dropshipper_name,
                'dropshipper_tele' => $total_order->dropshipper_tele,
                'order_detail' => $decode,
                'bank_accounts' => $bank_accounts,
                'subtotal_order' => $subtotal_order,
                'grandtotal_order' => $grandtotal_order,
                'ekspedition_total' => $save_order->ekspedition_total,
                'discount' => $total_order->discount,
                'unique_code' => $total_order->unique_code,
            ];

            $when = now()->addSeconds(3);
            $sending_mail = Mail::to($user->email)
            ->later($when, new OrderReceived($data));
            
            DB::commit();
            return response()->json([
                'result' => 'Order successfully saved',
                'status' => 'OK'
            ], 200);
            
        } else {
            DB::rollback();
            return response()->json([
                'result' => 'Data Order Cannot Saved',
                'status' => 'ERROR'
            ], 400); 
        }
    }

    public function history_order() {
        $user = auth('api')->user();
        $history = DB::table('orders')
        ->join('customer_service', 'customer_service.id', '=', 'orders.customer_service_id')
        ->where('orders.deleted_at', null)
        ->where('orders.customer_id', $user->id)
        ->orderBy('orders.id','DESC');
        if(request('status') > 0)$history = $history->where('status_order', 1);
        $history = $history->select([ // [ ]<-- biar lebih rapi aja
            'customer_service.name AS customer_service_name',
            'orders.id AS order_id',
            'orders.order_code',
            'orders.unique_code',
            'orders.created_date',
            'orders.shipping_address',
            'orders.payment_method',
            'orders.status_order',
            'orders.name',
            'orders.tele',
            'orders.province_name',
            'orders.city_name',
            'orders.subdistrict_name',
            'orders.zip',
            'orders.ekspedition_code',
            'orders.ekspedition_company',
            'orders.ekspedition_remark',
            'orders.ekspedition_total',
            'orders.discount',
            'orders.subtotal_order',
            'orders.grandtotal_order',
            'orders.resi_order',
            'orders.total_weight',
        ])
        ->paginate(10);

        if($history->isEmpty()){
            return response()->json([
                'result' => [],
                'status' => 'EMPTY'
            ]);
        }

        foreach ($history as $key => $value) {
            $order_detail = DB::table('orders_detail')
            ->join('products', 'products.id', '=', 'orders_detail.product_id')
            ->join('categories', 'categories.id', '=', 'products.categories_id')
            ->where('orders_detail.order_id', $value->order_id)
            ->select([
                'orders_detail.product_id',
                'orders_detail.product_name',
                'categories.name AS category_name',
                'orders_detail.product_qty',
                'orders_detail.product_price',
                'orders_detail.product_color',
                'orders_detail.product_size',
                'orders_detail.product_total_price',
            ])
            ->get();
            $product_price_detail = array();
            if($user->id == 40){
                foreach ($order_detail as $price) {
                    $product_price_detail[] = ProductPriceDetailModel::where('product_id', $price->product_id)
                    ->where('price_type', 'REGULER')
                    ->select('price_type','price')
                    ->first();
                }
                // $grandtotal_order_fake = 0;
                // foreach ($order_detail as $value) {
                //     $grandtotal_order_fake += $product_price_detail->price*$value->product_qty;
                // }
            }
            //dd($grandtotal_order_fake);
            $outputs[$key] = array(
                'order_id' => $value->order_id,
                'order_code' => $value->order_code,
                'created_date' => $value->created_date,
                'shipping_address' => $value->shipping_address,
                'payment_method' => $value->payment_method,
                'status_order_id' => $value->status_order,
                'status_order' => status_order($value->status_order),
                'name' => $value->name,
                'tele' => $value->tele,
                'province_name' => $value->province_name,
                'city_name' => $value->city_name,
                'subdistrict_name' => $value->subdistrict_name,
                'zip' => $value->zip,
                'unique_code' => $value->unique_code,
                'ekspedition_code' => $value->ekspedition_code,
                'ekspedition_company' => $value->ekspedition_company,
                'ekspedition_remark' => $value->ekspedition_remark,
                'ekspedition_total' => $value->ekspedition_total,
                'subtotal_order' => $value->subtotal_order,
                'total_weight' => $value->total_weight,
                'discount' => $value->discount,
                'grandtotal_order' => $value->grandtotal_order,
                'resi_order' => $value->resi_order,
                'customer_service_name' => $value->customer_service_name,
                'order_detail' => $order_detail,
                'product_price_detail' => $product_price_detail,
            );
        }

        return response()->json([
            'result' => $outputs,
            'status' => 'OK'
        ]);
    }

    public function confirmation_order(Request $request){

        $validator = Validator::make($request->all(), [
            'order_code' => 'required',
            'bank_sender' => 'required',
            'bank_receiver' => 'required',
            'date' => 'required|date',
            'evidence' => 'image|mimes:jpeg,png,jpg|required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        DB::beginTransaction();

        $user = auth('api')->user();
        $evidence = request('evidence');
        $get_id = OrderModel::where('order_code', request('order_code'))
        ->where('customer_id', $user->id)
        ->where('status_order', 1)
        ->select('id','grandtotal_order','order_code')
        ->first();

        if($get_id == ''){
            return response()->json([
                "result" => [
                    "order_code"=> [
                        "Kode order kamu tidak ditemukan"
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }
        if($evidence){
            $evidence = $evidence->storeAs('evidence_order', 'evidence-order-'.$user->id.'-'.$get_id->order_code.'.'.$evidence->getClientOriginalExtension());
            request()->evidence->move(public_path('storage/img/evidence_order'), $evidence); 
        }
        
        if(request('total_transfer') != $get_id->grandtotal_order){
            return response()->json([
                "result" => [
                    "total_transfer"=> [
                        "Jumlah yang kamu transfer tidak sesuai dengan total order kamu"
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }
        $transfer_date = date_create(request('date'));
        $save_confirmation = ConfirmationOrderModel::create([
            'order_id' => $get_id->id,
            'customer_id' => $user->id,
            'bank_sender' => request('bank_sender'),
            'bank_receiver' => request('bank_receiver'),
            'date' => date_format($transfer_date,"Y-m-d"),
            'evidence' => $evidence,
            'status' => 3, //cek mutasi
            'total_transfer' => request('total_transfer'),
        ]);
        if($save_confirmation){
            //update status kalo dia udah berhasil konfirmasi
            $update_status_order = OrderModel::findOrFail($get_id->id);
            $update_status_order->status_order = 3;
            $update_status_order->save();

            $when = now()->addSeconds(3);
            $data = [
                'to' => $user->email,
                'name' => $user->name,
                'order_code' => request('order_code'),
                'bank_sender' => request('bank_sender'),
                'bank_receiver' => request('bank_receiver'),
                'date' => request('date'),
                'total_transfer' => request('total_transfer'),
            ];
            $sending_mail = Mail::to($user->email)->later($when, new ConfirmationOrderMail($data));
            DB::commit();
            return response()->json([
                'result' => 'Confirmation Order successfully saved',
                'status' => 'OK'
            ], 200);
        } else {
            return response()->json([
                'result' => 'Confirmation Order Not successfully saved',
                'status' => 'ERROR'
            ], 400);
            DB::rollback();
        }
    }

    public function cancel_order_by_customer(){
        DB::beginTransaction();
        $this->validate(request(), [
            'id' => 'required',
            'remark_cancel_order' => 'required|min:5',
        ]);

        $cancel = OrderModel::findOrFail(request('id'));
        $cancel->status_order = 10;
        $cancel->remark_cancel_order = request('remark_cancel_order');
        $cancel_order = $cancel->save();

        if($cancel->payment_method == 'deposit'){
            $customer_depo = CustomerModel::findOrFail($cancel->customer_id);
            $return_depo = $customer_depo->deposit+$cancel->grandtotal_order;
            $customer_depo->deposit = $return_depo;
            $customer_depo->save();
            //buat history depo jika dia cancel order
            DepositModel::create([
                'customer_id' => $cancel->customer_id,
                'total' => $cancel->grandtotal_order,
                'balance' => $return_depo,
                'status' => 6, //Deposit di kembalikan
            ]);
        }
        
        //kembalikan stock jika dia cancel order
        $return_stock = OrderDetailModel::where('order_id', request('id'))->get();
        foreach ($return_stock as $stock) {
            $product_qty = $stock->product_qty;

            $product_more = ProductMoreDetailModel::findOrFail($stock->product_more_detail_id); //get data produk more detail
            $add_product_stock = $product_qty+$product_more->stock; //tambahkan stock nya
            $product_more->stock = $add_product_stock;
            $product_more->save(); //baru update ke product_detail_model table
        }

        $ekspedition_total = $cancel->ekspedition_total;
        
        $data = [
            'to' => $cancel->email,
            'name' => $cancel->name,
            'canceled_order' => $return_stock,
            'status' => status_order($cancel->status_order),
            'remark_cancel_order' => request('remark_cancel_order'),
            'order_code' => $cancel->order_code,
            'subtotal_order' => $cancel->subtotal_order,
            'grandtotal_order' => $cancel->grandtotal_order,
            'ekspedition_total' => $ekspedition_total,
            'discount' => $cancel->discount,
        ];

        $when = now()->addSeconds(3);
        $sending_mail = Mail::to($cancel->email)->later($when, new CancelOrder($data));

        DB::commit();

        if($cancel_order) {
            return response()->json([
                'result' => 'Canceled Order successfully saved',
                'status' => 'OK'
            ], 200);
        } else {
            return response()->json([
                'result' => 'Canceled Order Not successfully saved',
                'status' => 'ERROR'
            ], 400);
            DB::rollback();
        }
    }

    public function fetch_order_code() {
        $user = auth('api')->user();
        $ordercode = DB::table('orders')
        ->where('deleted_at', null)
        ->where('customer_id', $user->id)
        ->orderBy('id','DESC')
        ->where('status_order', 1)
        ->select('order_code')
        ->get();
        
        if($ordercode->isEmpty()){
            return response()->json([
                'result' => [],
                'status' => 'EMPTY'
            ]);
        }

        return response()->json([
            'result' => $ordercode,
            'status' => 'OK'
        ]);
    }
}
