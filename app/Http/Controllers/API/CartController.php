<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\CartModel;
use App\Model\ProductModel;

use DB;
use Validator;

class CartController extends Controller {
    
    public function list_cart() {
        // $user = auth('api')->user();
        // $customer_cart = DB::table('cart')
        // ->join('products', 'products.id', '=', 'cart.product_id')
        // ->join('product_more_detail', 'product_more_detail.id', '=', 'cart.product_more_detail_id')
        // ->join('product_image_detail', 'product_image_detail.product_id', '=', 'products.id')
        // ->where('product_more_detail.stock', '>', 0)
        // ->where('cart.customer_id', $user->id)
        // ->groupBy('cart.product_more_detail_id')
        // ->select([
        //     'products.id AS product_id',
        //     'product_image_detail.image AS picture',
        //     'products.name',
        //     'products.promo',
        //     'products.hot',
        //     'products.restock',
        //     'product_more_detail.stock',
        //     'cart.id',
        //     'cart.product_more_detail_id',
        //     'cart.price',
        //     'cart.qty',
        //     'cart.weight',
        //     'cart.color',
        //     'cart.size',
        //     'cart.total_price',
        // ])
        // ->get();

        // if($customer_cart->isEmpty()){
            return response()->json([
                'result' => [],
                'status' => 'EMPTY'
            ]);
        // }

        // return response()->json([
        //     'result' => $customer_cart, 
        //     'status' => 'OK'
        // ]);
    }

    public function save_cart(Request $request) {
        return response()->json([
            'result' => [],
            'status' => 'EMPTY'
        ]);

        // $validator = Validator::make($request->all(), [
        //     'product_id' => 'required|min:1',
        //     'product_more_detail_id' => 'required|min:1',
        //     'price' => 'required|min:1',
        //     'qty' => 'required|min:1|numeric',
        //     'weight' => 'required',
        //     'color' => 'required',
        // ]);

        // if ($validator->fails()) {
        //     return response()->json([
        //         'result' => $validator->errors(),
        //         'status' => 'ERROR'
        //     ], 401);            
        // }

        // DB::beginTransaction();

        // $user = auth('api')->user();

        // if(request('size') == ''){
        //     $size = 0;
        // } else {
        //     $size = request('size');
        // }

        // //cek ada gak product dengan produk id, customer id, more detail, color, size product yang sama di cart?
        // $check_product_in_cart = CartModel::where('product_id', request('product_id'))
        // ->where('customer_id', $user->id)
        // ->where('product_more_detail_id', request('product_more_detail_id'))
        // ->where('size', $size)
        // ->where('color', request('color'))
        // ->first();
        
        // if($check_product_in_cart != ''){
            //kalau ada update qty nya aja
            // $add_qty = $check_product_in_cart->qty+request('qty');
            // $add_weight = $check_product_in_cart->weight+request('weight');
            // $update_qty_prod_cart = DB::table('cart')
            // ->where('product_id', request('product_id'))
            // ->where('customer_id', $user->id)
            // ->where('product_more_detail_id', request('product_more_detail_id'))
            // ->update([
            //     'qty' => $add_qty,
            //     'weight' => $add_weight,
            //     'total_price' => request('total_price'),
            // ]);

        // } else {
            //kalau gak ada save seperti biasa nya
            // $add_to_cart = CartModel::create([
            //     'product_id' => request('product_id'),
            //     'product_more_detail_id' => request('product_more_detail_id'),
            //     'customer_id' => $user->id,
            //     'price' => request('price'),
            //     'qty' => request('qty'),
            //     'weight' => request('weight'),
            //     'color' => request('color'),
            //     'size' => $size,
            //     'total_price' => request('total_price'),
            // ]);

        // }

        // DB::commit();
        // return response()->json([
        //     'result' => 'Cart successfully saved',
        //     'status' => 'OK'
        // ], 200);
        
    }

    public function delete_cart(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|min:1',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        DB::beginTransaction();

        $cart = CartModel::where('id', request('id'))->first();
        $delete_cart = $cart->delete();

        DB::commit();
        if($delete_cart){
            return response()->json([
                'result' => 'Cart successfully deleted',
                'status' => 'OK'
            ], 200);
        } else {
            return response()->json([
                'result' => 'Data Cart Cannot Deleted',
                'status' => 'ERROR'
            ], 400);
            DB::rollback();
        }
    }

    public function delete_cart_by_customer(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|min:1',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        DB::beginTransaction();

        $delete_cart = DB::table('cart')
        ->where('customer_id', request('customer_id'))
        ->delete();

        DB::commit();
        if($delete_cart){
            return response()->json([
                'result' => 'Cart successfully deleted',
                'status' => 'OK'
            ], 200);
        } else {
            return response()->json([
                'result' => 'Data Cart Cannot Deleted',
                'status' => 'ERROR'
            ], 400);
            DB::rollback();
        }
    }

    public function update_qty_cart(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|min:1',
            'qty' => 'required|min:1',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        DB::beginTransaction();
        $update_qty = DB::table('cart')
        ->where('id', request('id'))
        ->update([
            'qty' => request('qty'),
            'weight' => request('weight'),
            'total_price' => request('total_price')
        ]);

        DB::commit();
        if($update_qty) {
            return response()->json([
                'result' => 'Cart Successfully Updated',
                'status' => 'OK'
            ], 200);
        } 
        else {
            return response()->json([
                'result' => 'Data Cart Cannot Updated',
                'status' => 'ERROR'
            ], 400);
            DB::rollback();
        }
    }
}
