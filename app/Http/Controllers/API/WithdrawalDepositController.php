<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

use App\Model\WithdrawalDepositModel;
use App\Model\DepositModel;
use App\Model\CustomerModel;

use App\Mail\WithdrawalPropose;

use Validator;
use DB;

class WithdrawalDepositController extends Controller {
    
	public function withdrawal(Request $request) {
		$validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'bank' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json([
                'result' => $validator->errors(),
                'status' => 'ERROR'
            ], 401);            
        }

        DB::beginTransaction();
        $user = auth('api')->user();
        $propose = request('amount');
        $bank = request('bank');
        //cek kurang dari 100k tak?
        if($propose < 100000){
            return response()->json([
                "result" => [
                    "amount"=> [
                        "Permintaan pencairan deposit minimal Rp. 100.000."
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }
        //jika permintaan pencairan lebih besar dari deposit si customer
        if($propose > $user->deposit){
            return response()->json([
                "result" => [
                    "amount"=> [
                        "Permintaan pencairan deposit anda terlalu besar dari deposit anda."
                    ]
                ], 
                "status" => "ERROR"
            ], 401);
        }

        //START GENERATE KODE WITHDRAWAL //
        $chars = "0123456789";
        $res = "";
        for ($i = 0; $i < 3; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        $kodewd = "WITHDRAWAL-" . strtoupper(indonesian_date(date('F'), 'F','')) . "-" . $res.$user->id;
        //END GENERATE KODE WITHDRAWAL //
        $save_wd = WithdrawalDepositModel::create([
                            'customer_id' => $user->id,
                            'code' => $kodewd,
                            'amount' => $propose,
                            'bank' => $bank,
                            'status' => 1, //status diajukan
                        ]);

        //kurangin deposit si customer dulu
        $customer_depo = CustomerModel::findOrFail($user->id);
        $customer_depo->deposit = $user->deposit - $propose;
        $customer_depo->save();

        DB::commit();

        $data = [
            'to' => $user->email,
            'name' => $user->name,
            'code' => $kodewd,
            'amount' => $propose,
            'bank' => $bank
        ];
        $when = now()->addSeconds(3);
        $sending_mail = Mail::to($user->email)->later($when, new WithdrawalPropose($data));

        if($save_wd){
            return response()->json([
                'result' => 'Withdrawal Deposit successfully saved',
                'status' => 'OK'
            ], 200);
        } else {
            return response()->json([
                'result' => 'Data Cannot Saved',
                'status' => 'ERROR'
            ], 400);
            DB::rollback();
        }
	}

    public function list_withdrawal() {
        $withdrawals = WithdrawalDepositModel::select([
            'customer_id',
            'code',
            'amount',
            'status',
        ])
        ->with('customer:id,name')->get();

        if($withdrawals->isEmpty()){
            return response()->json([
                'result' => [],
                'status' => 'EMPTY'
            ]);
        }

        foreach ($withdrawals as $key => $value) {

            if($value->status == 1){
                $status_detail = array(
                    'status' => $value->status, 
                    'remark' => 'Pencairan Diajukan'
                );
            } elseif ($value->status == 2) {
                $status_detail = $status_detail = array(
                    'status' => $value->status, 
                    'remark' => 'Pencairan Disetujui'
                );
            } elseif ($value->status == 3) {
                $status_detail = $status_detail = array(
                    'status' => $value->status, 
                    'remark' => 'Pencairan Ditolak'
                );
            }

            $outputs[$key] = array(
                'code' => $value->code,
                'amount' => $value->amount,
                'customer' => array(
                    'customer_id' => $value->customer->id,
                    'customer_name' => $value->customer->name
                ),
                'status' => $status_detail
            );
        }

        
        return response()->json([
            'result' => $outputs,
            'status' => 'OK'
        ]);
    }

}
