<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\BannerModel;
use App\Model\BannerDetailModel;
use App\Model\ProductModel;
use App\Model\ProductPriceDetailModel;
use App\Model\RatingModel;
use App\Model\ProductColorDetailModel;
use App\Model\ProductMoreDetailModel;
use App\Model\ProductImageDetailModel;


use DB;

class BannerController extends Controller {
    
	public function list_banner() {
		
		$banners = BannerModel::with([
			'details.product.category', 
			'details.product.priceDetails:id,product_id,price_type,price',
			'details.product.colorDetails.moreDetails:id,product_color_detail_id,stock,size', 
			'details.product.imageDetails:id,product_id,caption,image',
			'details.product.ratings'
		])->activeDate()->get();

		if($banners->isEmpty()){
			return response()->json([
                'result' => [],
                'status' => 'EMPTY'
            ]);
		}

		$result = [];

		foreach ($banners as $index => $banner) {
			$products = [];

			foreach ($banner->details as $key => $bannerDetail) {
				$productMoreDetails = $bannerDetail->product->colorDetails->reduce(function ($result, $colorDetails) {
				$result[$colorDetails->color] = $colorDetails->moreDetails;
					return $result;
				});
				if($productMoreDetails == ''){
					$productMoreDetails = (object) array();
				} 
				//perhitungan rating product wak
				$productRating = $bannerDetail->product->ratings;
				$productRatingItem = $productRating->count();
				$productRating = ($productRatingItem > 0) ? $productRating->sum('value_rating') / $productRatingItem : 0;
				$out_of_stock = true;
				foreach ($bannerDetail->product->colorDetails as $more) {
					$out_of_stock = true;
					if(!$more->moreDetails->isEmpty()){
						$out_of_stock = false;
						break;
					}
				}
				$product = array(
					'product_id' => $bannerDetail->product->id,
					'categories_id' => $bannerDetail->product->category->id,
					'categories_name' => $bannerDetail->product->category->name,
					'hot' => $bannerDetail->product->hot,
					'promo' => $bannerDetail->product->promo,
					'restock' => $bannerDetail->product->restock,
					'name' => $bannerDetail->product->name,
					'price' => $bannerDetail->product->priceDetails,
					'material' => $bannerDetail->product->material,
					'weight' => $bannerDetail->product->weight,
					'product_more_detail' => $productMoreDetails,
					'picture' => $bannerDetail->product->imageDetails,
					'rating_product' => $productRating,
					'total_customer_rating' => $productRatingItem,
					'created_date' => $bannerDetail->product->created_date
				);
				array_push($products, $product);
			}

			$bannerResult = array(
				'id' => $banner->id,
				'title' => $banner->title,
				'picture' => $banner->picture,
				'description' => $banner->description,
				'products' => $products,
			);

			array_push($result, $bannerResult);
		}

		return response()->json([
            'result' => $result,
            'status' => 'OK'
        ]);
	}
}
