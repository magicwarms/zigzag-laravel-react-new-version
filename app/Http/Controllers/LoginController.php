<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Model\UserAdminModel;
use App\Model\AttemptsUserAdminModel;

use Carbon\Carbon;

use Auth;

use App\Mail\AttemptLoginUserAdmin;

class LoginController extends Controller {

    public function index() {
        record_activity("Aktifitas mengunjugi halaman login admin");
        return view('login');
    }

    public function process_login(Request $request) {

    	$this->validate(request(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:8',
        ]);
        
        $email = filter_var($request->email, FILTER_VALIDATE_EMAIL);
        $password = $request->password;

        $check_status = UserAdminModel::where('email', $email)->first();
        if($check_status != ''){

            //checking brute force attack when login
            $checking_brute_force = $this->checkbruteforce($check_status->id);
            if($checking_brute_force == true){
                record_activity("akun ini dengan nama ".$check_status->name." telah melakukan banyak percobaan login.");
                //kirim email pemberitahuan ke user yang bersangkutan
                $data = [
                    'to' => $check_status->email,
                    'name' => $check_status->name
                ];
                $when = now()->addSeconds(2);
                Mail::to($check_status->email)->later($when, new AttemptLoginUserAdmin($data));

                return redirect()->back()->with('info','Untuk sementara akun anda telah terkunci, silakan hubungi programmer anda untuk melaporkan masalah ini. Terima kasih!.');
            }

            if($check_status->status_admin != 1){
                record_activity("akun tidak aktif ".$check_status->name);
                return redirect()->back()->with('info','Maaf, akun anda tidak aktif.');
            }

            $remembering = false;
            if($request->remember != ''){
                $remembering = true;
            }

        	if(Auth::attempt(['email' => $email, 'password' => $password], $remembering )) {
                $user = UserAdminModel::find(Auth::user()->id);
                $user->last_login = Carbon::now();
                $user->save();
                record_activity("Aktifitas login berhasil ".Auth::user()->name);
                return redirect()->route('beranda')->with('success','Halo!, Selamat Datang '.Auth::user()->name);
        	}

        } else {
            record_activity("Tidak menemukan akun");
            return redirect()->back()->with('warning','Maaf, kami tidak menemukan akun anda.'); 
        }

        //simpan waktu dia salah password atau login untuk mencegah brute force
        AttemptsUserAdminModel::create([
            'user_admin_id' => $check_status->id,
            'time' => time()
        ]);

        record_activity("email dan kata sandi pengguna salah");
    	return redirect()->back()->with('warning','Maaf, cek email dan kata sandi kamu kembali.');
    }

    public function logout(Request $request) {
        record_activity("Aktifitas logout ".Auth::user()->name);
    	Auth::guard('web')->logout();
        $request->session()->invalidate();
    	return redirect()->route('login')->with('success','Kamu sudah logout!');
    }

    function checkbruteforce($user_admin_id) {
        $now = time();
        $valid_attempts = $now - (2 * 60 * 60);
        
        $attempts = AttemptsUserAdminModel::where('user_admin_id', $user_admin_id)->where('time','>', $valid_attempts)->count();
        if($attempts  > 4){
            $user_admin = UserAdminModel::findOrFail($user_admin_id);
            $user_admin->status_admin = 0; //non aktifkan langsung status nya
            $user_admin->save();

            return true;
        } else {
            return false;
        }
    }
}
