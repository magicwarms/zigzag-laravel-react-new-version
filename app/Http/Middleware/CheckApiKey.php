<?php

namespace App\Http\Middleware;

use Closure;

class CheckApiKey {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        $envs = [
            'local',
            'staging',
            'production'
        ];
	    $api_key_secure = '953ab333-c05a-zigzag-4abf-b3db-6a8c25c0031e';
        if(in_array(app()->environment(), $envs)) {
            if($request->header('x-api-key') != $api_key_secure) {
                return response()->json([
                    'result' => 'You are not authorized to use this api.',
                    'status' => 'UNAUTHORIZED'
                ], 401);
            }
        }

        return $next($request);
    }
}
