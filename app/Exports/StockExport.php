<?php

namespace App\Exports;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

use App\Model\ProductModel;
use App\Model\ProductMoreDetailModel;

class StockExport implements FromView, WithColumnFormatting, ShouldAutoSize {

    public function view(): View {
    	$products = ProductModel::with([
			'priceDetails:id,product_id,price_type,price',
			'colorDetails.moreDetails:id,product_color_detail_id,stock,size'
		])
		->get();
        
		$today_date = date('d F Y');
		$sum_stock = ProductMoreDetailModel::sum('stock');
        return view('exports.stock', [
            'products' => $products,
            'today_date' => $today_date,
            'sum_stock' => $sum_stock
        ]);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'D' => '#,##0',
            'E' => '#,##0',
        ];
    }
}
