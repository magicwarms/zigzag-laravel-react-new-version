<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ViewProductModel extends Model {

    protected $table = 'views_product';
	protected $fillable = [
		'product_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'viewed_at'
    ];

    public $timestamps = false;
}
