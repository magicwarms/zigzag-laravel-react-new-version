<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WishlistModel extends Model {
    
    protected $table = 'wishlist';
	protected $fillable = [
		'product_id', 'customer_id', 'product_more_detail_id', 'price', 'color', 'size'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        
    ];

    public $timestamps = false;

}
