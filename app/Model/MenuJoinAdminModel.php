<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuJoinAdminModel extends Model {
	
	use SoftDeletes;

    protected $table = 'menu_join_admin';
	protected $fillable = [
        'user_admin_id', 'menu_admin_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public $timestamps = false;
}
