<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductMoreDetailModel extends Model {
    
	protected $table = 'product_more_detail';
	protected $fillable = [
        'product_color_detail_id', 'size','stock',
    ];

    public $timestamps = false;
}
