<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VisitorModel extends Model {
    
    protected $table = 'visitor';
	protected $fillable = [
		'ip','dates_visit'
    ];

    public $timestamps = false;
}
