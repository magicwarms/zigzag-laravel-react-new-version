<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductImageDetailModel extends Model {

    protected $table = 'product_image_detail';
	protected $fillable = [
        'product_id', 'image','caption'
    ];

    public $timestamps = false;
}
