<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CartModel extends Model {
    
    protected $table = 'cart';
	protected $fillable = [
		'customer_id', 'product_id', 'price', 'qty', 'weight','total_price','color','size','product_more_detail_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        
    ];

    public $timestamps = false;

}
