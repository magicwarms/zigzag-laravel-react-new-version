<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model {
    
    protected $table = 'products';
	protected $fillable = [
		'categories_id', 'hot', 'name','material', 'weight','promo','restock'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        
    ];

    public $timestamps = false;

    public function category()
    {
        return $this->hasOne(CategoriesModel::class, 'id', 'categories_id');
    }

    public function bannerDetail()
    {
        return $this->hasOne(BannerDetailModel::class, 'product_id', 'id');
    }

    public function priceDetails()
    {
        return $this->hasMany(ProductPriceDetailModel::class, 'product_id', 'id');
    }

    public function colorDetails()
    {
        return $this->hasMany(ProductColorDetailModel::class, 'product_id', 'id');
    }

    public function imageDetails()
    {
        return $this->hasMany(ProductImageDetailModel::class, 'product_id', 'id');
    }

    public function ratings()
    {
        return $this->hasMany(RatingModel::class, 'product_id', 'id');
    }
}
