<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerUpgradeModel extends Model {

    use SoftDeletes;
    protected $table = 'confirmation_upgrade_account';
    protected $fillable = [
    	'customer_id', 'bank_sender', 'bank_number_sender', 'bank_receiver', 'date','evidence','total_transfer','status','order_code','unique_code'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at','date'
    ];

    public $timestamps = false;
}
