<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CustomerServiceModel extends Model {

    protected $table = 'customer_service';
    protected $fillable = [
    	'name', 'whatsapp','address','status','line','commission','is_partner'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        
    ];

    public $timestamps = false;

}
