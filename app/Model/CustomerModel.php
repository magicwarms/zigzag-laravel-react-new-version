<?php

namespace App\Model;

use Laravel\Passport\HasApiTokens;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;

class CustomerModel extends Authenticatable {

    use HasApiTokens, Notifiable;

    protected $table = 'customers';
	protected $fillable = [
        'name', 'email','password','address','province','city','zip','tele','deposit','acc_type','status','login_with','customer_service_id','bank_account','subdistrict','total_order','code_forgot'
    ];
    
    public $timestamps = false;

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
	    'password'
	];
}
