<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BannerModel extends Model {
    
	protected $table = 'banner';
	protected $fillable = [
		'title', 'description','start_date','end_date','picture'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_date','start_date','end_date'
    ];

    public $timestamps = false;

    public function details()
    {
        return $this->hasMany(BannerDetailModel::class, 'banner_id', 'id');
    }

    public function scopeActiveDate($query)
    {
        return $query->whereDate('start_date', '<=', now())
                    ->whereDate('end_date', '>=', now());
    }
}
