<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BannerDetailModel extends Model {
    
	protected $table = 'banner_detail';
	protected $fillable = [
		'banner_id', 'product_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_date'
    ];

    public $timestamps = false;

    public function product()
    {
        return $this->hasOne(ProductModel::class, 'id', 'product_id');
    }

    public function banners()
    {
        return $this->hasOne(BannerModel::class, 'id', 'banner_id');
    }
}
