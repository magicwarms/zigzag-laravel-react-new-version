<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuModel extends Model {
    
	use SoftDeletes;

	protected $table = 'menu_admin';
	protected $fillable = [
        'name', 'icon', 'function', 'parent', 'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public $timestamps = false;
    
}
