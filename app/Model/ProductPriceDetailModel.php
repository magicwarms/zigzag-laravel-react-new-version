<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductPriceDetailModel extends Model {
    
	protected $table = 'product_price_detail';
	protected $fillable = [
        'product_id', 'price_type', 'price'
    ];

    public $timestamps = false;
}
