<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConfirmationOrderModel extends Model {

    use SoftDeletes;
    protected $table = 'confirmation_order';
	protected $fillable = [
		'order_id', 'customer_id', 'bank_sender', 'bank_receiver', 'date','evidence','total_transfer','status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'date'
    ];

    public $timestamps = false;
}
