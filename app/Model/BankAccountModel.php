<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BankAccountModel extends Model {
    
	protected $table = 'bank_accounts';
    protected $fillable = [
    	'bank_account','bank_name','under_name','picture'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_date','updated_date'
    ];

    public $timestamps = false;

}
