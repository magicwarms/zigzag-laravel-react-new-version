<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ViewOrderModel extends Model {
    
	protected $table = 'view_grandtotal_order';
    
}
