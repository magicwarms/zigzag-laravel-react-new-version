<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClientAuthModel extends Model {
    
    protected $table = 'oauth_clients';
}
