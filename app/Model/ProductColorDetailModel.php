<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductColorDetailModel extends Model {
    
	protected $table = 'product_color_detail';
	protected $fillable = [
        'product_id', 'color','is_shoes'
    ];

    public $timestamps = false;

    public function moreDetails()
    {
        return $this->hasMany(ProductMoreDetailModel::class, 'product_color_detail_id', 'id')->where('stock','>',0);
    }
}
