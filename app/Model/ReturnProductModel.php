<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReturnProductModel extends Model {
    
	protected $table = 'return_product';
	protected $fillable = [
		'reason', 'status', 'customer_id', 'return_code'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_date'
    ];

    public $timestamps = false;

    public function images() {
    	return $this->hasMany(ReturnProductImageModel::class, 'return_product_id', 'id');
    }

    public function customers() {
        return $this->hasOne(CustomerModel::class, 'id', 'customer_id');
    }
}
