<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AttemptsUserAdminModel extends Model {
    
	protected $table = 'attempts_login_user_admin';
	protected $fillable = [
		'user_admin_id', 'time'
    ];

    public $timestamps = false;

}
