<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ActivityModel extends Model {
    
	protected $table = 'activity_record';
	protected $fillable = [
		'name', 'activity', 'user_agent'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_date'
    ];

    public $timestamps = false;
}
