<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WithdrawalDepositModel extends Model {
    
	protected $table = 'withdrawal_deposit';
	protected $fillable = [
		'amount', 'customer_id', 'code', 'status','bank'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_date'
    ];

    public $timestamps = false;

    public function customer() {
        return $this->hasOne(CustomerModel::class, 'id', 'customer_id');
    }

}
