<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderDetailModel extends Model {

	protected $table = 'orders_detail';
	protected $fillable = [
		'order_id', 'product_id', 'product_name', 'product_qty','product_price','product_total_price','product_color','product_size','product_more_detail_id','is_return'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public $timestamps = false;
}
