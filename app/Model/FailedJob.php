<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FailedJob extends Model {
    protected $table = 'failed_jobs';
}
