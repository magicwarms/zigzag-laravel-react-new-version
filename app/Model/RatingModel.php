<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RatingModel extends Model {
    
	use SoftDeletes;
    protected $table = 'rating';
	protected $fillable = [
		'customer_id', 'product_id', 'value_rating'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public $timestamps = false;

}
