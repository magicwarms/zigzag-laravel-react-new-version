<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoriesModel extends Model {
    
    protected $table = 'categories';
    protected $fillable = [
    	'name', 'parent','picture'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        
    ];

    public function product()
    {
        return $this->hasOne(ProductModel::class, 'categories_id', 'id');
    }

    public $timestamps = false;

}
