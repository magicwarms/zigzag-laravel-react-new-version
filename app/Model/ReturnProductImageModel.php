<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReturnProductImageModel extends Model {
    
	protected $table = 'return_product_image';
	protected $fillable = [
		'return_product_id','picture'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_date'
    ];

    public $timestamps = false;

}
