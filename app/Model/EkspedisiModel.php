<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EkspedisiModel extends Model {

    use SoftDeletes;
    protected $table = 'ekspedisi';
	protected $fillable = [
		'img_file', 'name', 'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public $timestamps = false;
}
