<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepositModel extends Model {
    
	use SoftDeletes;
    protected $table = 'deposit';
	protected $fillable = [
		'customer_id', 'kode_order', 'total', 'status','bank','sender_bank','received_bank','evidence','transfer_date','balance'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at','transfer_date'
    ];

    public $timestamps = false;

}
