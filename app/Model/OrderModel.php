<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderModel extends Model {
    
	use SoftDeletes;
	protected $table = 'orders';
	protected $fillable = [
		'customer_id', 'customer_service_id', 'name', 'email','tele', 'province_name', 'city_name', 'zip', 'order_code', 'shipping_address','status_order','payment_method','ekspedition_company','ekspedition_remark','ekspedition_total','dropshipper_name','dropshipper_tele','grandtotal_order','resi_order','remark_cancel_order','jne_online_booking','discount','subtotal_order','total_weight','is_partner','subdistrict_name','ekspedition_code','unique_code','notes'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public $timestamps = false;

    public function customerService() {
        return $this->hasOne(CustomerServiceModel::class, 'id', 'customer_service_id');
    }

    public function orderDetails() {
        return $this->hasMany(OrderDetailModel::class, 'order_id', 'id');
    }

    public function customers() {
        return $this->hasOne(CustomerModel::class, 'id', 'customer_id');
    }
}
