<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OauthRefreshTokenModel extends Model {

    protected $table = 'oauth_refresh_tokens';
}
