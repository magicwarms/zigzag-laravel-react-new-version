<?php

use App\Model\MenuModel;
use App\Model\ActivityModel;
use App\Model\VisitorModel;
use Jenssegers\Agent\Agent;
use App\Jobs\SavingActivity;
use App\Jobs\SavingVisitor;
use App\Jobs\ViewerProduct;
use \Carbon\Carbon;

if (!function_exists('menu_active')) {
	function menu_active($parent = NULL, $child = NULL)
	{
		$auth = Auth::user()->id;
		$menu = DB::table('menu_admin')
			->join('menu_join_admin', 'menu_join_admin.menu_admin_id', '=', 'menu_admin.id')
			->select('menu_admin.id', 'menu_admin.name', 'menu_admin.function', 'menu_admin.parent', 'menu_admin.icon');
		if ($parent != NULL) {
			$menu->where('menu_admin.parent', 0);
		}
		if ($child != NULL) {
			$menu->where('menu_admin.parent', '!=', 0);
		}
		$menu->where('menu_join_admin.user_admin_id', $auth)
			->where('menu_admin.status', 1);
		$result = $menu->get();
		return $result;
	}
}

if (!function_exists('get_parent_menu_name')) {
	function get_parent_menu_name($id)
	{
		$get_parent = MenuModel::select('name')->where('id', $id)->first();
		return $get_parent;
	}
}

if (!function_exists('get_all_row_multiple_menu')) {
	function get_all_row_multiple_menu($id)
	{
		$menu = DB::table('menu_admin')
			->join('menu_join_admin', 'menu_join_admin.menu_admin_id', '=', 'menu_admin.id')
			->select('menu_admin.name', 'menu_admin.id')
			->where('menu_join_admin.user_admin_id', $id);

		$menu = $menu->get();
		return $menu;
	}
}

if (!function_exists('select_all_multiple_menu')) {
	function select_all_multiple_menu()
	{
		$multiple_menu = MenuModel::select('name', 'id')->get();
		return $multiple_menu;
	}
}

if (!function_exists('select_all_province')) {
	function select_all_province($id = NULL)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pro.rajaongkir.com/api/province?id=" . $id,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: d59049b12bec5f149cb709f386dbd012"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
			exit;
		} else {
			$response = json_decode($response, TRUE);
			return $response['rajaongkir']['results'];
		}
	}
}

if (!function_exists('selectall_city_by_province')) {
	function selectall_city_by_province($id = NULL, $id2 = NULL)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pro.rajaongkir.com/api/city?id=" . $id . "&province=" . $id2,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: d59049b12bec5f149cb709f386dbd012"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$response = json_decode($response, TRUE);
			return $response['rajaongkir']['results'];
		}
	}
}

if (!function_exists('selectall_subdistrict_by_city')) {
	function selectall_subdistrict_by_city($id = NULL, $id2 = NULL, $internal = NULL)
	{
		if ($internal != NULL) {
			$url = "https://pro.rajaongkir.com/api/subdistrict?city=" . $id . "&id=" . $id2;
		} else {
			$url = "https://pro.rajaongkir.com/api/subdistrict?city=" . $id;
		}

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: d59049b12bec5f149cb709f386dbd012"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$response = json_decode($response, TRUE);
			return $response['rajaongkir']['results'];
		}
	}
}

if (!function_exists('cost_ekspedisi')) {
	function cost_ekspedisi($origin, $destination, $weight, $courier)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "origin=" . $origin . "&originType=subdistrict&destination=" . $destination . "&destinationType=subdistrict&weight=" . $weight . "&courier=" . $courier . "",
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: d59049b12bec5f149cb709f386dbd012"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$response = json_decode($response, TRUE);
			return ($response['rajaongkir']);
		}
	}
}

if (!function_exists('cek_resi')) {
	function cek_resi($resi, $courier)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "waybill=" . $resi . "&courier=" . $courier . "",
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: d59049b12bec5f149cb709f386dbd012"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$response = json_decode($response, TRUE);
			return ($response['rajaongkir']);
		}
	}
}

if (!function_exists('status_order')) {
	function status_order($status_order)
	{
		if ($status_order == 1) {
			$status = 'Belum Pembayaran';
		} elseif ($status_order == 3) {
			$status = 'Proses pembayaran';
		} else if ($status_order == 4) {
			$status = 'Pembayaran disetujui';
		} else if ($status_order == 5) {
			$status = 'Proses digudang';
		} elseif ($status_order == 6) {
			$status = 'Barang terkirim';
		} elseif ($status_order == 7) {
			$status = 'Pesanan batal otomatis';
		} elseif ($status_order == 8) {
			$status = 'Pembayaran dari deposit';
		} elseif ($status_order == 9) {
			$status = 'Pesanan Selesai';
		} elseif ($status_order == 10) {
			$status = 'Pesanan dibatalkan oleh customer';
		} else {
			$status = 'Pembayaran ditolak';
		}

		return $status;
	}
}

if (!function_exists('record_activity')) {
	function record_activity($activity)
	{
		if (!empty(Auth::user()->name)) {
			$name = Auth::user()->name;
		} else {
			$name = 'Pengguna yang tidak diketahui - menggunakan ' . user_agent() . ' - IP:' . \Request::ip();
		}

		$saving_activity_data = [
			'name' => $name,
			'activity' => $activity,
			'user_agent' => user_agent(),
		];
		//delay dulu 40 detik save nya
		$send_saving_data = SavingActivity::dispatch($saving_activity_data)->delay(now()->addSeconds(10));
		$send_saving_data = true;
		if ($send_saving_data) {
			return true;
		} else {
			die('There was an error to save this data');
		}
	}
}

if (!function_exists('user_agent')) {
	function user_agent()
	{
		$agent = new Agent();

		if ($agent->isMobile()) {
			$agents = $agent->device() . ' - ' . $agent->browser();
		} else if ($agent->isDesktop()) {
			$agents = $agent->platform() . ' - ' . $agent->browser();
		} else if ($agent->isRobot()) {
			$agents = 'Robot ' . $agent->robot();
		} else if ($agent->isTablet()) {
			$agents = $agent->device() . ' - ' . $agent->browser();
		}

		return $agents;
	}
}

if (!function_exists('indonesian_date')) {
	function indonesian_date($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB')
	{
		if (trim($timestamp) == '') {
			$timestamp = time();
		} elseif (!ctype_digit($timestamp)) {
			$timestamp = strtotime($timestamp);
		}
		# remove S (st,nd,rd,th) there are no such things in indonesia :p
		$date_format = preg_replace("/S/", "", $date_format);
		$pattern = array(
			'/Mon[^day]/', '/Tue[^sday]/', '/Wed[^nesday]/', '/Thu[^rsday]/',
			'/Fri[^day]/', '/Sat[^urday]/', '/Sun[^day]/', '/Monday/', '/Tuesday/',
			'/Wednesday/', '/Thursday/', '/Friday/', '/Saturday/', '/Sunday/',
			'/Jan[^uary]/', '/Feb[^ruary]/', '/Mar[^ch]/', '/Apr[^il]/', '/May/',
			'/Jun[^e]/', '/Jul[^y]/', '/Aug[^ust]/', '/Sep[^tember]/', '/Oct[^ober]/',
			'/Nov[^ember]/', '/Dec[^ember]/', '/January/', '/February/', '/March/',
			'/April/', '/June/', '/July/', '/August/', '/September/', '/October/',
			'/November/', '/December/',
		);
		$replace = array(
			'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min',
			'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu',
			'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des',
			'Januari', 'Februari', 'Maret', 'April', 'Juni', 'Juli', 'Agustus', 'September',
			'Oktober', 'November', 'Desember',
		);
		$date = date($date_format, $timestamp);
		$date = preg_replace($pattern, $replace, $date);
		$date = "{$date}{$suffix}";
		return $date;
	}
}
if (!function_exists('viewed_product')) {
	function viewed_product($product_id)
	{

		$has_viewed = DB::table('views_product')
			->where('product_id', $product_id)
			->whereDate('viewed_at', '=', Carbon::today()->toDateString())
			->first();

		if ($has_viewed == null) {
			$saving_viewer_data = [
				'product_id' => $product_id
			];
			//delay dulu 10 detik save nya
			$send_saving_viewer = ViewerProduct::dispatch($saving_viewer_data)
				->delay(now()->addSeconds(10));

			if ($send_saving_viewer) {
				return true;
			} else {
				die('There was an error to save this data');
			}
		}
	}
}
if (!function_exists('random_color')) {
	function random_color()
	{
		$num = rand(10, 100000);
		$randomString = md5($num); // like "d73a6ef90dc6a ..."
		$r = substr($randomString, 0, 2); //1. and 2.
		$g = substr($randomString, 2, 2); //3. and 4.
		$b = substr($randomString, 4, 2); //5. and 6.

		return '#' . $r . $g . $b;
	}
}

if (!function_exists('record_visitor')) {
	function record_visitor()
	{
		$ip = \Request::ip();
		$saving_visitor_data = [
			'ip' => $ip,
			'dates_visit' => date('Y-m-d'),
		];

		$check_visitor = VisitorModel::where('ip', $ip)
			->where('dates_visit', date('Y-m-d'))
			->first();
		if ($check_visitor == '') {
			//delay dulu 20 detik save nya
			//gak mau dia save pakai queue kalo ada date, kan aneh!
			$send_saving_data = VisitorModel::create($saving_visitor_data);
		} else {
			return false;
		}
		if ($send_saving_data) {
			return true;
		} else {
			die('There was an error to save visitor data');
		}
	}
}
if (!function_exists('changeCodeProduct')) {
    function changeCodeProduct($productName) {
        $splitStr = str_split($productName);
        unset($splitStr[0], $splitStr[1], $splitStr[2]);
        $countStr = count($splitStr);
        $getLastStr = substr($productName, 3, $countStr);

        $getFirstThreeCharacter = substr($productName, 0, 3);
        if ($getFirstThreeCharacter == 'NYO') {
            return 'VEVE' . $getLastStr;
        } else if ($getFirstThreeCharacter == 'SP-') {
            return 'XOXO-' . $getLastStr;
        } else {
            return $productName;
        }
    }
}
