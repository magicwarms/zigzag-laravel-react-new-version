<?php

namespace App\Observers;

use App\Model\FailedJob;
use Illuminate\Support\Facades\Mail;
use App\Mail\FailedJobs;

class FailedJobObserver
{
    /**
     * Handle the failed job "created" event.
     *
     * @param  \App\Model\FailedJob  $failedJob
     * @return void
     */
    public function created(FailedJob $failedJob) {
        Mail::to('magicwarms@gmail.com')
        ->cc('rivayudha@msn.com')
        ->send(new FailedJobs());
    }

    /**
     * Handle the failed job "updated" event.
     *
     * @param  \App\Model\FailedJob  $failedJob
     * @return void
     */
    public function updated(FailedJob $failedJob)
    {
        //
    }

    /**
     * Handle the failed job "deleted" event.
     *
     * @param  \App\Model\FailedJob  $failedJob
     * @return void
     */
    public function deleted(FailedJob $failedJob)
    {
        //
    }

    /**
     * Handle the failed job "restored" event.
     *
     * @param  \App\Model\FailedJob  $failedJob
     * @return void
     */
    public function restored(FailedJob $failedJob)
    {
        //
    }

    /**
     * Handle the failed job "force deleted" event.
     *
     * @param  \App\Model\FailedJob  $failedJob
     * @return void
     */
    public function forceDeleted(FailedJob $failedJob)
    {
        //
    }
}
