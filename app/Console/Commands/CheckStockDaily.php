<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Mail;

use DB;

use App\Mail\CheckDailyStock;

class CheckStockDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checking:stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking order daily when stock less than 3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $products = DB::table('products')
        ->join('product_color_detail', 'product_color_detail.product_id', '=', 'products.id')
        ->join('product_more_detail', 'product_more_detail.product_color_detail_id', '=', 'product_color_detail.id')
        ->where('product_more_detail.stock', '<', 3)
        ->groupBy('products.name')
        ->limit(10)
        ->select([
            'name',
            'color',
            'size',
            'stock'
        ])
        ->get();
        if(!$products->isEmpty()){
            $data = [
                'products' => $products
            ];
            //$sending_mail = Mail::to($order->email)->later($when, new CancelOrder($data));
            $sending_mail = Mail::to('zigzagbatam@gmail.com')
            ->cc('ratu.india@yahoo.com')
            ->send(new CheckDailyStock($data));
            $this->info('Checking order successfully send.');
        } else {
            $this->info('nothing to send, checking stock is safe');
        }
    }
}
