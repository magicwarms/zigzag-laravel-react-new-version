<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

use App\Model\OrderModel;
use App\Model\OrderDetailModel;
use App\Model\ProductMoreDetailModel;
use App\Model\CustomerModel;

use App\Mail\CancelOrder;

use DB;

class CheckingOrderMoreThanTwoHours extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checking:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking customer orders more than two hours, if more than 2 hours, deleted it.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        
        $checking_order = DB::table('orders')
        ->where('status_order', 1)
        ->whereRaw('created_date <= (NOW() - INTERVAL 2 HOUR)')
        ->select([
            'id as order_id',
            'name',
            'email',
            'order_code',
            'ekspedition_total',
            'subtotal_order',
            'discount',
            'grandtotal_order'
        ])
        ->get();
        if(!$checking_order->isEmpty()){
            foreach ($checking_order as $order) {
                //kembalikan stock jika dia cancel order
                $return_stock = OrderDetailModel::where('order_id', $order->order_id)->get();
                foreach ($return_stock as $stock) {
                    $product_qty = $stock->product_qty;
                    $product_more = ProductMoreDetailModel::where('id', $stock->product_more_detail_id)->first(); //get data produk more detail
                    if($product_more != ''){
                        $add_product_stock = $product_qty+$product_more->stock; //tambahkan stock nya
                        $product_more->stock = $add_product_stock;
                        $product_more->save(); //baru update ke product_more_detail_model table
                    }
                }
                $ekspedition_total = $order->ekspedition_total;
                if($ekspedition_total == ''){
                    $ekspedition_total = 0;
                } else {
                    $ekspedition_total = $order->ekspedition_total;
                }
                $data = [
                    'to' => $order->email,
                    'name' => $order->name,
                    'canceled_order' => $return_stock,
                    'status' => 'Pembatalan Otomatis',
                    'remark_cancel_order' => 'Dibatalkan otomatis oleh sistem karena tidak ada aktifitas lebih dari 2 jam',
                    'order_code' => $order->order_code,
                    'subtotal_order' => $order->subtotal_order,
                    'grandtotal_order' => $order->grandtotal_order,
                    'ekspedition_total' => $ekspedition_total,
                    'discount' => $order->discount,
                ];
                $when = now()->addSeconds(5);
                $sending_mail = Mail::to($order->email)->later($when, new CancelOrder($data));
                $cancel = OrderModel::findOrFail($order->order_id);
                $cancel->status_order = 7;
                $cancel->remark_cancel_order = 'Dibatalkan otomatis oleh sistem karena tidak ada aktifitas lebih dari 2 jam';
                $cancel->save();
            }

            $this->info('Orders users successfully change status canceled by system.');

        } else {
            $this->info('Nothing to order canceled by system.');
        }
    }
}
