<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

use App\Model\CustomerModel;

use App\Mail\DowngradeAccountType;

class ResetTotalOrderCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:totalorder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Total Order Customer Every First date of Month, Specially for VIP Account Type. If customer order the contain is less than 4 product, wil switch to reguler account type.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        
        $check_minimal_order = CustomerModel::where('acc_type', 2) //VIP SAJA
        ->where('total_order', '<', 4) //kurang dari 4 item
        ->get();
        
        if(!$check_minimal_order->isEmpty()){
            foreach ($check_minimal_order as $min) {
                $update_acc_type = CustomerModel::findOrFail($min->id);
                $update_acc_type->acc_type = 1; //kembali ke acc type 1 (reguler)
                $update_acc_type->save();

                $data = [
                    'to' => $min->email,
                    'name' => $min->name
                ];
                $when = now()->addSeconds(10);
                $sending_mail = Mail::to($min->email)->later($when, new DowngradeAccountType($data));
                $this->info('Notification to user successfully send to '.$min->name);
            }
        } else {
            $this->info('We dont have data to downgrade account type customer now');
        }
        \DB::table('customers')->where('acc_type', 2)->update(['total_order' => 0]);
    }
}
