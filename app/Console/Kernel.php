<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

use App\Mail\NotificationCartCustomer;

use DB;

use \Carbon\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->command('checking:order')->everyThirtyMinutes(); //30 Menit sekali
        //$schedule->command('reset:totalorder')->monthlyOn(1, '12:00'); //setiap tgl 1 jam 12
        $schedule->command('backup:run --only-db')->daily(); //per hari
        //$schedule->command('backup:run --only-db')->cron('0 */6 * * *'); //6 jam sekali
        $schedule->command('backup:clean')->weekly(); //per minggu
        $schedule->command('backup:monitor')->cron('0 0 */5 * *'); //per 5 hari
        $schedule->command('backup:run --only-files')->cron('0 0 */2 * *'); //per 2 hari
        $schedule->command('checking:stock')->dailyAt('23:50');

        $schedule->command('route:clear')->cron('0 */6 * * *'); //per 6 jam
        $schedule->command('config:clear')->cron('0 */6 * * *'); //per 6 jam
        $schedule->command('cache:clear')->cron('0 */6 * * *'); //per 6 jam
        $schedule->command('view:clear')->cron('0 */6 * * *'); //per 6 jam
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
