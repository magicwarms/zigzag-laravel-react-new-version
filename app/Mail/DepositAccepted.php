<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DepositAccepted extends Mailable implements ShouldQueue {
    
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $to = $this->data['to'];
        $name = $this->data['name'];
        $kode_order = $this->data['kode_order'];
        $total = number_format($this->data['total'], 0,',','.');
        $grandtotal_deposit = number_format($this->data['grandtotal_deposit'], 0,',','.');

        return $this->view('email.deposit_accepted')
        ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
        ->with([
            'to' => $to,
            'name' => $name,
            'order_code' => $kode_order,
            'total' => $total,
            'grandtotal_deposit' => $grandtotal_deposit,
        ]);
    }
}
