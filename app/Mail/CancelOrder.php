<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CancelOrder extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $to = $this->data['to'];
        $name = $this->data['name'];
        $status = $this->data['status'];
        $order_code = $this->data['order_code'];
        $remark_cancel_order = $this->data['remark_cancel_order'];
        $canceled_order = $this->data['canceled_order'];
        $subtotal_order = $this->data['subtotal_order'];
        $grandtotal_order = $this->data['grandtotal_order'];
        $ekspedition_total = $this->data['ekspedition_total'];
        $discount = $this->data['discount'];

        return $this->view('email.cancel_order')
        ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
        ->with([
            'to' => $to,
            'name' => $name,
            'status' => $status,
            'order_code' => $order_code,
            'remark_cancel_order' => $remark_cancel_order,
            'canceled_order' => $canceled_order,
            'subtotal_order' => $subtotal_order,
            'grandtotal_order' => $grandtotal_order,
            'ekspedition_total' => $ekspedition_total,
            'discount' => $discount,
        ]);
    }
}
