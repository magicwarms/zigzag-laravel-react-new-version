<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProposeUpgradeAccount extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $to = $this->data['to'];
        $name = $this->data['name'];
        $status = $this->data['status'];
        $order_code = $this->data['order_code'];
        $bank_accounts = $this->data['bank_accounts'];
        $unique_code = $this->data['unique_code'];

        return $this->view('email.propose_upgrade_account')
        ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
        ->with([
            'to' => $to,
            'name' => $name,
            'status' => $status,
            'order_code' => $order_code,
            'bank_accounts' => $bank_accounts,
            'unique_code' => $unique_code+50000,
        ]);
    }
}
