<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WithdrawalDepositRejected extends Mailable implements ShouldQueue {
    
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $to = $this->data['to'];
        $name = $this->data['name'];
        $code = $this->data['code'];
        $amount = $this->data['amount'];
        $bank = $this->data['bank'];

        return $this->view('email.withdrawal_deposit_rejected')
        ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
        ->with([
            'to' => $to,
            'name' => $name,
            'code' => $code,
            'amount' => $amount,
            'bank' => $bank
        ]);
    }
}
