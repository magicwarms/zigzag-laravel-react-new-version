<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderReceived extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $to = $this->data['to'];
        $name = $this->data['name'];
        $order_code = $this->data['order_code'];
        $payment_method = $this->data['payment_method'];
        $tele = $this->data['tele'];
        $ekspedition_code = $this->data['ekspedition_code'];
        $ekspedition_company = $this->data['ekspedition_company'];
        $shipping_address = $this->data['shipping_address'];
        $city_name = $this->data['city_name'];
        $subdistrict_name = $this->data['subdistrict_name'];
        $province_name = $this->data['province_name'];
        $zip = $this->data['zip'];
        $dropshipper_name = $this->data['dropshipper_name'];
        $dropshipper_tele = $this->data['dropshipper_tele'];
        $order_detail = $this->data['order_detail'];
        $bank_accounts = $this->data['bank_accounts'];
        $subtotal_order = $this->data['subtotal_order'];
        $grandtotal_order = $this->data['grandtotal_order'];
        $ekspedition_total = $this->data['ekspedition_total'];
        $discount = $this->data['discount'];
        $unique_code = $this->data['unique_code'];
        
        return $this->view('email.order_received')
        ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
        ->with([
            'to' => $to,
            'name' => $name,
            'order_code' => $order_code,
            'payment_method' => $payment_method,
            'tele' => $tele,
            'ekspedition_code' => $ekspedition_code,
            'ekspedition_company' => $ekspedition_company,
            'shipping_address' => $shipping_address,
            'city_name' => $city_name,
            'subdistrict_name' => $subdistrict_name,
            'province_name' => $province_name,
            'zip' => $zip,
            'dropshipper_name' => $dropshipper_name,
            'dropshipper_tele' => $dropshipper_tele,
            'order_detail' => $order_detail,
            'bank_accounts' => $bank_accounts,
            'subtotal_order' => $subtotal_order,
            'grandtotal_order' => $grandtotal_order,
            'ekspedition_total' => $ekspedition_total,
            'discount' => $discount,
            'unique_code' => $unique_code,
        ]);
    }
}
