<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmationUpgradeAccount extends Mailable implements ShouldQueue {
    
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $to = $this->data['to'];
        $name = $this->data['name'];
        $order_code = $this->data['order_code'];
        $bank_sender = $this->data['bank_sender'];
        $bank_number_sender = $this->data['bank_number_sender'];
        $bank_receiver = $this->data['bank_receiver'];
        $date = $this->data['date'];
        $total_transfer = number_format($this->data['total_transfer'], 0,',','.');

        return $this->view('email.confirmation_upgrade_account')
        ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
        ->with([
            'to' => $to,
            'name' => $name,
            'order_code' => $order_code,
            'bank_sender' => $bank_sender,
            'bank_number_sender' => $bank_number_sender,
            'bank_receiver' => $bank_receiver,
            'date' => $date,
            'total_transfer' => $total_transfer,
        ]);
    }
}
