<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FailedJobServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        \App\Model\FailedJob::observe(\App\Observers\FailedJobObserver::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
